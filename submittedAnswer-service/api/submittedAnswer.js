'use strict';

var AWS = require('aws-sdk'),

  uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');

var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}



module.exports.addSubmittedAnswer = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if(validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }else{
      addSubmittedAns(event,context,validateTokenResp,callback);
    }
  }
  validateMultipleLogin();
}
function addSubmittedAns(event,context,validateTokenResp,callback){
  const loggedInUser = validateTokenResp.event.loggedInUser;
  const requestBody = JSON.parse(event.body);
  //check whether FeedbackAnswers present or not
  if (!requestBody.FeedbackAnswers && !requestBody.TypeOfBooking) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  //auto generated number for feedbackAnswer Id
  const FAID = Math.floor(1000 + Math.random() * 9000);
  //-- Get user details from email Id 
  var params = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": loggedInUser
    },
    KeyConditionExpression: "Email = :email"
  }
  documentClient.query(params, function (err, data) {
    if (err) {
      return ({ "statusCode": 400, "message": JSON.stringify(err) });
    } else {

      if (data.Items != null) {
        const userDetails = data.Items[0];
        var Id = uuid.v1();
        requestBody.FeedbackAnswers.forEach(element => {
          let questionParams = {
            TableName: process.env.FEEDBACKQUESTIONS_TABLE,
            Key: {
              "Id": element.FQID
            },
          };
          //console.log("questParams", questionParams);
          documentClient.get(questionParams, function (err, data) {
            console.log(data.Item['Question'])
            element.Question = data.Item['Question'];
            console.log(data.Item['Type'])
            element.Type = data.Item['Type'];
          })
        });
        var BookingId = requestBody.BookingId
        var TypeOfBooking = requestBody.TypeOfBooking
        var params = {
          Item: {
            "Id": Id,
            "FAID": String(FAID),
            "BookingId": BookingId,
            "TypeOfBooking": TypeOfBooking,
            "VisitorId": userDetails.Id,
            "VisitorName": userDetails.Name,
            "VisitorEmail": userDetails.Email,
            "FeedbackAnswers": requestBody.FeedbackAnswers,
            "Date&Time": new Date().toISOString(),
            "SubmittedOn": new Date().toISOString()
          },

          TableName: process.env.SUBMITTEDANSWER_TABLE
        };
        var params1 = {
          TableName: process.env.SUBMITTEDANSWER_TABLE,
          Key: {
            "Id": Id
          },
        };
        documentClient.get(params1, function (err, data) {
          if (err) {
            return callback(err, response(400, JSON.stringify(respObj)));
          } else {
            if (data.Item) {
              var respObj = { "message": "Feedback already exists." }
              return callback(err, response(400, JSON.stringify(respObj)));
            } else {
              var updateParams = {
                TableName: process.env.BOOKING_TABLE,
                Key: {
                  "Id": BookingId
                },
              }
              //console.log("updateParams", updateParams)
              documentClient.get(updateParams, function (err, data) {
                if (err) {
                  console.log("data comming 108")
                  return response(400, JSON.stringify(err));
                } else {
                  let dataList = data.Item;
                  //console.log("data comming", dataList)
                  let feedbackStatus = {
                    TableName: process.env.BOOKING_TABLE,
                    Key: {
                      "Id": BookingId
                    },
                    UpdateExpression: 'set #FeedbackStatus = :FS,#UpdatedAt = :UA',
                    ExpressionAttributeValues: {
                      ':FS': true,
                      ':UA': new Date().toISOString(),
                    },
                    ExpressionAttributeNames: {
                      '#FeedbackStatus': 'FeedbackStatus',
                      '#UpdatedAt': 'UpdatedAt',
                    },
                    ReturnValues: "UPDATED_NEW"
                  };
                  const responseObj = {
                    "FeedbackStatus": true,
                  }
                  documentClient.update(feedbackStatus, function (err, data) {
                    if (err) {
                      return callback(err, response(400, err));
                    } else {
                      var respObj = { "responseObj": responseObj, "message": "Thank You for your FeedBack" }
                      documentClient.put(params, function (err, data) {
                        // console.log("data " + JSON.stringify(respObj))
                        return callback(err, response(200, JSON.stringify(respObj)));
                      });
                    }
                  });
                }
              });
            }
          }
        })
      } else {
        return ({ "statusCode": 404, "userData": {}, "message": "User details not found." })
      }
    }
  })
}

module.exports.getSubmittedAnswer = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  const id = event.pathParameters.Id;
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params = {
    TableName: process.env.SUBMITTEDANSWER_TABLE,
    ExpressionAttributeValues: {
      ":Id": id
    },
    KeyConditionExpression: 'Id = :Id ',
  };
  //console.log("params.. " + JSON.stringify(params));
  documentClient.query(params, function (err, data) {
    // let data1 = data.filter();
    if (err) {
      // console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return callback(err, response(400, JSON.stringify(err)));
    } else {
      //console.log("GetItem succeeded:", JSON.stringify(data));
      //console.log("count of records ... " + data.Count)
      if (data.Count > 0) {
        var respObj = { "responseObj": data.Items[0], "message": "Answer's fetched successfully." }
        return callback(null, response(200, JSON.stringify(respObj)));
      } else {
        var respObj = { "message": + id + " not found." }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  });
}
module.exports.getAllAnswers = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();

  var params = {
    TableName: process.env.SUBMITTEDANSWER_TABLE,

  }
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    var finalResponse = data;
    var respObj = { "responseObj": finalResponse, "message": "Feedback data fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));
  });
}

