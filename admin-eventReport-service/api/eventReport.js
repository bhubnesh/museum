'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
const _ = require('underscore');


var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}


module.exports.getEventReport = function (event, context, callback) {
    debugger;
    globalEvent = event;
    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();

    
    const requestParams = (event.queryStringParameters);
    if (!requestParams.EventIds) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }


    if ((requestParams.EventIds).length > 0) {

        var eventIdString = requestParams.EventIds;
        var eventIdArray = eventIdString.split(',');

        const eventData = [];
        var count = eventIdArray.length;
        eventIdArray.map(item => {

            let params1 = {
                TableName: process.env.REPORTCOUNT_TABLE,
                Key: {
                    "CountName": "EventCount",
                    "recordDate": item
                }
            };
            console.log("params.. " + JSON.stringify(params1));
            documentClient.get(params1, function (err, data) {
                if (err) {
                    console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                    return response(400, JSON.stringify(err));
                } else {
                    var singleItem = {};
                    var event = data.Item;
                    console.log("event",data)
                    if (!event || event == undefined) {
                        singleItem.EventId = item;
                        singleItem.EventType = "Public",
                            singleItem.Attended = 0,
                            singleItem.Booked = 0
                        eventData.push(singleItem);
                        count--;
                    } else {
                        singleItem.EventId = event.recordDate;
                        singleItem.EventType = "Public",
                        singleItem.Attended = (event.TotalNoOfParticipantVisited != undefined) ? event.TotalNoOfParticipantVisited :"0",
                        singleItem.Booked =  event.hitCount,
                        eventData.push(singleItem);
                        count--;

                    }
                    if (count == 0) {
                        var respObj = { "message": "Event data fetched successfully.", "responseObj": eventData }
                        return callback(null, response(200, JSON.stringify(respObj)));
                    }
                }
            })


        })
    } else {
        var respObj = { "message": "Data not found." }
        return callback(null, response(200, JSON.stringify(respObj)));
    }

}
