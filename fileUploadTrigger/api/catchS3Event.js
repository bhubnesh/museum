const aws = require('aws-sdk');
aws.config.update({region:'ap-south-1'});
 
const s3 = new aws.S3({ apiVersion: '2006-03-01' });
 
const documentClient = new aws.DynamoDB.DocumentClient();
 

exports.processTrigger = async (event, context) => {
    console.log('Received event:', JSON.stringify(event, null, 2));
 
    // Get the object from the event and show its content type
    const bucket = event.Records[0].s3.bucket.name;
    const key = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, ' '));
    const params = {
        Bucket: bucket,
        Key: key,
    };
    
    console.log(params);
    
   
   
 
    try {
  //finally get the HEAD for the s3Object
    const head = await s3.headObject(params).promise();
 
    var meta = head['Metadata']
    console.log(meta);
    var mnmentityid=meta['mnmentityid'];
    var mnmentitytype=meta['mnmentitytype'];
     console.log("mnmentityid : " +mnmentityid );
    console.log("mnmentitytype : " + meta['mnmentitytype']);
    const imageUrl='https://'+bucket+'.s3.ap-south-1.amazonaws.com/'+key;
    if(mnmentitytype == "User"){
       //updateUser(mnmentityid, url); 
       var phone = meta['mnmuserphone'];
 
       //-- Update record with urlff
       var updateParams = {
         TableName: process.env.USER_TABLE,
         Key: {
           "Email": mnmentityid,
           "Phone": phone
         },
         UpdateExpression: 'SET imageUrl = :imageUrl',
         ExpressionAttributeValues: {
           ':imageUrl': imageUrl
         },
         ReturnValues: 'UPDATED_NEW'
       };
       console.log("update params.. " + JSON.stringify(updateParams));
       let _result = "";
      try {
          _result = await documentClient.update(updateParams).promise();
        } catch(ex){
           _result = ex;
        }
        console.log("_result"+_result);
    }else if(mnmentitytype == "Exhibit"){
        //updateUser(mnmentityid, url); 
       var artifactType = meta['mnmartifacttype'];
       var artifactSubType = meta['mnmartifactsubtype'];
       var exhibitDetail;
       var language = meta['mnmlanguage'];
       console.log("artifactType:"+artifactType);
       console.log("language:"+language);
       if(artifactType == "AudioVideo"){
         console.log("In AudioVideo section");
            var updatedAudVid;
            //-- get current video values
            try{
                  var paramsGet = {
                    TableName: process.env.EXHIBIT_TABLE,
                        ExpressionAttributeValues: {
                      ":emp": mnmentityid
                   },
                    KeyConditionExpression: 'Id = :emp',
              
                 };
                 var result = await documentClient.query(paramsGet).promise()
                  console.log(JSON.stringify(result));
                  exhibitDetail=result.Items[0];
            } catch (error) {
              console.error(error);
          }
           if(exhibitDetail.AudioVideo == undefined){
             console.log("In Empty");
              var audVid=new Array();
              var videoUrl;
              var audioUrl;
              if(artifactSubType=="Video"){
                videoUrl=imageUrl;
                audioUrl=''; 
              }else{
                audioUrl=imageUrl;
                videoUrl='';
              }
              let audVidData = {
                 "language" : language,
                "videoUrl" : videoUrl,
                "audioUrl" : audioUrl,
              };
              audVid.push(audVidData);
              updatedAudVid=audVid;
          }else{
            console.log("In not Empty");
            let audVidInDb=exhibitDetail.AudioVideo;
            console.log("audVidInDb"+audVidInDb);
            var languageEntryFound=false;
            for(var i=0;i<audVidInDb.length;i++){
              audVidEntry=audVidInDb[i];
              if(audVidEntry.language==language){
                languageEntryFound=true;
                if(artifactSubType=="Video"){
                  audVidEntry.videoUrl=imageUrl;
 
                }else{
                  audVidEntry.audioUrl=imageUrl;
 
                } 
                updatedAudVid=audVidInDb;
              }
              console.log(languageEntryFound);
              if(!languageEntryFound){
              var videoUrl;
              var audioUrl;
              if(artifactSubType=="Video"){
                videoUrl=imageUrl;
                audioUrl=''; 
              }else{
                audioUrl=imageUrl;
                videoUrl='';
              }
              let audVidData = {
                 "language" : language,
                "videoUrl" : videoUrl,
                "audioUrl" : audioUrl,
              };
              audVidInDb[audVidInDb.length]=audVidData;
              
              }
              updatedAudVid=audVidInDb;
            }
          }
 
       var updateParams = {
        TableName: process.env.EXHIBIT_TABLE,
        Key: {
          "Id": mnmentityid
        },
        UpdateExpression: 'SET AudioVideo = :audioVideo',
        ExpressionAttributeValues: {
          ':audioVideo': updatedAudVid
        },
        ReturnValues: 'UPDATED_NEW'
      };
      console.log("update params.. " + JSON.stringify(updateParams));
      let _result = "";
     try {
         _result = await documentClient.update(updateParams).promise();
       } catch(ex){
          _result = ex;
       }
       console.log("_result"+_result);
    }else{
            //-- Update other url params
            
            var updateParams = {
              TableName: process.env.EXHIBIT_TABLE,
              Key: {
                "Id": mnmentityid
              },
              UpdateExpression: 'SET '+artifactType+' = :url',
              ExpressionAttributeValues: {
                ':url': imageUrl
              },
              ReturnValues: 'UPDATED_NEW'
            };
            console.log("update params.. " + JSON.stringify(updateParams));
            let _result = "";
           try {
               _result = await documentClient.update(updateParams).promise();
             } catch(ex){
                _result = ex;
             }
             console.log("_result"+_result);
       }
       }else if(mnmentitytype == "ExploreMuseum"){
        //-- Update other url params
        var artifactType = meta['mnmartifacttype'];
        var updateParams = {
          TableName: process.env.CEXPLOREMUSEUM_TABLE,
          Key: {
            "Id": mnmentityid
          },
          UpdateExpression: 'SET '+artifactType+' = :url',
          ExpressionAttributeValues: {
            ':url': imageUrl
          },
          ReturnValues: 'UPDATED_NEW'
        };
        console.log("update params.. " + JSON.stringify(updateParams));
        let _result = "";
       try {
           _result = await documentClient.update(updateParams).promise();
         } catch(ex){
            _result = ex;
         }
         console.log("_result"+_result);
   }else if(mnmentitytype == "Event"){
    //-- Update other url params
    var artifactType = meta['mnmartifacttype'];
    var updateParams = {
      TableName: process.env.EVENT_TABLE,
      Key: {
        "Id": mnmentityid
      },
      UpdateExpression: 'SET '+artifactType+' = :url',
      ExpressionAttributeValues: {
        ':url': imageUrl
      },
      ReturnValues: 'UPDATED_NEW'
    };
    console.log("update params.. " + JSON.stringify(updateParams));
    let _result = "";
   try {
       _result = await documentClient.update(updateParams).promise();
     } catch(ex){
        _result = ex;
     }
     console.log("_result"+_result);
}
    
 
    } catch (err) {
        console.log(err);
        const message = `Error getting object ${key} from bucket ${bucket}. Make sure they exist and your bucket is in the same region as this function.`;
        console.log(message);
        throw new Error(message);
    }
};
  
  function updateExhibit(uuid, url){
    return "success";
};
function isEmpty (value){
    if(value==undefined ||
      value==null  )
      return true;
    else
      return false; 
};