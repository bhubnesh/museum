'use strict';

var AWS = require('aws-sdk'),

  uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}


module.exports.addFeedbackQuestion = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      addFeedbackQ(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}

function addFeedbackQ(event, context, validateTokenResp, callback) {
  const loggedInUser = event.loggedInUser;
  const requestBody = JSON.parse(event.body);

  if (!requestBody.Question || !requestBody.Type) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  //-- Get user details from email Id 
  var searchWithEmail = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": loggedInUser
    },
    KeyConditionExpression: "Email = :email"
  }

  console.log("Search Params ..." + JSON.stringify(searchWithEmail));


  documentClient.query(searchWithEmail, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return ({ "statusCode": 400, "message": JSON.stringify(err) });
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      console.log("count of records fro email ... " + data.Count)
      if (data.Count > 0) {
        console.log("Full User Object ... " + JSON.stringify(data.Items[0]));
        const userDetails = data.Items[0];
        var Id = uuid.v1();
        //-- Get order number of the record

        var countRecordsParams = {
          TableName: process.env.FEEDBACKQUESTIONS_TABLE,
        }
        documentClient.scan(countRecordsParams, function (err, data) {
          if (err) {
            return callback(err, response(400, JSON.stringify(err)));
          }
          console.log("Count of records..  " + data.Count);
          var feedBackRecords = data.Items;
          console.log("feedBackRecords... " + JSON.stringify(feedBackRecords));
          var max = feedBackRecords.reduce((acc, item) => acc = acc > item.OrderNum ? acc : item.OrderNum, 0);
          // let max = 0;
          // feedBackRecords.forEach(record => {
          //     if (record.OrderNum > max) {
          //       max = record.OrderNum;
          //     }
          //   });

          const OrderNum = max + 1;

          console.log("Order num ... " + (max + 1));


          var params = {
            Item: {
              "Id": Id,
              "Question": requestBody.Question,
              "Type": requestBody.Type,
              "NumberOfoptions": requestBody.NumberOfoptions,
              "Option1": requestBody.Option1,
              "Option2": requestBody.Option2,
              "Option3": requestBody.Option3,
              "Option4": requestBody.Option4,
              "OrderNum": OrderNum,
              "CreatedBy": userDetails.Name,
              "UpdatedBy": userDetails.Name,
              "CreatedAt": new Date().toISOString(),
              "UpdatedAt": new Date().toISOString()
            },
            TableName: process.env.FEEDBACKQUESTIONS_TABLE
          };
          console.log("Add Params... " + JSON.stringify(params));

          //-- Check whether FAQ already exists
          var params1 = {
            TableName: process.env.FEEDBACKQUESTIONS_TABLE,
            Key: {
              "Id": Id
            }
          };
          console.log("params.. " + JSON.stringify(params1));
          documentClient.get(params1, function (err, data) {
            if (err) {
              console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
              return response(400, JSON.stringify(err));
            } else {
              console.log("GetItem succeeded:", JSON.stringify(data));
              if (data.Item) {
                console.log("FeedbackQuestion" + requestBody.SRNO + " already exists.");
                var respObj = { "message": "FeedbackQuestion already exists" }
                return callback(err, response(400, JSON.stringify(respObj)));
              } else {
                var respObj = { "responseObj": data.Attributes, "message": "FeedbackQuestion Added successfully..!" }
                documentClient.put(params, function (err, data) {
                  return callback(err, response(200, JSON.stringify(respObj)));
                });
              }
            }
          });
        })
      } else {
        return ({ "statusCode": 404, "userData": {}, "message": "User details not found." })
      }
    }
  })




}
//-- Delete FeedbackQuestion code 

module.exports.deleteFeedbackQuestion = function (event, context, callback) {
  globalEvent = event;
  //-- validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  const id = event.pathParameters.Id;
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params = {
    TableName: process.env.FEEDBACKQUESTIONS_TABLE,
    Key: {
      "Id": id

    }

  };
  console.log("params.. " + JSON.stringify(params));
  documentClient.delete(params, function (err, data) {
    if (err) {
      return callback(err, response(400, err));
    }
    console.log(data);
    var respObj = { "message": "FeedbackQuestion Deleted successfully..!" }
    return callback(err, response(200, JSON.stringify(respObj)));
  });
}
// update the FeedbackQuestion Language Details

module.exports.updateFeedbackQuestion = function (event, context, callback) {
  globalEvent = event;
  //-- validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();

  const id = event.pathParameters.Id;
  const requestBody = JSON.parse(event.body);
  if (!id) {
    var respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //-- check whether record with given id exists or not 
  var params1 = {
    TableName: process.env.FEEDBACKQUESTIONS_TABLE,
    Key: {
      "Id": id
    },
  };
  console.log("search params.. " + JSON.stringify(params1));
  documentClient.get(params1, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      if (data.Item) {
        console.log("Alert data with given id exists.");
        const feedBackQData = data.Item;
        //-- Get currently logged in user info
        var loggedInUser = event.loggedInUser;
        //-- Get user details from email Id 

        var searchWithEmail = {
          TableName: process.env.VISITOR_TABLE,
          ExpressionAttributeValues: {
            ":email": loggedInUser
          },
          KeyConditionExpression: "Email = :email"
        }
        console.log("Search Params ..." + JSON.stringify(searchWithEmail));

        documentClient.query(searchWithEmail, function (err, data) {
          if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return ({ "statusCode": 400, "message": JSON.stringify(err) });
          } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            console.log("count of records fro email ... " + data.Count)
            if (data.Count > 0) {
              console.log("Full User Object ... " + JSON.stringify(data.Items[0]));
              const userDetails = data.Items[0];

              var updateParams = {
                TableName: process.env.FEEDBACKQUESTIONS_TABLE,
                Key: {
                  "Id": id

                },
                UpdateExpression: 'set #Question = :Q, #Type = :A,#NumberOfoptions = :N,#Option1 = :P,#Option2 = :T,#Option3 = :I,#Option4 = :O,#OrderNum = :ON,#UpdatedBy = :U,#UpdatedAt = :UA',
                ExpressionAttributeValues: {
                  ':Q': requestBody.Question,
                  ':A': requestBody.Type,
                  ':N': requestBody.NumberOfoptions,
                  ':P': requestBody.Option1,
                  ':T': requestBody.Option2,
                  ':I': requestBody.Option3,
                  ':O': requestBody.Option4,
                  ':ON': requestBody.OrderNum,
                  ':U': userDetails.Name,
                  ':UA': new Date().toISOString()
                },
                ExpressionAttributeNames: {
                  '#Question': 'Question',
                  '#Type': 'Type',
                  '#NumberOfoptions': 'NumberOfoptions',
                  '#Option1': 'Option1',
                  '#Option2': 'Option2',
                  '#Option3': 'Option3',
                  '#Option4': 'Option4',
                  '#OrderNum': 'OrderNum',
                  '#UpdatedBy': 'UpdatedBy',
                  "#UpdatedAt": "UpdatedAt"

                },
                ReturnValues: "UPDATED_NEW"
              };

              console.log("Update params.. " + JSON.stringify(updateParams));
              documentClient.update(updateParams, function (err, data) {
                if (err) {
                  return callback(err, response(400, err));
                }
                const responseObj = {
                  "Id": feedBackQData.Id,
                  "Question": requestBody.Question,
                  "Type": requestBody.Type,
                  "NumberOfoptions": requestBody.NumberOfoptions,
                  "Option1": requestBody.Option1,
                  "Option2": requestBody.Option2,
                  "Option3": requestBody.Option3,
                  "Option4": requestBody.Option4,
                  "OrderNum": requestBody.OrderNum,
                  "CreatedBy": feedBackQData.CreatedBy,
                  "UpdatedBy": userDetails.Name,
                  "CreatedAt": feedBackQData.CreatedAt,
                  "UpdatedAt": new Date().toISOString(),
                }
                console.log("update params .. " + JSON.stringify(updateParams));

                var respObj = { "responseObj": responseObj, "message": "Feedback Questions  updated successfully..!" }
                return callback(err, response(200, JSON.stringify(respObj)));
              });
            } else {
              console.log("Logged in user details not found");
              var respObj = { "message": "Logged in user details not found" }
              return callback(null, response(404, JSON.stringify(respObj)));
            }
          }
        })
      }
    }
  })

}

// get  the All FeedbackQuestion Language Details

module.exports.getAllFeedbackQuestion = function (event, context, callback) {
  globalEvent = event;
  //-- validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  var params = {
    TableName: process.env.FEEDBACKQUESTIONS_TABLE,
  }
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    var finalResponse = data;
    var museumData = data.Items;
    const sortedData = museumData.sort((a, b) => new Date(a.OrderNum) - new Date(b.OrderNum));

    console.log("sorted... " + JSON.stringify(sortedData))
    finalResponse.Items = sortedData;
    var respObj = { "responseObj": finalResponse, "message": "FeedbackQuestion data fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));
  })

}

// get particular FeedbackQuestion Language details 

module.exports.getFeedbackQuestion = function (event, context, callback) {
  globalEvent = event;
  //-- validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  const id = event.pathParameters.Id;
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  var params = {
    TableName: process.env.FEEDBACKQUESTIONS_TABLE,
    ExpressionAttributeValues: {
      ":feedback": id
    },
    KeyConditionExpression: 'Id = :feedback',
  };
  console.log("params.. " + JSON.stringify(params));
  documentClient.query(params, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return callback(err, response(400, JSON.stringify(err)));
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data)); 6
      console.log("count of records ... " + data.Count)
      if (data.Count > 0) {
        var respObj = { "responseObj": data.Items[0], "message": "FeedbackQuestion  data fetched successfully." }
        return callback(null, response(200, JSON.stringify(respObj)));
      } else {
        var respObj = { "message": "FeedbackQuestion with type- " + id + " not found." }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })

}

//-- Delete multiple FeedbackQuestion code 

module.exports.deleteMultipleFeedbackQuestion = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);

  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();



  if (!requestBody.listOfFeedbackQuestion) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var listOfFeedbackQuestion = requestBody.listOfFeedbackQuestion;
  console.log("size of List of FeedbackQuestion .. " + listOfFeedbackQuestion.length);
  var count = listOfFeedbackQuestion.length;
  if (listOfFeedbackQuestion.length > 0) {

    listOfFeedbackQuestion.forEach(feedque => {
      var id = feedque.id;
      var params = {
        TableName: process.env.FEEDBACKQUESTIONS_TABLE,
        Key: {
          "Id": id

        }

      };
      console.log("params.. " + JSON.stringify(params));
      documentClient.delete(params, function (err, data) {
        if (err) {
          return callback(err, response(400, err));
        }
        console.log(data);
        var message = "Feedback Question of Id - " + id + " deleted successfully..!"
        console.log(message + count);
        count--;
        if (count == 0) {
          var respObj = { "message": "Feedback Question with given ids deleted successfully.. !" }
          return callback(err, response(200, JSON.stringify(respObj)));
        }
      });
    });

  } else {
    var respObj = { "message": "list of Feedback Question ids to be deleted not found." }
    return callback(err, response(404, JSON.stringify(respObj)));
  }
}
