'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();

const slotsFilters = require('../api/slotsFilter');
var auth = require('../auth/token');
var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}

module.exports.getVisitorBookings = function (event, context, callback) {

    globalEvent = event;

    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();


    const requestParams = (event.queryStringParameters);

    if (!requestParams.Date) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }


    var filter = false;

    if (!requestParams.FullyBooked && !requestParams.PartiallyBooked && !requestParams.NoBookings && !requestParams.TimeFrom &&
        !requestParams.TimeTo && !requestParams.Confirmed && !requestParams.CheckedIn && !requestParams.Completed && !requestParams.Cancelled
        && !requestParams.WheelChair && !requestParams.HearingImpaired && !requestParams.VisuallyImpaired) {
        console.log("No filters ..");

    } else {
        filter = true;
    }
    if (filter == false) {
        debugger;
        var params = {
            TableName: process.env.BOOKING_TABLE,
            IndexName: "BookingDate-index",
            KeyConditionExpression: "BookingDate = :bdate",
            ExpressionAttributeValues: {
                ":bdate": requestParams.Date
            }
        };
        console.log("params.. " + JSON.stringify(params));
        documentClient.query(params, function (err, data) {
            if (err) {
                return callback(err, response(400, JSON.stringify(err)));
            } else {
                var bookingData = data.Items;
                if (bookingData.length <= 0) {
                    var respObj = { "message": "No records found for given date." }
                    return callback(null, response(404, JSON.stringify(respObj)));
                }
                var slotsToFilter = [];
                slotsFilters.finalResponse(event, arrayOfFlags, bookingData, slotsToFilter, callback);
            }

        })


    } else {
        //-- If filters applied


        var confirmedStatus = false;
        var checkedInStatus = false;
        var completedStatus = false;
        var cancelledStatus = false;
        var wcFlag = false;
        var hiFlag = false;
        var viFlag = false;
        var fullyBookedFlag = false;
        var partiallyBookedFlag = false;
        var noBookingsFlag = false;
        var timeFromFlag = false;
        var timeToFlag = false

        var arrayOfFlags = {};
        arrayOfFlags.bookingDate = requestParams.Date;
        //-- Set flags true if filters are appied for given status and services
        requestParams.Confirmed ? confirmedStatus = true : confirmedStatus = false;
        requestParams.CheckedIn ? checkedInStatus = true : checkedInStatus = false;
        requestParams.Completed ? completedStatus = true : completedStatus = false;
        requestParams.Cancelled ? cancelledStatus = true : cancelledStatus = false;
        arrayOfFlags.confirmedStatus = confirmedStatus;
        arrayOfFlags.checkedInStatus = checkedInStatus;
        arrayOfFlags.completedStatus = completedStatus;
        arrayOfFlags.cancelledStatus = cancelledStatus;

        requestParams.WheelChair ? wcFlag = true : wcFlag = false;
        requestParams.HearingImpaired ? hiFlag = true : hiFlag = false;
        requestParams.VisuallyImpaired ? viFlag = true : viFlag = false;
        arrayOfFlags.wcFlag = wcFlag;
        arrayOfFlags.viFlag = viFlag;
        arrayOfFlags.hiFlag = hiFlag;

        requestParams.FullyBooked ? fullyBookedFlag = true : fullyBookedFlag = false;
        requestParams.PartiallyBooked ? partiallyBookedFlag = true : false;
        requestParams.NoBookings ? noBookingsFlag = true : false;
        arrayOfFlags.fullyBookedFlag = fullyBookedFlag;
        arrayOfFlags.partiallyBookedFlag = partiallyBookedFlag;
        arrayOfFlags.noBookingsFlag = noBookingsFlag;

        requestParams.TimeFrom ? timeFromFlag = true : timeFromFlag = false;
        requestParams.TimeTo ? timeToFlag = true : timeToFlag = false;

        arrayOfFlags.timeFromFlag = timeFromFlag;
        arrayOfFlags.timeToFlag = timeToFlag;

        console.log("Array of flags ... " + JSON.stringify(arrayOfFlags));

        slotsFilters.filterswithSlots(event, arrayOfFlags, callback);


    }


}