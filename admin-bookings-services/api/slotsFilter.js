'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
var globalEvent = "";
const _ = require('underscore');

function response(statusCode, message) {

    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Origin;
    }
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Host;
    }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museum.mahindra.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
    }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}


module.exports = {

    filterswithSlots: function (event, arrayOfFlags, callback) {
        globalEvent = event;
        const requestParams = (event.queryStringParameters);
        var slotsToFilter = [];
        var bookingDataBasedOnStatus = [];

        //-- Check slots data available for given date
        var searchWithDate = {
            TableName: process.env.BOOKINGDATE_TABLE,
            ExpressionAttributeValues: {
                ":date": arrayOfFlags.bookingDate
            },

            KeyConditionExpression: "BookingDate = :date"
        }
        documentClient.query(searchWithDate, function (err, data) {
            if (err) {
                console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                return response(400, JSON.stringify(err));
            } else {
                if (data.Count > 0) {
                    var slotArray = [];
                    slotArray = data.Items[0].SlotsDetails;
                    var slotArrayCount = slotArray.length;
                    if (slotArray.length > 0) {
                        slotArray.forEach(slot => {
                            //-- If time range provided
                            if (arrayOfFlags.timeFromFlag == true && arrayOfFlags.timeToFlag == true) {
                                if (parseFloat(slot.slot24Hr) >= parseFloat(requestParams.TimeFrom) && parseFloat(slot.slot24Hr) <= parseFloat(requestParams.TimeTo)) {
                                    //-- If any of the booking slots falg is true along with time range
                                    if (arrayOfFlags.fullyBookedFlag == true || arrayOfFlags.partiallyBookedFlag == true || arrayOfFlags.noBookingsFlag == true) {
                                        if (arrayOfFlags.fullyBookedFlag == true) {
                                            if (slot.bookingsAvailable == 0) {
                                                slotsToFilter.push({ "SlotTime": slot.slot });
                                            }
                                        }
                                        if (arrayOfFlags.partiallyBookedFlag == true) {
                                            if (slot.bookingsAvailable != 0) {
                                                slotsToFilter.push({ "SlotTime": slot.slot })
                                            }
                                        }
                                        if (arrayOfFlags.noBookingsFlag == true) {
                                            if (slot.bookingsDone == 0) {
                                                slotsToFilter.push({ "SlotTime": slot.slot })
                                            }
                                        }
                                    } else {
                                        //-- If filters for only from time to time and not booking type
                                        slotsToFilter.push({ "SlotTime": slot.slot });
                                    }
                                }
                                slotArrayCount--;
                                if (slotArrayCount == 0) {
                                    filterBookingDataAccoringToSlotsAndStatus(event, arrayOfFlags, slotsToFilter, callback);
                                }
                            }
                            //-- If no time range and no slot filter
                            else {
                                if (!arrayOfFlags.fullyBookedFlag && !arrayOfFlags.partiallyBookedFlag && !arrayOfFlags.noBookingsFlag) {
                                    filterAccordingToStatusandService(event, arrayOfFlags, bookingDataBasedOnStatus, slotsToFilter, callback);
                                }else if (arrayOfFlags.fullyBookedFlag == true || arrayOfFlags.partiallyBookedFlag == true || arrayOfFlags.noBookingsFlag == true) {
                                    if (arrayOfFlags.fullyBookedFlag == true) {
                                        if (slot.bookingsAvailable == 0) {
                                            slotsToFilter.push({ "SlotTime": slot.slot });
                                        }
                                    }
                                    if (arrayOfFlags.partiallyBookedFlag == true) {
                                        if (slot.bookingsAvailable != 0) {
                                            slotsToFilter.push({ "SlotTime": slot.slot })
                                        }
                                    }
                                    if (arrayOfFlags.noBookingsFlag == true) {
                                        if (slot.bookingsDone == 0) {
                                            slotsToFilter.push({ "SlotTime": slot.slot })
                                        }
                                    }
                                    slotArrayCount--;
                                    if (slotArrayCount == 0) {
                                        filterBookingDataAccoringToSlotsAndStatus(event, arrayOfFlags, slotsToFilter, callback);
                                    }
                                }
                            }

                        })


                    } else {
                        var respObj = { "message": "No Data found." }
                        return callback(null, response(404, JSON.stringify(respObj)));
                    }
                } else {
                    var respObj = { "message": "No data found." }
                    return callback(null, response(404, JSON.stringify(respObj)));
                }
            }
        })



    },

    finalResponse: function (event, arrayOfFlags, bookingDataBasedOnStatus, slotsToFilter, callback) {
        globalEvent = event;
        var finalBookingData = [];
        var singleBooking = {};
        var count = bookingDataBasedOnStatus.length;
        console.log("Booking records count ---- " + count);
        bookingDataBasedOnStatus.forEach(booking => {
            singleBooking.Id = booking.Id;
            singleBooking.VisitorEmail = booking.VisitorEmail;
            singleBooking.VisitorName = booking.VisitorName;
            singleBooking.ContactNumber = booking.VisitorPhone;
            singleBooking.CountryCode = booking.CountryCode;
            if (booking.ActualNoOfAdults != undefined) {
                singleBooking.ActualNoOfAdults = booking.ActualNoOfAdults;
            }
            singleBooking.NoOfChildren = booking.NoOfChildren;
            if (booking.ActualNoOfChildren != undefined) {
                singleBooking.ActualNoOfChildren = booking.ActualNoOfChildren;
            }
            singleBooking.NoOfAdults = booking.NoOfAdults;
            if (booking.TotalNoOfParticipantVisited != undefined) {
                singleBooking.TotalNoOfParticipantVisited = booking.TotalNoOfParticipantVisited;
            }
            singleBooking.NoOfParticipants = booking.NoOfParticipants;
            singleBooking.AssistanceRequired = booking.AssistanceRequired;
            singleBooking.WheelChairAssistanceRequired = booking.WheelChairAssistanceRequired;
            singleBooking.WCRequiredCount = booking.WCRequiredCount;
            singleBooking.HIAssistanceRequired = booking.HIAssistanceRequired;
            singleBooking.HIHelp = booking.HIHelp;
            singleBooking.VIAssistanceRequired = booking.VIAssistanceRequired;
            singleBooking.VIRequiredCount = booking.VIRequiredCount;
            singleBooking.SimulatorRequired = booking.SimulatorRequired;
            singleBooking.BookingStatus = booking.BookingStatus;
            singleBooking.TypeOfBooking = booking.TypeOfBooking;


            var obj = {};
            obj[booking.SlotTime] = singleBooking
            singleBooking = {};
            finalBookingData.push(obj);

            count--;

            if (count == 0) {
                var respObj = { "message": "Booking data filtered successfully.", "responseObj": finalBookingData }
                return callback(null, response(200, JSON.stringify(respObj)));
            }
        })


    }

}

function filterAccordingToStatusandService(event, arrayOfFlags, bookingDataBasedOnStatus, slotsToFilter, callback) {
    console.log("Inside filterAccordingToStatusandService .... ");
    var params = {
        TableName: process.env.BOOKING_TABLE,
        IndexName: "BookingDate-index",
        KeyConditionExpression: "BookingDate = :bdate",
        ExpressionAttributeValues: {
            ":bdate": arrayOfFlags.bookingDate
        }

    };
    console.log("params.. " + JSON.stringify(params));
    documentClient.query(params, function (err, data) {
        if (err) {
            return callback(err, response(400, JSON.stringify(err)));
        } else {
            if (data.Count > 0) {
                //-- If filter done according to slots then assign bookingData == filtered data
                var bookingData = [];
               
                bookingDataBasedOnStatus.length > 0 ? bookingData = bookingDataBasedOnStatus : bookingData = data.Items;

                console.log("bookingData length ...." + bookingData.length);
                var filterdAccoToStatus = [];
                var count = bookingData.length;
                console.log(count);
                bookingData.forEach(booking => {

                    if (!arrayOfFlags.completedStatus && !arrayOfFlags.confirmedStatus && !arrayOfFlags.cancelledStatus) {
                        //-- Add booking data if no filter for status
                        filterdAccoToStatus.push(booking);
                    }
                    if (arrayOfFlags.confirmedStatus == true) {
                        console.log("Status .... " + booking.BookingStatus);
                        if ((booking.BookingStatus) == "Confirmed") {
                            filterdAccoToStatus.push(booking);
                        }
                    }
                    if (arrayOfFlags.completedStatus == true) {
                        if (booking.BookingStatus == "Completed") {
                            filterdAccoToStatus.push(booking);
                        }
                    }
                    if (arrayOfFlags.cancelledStatus == true) {
                        if (booking.BookingStatus == "Cancelled") {
                            filterdAccoToStatus.push(booking);
                        }
                    }

                    count--;

                    if (count == 0) {

                        if (filterdAccoToStatus.length == 0) {
                            var respObj = { "message": "No records found for given filter." }
                            return callback(null, response(404, JSON.stringify(respObj)));
                        }
                        filterDataAccordingToServices(event, arrayOfFlags, filterdAccoToStatus, slotsToFilter, callback);
                    }
                })
            }
        }
    })
}
function filterBookingDataAccoringToSlotsAndStatus(event, arrayOfFlags, slotsToFilter, callback) {

    console.log("Inside filterBookingDataAccoringToSlotsAndStatus .......  " + JSON.stringify(slotsToFilter));
    var params = {
        TableName: process.env.BOOKING_TABLE,
        IndexName: "BookingDate-index",
        KeyConditionExpression: "BookingDate = :bdate",
        ExpressionAttributeValues: {
            ":bdate": arrayOfFlags.bookingDate
        }

    };
    console.log("params.. " + JSON.stringify(params));
    documentClient.query(params, function (err, data) {
        if (err) {
            return callback(err, response(400, JSON.stringify(err)));
        } else {
            if (data.Count > 0) {
                var bookingData = data.Items;
                var bookingDataBasedOnStatus = [];
                var count = bookingData.length;
                console.log("Booking data count ... " + count);
                if (bookingData.length == 0) {
                    var respObj = { "message": "Booking data not available for the given date." }
                    return callback(null, response(404, JSON.stringify(respObj)));
                }

                bookingData.forEach(booking => {
                    slotsToFilter.forEach(slot => {
                        var slot_inString = slot.SlotTime;
                        var booking_inString = booking.SlotTime;
                        // console.log("S  *******" + (slot_inString).substring(0, 5));
                        // console.log("B  *******" + (booking_inString).substring(0, 5));
                        if ((slot_inString).substring(0, 2) == (booking_inString).substring(0, 2)) {
                            bookingDataBasedOnStatus.push(booking);
                        }

                    })
                    count--;

                    if (count == 0) {

                        if (bookingDataBasedOnStatus.length == 0) {
                            var respObj = { "message": "No records found for given filter." }
                            return callback(null, response(404, JSON.stringify(respObj)));
                        }
                        filterAccordingToStatusandService(event, arrayOfFlags, bookingDataBasedOnStatus, slotsToFilter, callback);
                    }
                })
            } else {
                var respObj = { "message": "No Data found." }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })
}

function filterDataAccordingToServices(event, arrayOfFlags, bookingDataBasedOnStatus, slotsToFilter, callback) {
    globalEvent = event;
    var bookingDataBasedOnServices = [];

    if (!arrayOfFlags.wcFlag && !arrayOfFlags.viFlag && !arrayOfFlags.hiFlag) {
        //-- No More data to filter, call function to modify response
        getFinalResponse(event, arrayOfFlags, bookingDataBasedOnStatus, slotsToFilter, callback);
    }

    var count = bookingDataBasedOnStatus.length;
    bookingDataBasedOnStatus.forEach(booking => {


        if (arrayOfFlags.wcFlag == true) {
            if (booking.WheelChairAssistanceRequired == true) {
                bookingDataBasedOnServices.push(booking);
            }
        }
        if (arrayOfFlags.viFlag == true) {
            if (booking.VIRequiredCount > 0) {
                bookingDataBasedOnServices.push(booking);
            }
        }
        if (arrayOfFlags.hiFlag == true) {
            if (booking.HIAssistanceRequired == true) {
                bookingDataBasedOnServices.push(booking);
            }
        }
        count--;
        if (count == 0) {
            if (bookingDataBasedOnServices.length == 0) {
                var respObj = { "message": "No records found for given filter." }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
            getFinalResponse(event, arrayOfFlags, bookingDataBasedOnServices, slotsToFilter, callback);
        }

    })

}

function getFinalResponse(event, arrayOfFlags, bookingDataBasedOnStatus, slotsToFilter, callback) {
    globalEvent = event;
    var finalBookingData = [];
    var singleBooking = {};


    //-- Remove duplocate records with same bookingId.
    var uniqueBookingArray = _.uniq(bookingDataBasedOnStatus, 'Id');
    var BookingArrayCount = uniqueBookingArray.length;
    console.log("Final Array ..... " + JSON.stringify(uniqueBookingArray));
    if (BookingArrayCount > 0) {
        uniqueBookingArray.forEach(booking => {
            singleBooking.Id = booking.Id;
            singleBooking.VisitorEmail = booking.VisitorEmail;
            singleBooking.VisitorName = booking.VisitorName;
            singleBooking.ContactNumber = booking.VisitorPhone;
            singleBooking.CountryCode = booking.CountryCode;
            if (booking.ActualNoOfAdults != undefined && booking.ActualNoOfAdults != "") {
                singleBooking.ActualNoOfAdults = booking.ActualNoOfAdults;
            }
            singleBooking.NoOfChildren = booking.NoOfChildren;
            if (booking.ActualNoOfChildren != undefined && booking.ActualNoOfChildren != "") {
                singleBooking.ActualNoOfChildren = booking.ActualNoOfChildren;
            }
            singleBooking.NoOfAdults = booking.NoOfAdults;
            if (booking.TotalNoOfParticipantVisited != undefined && booking.TotalNoOfParticipantVisited != "") {
                singleBooking.TotalNoOfParticipantVisited = booking.TotalNoOfParticipantVisited;
            }
            singleBooking.NoOfParticipants = booking.NoOfParticipants;
            singleBooking.AssistanceRequired = booking.AssistanceRequired;
            singleBooking.WheelChairAssistanceRequired = booking.WheelChairAssistanceRequired;
            singleBooking.WCRequiredCount = booking.WCRequiredCount;
            singleBooking.HIAssistanceRequired = booking.HIAssistanceRequired;
            singleBooking.HIHelp = booking.HIHelp;
            singleBooking.VIAssistanceRequired = booking.VIAssistanceRequired;
            singleBooking.VIRequiredCount = booking.VIRequiredCount;
            singleBooking.SimulatorRequired = booking.SimulatorRequired;
            singleBooking.BookingStatus = booking.BookingStatus;
            singleBooking.TypeOfBooking = booking.TypeOfBooking;

            var obj = {};
            obj[booking.SlotTime] = singleBooking;
            finalBookingData.push(obj);
            singleBooking = {};


            BookingArrayCount--;
            console.log("BookingArrayCount " + BookingArrayCount);


            if (BookingArrayCount == 0) {
                console.log("Final Response .............. " + finalBookingData);
                var respObj = { "message": "Booking data filtered successfully.", "responseObj": finalBookingData }
                return callback(null, response(200, JSON.stringify(respObj)));
            }
        })
    } else {
        var respObj = { "message": "No records found for given date." }
        return callback(null, response(404, JSON.stringify(respObj)));
    }

}




