'use strict';
var atob = require('atob');
var CryptoJS = require("crypto-js");
const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();

module.exports = {

    generateJwtToken: function (email) {
        var jwt = require('njwt');
        const secretKeyJWT = "WhenWeAllGiveThePowerWeAllGiveTheBestEveryMinuteOfAnHourDontThinkAboutARestThenYouAllGetThePower";
        var randomUserId = Math.floor(1000 + Math.random() * 9000);
        console.log("sessiionId..." + randomUserId);
        var claims = {
            iss: "http://museum.com/",
            sub: email,
            email: email,
            scope: "self, admins",
            sessionId: randomUserId
        };
        const token = jwt.create(claims, secretKeyJWT);
        //-- Set Expiration time = 1Hr
        token.setExpiration(new Date().getTime() + (1 * 60 * 60 * 1000));
        var jwtToken = token.compact();
        console.log(jwtToken);

        //-- Encrypt JWT token
        var ciphertext = CryptoJS.AES.encrypt(jwtToken, 'OurBusinessIsOurBusinessNoneOfYourBusiness').toString();
        return ciphertext;
    },


    validateToken: async function (event, context, callback) {

        var jwt = require('njwt');
        const secretKeyJWT = "WhenWeAllGiveThePowerWeAllGiveTheBestEveryMinuteOfAnHourDontThinkAboutARestThenYouAllGetThePower";
        var reqEvent = event;
        console.log("Inside validate token method... ");
        if (event.headers.Authorization == undefined || !event.headers.Authorization) {
            return ({ "statusCode": 401, "message": "Unauthorized user" })
        }
        var authTokenDecrypted = event.headers.Authorization;

        if (authTokenDecrypted == undefined || authTokenDecrypted == null) {
            return ({ "statusCode": 401, "message": "Unauthorized user" })
        }
        if (authTokenDecrypted.startsWith("Bearer")) {
            var authToken = authTokenDecrypted.substring(7);

            //--- Decrypt JWT Token
            var bytes = CryptoJS.AES.decrypt(authToken, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
            var token = bytes.toString(CryptoJS.enc.Utf8);

            var base64Url = atob(token.split('.')[1]);

            try {
                console.log("Inside try block")
                var verifiedToken = jwt.verify(token, secretKeyJWT);
                if (verifiedToken) {
                    var loggedInUser = JSON.parse(base64Url).email;
                    var sessionId = JSON.parse(base64Url).sessionId;
                    console.log("sessionId From token----- " + sessionId);
                    reqEvent.loggedInUser = loggedInUser.toLowerCase();
                    reqEvent.SessionId = sessionId;

                    //-- Get User record and check whether extracted sessionId from token matches with existing session Id from DB
                    //-- If matches, then allow user to access else send "Session Expired" error msg with statusCode 509 to FE

                    var params = {
                        TableName: process.env.VISITOR_TABLE,
                        ExpressionAttributeValues: {
                            ":email": loggedInUser
                        },
                        KeyConditionExpression: 'Email = :email ',
                    };
                    var data = await documentClient.query(params).promise();
                    if (!data) {
                        return ({ "statusCode": 400, "message": "Error" });
                    } else {
                        if (data.Count > 0) {
                            var sessionIdFromDB = String(data.Items[0].SessionId)
                            console.log("sessionIdFromDB ... " + String(data.Items[0].SessionId));
                            if (sessionId == sessionIdFromDB) {
                                return ({ "statusCode": 200, "message": "Authorized user" });
                            } else {
                                return ({ "statusCode": 509, "message": "Session Expired Due To Multiple Login With Same Email Id." });
                            }
                        } else {
                            console.log("Visitor not found");
                            return ({ "statusCode": 404, "message": "Visitor not found." });
                        }
                    }

                } else {
                    return ({ "statusCode": 403, "message": "Unauthorized user" })
                }
            }
            catch (e) {
                console.log(e);
                return ({ "statusCode": 401, "message": "Unauthorized user." })
            }

        } else {
            return ({ "statusCode": 401, "message": "Unauthorized user." })
        }
    }


};
