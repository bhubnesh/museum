'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();

var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}


module.exports.addWorkingDay = function (event, context, callback) {
    globalEvent = event;
    console.log(event);
    const requestBody = JSON.parse(event.body);
    console.log("JSON req body .. " + requestBody);
    if (!requestBody.WorkingDaysData) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }

    var params = {
        Item: {
            "Id": 1,
            "WorkingDaysData": requestBody.WorkingDaysData

        },
        TableName: process.env.WORKINGDAY_TABLE
    };
    console.log(params);

    //-- Check whether Working day already exists

    var params1 = {
        TableName: process.env.WORKINGDAY_TABLE,
        Key: {
            "Id": 1

        },

    };
    console.log("params.. " + JSON.stringify(params1));
    documentClient.get(params1, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            if (data.Item) {
                console.log("Details already exists.");
                var respObj = { "message": "WorkingDays and Hours details already exists." }
                //-- Update Workingday  Details
                var updateParams = {
                    Item: {
                        "Id": 1,
                        "WorkingDaysData": requestBody.WorkingDaysData
                    },
                    TableName: process.env.WORKINGDAY_TABLE
                };
                console.log("Update params.. " + JSON.stringify(updateParams));
                documentClient.put(updateParams, function (err, data) {
                    if (err) {
                        return callback(err, response(400, JSON.stringify(err)));
                    }
                    console.log(data);
                    var respObj = { "message": "WorkingDays and Hours updated successfully..!" }
                    return callback(err, response(200, JSON.stringify(respObj)));
                });

            } else {
                var respObj = { "message": "WorkingDays and Hours created successfully..!" }
                documentClient.put(params, function (err, data) {
                    return callback(err, response(200, JSON.stringify(respObj)));
                });
            }
        }
    })

}


//-- Get Working days data

module.exports.getWorkingDay = function (event, context, callback) {
    globalEvent = event;
    var params = {
        TableName: process.env.WORKINGDAY_TABLE,
        ExpressionAttributeValues: {
            ":id": 1
        },
        KeyConditionExpression: '#id = :id',
        ExpressionAttributeNames: {
            "#id": "Id"
        }

    };
    console.log("params.. " + JSON.stringify(params));
    documentClient.query(params, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return callback(err, response(400, JSON.stringify(err)));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            console.log("count of records ... " + data.Count)
            if (data.Count > 0) {
                var respObj = { "responseObj": data.Items[0], "message": "WorkingDays and Hours fetched successfully." }
                return callback(null, response(200, JSON.stringify(respObj)));
            } else {
                var respObj = { "message": "WorkingDays and Hours details not found." }
                return callback(null, response(200, JSON.stringify(respObj)));
            }
        }
    })
}