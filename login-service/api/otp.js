'use strict';

var AWS = require('aws-sdk'),
  documentClient = new AWS.DynamoDB.DocumentClient();
const { config } = require('process');
const auth = require('../auth/token');
const notification = require('../notifications/sendNotification');
const axios = require('axios');
const CryptoJS = require('crypto-js');
let apiEndpoints = require('../config/apiEndpoints');
var atob = require('atob');
var globalEvent = "";


function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  //console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  //console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  //console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}

module.exports.generateOtp = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);
  console.log("requestBody",requestBody)
  console.log("requestBody.Email",requestBody.Email)
  //-- Decrypt Email Id
  var decryptedEmail = CryptoJS.AES.decrypt(requestBody.Email, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.Email = decryptedEmail.toString(CryptoJS.enc.Utf8);
  if (requestBody.Phone) {
    //-- Decrypt Phone
    var decryptedPhone = CryptoJS.AES.decrypt(requestBody.Phone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.Phone = decryptedPhone.toString(CryptoJS.enc.Utf8);
  }
  console.log(":::::::::",requestBody.Email)
  console.log(":::::::::",requestBody.Phone)
  // limitnig 5 otp attempts
  let otpParams2 = {
    TableName: process.env.OTP_TABLE,
    ExpressionAttributeValues: {
      ":email": requestBody.Email.toLowerCase()
    },
    KeyConditionExpression: 'Email = :email'
  };
  //console.log("params.. " + JSON.stringify(otpParams2));
  documentClient.query(otpParams2, function (err, data) {

    console.log("data ", data)
    if (data != null && typeof data.Items[0] != null && typeof data.Items[0] != 'undefined') {
      var GenOTPAttempt = data.Items[0].GenOTPAttempt;
    }
    if (typeof GenOTPAttempt == 'undefined' && GenOTPAttempt == null) {
      GenOTPAttempt = 0;
      //console.log("GenOTPAttempt if case :: ", GenOTPAttempt);
    } else {
      GenOTPAttempt = parseInt(GenOTPAttempt) + 1;
      //console.log("GenOTPAttempt else case:: ", GenOTPAttempt);
    }
    if (parseInt(GenOTPAttempt) > 4) {
      //console.log("otpParams ::",otpParams)
      var respObj = { "message": "You Have Exhausted OTP Attempts, Please Login Again! " }
      return callback(err, response(303, JSON.stringify(respObj)));
    }
    let otpParams1 = {
      TableName: process.env.OTP_TABLE,
      Key: {
        "Email": requestBody.Email.toLowerCase(),
      },
      UpdateExpression: 'set #GenOTPAttempt = :GenOTPAttempt',
      ExpressionAttributeNames: {
        "#GenOTPAttempt": "GenOTPAttempt"
      },
      ExpressionAttributeValues: {
        ':GenOTPAttempt': GenOTPAttempt,
      },
      ReturnValues: "UPDATED_NEW"
    };
    //console.log("otpParams1.. " + JSON.stringify(otpParams1));
    documentClient.update(otpParams1, function (err, data) {
      if (err) console.log(err);
      else console.log("data ", data);
    });
  //})
  console.log("requestBody.Email.toLowerCase() ::",requestBody.Email)
        if (!requestBody.Email.toLowerCase()) {
          let respObj = { "message": "Email is mandatory !" }
          return callback(
            null,
            response(400, JSON.stringify(respObj))
          );
        }
        var emailId = (requestBody.Email).toLowerCase();
        //-- Generate 4 digit OTP
        const otp = Math.floor(1000 + Math.random() * 9000);
        //console.log("OTP generated  -- " + String(otp));
        //-- Check whether visitor exists or not
        //-- If provided Email
        let params1 = {
          TableName: process.env.VISITOR_TABLE,
          ExpressionAttributeValues: {
            ":email": emailId
          },
          KeyConditionExpression: 'Email = :email ',
        };
       // console.log("params.. " + JSON.stringify(params1));
  documentClient.query(params1, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
      //console.log("count of records ... " + data.Count)
      if (data.Count > 0) {

        let visitorData = data.Items[0];
        if (!requestBody.Phone) {
          var phone = visitorData.CountryCode + visitorData.Phone;
         // console.log("Phone.... " + phone);
        } else {
          var phone = requestBody.CountryCode + requestBody.Phone;
         // console.log("Phone.... " + phone);
        }
        var countryCodeSign = phone.substring(0, 1);
        if (countryCodeSign === "+") {
          var modifiedPhone = phone.substring(1);
        } else {
          var modifiedPhone = phone;
        }
        //-- Send OTP on both Email and phone
        let otpParams = {
          TableName: process.env.OTP_TABLE,
          Item: {
            "Email": emailId,
            "Phone": modifiedPhone,
            "GenOTPAttempt": GenOTPAttempt,
            "OTP": String(otp),
            "CreatedAt": new Date().toISOString(),
            "UpdatedAt": new Date().toISOString(),
          }
        };
        //console.log("otpParams ::",otpParams)
        documentClient.put(otpParams,function (err, data) {
          //let respObj = { "message": "OTP :" + otp + " generated successfully..!" }
          let respObj = { "message": "OTP has been sent to your registered mobile number.!" }
          visitorData.OTP = otp;
          if (GenOTPAttempt < 5) {
            //-- Call function to send SMS
            var smsText = "Use verification code " + otp + " for Mahindra Museum login -M & M Ltd."
            var typeOfTemplate = "1107162712292844538";
            var subject = "Mahindra Museum Login: Verification Otp"
            //console.log("emailId ", emailId)
            var smsNotification = notification.sendSMSNotification(modifiedPhone, smsText, typeOfTemplate);
            //console.log("globalEvent.from out::&&&&********",requestBody.from)
            if(requestBody.from == undefined && requestBody.from != "updateProfile" ){
              //console.log("globalEvent.from in *******************::",requestBody.from)
              var emailNotification = notification.sendEmailNotification(emailId, subject, smsText)
           }
          }
          console.log("visitorData :::: ",visitorData)
          //-- Craete JWT token for update profile

          var jwtToken1 = globalEvent.headers.Authorization;
         // console.log("token from API header .. " + jwtToken1);
          let jwtToken = "";
          var authToken = "";
  //  if(visitorData.SessionId == undefined && visitorData.SessionId == ""){
          if (!jwtToken1) {
            jwtToken = auth.generateJwtToken(requestBody.Email);
            if (jwtToken == null) {
              let respObj = { "message": "Error creating JWT token." }
              return callback(
                null,
                response(400, JSON.stringify(respObj))
              );
            }
            //-- Update Session Id into DB
            // Decrypt JWT Token
            var bytes = CryptoJS.AES.decrypt(jwtToken, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
            var token = bytes.toString(CryptoJS.enc.Utf8);
            var base64Url = atob(token.split('.')[1]);
            //-- Extract session Id from token

            var sessionId = JSON.parse(base64Url).sessionId;
           // if(sessionId != event.sessionId){}
           var paramsToCheck = {
            TableName: process.env.VISITOR_TABLE,
            Key: {
              "Email": requestBody.Email,
              "Phone": requestBody.Phone
            },
          };
          documentClient.query(paramsToCheck, function (err, data) {
            if (err) {
              return callback(err, response(400, err));
            } else {
              console.log("check data check 1")
              console.log("check data check",data)
            // if(data.Items[0].SessionId == undefined && data.Items[0].SessionId == ""){
              var paramsToUpdate = {
                TableName: process.env.VISITOR_TABLE,
                Key: {
                  "Email": requestBody.Email,
                  "Phone": requestBody.Phone
                },
  
                UpdateExpression: 'set #sessionId = :sessionId',
                ExpressionAttributeValues: {
                  ':sessionId': sessionId
                },
                ExpressionAttributeNames: {
                  '#sessionId': 'SessionId'
                },
                ReturnValues: "UPDATED_NEW"
              };
              documentClient.update(paramsToUpdate, function (err, data) {
                if (err) {
                  return callback(err, response(400, err));
                } else {
                  authToken = "Bearer " + jwtToken;
                  console.log("Session Id updated successfully .. ");
                }
              })
            // }
            } 
          })
            } else {
              console.log("jwtToken");
            jwtToken1 = jwtToken1.slice(7);
            jwtToken = jwtToken1;
            authToken = globalEvent.headers.Authorization;
          }
         // console.log("JwtToken ... " + jwtToken);
          if (requestBody.Phone && requestBody.CountryCode) {            
            if (requestBody.Phone.toString() != visitorData.Phone.toString()) {
              //-- Update Mobile number and country code
              const updateVisiter_URL = apiEndpoints.API_URLS.updateProfilePath + (requestBody.Email).toLowerCase();
             // console.log("updateVisiter_URL",updateVisiter_URL)
              var encryptedEmail = CryptoJS.AES.encrypt(visitorData.Email, 'OurBusinessIsOurBusinessNoneOfYourBusiness').toString();
              var encryptedPhone = CryptoJS.AES.encrypt(visitorData.Phone, 'OurBusinessIsOurBusinessNoneOfYourBusiness').toString();
              var updateParams = {
                "Name": visitorData.Name,
                "Password": visitorData.Password,
                "Email": encryptedEmail,
                "CountryCode": requestBody.CountryCode,
                "Phone": encryptedPhone,
                "Gender": visitorData.Gender,
                "Age": visitorData.Age,
                "Role": "Visitor",
                "SessionId": visitorData.SessionId ? visitorData.SessionId : ""
              };
             // console.log("UpdateParams ... ",JSON.stringify(updateParams));
              axios.put(updateVisiter_URL, (updateParams),{
                headers: {
                  'Authorization': authToken
                }
              })
                .then(resp => {
                 // console.log("resp *******************")
                  if (resp.status != 200) {
                    return callback(null, response(resp.status, resp.message));
                  } else {
                    return callback(null, response(200, JSON.stringify(respObj)));
                  }
                });
            } else {
              return callback(null, response(200, JSON.stringify(respObj)));
            }
          } else {
            return callback(null, response(200, JSON.stringify(respObj)));
          }
        });
      } else {
       // console.log("Visitor not found");
        let respObj = { "message": "Visitor not found." }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
})
}

module.exports.verifyOTP = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);

  //-- Decrypt Email Id
  var decryptedEmail = CryptoJS.AES.decrypt(requestBody.Email, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.Email = decryptedEmail.toString(CryptoJS.enc.Utf8);
  
  if (requestBody.Phone) {
    //-- Decrypt Email Id
    var decryptedPhone = CryptoJS.AES.decrypt(requestBody.Phone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.Phone = decryptedPhone.toString(CryptoJS.enc.Utf8);
    console.log("requestBody.Phone ::",requestBody.Phone)
  }

  
  if (!requestBody.OTP || !requestBody.Email.toLowerCase()) {
    let respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  let otpParams = {
    TableName: process.env.OTP_TABLE,
    ExpressionAttributeValues: {
      ":email": requestBody.Email.toLowerCase()
    },
    KeyConditionExpression: 'Email = :email'
  };

 // console.log("params.. " + JSON.stringify(otpParams));
  documentClient.query(otpParams, function (err, data) {
    if (err) {
     // console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
      console.log("data ::",data)
     // console.log("GetItem succeeded:", JSON.stringify(data));
      if (data.Count > 0) {
       // console.log("OTP details exists.");
        let originalOTPItem = data.Items[0];
        let phone = originalOTPItem.Phone;
       console.log("phone :: ", phone)
        //-- Verify OTP
        let actualOTP = JSON.stringify(data.Items[0].OTP);
        let reqOtp = JSON.stringify(requestBody.OTP);
       // console.log("reqOtp .. " + reqOtp);
       // console.log("actualOTP .. " + actualOTP);
        if (reqOtp.toString() === actualOTP.toString()) {
          let otpParams = {
            TableName: process.env.OTP_TABLE,
            Key: {
              "Email": requestBody.Email.toLowerCase(),
            },
            UpdateExpression: 'SET OTPAttempt = :OTPAttempt, GenOTPAttempt = :GenOTPAttempt ',
            ExpressionAttributeValues: {
              ':OTPAttempt': 0,
              ':GenOTPAttempt' : 0
            }
          };

          documentClient.update(otpParams).promise();
          //-- fetch full user data with given emailID
          let params = {
            TableName: process.env.VISITOR_TABLE,
            ExpressionAttributeValues: {
              ":email": requestBody.Email.toLowerCase()
            },
            KeyConditionExpression: 'Email = :email ',
          };
          //console.log("params.. " + JSON.stringify(params));
          documentClient.query(params, function (err, data) {
            if (err) {
            //  console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
              return response(400, JSON.stringify(err));
            } else {
              //console.log("GetItem succeeded:", JSON.stringify(data));
              //console.log("count of records ... " + data.Count)
              if (data.Count > 0) {
                let originalItem = data.Items[0];
                //-- Delete exisitng record from USer table as we are changing one of the key value(Mobile num)
                let deleteParams = {
                  TableName: process.env.VISITOR_TABLE,
                  Key: {
                    "Email": originalItem.Email,
                    "Phone": originalItem.Phone
                  },
                  ConditionExpression: "Email = :email",
                  ExpressionAttributeValues: {
                    ":email": originalItem.Email
                  }
                };
                //console.log("Delete Params .. " + JSON.stringify(deleteParams));
                documentClient.delete(deleteParams, function (err, data) {
                  if (err) {
                    return callback(err, response(400, JSON.stringify(err)));
                  }
                  //console.log(data);
                  //console.log("Visitor deleted successfully..!");

                  var jwtToken1 = globalEvent.headers.Authorization;
                  // var authToken = jwtToken1.substring(7);
                  let jwtToken = "";
                  var authToken = "";
                  if (!jwtToken1) {
                    jwtToken = auth.generateJwtToken(requestBody.Email);
                    if (jwtToken == null) {
                      let respObj = { "message": "Error creating JWT token." }
                      return callback(
                        null,
                        response(400, JSON.stringify(respObj))
                      );
                    }
                    authToken = "Bearer " + jwtToken;
                    //-- Update Session Id into DB
                    // Decrypt JWT Token
                    var bytes = CryptoJS.AES.decrypt(jwtToken, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
                    var token = bytes.toString(CryptoJS.enc.Utf8);

                    var base64Url = atob(token.split('.')[1]);
                    var userPhone = "";
                    var userPhone1 = "";
                    console.log("**********************")
                    if (!requestBody.Phone) {
                      userPhone1 = originalOTPItem.Phone
                      console.log("check",userPhone1)
                      userPhone = userPhone1.substr(2,11)
                      console.log("not change",userPhone)
                    } else {
                      userPhone1 = requestBody.Phone
                      userPhone = userPhone1.substr(2,11)
                      console.log("change",userPhone)
                    }
                    //console.log("USer Phone number ... " + userPhone);
                    //-- Extract session Id from token
                    var sessionId = JSON.parse(base64Url).sessionId;
                  } else {
                    jwtToken1 = jwtToken1.slice(7);
                    jwtToken = jwtToken1;
                    authToken = jwtToken;
                  }
                  //-- Now add this as a new record retaining some original values 
                  let updateParams = {
                    Item: {
                      "Id": originalItem.Id,
                      "UpdatedAt": new Date().toISOString(),
                      "ImageUrl": originalItem.ImageUrl,
                      "Name": originalItem.Name,
                      "Password": originalItem.Password,
                      "Email": originalItem.Email,
                      "Phone": userPhone,
                      "CountryCode": originalItem.CountryCode,
                      "Gender": originalItem.Gender,
                      "Age": originalItem.Age,
                      "Status": "Active",
                      "Role": originalItem.Role,
                      "Source": originalItem.Source,
                      "CreatedAt": originalItem.CreatedAt,
                      "SessionId": originalItem.SessionId ? originalItem.SessionId : sessionId
                    },
                    TableName: process.env.VISITOR_TABLE
                  };
                  //console.log("params.. " + JSON.stringify(updateParams));
                  documentClient.put(updateParams, function (err, data) {
                    //console.log("After updating Visitor record.. " + JSON.stringify(data));

                    //-- Get visiors data

                    let params1 = {
                      TableName: process.env.VISITOR_TABLE,
                      Key: {
                        "Email": requestBody.Email.toLowerCase(),
                        "Phone": userPhone
                      }
                    };
                    //console.log("params.. " + JSON.stringify(params1));
                    documentClient.get(params1, function (err, data) {
                      if (err) {
                      //  console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                        return response(400, JSON.stringify(err));
                      } else {
                        //console.log("GetItem succeeded:", JSON.stringify(data));

                        let VisitorData = data.Item;
                        let resBody = {
                          Id: VisitorData.Id,
                          Email: VisitorData.Email,
                          ImageUrl: VisitorData.ImageUrl,
                          Name: VisitorData.Name,
                          Phone: userPhone,
                          CountryCode: VisitorData.CountryCode,
                          Gender: VisitorData.Gender,
                          Age: VisitorData.Age,
                          Occupation: VisitorData.Occupation,
                          Organization: VisitorData.Organization,
                          Status: VisitorData.Status,
                          Role: VisitorData.Role,
                          Source: VisitorData.Source,
                          JwtToken: jwtToken
                        };
                        let respObj = { "responseObj": resBody, "message": "OTP verified.Visitor status chnaged to active." }
                        return callback(err, response(200, JSON.stringify(respObj)));
                      }
                    });
                  });
                })
              } else {
               // console.log("Visitor not found");
                let respObj = { "message": "Visitor not found." }
                return callback(null, response(404, JSON.stringify(respObj)));
              }
            }
          })
        } else {
          let respObj = { "message": "Wrong OTP ! " }
          // updating otp attempts
          let OTPAttempt = data.Items[0].OTPAttempt;

          if (typeof OTPAttempt == 'undefined') {
            OTPAttempt = 1
          } else {
            OTPAttempt = OTPAttempt + 1;
          }
          let otpParams = {
            TableName: process.env.OTP_TABLE,
            Key: {
              "Email": requestBody.Email.toLowerCase(),
            },
            UpdateExpression: 'SET OTPAttempt = :OTPAttempt',
            ExpressionAttributeValues: {
              ':OTPAttempt': OTPAttempt
            }
          };
          documentClient.update(otpParams).promise();

          if (parseInt(OTPAttempt) >= 5) {
            respObj = { "message": "You Have Exhausted OTP Attempts, Please Login Again! " }
            return callback(err, response(303, JSON.stringify(respObj)));

          } else {
            respObj = { "message": "Wrong OTP Attempts, Please Try Again! " }
            return callback(err, response(400, JSON.stringify(respObj)));

          }


        }
      } else {
        let respObj = { "message": "Otp details not found." }
        return callback(err, response(404, JSON.stringify(respObj)));
      }
    }
  });
}

