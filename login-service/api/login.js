'use strict';

const AWS = require('aws-sdk'),
  uuid = require('uuid');
var atob = require('atob');
let options = {}
var CryptoJS = require("crypto-js")
// if (process.env.IS_OFFLINE) {
//   options = {
//     region: "localhost",
//     endpoint: "http://localhost:8000"
//   }
// };

const documentClient = new AWS.DynamoDB.DocumentClient(options);
const auth = require('../auth/token');


const { concat } = require('async');
const notification = require('../notifications/sendNotification');

var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  //console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  //console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  //console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com','http://museum.mahindra.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}


module.exports.hello = function (event, context, callback) {
  globalEvent = event;
  //console.log("Inside Hello !")
  return callback(err, response(200, "Hello Wold"))
}

module.exports.signup = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);
  //console.log("Event ... " + JSON.stringify(event));

  //-- Decrypt Email Id
  var decryptedEmail = CryptoJS.AES.decrypt(requestBody.Email, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.Email = decryptedEmail.toString(CryptoJS.enc.Utf8);

    //-- Decrypt Phone Number
    var decryptedPhone = CryptoJS.AES.decrypt(requestBody.Phone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.Phone = decryptedPhone.toString(CryptoJS.enc.Utf8);
  
  if (!requestBody.Email.toLowerCase() || !requestBody.Password || !requestBody.CountryCode || !requestBody.Phone || !requestBody.Name
    || !requestBody.Role) {
    var respObj = { "message": "Manadatory Input Parameters Are Missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  var bytes = CryptoJS.AES.decrypt(requestBody.Password, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.Password = bytes.toString(CryptoJS.enc.Utf8);

  debugger;
  var Source = "";
  !(requestBody.Source) ? Source = "ManualSignup" : Source = requestBody.Source;
  //console.log("Source ... " + Source);

  var email = (requestBody.Email).toLowerCase();
  var params = {
    Item: {
      "Id": uuid.v1(),
      "CreatedAt": new Date().toISOString(),
      "UpdatedAt": new Date().toISOString(),
      "ImageUrl": "",
      "Name": requestBody.Name,
      "Password": requestBody.Password,
      "Email": email,
      "Phone": requestBody.Phone,
      "CountryCode": requestBody.CountryCode,
      "Gender": requestBody.Gender,
      "Age": requestBody.Age,
      "Status": "Inactive",
      "Role": requestBody.Role,
      "Source": requestBody.Source ? requestBody.Source : "ManualSignup",
      "NoOfVisits": 0,
      "LastVisitedDate": ""

    },
    TableName: process.env.VISITOR_TABLE
  };
  //console.log("coming data *****",params);

  var searchWithEmail = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": email
    },
    KeyConditionExpression: "Email = :email"
  }

 // console.log("Search Params ..." + JSON.stringify(searchWithEmail));

  documentClient.query(searchWithEmail, function (err, data) {
    if (err) {
    //  console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
     // console.log("GetItem succeeded:", JSON.stringify(data));
      if (data.Count > 0) {
       // console.log("Full User Object ... " + JSON.stringify(data.Items[0]));
       // console.log("Status ... " + JSON.stringify(data.Items[0].Status))
        var activeStatus = "Active";
        if ((data.Items[0].Status).toString() === activeStatus.toString() && data.Items[0].Role == "Visitor") {
          //-- User already exists.
         // console.log("User With EmailId Already Exists.");
          var respObj = { "message": "User With EmailId Already Exists." }
          return callback(null, response(400, JSON.stringify(respObj)));
        } else {
          var respObj = { "message": "User Created Successfully..!" }
          documentClient.put(params, function (err, data) {
           // console.log("Data which we save ::",data)
            var subject = "Mahindra Musuem : SignedUp successfully"
            var text = "Now you successfully Registred with Mahindra Museum. Now please login with your Credentials"
            var emailNotification = notification.sendEmailNotification(email, subject, text)
            return callback(err, response(200, JSON.stringify(respObj)));
          })
        }
      } else {
        //-- Check whether User with mobile number exists
        var respObj = { "message": "User Created successfully..!" }
        documentClient.put(params, function (err, data) {
         // console.log("data ::",data)
          var subject = "Mahindra Musuem : SignedUp successfully"
          var text = "Now you successfully Registred with Mahindra Museum. Now please login with your Credentials"
          var emailNotification = notification.sendEmailNotification(email, subject, text)
          return callback(err, response(200, JSON.stringify(respObj)));
        })
      }
    }
  })
}

module.exports.login = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);
  if (!requestBody.Password || !requestBody.Email.toLowerCase()) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //-- Decrypt Email Id
  var decryptedEmail = CryptoJS.AES.decrypt(requestBody.Email, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.Email = decryptedEmail.toString(CryptoJS.enc.Utf8);

  //-- generate JWT token
  var jwtToken = auth.generateJwtToken(requestBody.Email.toLowerCase());
  if (jwtToken == null) {
    var respObj = { "message": "Error creating JWT token." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  var email = (requestBody.Email).toLowerCase();
 // console.log("Email.. " + email)

  var params = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": email
    },
    KeyConditionExpression: 'Email = :email ',
  };
 // console.log("params.. " + JSON.stringify(params));
  documentClient.query(params, function (err, data) {
    if (err) {
     // console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
      if (data.Count > 0) {
        var decryptedPassword = "";
       // console.log("requestBody.Password ", requestBody.Password)

        var bytes = CryptoJS.AES.decrypt(requestBody.Password, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
        //console.log("bytes ", bytes)
        decryptedPassword = bytes.toString(CryptoJS.enc.Utf8);
        //console.log("decryptedPassword ", decryptedPassword)
        var password = JSON.stringify(data.Items[0].Password);
        var password2 = JSON.stringify(decryptedPassword);
        //console.log("password .. " + password);
       // console.log("password2 .. " + password2);
        if (password.toString()  === password2.toString()) {
         // console.log("credentials matched !");

          let pwdParams = {
            TableName: process.env.VISITOR_TABLE,

            Key: {
              "Phone": data.Items[0].Phone,
              "Email": email,

            },
            UpdateExpression: 'SET pwdAttempt = :pwdAttempt',
            ExpressionAttributeValues: {
              ':pwdAttempt': 0
            }
          };

          documentClient.update(pwdParams).promise();
          //- Create JWT token
          var VisitorData = data.Items[0]
          var resBody = {
            Id: VisitorData.Id,
            Email: VisitorData.Email,
            ImageUrl: VisitorData.ImageUrl,
            Name: VisitorData.Name,
            Phone: VisitorData.Phone,
            CountryCode: VisitorData.CountryCode,
            Gender: VisitorData.Gender,
            JwtToken: jwtToken,
            Age: VisitorData.Age,
            Status: VisitorData.Status,
            Role: VisitorData.Role,
            EmpId: VisitorData.EmpId,
            Source: VisitorData.Source
          };
          // var subject = "Mahindra Musuem Login Successful"
          // var text = "Welcome to Mahindra Museum. Now you can book your Visit"
          // var emailNotification = notification.sendEmailNotification(VisitorData.Email,subject,text) 
          // var respObj = { "message": "Login successful", "responseObj": resBody }
          // console.log("Response ... " + JSON.stringify(response(200, JSON.stringify(respObj))));
          // return callback(null, response(200, JSON.stringify(respObj)));

          //-- Update user record with generetaed session Id 

          // Decrypt JWT Token
          var bytes = CryptoJS.AES.decrypt(jwtToken, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
          var token = bytes.toString(CryptoJS.enc.Utf8);

          var base64Url = atob(token.split('.')[1]);

          //-- Extract session Id from token
          var sessionId = JSON.parse(base64Url).sessionId;
         // console.log("sessionId ----- " + sessionId);
          var params = {
            TableName: process.env.VISITOR_TABLE,
            Key: {
              "Email": VisitorData.Email,
              "Phone": VisitorData.Phone
            },

            UpdateExpression: 'set #sessionId = :sessionId',
            ExpressionAttributeValues: {
              ':sessionId': sessionId
            },
            ExpressionAttributeNames: {
              '#sessionId': 'SessionId'

            },
            ReturnValues: "UPDATED_NEW"
          };

          documentClient.update(params, function (err, data) {
            if (err) {
              return callback(err, response(400, err));
            } else {
              let otpParams1 = {
                TableName: process.env.OTP_TABLE,
                Key: {
                  "Email": requestBody.Email.toLowerCase(),
                },
                UpdateExpression: 'set #GenOTPAttempt = :GenOTPAttempt',
                ExpressionAttributeNames: {
                  "#GenOTPAttempt": "GenOTPAttempt"
                },
                ExpressionAttributeValues: {
                  ':GenOTPAttempt': 0,
                },
                ReturnValues: "UPDATED_NEW"
              };
           //   console.log("otpParams1.. " + JSON.stringify(otpParams1));
              documentClient.update(otpParams1, function (err, data) {
                if (err) console.log(err);
                else console.log("data ", data);
              });
              var respObj = { "message": "Login successful", "responseObj": resBody }
             // console.log("Response ... " + JSON.stringify(response(200, JSON.stringify(respObj))));
              return callback(null, response(200, JSON.stringify(respObj)));
            }

          })
        } else {
         // console.log("Invalid credentials");
          let pwdAttempt = data.Items[0].pwdAttempt;

          if (typeof pwdAttempt == 'undefined') {
            pwdAttempt = 1
          } else {
            pwdAttempt = pwdAttempt + 1;
          }
          let pwdParams = {
            TableName: process.env.VISITOR_TABLE,

            Key: {
              "Phone": data.Items[0].Phone,
              "Email": email,

            },
            UpdateExpression: 'SET pwdAttempt = :pwdAttempt',
            ExpressionAttributeValues: {
              ':pwdAttempt': pwdAttempt
            }
          };
          documentClient.update(pwdParams).promise();

          var respObj = '';
          if (parseInt(pwdAttempt) >= 5) {
            respObj = { "message": "You Have Exhausted Password Attempts, Please Login Again Or Use Forgot Password!" }
            return callback(null, response(303, JSON.stringify(respObj)));
          } else {
            respObj = { "message": "Invalid credentials" }
            return callback(null, response(400, JSON.stringify(respObj)));
          }



        }
      } else {
        //console.log("Visitor not found");
        var respObj = { "message": "Visitor not found." }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  });
}
module.exports.forgotPassword = function (event, context, callback) {
  globalEvent = event;
  var requestBody = JSON.parse(event.body);

  //-- Decrypt Email Id
  var decryptedEmail = CryptoJS.AES.decrypt(requestBody.Email, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.Email = decryptedEmail.toString(CryptoJS.enc.Utf8);

  if (!requestBody.Email) {
    var respObj = { "message": "Mandatory Params Are Missing" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var email = requestBody.Email.toLowerCase();
  var params = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": email
    },
    KeyConditionExpression: 'Email = :email ',

  };

  //-- Check whether Visitor exists

  //console.log("params.. " + JSON.stringify(params));
  documentClient.query(params, function (err, data) {
    if (err) {
    //  console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
      //console.log("GetItem succeeded:", JSON.stringify(data));
     // console.log("count of records ... " + data.Count)
      if (data.Count > 0) {
        var visitorData = data.Items[0];
       // console.log("Visitor Data.. " + JSON.stringify(visitorData));
        //-- Generate OTP and send to mobile number and emailId
        //-- Generate 4 digit OTP
        const otp = Math.floor(1000 + Math.random() * 9000);
       // console.log("OTP generated  -- " + String(otp));
        if (!requestBody.Phone) {
          var phone = visitorData.CountryCode + visitorData.Phone;
         // console.log("Phone.... " + phone);
        } else {
          var phone = visitorData.CountryCode + requestBody.Phone;
         // console.log("Phone.... " + phone);
        }
        var countryCodeSign = phone.substring(0, 1);
        if (countryCodeSign === "+") {
          var modifiedPhone = phone.substring(1);
        } else {
          var modifiedPhone = phone;
        }
        var emailId = requestBody.Email.toLowerCase()
        var otpParams = {
          TableName: process.env.OTP_TABLE,
          Item: {
            "Email": emailId,
            "Phone": modifiedPhone,
            "GenOTPAttempt":0,
            "OTP": String(otp),
            "CreatedAt": new Date().toISOString(),
            "UpdatedAt": new Date().toISOString(),
          }
        };
        documentClient.put(otpParams, function (err, data) {


          //var respObj = { "message": "OTP :" + otp + " generated successfully..!" }
          var respObj = { "message": "OTP has been sent to your registered mobile number.!" }
          visitorData.OTP = otp;

          //-- Call function to send SMS
          var smsText = "Use verification code " + otp + " for Mahindra Museum password reset -M & M Ltd."
          var typeOfTemplate = "1107162712299964032";

          var smsNotification = notification.sendSMSNotification(modifiedPhone, smsText, typeOfTemplate);

          // var text = "Use verification code "+otp+" for Mahindra Museum password reset -M & M Ltd."
          var subject = "Reset Password"
          var emailNotification = notification.sendEmailNotification(emailId, subject, smsText)
          return callback(err, response(200, JSON.stringify(respObj)));
        });
      }
      else {
        var respObj = { "message": "Visitor Not Found ..!" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}

module.exports.resetPassword = function (event, context, callback) {
  globalEvent = event;
  var requestBody = JSON.parse(event.body);
  const id = event.pathParameters.Email.toLowerCase();

  if (!id || !requestBody.Password) {
    var respObj = { "message": "Mandatory input params are missing" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  var bytes = CryptoJS.AES.decrypt(requestBody.Password, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.Password = bytes.toString(CryptoJS.enc.Utf8);

  var params = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": id
    },
    KeyConditionExpression: 'Email = :email ',

  };
  //-- Check whether Visitor exists



 // console.log("params.. " + JSON.stringify(params));
  documentClient.query(params, function (err, data) {
    if (err) {
     // console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
      //console.log("GetItem succeeded:", JSON.stringify(data));
     // console.log("count of records ... " + data.Count)
      if (data.Count > 0) {
        var email = id;
        var phone = data.Items[0].Phone;
        var password = data.Items[0].Password;

       // console.log("*****" + requestBody.Password);

        if (JSON.stringify(password) == JSON.stringify(requestBody.Password)) {
          var respObj = { "message": "New Password Should Not Be Same As Old Password." }
          return callback(
            null,
            response(400, JSON.stringify(respObj))
          );
        }
        //-- Update record with given password
        var updateParams = {
          TableName: process.env.VISITOR_TABLE,
          Key: {
            "Email": email,
            "Phone": phone
          },
          UpdateExpression: 'SET Password = :password',
          ExpressionAttributeValues: {
            ':password': requestBody.Password
          },
          ReturnValues: 'ALL_NEW'
        };
       // console.log("update params.. " + updateParams);
        documentClient.update(updateParams, function (err, data) {
          if (err) {
         //   console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
          } else {
           // console.log("updated data.. " + JSON.stringify(data));
            var VisitorData = data.Attributes
            var resBody = {
              Id: VisitorData.Id,
              Email: VisitorData.Email,
              ImageUrl: VisitorData.ImageUrl,
              Name: VisitorData.Name,
              Phone: VisitorData.Phone,
              CountryCode: VisitorData.CountryCode,
              Gender: VisitorData.Gender,
              Age: VisitorData.Age,
              Status: VisitorData.Status,
              Role: VisitorData.Role,
              EmpId: VisitorData.EmpId,
              Source: VisitorData.Source
            };
            var respObj = { "message": "Visitor Updated Successfully..", "responseObj": resBody }
            return callback(err, response(200, JSON.stringify(respObj)))
          }
        })
      } else {
        var respObj = { "message": "Visitor Not Found", }
        return callback(err, response(200, JSON.stringify(respObj)));
      }
    }
  })
}
module.exports.changePassword = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    if (validateTokenResp.statusCode != 200) {
     // console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();
  var requestBody = JSON.parse(event.body);
  const id = event.pathParameters.Email.toLowerCase();

  if (!id || !requestBody.NewPassword || !requestBody.ConfirmPassword || !requestBody.CurrentPassword) {
    var respObj = { "message": "Mandatory input params are missing" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var bytes = CryptoJS.AES.decrypt(requestBody.CurrentPassword, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.CurrentPassword = bytes.toString(CryptoJS.enc.Utf8);
  var bytes = CryptoJS.AES.decrypt(requestBody.ConfirmPassword, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.ConfirmPassword = bytes.toString(CryptoJS.enc.Utf8);
  var bytes = CryptoJS.AES.decrypt(requestBody.NewPassword, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.NewPassword = bytes.toString(CryptoJS.enc.Utf8);
  var newPassword = requestBody.NewPassword

  var newPassword = requestBody.NewPassword
  var confirmPassword = requestBody.ConfirmPassword
  //console.log("New Password .. " + String(newPassword) + "   Confirm password .. " + String(confirmPassword))
  if (String(newPassword) != String(confirmPassword)) {
    var respObj = { "message": "Password did not match" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  var params = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": id
    },
    KeyConditionExpression: 'Email = :email ',

  };
  //-- Check whether Visitor exists

 // console.log("params.. " + JSON.stringify(params));
  documentClient.query(params, function (err, data) {
    if (err) {
   //   console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
    //  console.log("GetItem succeeded:", JSON.stringify(data));
     // console.log("count of records ... " + data.Count)
      if (data.Count > 0) {
        //-- Check whether given current password matches with registered password
        var password = data.Items[0].Password;
        var email = id;
        var phone = data.Items[0].Phone;

       // console.log("Current password .. " + JSON.stringify(requestBody.CurrentPassword));
        if (JSON.stringify(password) != JSON.stringify(requestBody.CurrentPassword)) {
          var respObj = { "message": "Given Password Did Not Match With Registered Password" }
          return callback(
            null,
            response(400, JSON.stringify(respObj))
          );
        }
        if (JSON.stringify(password) == JSON.stringify(newPassword)) {
          var respObj = { "message": "New Password Should Not Be Same As Old Password." }
          return callback(
            null,
            response(400, JSON.stringify(respObj))
          );
        }

        //-- Update record with given password
        var updateParams = {
          TableName: process.env.VISITOR_TABLE,
          Key: {
            "Email": email,
            "Phone": phone
          },
          UpdateExpression: 'SET Password = :password',
          ExpressionAttributeValues: {
            ':password': newPassword
          },
          ReturnValues: 'ALL_NEW'
        };
       // console.log("update params.. " + JSON.stringify(updateParams));
        documentClient.update(updateParams, function (err, data) {
          if (err) {
           // console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
          } else {
           // console.log("updated data.. " + JSON.stringify(data));
            var VisitorData = data.Attributes
            var resBody = {
              Id: VisitorData.Id,
              Email: VisitorData.Email,
              ImageUrl: VisitorData.ImageUrl,
              Name: VisitorData.Name,
              Phone: VisitorData.Phone,
              CountryCode: VisitorData.CountryCode,
              Gender: VisitorData.Gender,
              Age: VisitorData.Age,
              Status: VisitorData.Status,
              Role: VisitorData.Role,
              EmpId: VisitorData.EmpId,
              Source: VisitorData.Source
            };
            var respObj = { "message": "Visitor Updated Successfully..", "responseObj": resBody }
            return callback(err, response(200, JSON.stringify(respObj)))
          }
        })
      } else {
        return callback(err, response(200, JSON.stringify("Visitor Not Found ..!")));
      }
    }
  })
}