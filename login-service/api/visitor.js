'use strict';

var AWS = require('aws-sdk'),
    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
var CryptoJS = require("crypto-js");
var globalEvent = "";


function response(statusCode, message) {

    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
   // console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
   // console.log("Origin From Event ... " + origin);
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Origin;
    }
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Host;
    }
   // console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
    }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}


module.exports.updateProfile = function (event, context, callback) {
    globalEvent = event;
    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
            console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        } else {
            updateUserProfile(event, validateTokenResp, context, callback);
        }
    }
    validateMultipleLogin();
}
function updateUserProfile(event, validateTokenResp, context, callback) {
      console.log("comming from generateotp")
    const id = event.pathParameters.Email.toLowerCase();
    const requestBody = JSON.parse(event.body);

    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.Email, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.Email = decryptedEmail.toString(CryptoJS.enc.Utf8);

    //-- Decrypt Phone 
    var decryptedPhone = CryptoJS.AES.decrypt(requestBody.Phone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.Phone = decryptedPhone.toString(CryptoJS.enc.Utf8);
    console.log("request Phone ... " + requestBody.Phone);
    
    if (!id || !requestBody.CountryCode) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }
    var params = {
        TableName: process.env.VISITOR_TABLE,
        ExpressionAttributeValues: {
            ":email": id
        },
        KeyConditionExpression: 'Email = :email ',
    };
   console.log("params.. " + JSON.stringify(params));
    documentClient.query(params, function (err, data) {
        if (err) {
     //       console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
          //  console.log("GetItem succeeded:", JSON.stringify(data));
           // console.log("count of records ... " + data.Count)
            if (data.Count > 0) {
                var originalItem = data.Items[0];
                //-- As er are updating keys of DynamoDB, need to 1st delete existing record
                var deleteParams = {
                    TableName: process.env.VISITOR_TABLE,
                    Key: {
                        "Email": originalItem.Email,
                        "Phone": originalItem.Phone
                    },
                    ConditionExpression: "Email = :email",
                    ExpressionAttributeValues: {
                        ":email": originalItem.Email
                    }
                };
             //   console.log("Delete Params .. " + JSON.stringify(deleteParams));
                documentClient.delete(deleteParams, function (err, data) {
                    if (err) {
                        return callback(err, response(400, JSON.stringify(err)));
                    }
                 //   console.log(data);
                // console.log("Visitor deleted successfully..!");
                 // console.log("Phone number to be updated ... "+ requestBody.Phone);

                    //-- Now add this as a new record retaining some original values 
                    var updateParams = {
                        Item: {
                            "Id": originalItem.Id,
                            "UpdatedAt": new Date().toISOString(),
                            "ImageUrl": originalItem.ImageUrl,
                            "Name": requestBody.Name ? requestBody.Name : "",
                            "Password": originalItem.Password,
                            "Email": requestBody.Email.toLowerCase(),
                            "Phone": requestBody.Phone,
                            "CountryCode": requestBody.CountryCode,
                            "Gender": requestBody.Gender,
                            "Age": requestBody.Age,
                            "Status": originalItem.Status,
                            "Role": originalItem.Role,
                            "EmpId": originalItem.EmpId,
                            "Source": originalItem.Source,
                            "CreatedAt": originalItem.CreatedAt,
                            "SessionId" : originalItem.SessionId ? originalItem.SessionId : ""
                        },
                        TableName: process.env.VISITOR_TABLE
                    };
                    console.log("params from update profile.. " + JSON.stringify(updateParams));
                    documentClient.put(updateParams, function (err, data) {
                        if (err) {
                            return callback(err, response(400, JSON.stringify(err)));
                        }
                       // console.log(data);
                        var responseObj = {
                            "Id": originalItem.Id,
                            "UpdatedAt": new Date().toISOString(),
                            "Name": requestBody.Name,
                            "Email": requestBody.Email.toLowerCase(),
                            "Phone": requestBody.Phone,
                            "CountryCode": requestBody.CountryCode,
                            "Gender": requestBody.Gender,
                            "Age": requestBody.Age,
                            "Status": originalItem.Status,
                            "Role": originalItem.Role,
                            "EmpId": originalItem.EmpId,
                            "Source": originalItem.Source,
                            "ImageUrl": originalItem.ImageUrl,
                            "CreatedAt": originalItem.CreatedAt,
                        }
                       // console.log("error found")
                        var respObj = { "responseObj": responseObj, "message": "Visitor Updated Successfully..!" }
                        return callback(err, response(200, JSON.stringify(respObj)));
                    });
                });

            } else {
               // console.log("Visitor not found");
                var respObj = { "message": "Visitor not found." }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })
}
module.exports.getAllVisitors = function (event, context, callback) {
    globalEvent = event;
    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
           // console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
    }
    validateMultipleLogin();

    var params = {
        TableName: process.env.VISITOR_TABLE,
    }
    documentClient.scan(params, function (err, data) {
        if (err) {
            return callback(err, response(400, JSON.stringify(err)));
        }
       // console.log(data);
        var visitorData = data.Items;
        const sortedData = visitorData.sort((a, b) => new Date(a.CreatedAt) - new Date(b.CreatedAt));
        //console.log("sorted... " + JSON.stringify(sortedData))

        //-- For this sorted data, fetch details no of counts visitor has visited the museum
        //-- Consider booking status - Confirmed,Cancelled  and completed


        if (!visitorData.NoOfVisits) {
            visitorData.NoOfVisits = 0;
        }
        var respObj = { "responseObj": sortedData, "message": "Visitors data fetched successfully..!" }
        return callback(null, response(200, JSON.stringify(respObj)));
    })
}

module.exports.getVisitor = function (event, context, callback) {
    globalEvent = event;
    const id = event.pathParameters.Email.toLowerCase();
    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
       // console.log("Response From validate method.... " + JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
           // console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        } else {
            getVisitorData();
        }
    }
    validateMultipleLogin();

    function getVisitorData() {
        if (!id) {
            var respObj = { "message": "Manadatory input parameters are missing." }
            return callback(
                null,
                response(400, JSON.stringify(respObj))
            );
        }

        var params = {
            TableName: process.env.VISITOR_TABLE,
            ExpressionAttributeValues: {
                ":email": id
            },
            KeyConditionExpression: 'Email = :email '

        };
        //console.log("params.. " + JSON.stringify(params));
        documentClient.query(params, function (err, data) {
            if (err) {
              //  console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                return callback(err, response(400, JSON.stringify(err)));
            } else {
               // console.log("GetItem succeeded:", JSON.stringify(data));
               // console.log("count of records ... " + data.Count)
                if (data.Count > 0) {
                    var respObj = { "responseObj": data.Items[0], "message": "Visitor's data fetched successfully." }
                    return callback(null, response(200, JSON.stringify(respObj)));
                }
                else {
                    var respObj = { "message": "Visitor with emailId- " + id + " not found." }
                    return callback(null, response(200, JSON.stringify(respObj)));
                }
            }
        })
    }


}
//verify Mobile Number
module.exports.verifyMobileNum = function (event, context, callback) {
    globalEvent = event;
    const id = event.pathParameters.Email.toLowerCase();
    const Phone = JSON.stringify(event.pathParameters.Phone);

    if (!id || !Phone) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }

    var params = {
        TableName: process.env.VISITOR_TABLE,
        ExpressionAttributeValues: {
            ":email": id
        },
        KeyConditionExpression: 'Email = :email',

    };
    //console.log("params.. " + JSON.stringify(params));
    documentClient.query(params, function (err, data) {
        if (err) {
           // console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return callback(err, response(400, JSON.stringify(err)));
        } else {
           // console.log("GetItem succeeded:", JSON.stringify(data));
           // console.log("count of records ... " + data.Count)
            if (data.Count > 0) {

                var registeredPhone = JSON.stringify(data.Items[0].Phone);
               // console.log("registeredPhone .. " + registeredPhone);
               // console.log("Phone .. " + Phone);
                if (Phone.toString() === registeredPhone.toString()) {
                  //  console.log("Phone number matched !");
                    //- Create JWT token

                    var VisitorData = data.Items[0]
                 console.log("Visitor data .. " + JSON.stringify(VisitorData))
                    var resBody = {
                        Id: VisitorData.Id,
                        Email: VisitorData.Email,
                        Name: VisitorData.Name,
                        Phone: VisitorData.Phone,
                        Gender: VisitorData.Gender,
                        Age: VisitorData.Age,
                        Status: VisitorData.Status
                    };
                    var respObj = { "message": "Mobile number matched! send OTP for verification.", "responseObj": resBody }
                    return callback(null, response(200, JSON.stringify(respObj)));
                } else {
                   // console.log("Please enter registered mobile number.");
                    var respObj = { "message": "Invalid Mobile Num. Please enter registered mobile number" }
                    return callback(null, response(400, JSON.stringify(respObj)));
                }
            }
            else {
                var respObj = { "message": "Visitor with emailId- " + id + " not found." }
                return callback(null, response(200, JSON.stringify(respObj)));
            }
        }
    })
}
//Image update
module.exports.updateImageProfile = function (event, context, callback) {
    globalEvent = event;
    const value = event.pathParameters.Email.toLowerCase();
    const requestBody = JSON.parse(event.body);
    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
            console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
    }
    validateMultipleLogin();
    if (!value || !requestBody.ImageUrl) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }
    var params = {
        TableName: process.env.VISITOR_TABLE,
        ExpressionAttributeValues: {
            ":email": value
        },
        KeyConditionExpression: 'Email = :email ',

    };
    documentClient.query(params, function (err, data) {
        if (err) {
            return response(400, JSON.stringify(err));
        } else {
           // console.log("check :: ", data)
            if (data.Count > 0) {
                var originalItem = data.Items[0];
                //-- As er are updating keys of DynamoDB, need to 1st delete existing record
                var Params = {
                    TableName: process.env.VISITOR_TABLE,
                    Key: {
                        "Email": originalItem.Email,
                        "Phone": originalItem.Phone
                    },
                    ConditionExpression: "Email = :email",
                    ExpressionAttributeValues: {
                        ":email": originalItem.Email
                    }
                };
                //console.log("Search Params ..." + JSON.stringify(params));
                documentClient.query(params, function (err, data) {
                    if (err) {
                        return ({ "statusCode": 400, "message": JSON.stringify(err) });
                    } else {
                        if (data.Count > 0) {
                            console.log("Full User Object ... " + JSON.stringify(data.Items[0]));
                            const userDetails = data.Items[0];
                            var params = {
                                TableName: process.env.VISITOR_TABLE,
                                Key: {
                                    "Email": value,
                                    "Phone": userDetails.Phone
                                },

                                UpdateExpression: 'set #ImageUrl = :IURL',
                                ExpressionAttributeValues: {
                                    ':IURL': requestBody.ImageUrl
                                },
                                ExpressionAttributeNames: {
                                    '#ImageUrl': 'ImageUrl'

                                },
                                ReturnValues: "UPDATED_NEW"
                            };

                          //  console.log("params.. " + JSON.stringify(params));
                            documentClient.update(params, function (err, data) {
                                if (err) {

                                    return callback(err, response(400, err));
                                }
                              //  console.log(data);
                                const responseObj = {
                                    "Id": userDetails.Id,
                                    "Email": userDetails.Email,
                                    "Phone": userDetails.Phone,
                                    "ImageUrl": requestBody.ImageUrl,
                                    "Name": userDetails.Name,
                                    "CountryCode": userDetails.CountryCode,
                                    "Gender": userDetails.Gender,
                                    "Age": userDetails.Age,
                                    "Status": userDetails.Status,
                                    "Role": userDetails.Role,
                                    "EmpId": userDetails.EmpId,
                                    "Source": userDetails.Source,
                                    "CreatedAt": userDetails.CreatedAt
                                }
                                var respObj = { "responseObj": responseObj, "message": " Updated Successfully..!" }
                                return callback(err, response(200, JSON.stringify(respObj)));
                            })

                        } else {
                          //  console.log("Logged in user details not found");
                            var respObj = { "message": "Logged in user details not found" }
                            return callback(null, response(404, JSON.stringify(respObj)));
                        }
                    }
                })
            } else {
                var respObj = { "message": "Visitor  data with given id not found" }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })
}
module.exports.getAllVisitorImages = function (event, context, callback) {
    globalEvent = event;
    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
         //   console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
    }
    validateMultipleLogin();
    let params = {
        TableName: process.env.VISITOR_TABLE,
    }
    documentClient.scan(params, function (err, data) {
        if (err) {
           // console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return callback(err, response(400, JSON.stringify(err)));
        } else {
            if (data != null) {
             //   console.log("scan done")
                let imageDataUrl = data.Items;
                let responseObj = []
                imageDataUrl.forEach(function (user) {
                    let imageData = user.ImageUrl;
                    if (imageData && imageData != 'undefined' && imageData != undefined) {
                        responseObj.push({ "image": imageData })
                    }
                });
                if (responseObj) {
                    let respObj = { "responseObj": responseObj, "message": "visitor image fatched" }
                    return callback(null, response(200, JSON.stringify(respObj)));
                }
                else {
                    let respObj = { "responseObj": responseObj, "message": "Image not found" }
                    return callback(null, response(200, JSON.stringify(respObj)));
                }
            }
        }
    });
}
//get image by Id
module.exports.getVisitorImagesById = function (event, context, callback) {
    globalEvent = event;
    const id = event.pathParameters.Email.toLowerCase();
    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
        if (validateTokenResp.statusCode != 200) {
            console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
    }
    validateMultipleLogin();
    if (!id) {
        let respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }
    let params = {
        TableName: process.env.VISITOR_TABLE,
        ExpressionAttributeValues: {
            ":email": id
        },
        KeyConditionExpression: 'Email = :email '
    };
   // console.log("params.. " + JSON.stringify(params));
    documentClient.query(params, function (err, data) {
        if (err) {
          //  console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return callback(err, response(400, JSON.stringify(err)));
        } else {
         //   console.log("GetItem succeeded:", JSON.stringify(data));
          //  console.log("count of records ... " + data.Count)
            if (data.Count > 0) {
            //    console.log("checking ", data)
                const ImageById = data.Items[0];
                let responseObj = {
                    "Name": ImageById.Name,
                    "ImageUrl": ImageById.ImageUrl
                }
              //  console.log("check", responseObj)
                let respObj = { "responseObj": responseObj, "message": "visitor image fatched" }
                return callback(null, response(200, JSON.stringify(respObj)));
            }
            else {
                let respObj = { "message": "Visitor with emailId- " + id + " not found." }
                return callback(null, response(200, JSON.stringify(respObj)));
            }
        }
    })
}