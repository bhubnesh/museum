'use strict';

var uuid = require('uuid');
const auth = require('../auth/token');
const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();
var CryptoJS = require("crypto-js");
var atob = require('atob');

var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com','http://museum.mahindra.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}


module.exports.signUpWithGoogle = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);
  console.log("RequestBody ... " + JSON.stringify(requestBody));
  if (!requestBody.user.email || !requestBody.user.name) {
    
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }


  var reqBody = {
    Item: {
      "Id": uuid.v1(),
      "CreatedAt": new Date().toISOString(),
      "UpdatedAt": new Date().toISOString(),
      "Name": requestBody.user.name,
      "Password": "test@123",
      "Email": requestBody.user.email.toLowerCase(),
      "Phone": "9999999999",
      "CountryCode": "+91",
      "Gender": "",
      "Age": 0,
      "Status": "Inactive",
      "Role": "Visitor",
      "Source": "Google",
      "ImageURL": "",
      "NoOfVisits": 0,
      "LastVisitedDate": ""

    },
    TableName: process.env.VISITOR_TABLE
  };

  console.log("reqBody ==============> " + reqBody);
  var email = requestBody.user.email.toLowerCase();
  var searchWithEmail = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": email
    },
    KeyConditionExpression: "Email = :email"
  }

  console.log("Search Params ..." + JSON.stringify(searchWithEmail));

  documentClient.query(searchWithEmail, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      console.log("count of records for email ... " + data.Count)
      if (data.Count > 0) {
        var activeStatus = "Active";
        if ((data.Items[0].Status) === activeStatus && data.Items[0].Source != "Google") {
          //-- User already exists.
          console.log("User with emailId already exists.");
          var respObj = { "message": "User with emailId already exists." }
          return callback(null, response(400, JSON.stringify(respObj)));

        } else {
          //-- generate JWT token
          var jwtToken = auth.generateJwtToken(email);
          if (jwtToken == null) {
            var respObj = { "message": "Error creating JWT token." }
            return callback(
              null,
              response(400, JSON.stringify(respObj))
            );
          }
          var resp = {
            "Id": data.Items[0].Id,
            "CreatedAt": data.Items[0].CreatedAt,
            "UpdatedAt": data.Items[0].UpdatedAt,
            "Name": data.Items[0].Name,
            "Password": data.Items[0].Password ? data.Items[0].Password : "",
            "Email": data.Items[0].Email,
            "Phone": data.Items[0].Phone,
            "CountryCode": data.Items[0].CountryCode,
            "Gender": data.Items[0].Gender ? data.Items[0].Gender : "",
            "Age": data.Items[0].Age ? data.Items[0].Age : "",
            "Status": data.Items[0].Status,
            "Role": data.Items[0].Role,
            "Source": data.Items[0].Source,
            "ImageURL": data.Items[0].ImageURL ? data.Items[0].ImageURL : ""

          }


          // Decrypt JWT Token
          var bytes = CryptoJS.AES.decrypt(jwtToken, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
          var token = bytes.toString(CryptoJS.enc.Utf8);

          var base64Url = atob(token.split('.')[1]);

          //-- Extract session Id from token
          var sessionId = JSON.parse(base64Url).sessionId;
          console.log("sessionId ----- " + sessionId);
          //-- Store this session Id into DB
          var params = {
            TableName: process.env.VISITOR_TABLE,
            Key: {
              "Email": data.Items[0].Email,
              "Phone": data.Items[0].Phone
            },

            UpdateExpression: 'set #sessionId = :sessionId',
            ExpressionAttributeValues: {
              ':sessionId': sessionId
            },
            ExpressionAttributeNames: {
              '#sessionId': 'SessionId'

            },
            ReturnValues: "UPDATED_NEW"
          };

          documentClient.update(params, function (err, data) {
            if (err) {
              return callback(err, response(400, err));
            } else {
              let otpParams1 = {
                TableName: process.env.OTP_TABLE,
                Key: {
                  "Email": requestBody.Email
                },
                UpdateExpression: 'set #GenOTPAttempt = :GenOTPAttempt',
                ExpressionAttributeNames: {
                  "#GenOTPAttempt": "GenOTPAttempt"
                },
                ExpressionAttributeValues: {
                  ':GenOTPAttempt': 0,
                },
                ReturnValues: "UPDATED_NEW"
              };
           //   console.log("otpParams1.. " + JSON.stringify(otpParams1));
              documentClient.update(otpParams1, function (err, data) {
                if (err) console.log(err);
                else console.log("data ", data);
              });
              var respObj = { "message": "Login successful", "responseObj": resp, "JwtToken": jwtToken }
              console.log("Response ... " + JSON.stringify(respObj));
              return callback(null, response(200, JSON.stringify(respObj)));

            }
          })
        }

      } else {
        //-- generate JWT token
        var jwtToken = auth.generateJwtToken(email);
        if (jwtToken == null) {
          var respObj = { "message": "Error creating JWT token." }
          return callback(
            null,
            response(400, JSON.stringify(respObj))
          );
        }

        // Decrypt JWT Token
        var bytes = CryptoJS.AES.decrypt(jwtToken, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
        var token = bytes.toString(CryptoJS.enc.Utf8);
        var base64Url = atob(token.split('.')[1]);

        //-- Extract session Id from token
        var sessionId = JSON.parse(base64Url).sessionId;
        console.log("sessionId ----- " + sessionId);
        reqBody.Item.SessionId = sessionId;
        console.log("Session Id DB .... " + reqBody.Item.SessionId);

        documentClient.put(reqBody, function (err, data) {
          let otpParams1 = {
            TableName: process.env.OTP_TABLE,
            Key: {
              "Email": requestBody.Email
            },
            UpdateExpression: 'set #GenOTPAttempt = :GenOTPAttempt',
            ExpressionAttributeNames: {
              "#GenOTPAttempt": "GenOTPAttempt"
            },
            ExpressionAttributeValues: {
              ':GenOTPAttempt': 0,
            },
            ReturnValues: "UPDATED_NEW"
          };
       //   console.log("otpParams1.. " + JSON.stringify(otpParams1));
          documentClient.update(otpParams1, function (err, data) {
            if (err) console.log(err);
            else console.log("data ", data);
          });
          console.log("created user!");
          var respObj = { "message": "User created successfully..!", "responseObj": reqBody.Item, "JwtToken": jwtToken }
          return callback(null, response(200, JSON.stringify(respObj)));
        })
      }
    }
  })

}
