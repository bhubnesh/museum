'use strict';


var AWS = require('aws-sdk'),

  documentClient = new AWS.DynamoDB.DocumentClient();
const moment = require('moment');

module.exports.hitCountMonthly = async (event) => {
  //var requestBody = JSON.parse(event.body);
  console.log("event",event)
  console.log("event ", JSON.stringify(event.Records[0].dynamodb))
  var rec = event.Records[0]
  let records = [];
  records.push(rec)
  //console.log("records",records)
  for (const record of records) {
    if (record.dynamodb.NewImage.TypeOfBooking.S == 'Normal') {
      let Feedbacksubmit = record.dynamodb.NewImage.FeedbackAnswers.L;
      for (const data1 of Feedbacksubmit) {
        var arrayDatainfo = data1['M']
        //console.log("Hello dddd20", arrayDatainfo)
        if (arrayDatainfo.NoOfStars.S != undefined && arrayDatainfo.NoOfStars.S != null && arrayDatainfo.NoOfStars.S != '') {
          // console.log("arrayDatainfo 22 ", arrayDatainfo.NoOfStars.S)
          var noOfStars = arrayDatainfo['NoOfStars']['S']
          // var stars = noOfStars.split(' ').join('_');
          var stars = noOfStars
          var count = 0;
          var date = new Date();
          var todaysDate = moment(date).format('YYYY-MM')

          var params = {
            TableName: process.env.REPORTSCOUNT_TABLE,
            Key: {
              "CountName": "FeedbackBookingMonthly",
              "recordDate": todaysDate + "_" + stars,
            }
          }
          
          // Check if record present
          var data = await documentClient.get(params).promise()
          var element = data.Item;
          console.log("data ", data)
          if (typeof element != "undefined" && element != "{}") {
            console.log("inside update count")
            var updateParams = {
              TableName: process.env.REPORTSCOUNT_TABLE,
              Key: {
                "CountName": "FeedbackBookingMonthly",
                "recordDate": element.recordDate,
              },
              UpdateExpression: "set hitCount=hitCount+:val",
              ExpressionAttributeValues: {
                ":val": 1,
              },
            }
            console.log(updateParams)
            var res = await documentClient.update(updateParams).promise();
            console.log(" IN UPDATING ", res)
          } else {
            // if no, insert
            count = 1;
            var updateParams = {
              TableName: process.env.REPORTSCOUNT_TABLE,
              Item: {
                "CountName": "FeedbackBookingMonthly",
                "recordDate": todaysDate + "_" + stars,
                "hitCount": count

              }
            }
            var data = await documentClient.put(updateParams).promise()
            console.log(" IN INSERTING ", data)
          }
        }
      }
    }else{
      console.log("please check Booking Type")
    }
  }
};
