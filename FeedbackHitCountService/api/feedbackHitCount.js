'use strict';


var AWS = require('aws-sdk'),

  documentClient = new AWS.DynamoDB.DocumentClient();
const moment = require('moment');

module.exports.hitCount = async (event) => {
  //var requestBody = JSON.parse(event.body);
  console.log("event",event)
  console.log("event ", JSON.stringify(event.Records.dynamodb))
  var rec = event.Records[0]
  if(rec.eventName == "INSERT"){
  let records = [];
  records.push(rec)
  //console.log("records",records)
  for (const record of records) {
    //console.log("records ",record)
    //console.log("check1",record.dynamodb.NewImage.TypeOfBooking)
    if (record.dynamodb.NewImage.TypeOfBooking.S == 'Normal') {
      let Feedbacksubmit = record.dynamodb.NewImage.FeedbackAnswers.L;
      for (const data1 of Feedbacksubmit) {
        var arrayDatainfo = data1['M']
        //console.log("Hello dddd20", arrayDatainfo)
        if (arrayDatainfo.NoOfStars.S != undefined && arrayDatainfo.NoOfStars.S != null && arrayDatainfo.NoOfStars.S != '') {
           console.log("arrayDatainfo 22 ", arrayDatainfo.NoOfStars.S)
          var noOfStars = arrayDatainfo['NoOfStars']['S']
          // var stars = noOfStars.split(' ').join('_');
          var stars = noOfStars
          var count = 0;
          var date = new Date();
          var todaysDate = moment(date).format('YYYY-MM-DD')
          var params = {
            TableName: process.env.REPORTSCOUNT_TABLE,
            Key: {
              "CountName": "FeedbackBookingDaily",
              "recordDate": todaysDate + "_" + stars,
            }
          }
          // Check if record present
          var data = await documentClient.get(params).promise()
          var element = data.Item;
          console.log("data ", data)
          if (typeof element != "undefined" && element != "{}") {
            console.log("inside update count")
            var updateParams = {
              TableName: process.env.REPORTSCOUNT_TABLE,
              Key: {
                "CountName": "FeedbackBookingDaily",
                "recordDate": element.recordDate,
              },
              UpdateExpression: "set hitCount=hitCount+:val",
              ExpressionAttributeValues: {
                ":val": 1,
              },
            }
            console.log(updateParams)
            var res = await documentClient.update(updateParams).promise();
            console.log(" IN UPDATING ", res)
          } else {
            // if no, insert
            count = 1;
            var updateParams = {
              TableName: process.env.REPORTSCOUNT_TABLE,
              Item: {
                "CountName": "FeedbackBookingDaily",
                "recordDate": todaysDate + "_" + stars,
                "hitCount": count

              }
            }
            var data = await documentClient.put(updateParams).promise()
            console.log(" IN INSERTING ", data)
          }
        }
      }
    }
  }
}else{
  console.log("Please enter TypeOfBooking as Normal")
}
};

function response(statusCode, message) {
  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      'X-XSS-Protection' : '1; mode=block',
       'x-frame-options': 'SAMEORIGIN',
      'x-content-type': 'nosniff',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}