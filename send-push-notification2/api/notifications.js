'use strict';


const { SNS } = require('aws-sdk');
var AWS = require('aws-sdk');
var sns = new AWS.SNS();
var moment = require('moment');
const topicName  ='EventTmrw';

var documentClient = new AWS.DynamoDB.DocumentClient({ convertEmptyValues: true });
var todaysDate = new Date();
todaysDate.setDate(todaysDate.getDate() + 1)
const textDate = moment(todaysDate).format('DD MMM YYYY')
var title = "This is your reminder for the booking to the Mahindra Museum on "+textDate;

let payload = {
    default:'default',
    GCM: {
        notification: {
            title: title,
            body: 'You have a visit Tomorrow',
           
            sound: 'default'
        }
    },
  
    APNS: JSON.stringify({
        aps: {
          alert:  {
            title: title,
            body: 'You have a visit Tomorrow',
            action: "action url"
        },
        sound:"default"
          
        }
      })

   
}

payload.GCM = JSON.stringify(payload.GCM);



payload = JSON.stringify(payload);


var params = {
    Message: payload,
    Subject: "M&M Museum visit Tommorow!",

    TargetArn: "arn:aws:sns:ap-south-1:176378803823:endpoint/GCM/MyAndriodNotApp/7b7a0b5c-00c3-3173-82b8-9d28d66cf03d",
   
    MessageStructure: 'json'

};


var subscribeParam = {
    Protocol: 'application',
    TopicArn: 'topicArn',
    Endpoint: 'MOBILE_END_point'
}

var lstTopicParams = {
    NextToken: ''
};

var emailIds = {
    TableName: process.env.BOOKINGINFO_TABLE,
    ProjectionExpression: "BookingDate, ParticipantDetails,VisitorEmail",

};


module.exports.sendPush = function (event, context, callback) {
    var eventText = (event);
    console.log("todaysDate :: ", todaysDate)
    
    console.log("todaysDate +1 :: ", moment(todaysDate).format('YYYY-MM-DD'))
    const tomorrowsDate = moment(todaysDate).format('YYYY-MM-DD');
    var items = [];

    async function fetchUsers() {
        //console.log(fullData,"/n");

        var data = await documentClient.scan(emailIds).promise();
        console.log("data :: ", data);
        // documentClient.scan(emailIds, function (err, data) {
        //     if (err) {
        //         console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
        //         return response(400, JSON.stringify(err));
        //     } else {

        // Fething user having booking for Tommorow
        data.Items.forEach(

            element => {

                //if (typeof element.ParticipantDetails.Adult1 !== 'undefined') {
                    element.BookingDate = element.BookingDate.substring(0,10);
                    console.log("Booking Date : ",element.BookingDate)
                    console.log("tomorrow Date : ",tomorrowsDate)
                if (element.BookingDate == tomorrowsDate) {

                    console.log(element.BookingDate, element.VisitorEmail)
                    items = items.concat(element.VisitorEmail);
                }
                //}
            }
        )

            console.log("items : ",items)
        // tokens.ExpressionAttributeValues = emailObject;
        for (const itmem of items) {

            //get tokens
            var tokens = {
                TableName: process.env.DEVICE_TOKENS,

                ProjectExpression: "Id,arn",
                KeyConditionExpression: "#Id = :email",
                // Filterexpression : "Id IN ("+Object.keys(emailObject).toString()+")",

                ExpressionAttributeNames: {

                    "#Id": "Id"
                },

                ExpressionAttributeValues: {

                    ":email": itmem,

                }
            }


            console.log(tokens);
            var res = await documentClient.query(tokens).promise();
            console.log("res :: ",res);
            subscribeParam.Endpoint = res.Items[0].ARN;
            // console.log("subscribeParam 1:: ",subscribeParam);

            //subcribe to topic
            console.log("subscribeParam :: ",subscribeParam);                 
            var subs = await sns.subscribe(subscribeParam).promise();
            console.log("Subscribe :: ", subs);


        }

        if (data.LastEvaluatedKey) {

            emailIds.ExclusiveStartKey = data.LastEvaluatedKey;

             await fetchUsers();
        } else {
            console.log("Dont have the LastEvaluatedKey")


            publishNotification(params);
        }


    }
    let respObj = { "message": "Notifiation Sent!!!" }
    var err2 = "error"
    getTopics();
    fetchUsers();
    console.log("out of all function!!")
    return callback(null, response(200, JSON.stringify(respObj)));
    //return '200';
    


}
console.log("out of all loop!!")
//publich message to topic
async function publishNotification(params) {
    // let payload = {
    //     default:'default',
    //     GCM: {
    //         notification: {
    //             title: 'M&M Museum visit tomorrow',
    //             body: 'You have a visit Tomorrow',
    //             sound: 'default'
    //         }
    //     }
    // }
    // payload.GCM = JSON.stringify(payload.GCM);

    // payload = JSON.stringify(payload);
    // params.message = payload;
    params.TargetArn = await getTopics();
    console.log("params::::::: ", params)
    var data  = await sns.publish(params).promise();
    console.log(data)
    // var deleteParams = {
    //     TopicArn: params.TargetArn
    //   };
    // await  sns.deleteTopic(deleteParams).promise();
    // var cretaeParams = {
    //     Name: topicName, /* required */
        
    //   };
    //   await sns.createTopic(cretaeParams).promise();
    
}
function response(statusCode, message) {
    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      'X-XSS-Protection' : '1; mode=block',
       'x-frame-options': 'SAMEORIGIN',
      'x-content-type': 'nosniff',
            'Access-Control-Allow-Credentials': true,

        },
        body: message
    };
}

// Fetching topics
async function getTopics() {
    var topicArn = '';
    const result = await sns.listTopics(lstTopicParams).promise();
    console.log("topic::", result);           // successful response
    result.Topics.forEach(
        topic => {
            if (topic.TopicArn.includes(topicName)) {

                subscribeParam.TopicArn = topic.TopicArn;
                topicArn = topic.TopicArn;

                return topicArn;
            }
        }
    )

    return topicArn;
}