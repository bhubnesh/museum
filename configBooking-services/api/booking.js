'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const moment = require('moment');
const axios = require('axios');
let paths = require('../config/apiEndpoints');
const auth = require('../auth/token');
var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}



module.exports.addBooking = function (event, context, callback) {
    globalEvent = event;
    console.log(event);
    const requestBody = JSON.parse(event.body);
    //-- Validate Token
    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();

    if (!requestBody.MaximumParticipantsAllowedPerSlot) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }
    var params = {
        Item: {
            "Id": 1,
            "NumberOfDaysBookingWillBeOpen": requestBody.NumberOfDaysBookingWillBeOpen,
            "MaximumParticipantsAllowedPerSlot": requestBody.MaximumParticipantsAllowedPerSlot,
            "MaximumParticipantsAllowedToBookviaAppPerBooking": requestBody.MaximumParticipantsAllowedToBookviaAppPerBooking,
            "SlotInterval": requestBody.SlotInterval,
            "MaximumParticipantsAllowedToBookviaAppPerBookingperWebsite": requestBody.MaximumParticipantsAllowedToBookviaAppPerBookingperWebsite,
            "VisuallyImpairedAssistance": requestBody.VisuallyImpairedAssistance,
            "VIAMaximumAllowedPerBooking": requestBody.VIAMaximumAllowedPerBooking,
            "VIAMaximumAvailablePerSlot": requestBody.VIAMaximumAvailablePerSlot,
            "VIAAllowUsersToBookAfter": requestBody.VIAAllowUsersToBookAfter,
            "WheelChairAssistance": requestBody.WheelChairAssistance,
            "WCAMaximumAllowedPerBooking": requestBody.WCAMaximumAllowedPerBooking,
            "WCAMaximumAvailablePerSlot": requestBody.WCAMaximumAvailablePerSlot,
            "HearingImpairedAssistance": requestBody.HearingImpairedAssistance,
            "HIAMaximumAvailablePerSlot": requestBody.HIAMaximumAvailablePerSlot,
            "HIAAllowUsersToBookAfter": requestBody.HIAAllowUsersToBookAfter,
            "ManageSimulatorSettings": requestBody.ManageSimulatorSettings,
            "AllowVisitorCancellationBefore": requestBody.AllowVisitorCancellationBefore,
            "AllowVisitorRescheduleBefore": requestBody.AllowVisitorRescheduleBefore

        },
        TableName: process.env.CONFIGBOOKING_TABLE
    };
    console.log(params);

    //-- Check whether Booking already exists

    var params1 = {
        TableName: process.env.CONFIGBOOKING_TABLE,
        Key: {
            "Id": 1
        },

    };
    console.log("params.. " + JSON.stringify(params1));
    documentClient.get(params1, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            var record = data.Item;
            if (data.Item) {
                console.log("Booking details already exists.");
                var respObj = { "message": "Booking details already exists." }
                //-- Update Booking  Details
                var updateParams = {
                    Item: {
                        "Id": 1,
                        "NumberOfDaysBookingWillBeOpen": requestBody.NumberOfDaysBookingWillBeOpen,
                        "MaximumParticipantsAllowedPerSlot": requestBody.MaximumParticipantsAllowedPerSlot,
                        "MaximumParticipantsAllowedToBookviaAppPerBooking": requestBody.MaximumParticipantsAllowedToBookviaAppPerBooking,
                        "SlotInterval": requestBody.SlotInterval,
                        "MaximumParticipantsAllowedToBookviaAppPerBookingperWebsite": requestBody.MaximumParticipantsAllowedToBookviaAppPerBookingperWebsite,
                        "VisuallyImpairedAssistance": requestBody.VisuallyImpairedAssistance,
                        "VIAMaximumAllowedPerBooking": requestBody.VIAMaximumAllowedPerBooking,
                        "VIAMaximumAvailablePerSlot": requestBody.VIAMaximumAvailablePerSlot,
                        "VIAAllowUsersToBookAfter": requestBody.VIAAllowUsersToBookAfter,
                        "WheelChairAssistance": requestBody.WheelChairAssistance,
                        "WCAMaximumAllowedPerBooking": requestBody.WCAMaximumAllowedPerBooking,
                        "WCAMaximumAvailablePerSlot": requestBody.WCAMaximumAvailablePerSlot,
                        "HearingImpairedAssistance": requestBody.HearingImpairedAssistance,
                        "HIAMaximumAvailablePerSlot": requestBody.HIAMaximumAvailablePerSlot,
                        "HIAAllowUsersToBookAfter": requestBody.HIAAllowUsersToBookAfter,
                        "ManageSimulatorSettings": requestBody.ManageSimulatorSettings,
                        "AllowVisitorCancellationBefore": requestBody.AllowVisitorCancellationBefore,
                        "AllowVisitorRescheduleBefore": requestBody.AllowVisitorRescheduleBefore
                    },
                    TableName: process.env.CONFIGBOOKING_TABLE
                };
                console.log("Update params.. " + JSON.stringify(updateParams));
                documentClient.put(updateParams, function (err, data) {
                    if (err) {
                        return callback(err, response(400, JSON.stringify(err)));
                    }
                    console.log(data);
                    if (requestBody.SlotInterval != record.SlotInterval) {
                        var todaysDate = moment().format('YYYY-MM-DD');
                        
                        var startDate = moment(todaysDate).add(1, 'days');
                        var endDate = startDate.clone().add(90, 'days')
                        var details = {
                            "event": event,
                            "DateFrom": startDate,
                            "DateTo": endDate,
                            "TimeHrFrom": "00",
                            "TimeHrTo": "23",
                            "TimeMinFrom": "01",
                            "TimeMinTo": "59",
                            "TimeTypeFrom": "am",
                            "TimeTypeTo": "pm"
                        }
                        console.log("line 113")
                        const bookingCancel_URL = paths.API_URLS.findAndCancelBookingUrl;
                        axios.post(bookingCancel_URL, (JSON.stringify(details)), {
                            headers: {
                                'Authorization': event.headers.Authorization
                            }
                        }).then(respn => { console.log(respn.statusCode) })
                    }
                    var respObj = { "message": "Booking updated successfully..!" }
                    return callback(err, response(200, JSON.stringify(respObj)));
                });

            } else {
                var respObj = { "message": "Booking created successfully..!" }
                documentClient.put(params, function (err, data) {
                    return callback(err, response(200, JSON.stringify(respObj)));
                });
            }
        }
    })

}


// get particular Booking details 

module.exports.getBooking = function (event, context, callback) {
    globalEvent = event;

    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();


    var params = {
        TableName: process.env.CONFIGBOOKING_TABLE,
        ExpressionAttributeValues: {
            ":id": 1
        },
        KeyConditionExpression: '#id = :id',
        ExpressionAttributeNames: {
            "#id": "Id"
        }

    };
    console.log("params.. " + JSON.stringify(params));
    documentClient.query(params, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return callback(err, response(400, JSON.stringify(err)));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            console.log("count of records ... " + data.Count)
            if (data.Count > 0) {

                const noOfDaysForBooking = data.Items[0].NumberOfDaysBookingWillBeOpen;
                console.log("noOfDaysForBooking .... " + noOfDaysForBooking)
                const fromDateForBooking = moment(new Date()).format('YYYY-MM-DD');
                console.log("From Date ... " + fromDateForBooking);
                var toDate = new Date();
                toDate.setDate(toDate.getDate() + noOfDaysForBooking);
                const toDateForBooking = moment(toDate).format('YYYY-MM-DD');
                console.log("To Date ... " + toDateForBooking);
                data.Items[0].fromDateForBooking = fromDateForBooking;
                data.Items[0].toDateForBooking = toDateForBooking;

                var respObj = { "responseObj": data.Items[0], "message": "Booking details fetched successfully." }
                return callback(null, response(200, JSON.stringify(respObj)));
            } else {
                var respObj = { "message": "Booking with type- " + id + " not found." }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })

}