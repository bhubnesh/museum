'use strict';

var AWS = require('aws-sdk'),

  documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
const _ = require('underscore');
const moment = require('moment');
const { CostExplorer } = require('aws-sdk');


var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com', 'http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}



module.exports.getMonthlyBookingReport = function (event, context, callback) {
  debugger;
  globalEvent = event;
  //   const validateMultipleLogin = async() =>{
  //     const validateTokenResp =  await auth.validateToken(event, context, callback);
  //     //console.log("Response.... "+ JSON.stringify(validateTokenResp));

  //         if  (validateTokenResp.statusCode != 200) {
  //             console.log("Response .............. "+ JSON.stringify(validateTokenResp));
  //             return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
  //         }
  // }
  // validateMultipleLogin();

  const requestParams = (event.queryStringParameters);
  if (!requestParams.FromMonth || !requestParams.ToMonth) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  const fromDate = requestParams.FromMonth;
  const toDate = requestParams.ToMonth;
  event.fromDate = fromDate;
  event.toDate = toDate;

  var fromYear = fromDate.substring(0, 4);
  var fromMonth = fromDate.substring(5, 7);
  var toYear = toDate.substring(0, 4);
  var toMonth = toDate.substring(5, 7);
  event.fromYear = fromYear;
  event.toYear = toYear;
  event.fromMonth = fromMonth;
  event.toMonth = toMonth;


  var searchWithMonth = {
    TableName: process.env.REPORTCOUNT_TABLE,
    ExpressionAttributeValues: {
      ":countName": "BookingMonthly"
    },

    KeyConditionExpression: "CountName = :countName "
  }

  
  documentClient.query(searchWithMonth, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {

      if (data.Count > 0) {
        var countRecords = data.Items;
        filterDataMonthWise(event, countRecords, callback);

      } else {
        var respObj = { "message": "Data not found." }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })

}

function filterDataMonthWise(event, countRecords, callback) {
  var timeSpan = [];
  var yearMonth = [];
  var bookViaApp = [];
  var bookViaWebsite = [];
  
  var month = (event.fromMonth);
  var year = (event.fromYear);



  while (!(parseInt(year) == parseInt(event.toYear) && parseInt(month) == parseInt(event.toMonth))) {
    if (month < 12) {
      month++;
    } else {
      month = 1;
      year++;
    }
    console.log(month + '-' + year);
    (month <= 9) ? month = ("0" + month) : month = month.toString();
    // timeSpan.push(month);
    yearMonth.push(year + "-" + month);
  }


  var count = countRecords.length;
  countRecords.forEach(item => {
    var recordYear = (item.recordDate).substring(0, 4);
    var recordMonth = (item.recordDate).substring(5, 7);
    var recordYearMonth = recordYear + "-" + recordMonth;
    var monthCount = yearMonth.length;
    if (monthCount > 0) {
      yearMonth.forEach(time => {
        if (time == recordYearMonth) {
          timeSpan.push(recordMonth);

          var hitCountForBooking = 0;
          (!item.hitCount) ? hitCountForBooking = 0 : hitCountForBooking = item.hitCount;


          //-- Hit count

          //-- Book via app or website count
          if ((item.recordDate).substring(8, 9) == 0 && (item.recordDate).substring(8, 9) != 1) {
            var obj = {};
            obj[recordYearMonth] = hitCountForBooking;
            bookViaApp.push(obj);
          } else {
            var obj = {};
            obj[recordYearMonth] = 0;
            bookViaApp.push(obj);
          }
          //-- Website count
          if ((item.recordDate).substring(8, 9) == 1 && (item.recordDate).substring(8, 9) != 0) {
            var obj = {};
            obj[recordYearMonth] = hitCountForBooking;
            bookViaWebsite.push(obj);
          } else {
            var obj = {};
            obj[recordYearMonth] = 0;
            bookViaWebsite.push(obj);
          }
          if ((item.recordDate).substring(8, 9) != 1 && (item.recordDate).substring(8, 9) != 0) {
            var obj = {};
            obj[recordYearMonth] = 0;
            bookViaWebsite.push(obj);
            bookViaApp.push(obj);
          }
        }
      })
    } else {
      var respObj = { "message": "No data found!" }
      return callback(null, response(404, JSON.stringify(respObj)));
    }
    count--;
    if (count == 0) {
      var non_duplidated_data = _.uniq(timeSpan);

      //-- get checkedIn count 
      getCheckedIncount(yearMonth, non_duplidated_data, bookViaApp, bookViaWebsite, callback);

    }
  })

}
function getCheckedIncount(yearMonth, non_duplidated_data, bookViaApp, bookViaWebsite, callback) {
  var checkIn = [];
  
  var searchWithMonth = {
    TableName: process.env.REPORTCOUNT_TABLE,
    ExpressionAttributeValues: {
      ":countName": "CheckInCountMonthly"
    },

    KeyConditionExpression: "CountName = :countName "
  }

  
  documentClient.query(searchWithMonth, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
      if (data.Count > 0) {
        var countRecords1 = data.Items;
        var count = countRecords1.length;
        countRecords1.forEach(item => {
          var recordYear = (item.recordDate).substring(0, 4);
          var recordMonth = (item.recordDate).substring(5, 7);
          var recordYearMonth = recordYear + "-" + recordMonth;
          var monthCount = yearMonth.length;
          if (monthCount > 0) {
            yearMonth.forEach(time => {
              if (time == recordYearMonth) {

                //-- checkIn count
                if (item.checkInCount) {
                  var obj = {};
                  obj[recordYearMonth] = item.checkInCount;
                  checkIn.push(obj);
                } else {
                  var obj = {};
                  obj[recordYearMonth] = 0;
                  checkIn.push(obj);
                }

              }
            })
          }
          count--;
          if (count == 0) {

            var respData = {
              TimeSpan: non_duplidated_data,
              Datasets: [
                {
                  label: "BookedViaApp",
                  data: bookViaApp
                },
                {
                  label: "BookedViaWebsite",
                  data: bookViaWebsite
                },
                {
                  label: "CheckInParticipants",
                  data: checkIn
                },
              ]
            }
            console.log("respData", respData)
            var respObj = { "responseObj": respData, "message": "Booking report fetched successfully..!" }
            return callback(null, response(200, JSON.stringify(respObj)));
          }

        })

      } else {
        console.log("Checked In count not found.");

        var respData = {
          TimeSpan: non_duplidated_data,
          Datasets: [
            {
              label: "BookedViaApp",
              data: bookViaApp
            },
            {
              label: "BookedViaWebsite",
              data: bookViaWebsite
            },
            {
              label: "CheckInParticipants",
              data: 0
            },
          ]
        }
        console.log("respData", respData)
        var respObj = { "responseObj": respData, "message": "Booking report fetched successfully..!" }
        return callback(null, response(200, JSON.stringify(respObj)));
      }
    }
  })
}