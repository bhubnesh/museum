'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
const _ = require('underscore');

var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}



module.exports.getDailyBookinggReport = function (event, context, callback) {
    debugger;
    globalEvent = event;
    const date = event.pathParameters.Date;
    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();

    if (!date) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }
    var slotsToFilter = [];
    var slots = [];

    //-- Check slots data available for given date and get all the slots
    var searchWithDate = {
        TableName: process.env.BOOKINGDATE_TABLE,
        ExpressionAttributeValues: {
            ":date": date
        },
        KeyConditionExpression: "BookingDate = :date"
    }

    console.log("Search Params ..." + JSON.stringify(searchWithDate));

    documentClient.query(searchWithDate, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {

            if (data.Count > 0) {
                var slotArray = [];
                slotArray = data.Items[0].SlotsDetails;
                var slotArrayCount = slotArray.length;
                if (slotArrayCount > 0) {
                    slotArray.forEach(slot => {
                        slotsToFilter.push({ "SlotTime": slot.slot });
                        slots.push(slot.slot);
                        slotArrayCount--;
                        if (slotArrayCount == 0) {
                            console.log("Slots to filter ... " + JSON.stringify(slotsToFilter));
                            filterBookingData(date, slotsToFilter, slots, callback);
                        }
                    })
                } else {
                    var respObj = { "message": "SLot details not available for the given date." }
                    return callback(null, response(404, JSON.stringify(respObj)));
                }
            } else {
                var respObj = { "message": "Slot details not available for the given date." }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })
}
function filterBookingData(date, slotsToFilter, slots, callback) {

    var params = {
        TableName: process.env.BOOKING_TABLE,
        IndexName: "BookingDate-index",
        KeyConditionExpression: "BookingDate = :bdate",
        ExpressionAttributeValues: {
            ":bdate": date
        }

    };

    console.log("params.. " + JSON.stringify(params));
    documentClient.query(params, function (err, data) {
        if (err) {
            return callback(err, response(400, JSON.stringify(err)));
        } else {
            console.log * ("Booking data length.. " + JSON.stringify(data));
            var bookingData = data.Items;
            console.log * ("Booking data length.. " + bookingData);
            var bookingDataBasedOnSlot = [];
            var bookingCount = bookingData.length;

            if (bookingData.length > 0) {
                bookingData.forEach(booking => {
                    slotsToFilter.forEach(slot => {
                        var slot_inString = slot.SlotTime;
                        var booking_inString = booking.SlotTime;
                        if ((slot_inString).substring(0, 2) == (booking_inString).substring(0, 2)) {
                            //if (slot.SlotTime == booking.SlotTime) {
                            bookingDataBasedOnSlot.push(booking);
                            bookingCount--;
                            if (bookingCount == 0) {
                                finalResponse(date, bookingDataBasedOnSlot, slotsToFilter, slots, callback);
                            }
                        }
                    })
                })
            } else {
                var respObj = { "message": "Booking data not available for the given date." }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })
}

function finalResponse(date, bookingDataBasedOnSlot, slotsToFilter, slots, callback) {

    var finalBookingData = [];
    var singleBooking = {};
    var count = bookingDataBasedOnSlot.length;
    bookingDataBasedOnSlot.forEach(booking => {
        if (booking.TypeOfBooking == "Normal") {
            //            singleBooking.Id = booking.Id;
            //          singleBooking.NoOfParticipants = booking.NoOfParticipants,
            singleBooking.BookingStatus = booking.BookingStatus;
            singleBooking.TypeOfBooking = booking.TypeOfBooking;
            singleBooking.BookingVia = booking.BookingVia;
            singleBooking.ActualNoOfParticipants = booking.TotalNoOfParticipantVisited;

            var obj = {};
            obj[booking.SlotTime] = singleBooking
            singleBooking = {};
            finalBookingData.push(obj);
            count--;
        } else {
            count--;
        }
        if (count == 0) {
            filterResponse(finalBookingData, slotsToFilter, slots, callback);
            // var respObj = { "message": "Booking data filtered successfully.", "responseObj": finalBookingData }
            // return callback(null, response(200, JSON.stringify(respObj)));
        }
    })

}
function filterResponse(finalBookingData, slotsToFilter, slots, callback) {

    const groupBySlotsData = finalBookingData.reduce((acc, val) => {
        const time = Object.keys(val)[0];
        acc[time] = acc[time] ? [...acc[time], { ...val[time] }] : [{ ...val[time] }];
        return acc;
    }, {});


    var appData = [];
    var websiteData = [];
    var checkInData = [];
    var checkInCount = 0;
    var appCount = 0;
    var websiteCount = 0;



    var timeSpan = (Object.keys(groupBySlotsData));

    var timeSpanCount = timeSpan.length;
    timeSpan.forEach(time => {

        var slotArray = groupBySlotsData[time];
        var count = slotArray.length;
        slotArray.map(slotData => {

            slotData.BookingStatus == "CheckedIn" ? checkInCount = checkInCount + slotData.ActualNoOfParticipants : checkInCount = checkInCount + 0;
            slotData.BookingVia == 0 ? appCount = appCount + 1 : websiteCount = websiteCount + 1;
            count--;

            if (count == 0) {
                var obj1 = {};
                obj1[time] = appCount;
                appData.push(obj1);
                console.log("appCount.. " + appCount);
                appCount = 0;

                var obj2 = {};
                obj2[time] = websiteCount;
                websiteData.push(obj2);
                console.log("websiteCount.. " + websiteCount);
                websiteCount = 0;
                var obj3 = {};
                obj3[time] = checkInCount;
                checkInData.push(obj3);
                console.log("checkInCount ... " + checkInCount);
                checkInCount = 0;
                console.log("Time count ... " + timeSpanCount);

            }

        })
        timeSpanCount--;
        if (timeSpanCount == 0) {
            var respData = {
                TotalSlots: slots,
                TimeSpan: timeSpan,
                Datasets: [
                    {
                        label: "BookedViaApp",
                        data: appData
                    },
                    {
                        label: "BookedViaWebsite",
                        data: websiteData
                    },
                    {
                        label: "CheckInParticipants",
                        data: checkInData
                    },
                ]
            }
            var respObj = { "responseObj": respData, "message": "Booking report fetched successfully..!" }
            return callback(null, response(200, JSON.stringify(respObj)));
        }
    })
}