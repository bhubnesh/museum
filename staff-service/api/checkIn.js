'use strict';

var AWS = require('aws-sdk'),

  documentClient = new AWS.DynamoDB.DocumentClient();

const auth = require('../auth/token');
var globalEvent = "";

function response(statusCode, message) {
    
  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if(origin == ""|| origin == undefined || !origin){
     origin = globalEvent.headers.Origin;
  }
  if(origin == ""|| origin == undefined || !origin){
    origin = globalEvent.headers.Host;
 }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
      originsAllowed = origin;
 }

  return {
      statusCode: statusCode,
      'headers': {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer authToke',
          'Access-Control-Allow-Credentials': true,
          'Access-Control-Allow-Origin' : originsAllowed,
          'X-XSS-Protection': '1; mode=block',
          'X-Frame-Options': 'SAMEORIGIN',
          'X-Content-Type-Options': 'nosniff',
          'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

      },
      body: message
  };
}

module.exports.checkIn = function (event, context, callback) {
    globalEvent = event;
  //const id = event.pathParameters.Id;
  const requestBody = JSON.parse(event.body);
  if (!requestBody.Id) {
    let respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  let id = requestBody.Id;

  var totalParticipants = parseInt(requestBody.ActualNoOfAdults) + parseInt(requestBody.ActualNoOfChildren)
  
  let params = {
    TableName: process.env.BOOKING_TABLE,
    Key: {
      "Id": id
    },
  }
  //console.log(params)
  documentClient.get(params, function (err, data) {
    if (err) {
      return response(400, JSON.stringify(err));
    } else {
      if (data.Item) {
        const booking = data.Item;
        if (booking.BookingStatus == "Confirmed") {
          let params1 = {
            TableName: process.env.BOOKING_TABLE,
            Key: {
              "Id": id
            },
            UpdateExpression: 'set #BookingStatus = :S, #ActualNoOfAdults = :ACNA, #ActualNoOfChildren = :ACNC, #TotalNoOfParticipantVisited = :TN ',
            ExpressionAttributeValues: {
              ':S': "CheckedIn",
              ':ACNA':requestBody.ActualNoOfAdults,
              ':ACNC':requestBody.ActualNoOfChildren,
              ':TN':totalParticipants
            },
            ExpressionAttributeNames: {
              '#BookingStatus': 'BookingStatus',
              '#ActualNoOfAdults': 'ActualNoOfAdults',
              '#ActualNoOfChildren': 'ActualNoOfChildren',
              '#TotalNoOfParticipantVisited':'TotalNoOfParticipantVisited'
            },
            ReturnValues: "UPDATED_NEW"
          };
          documentClient.update(params1, function (err, data) {
            if (err) {
              return callback(err, response(400, err));
            }else{   
                    var ActualNoParticipantsVisited = parseInt(requestBody.ActualNoOfAdults) + parseInt(requestBody.ActualNoOfChildren);
                    var responseData = { "ActualNoOfParticipantsVisited" : ActualNoParticipantsVisited , "NoOfParticipantsForBooking" : booking.NoOfParticipants}
                    let respObj = { "message": "Booking Status has changed to - Checked-In", "responseObj" : responseData }
                    return callback(err, response(200, JSON.stringify(respObj))); 
            }  
            });
        }
        else {
          let respObj = { "message": "Given id Status is already updated with Check-in" }
          return callback(null, response(404, JSON.stringify(respObj)));
        }
      }
      else {
        let respObj = { "message": "No response found may be data not there" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}