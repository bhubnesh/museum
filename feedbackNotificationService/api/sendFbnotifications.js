'use strict';

const { SNS } = require('aws-sdk');
var AWS = require('aws-sdk');
var sns = new AWS.SNS();
const date = require('date-and-time')
var documentClient = new AWS.DynamoDB.DocumentClient({ convertEmptyValues: true });
var moment = require('moment');
var moment = require('moment-timezone');

const topicName = 'feedbackProdNotificationTopic';
var todaysDate = new Date();

var subscribeParam = {
  Protocol: 'application',
  TopicArn: 'topicArn',
  Endpoint: 'MOBILE_END_point'
}



let payload = {
  default: 'default',
  GCM: {
    notification: {
      title: 'Hey! We hoped you enjoyed your visit to the Mahindra Museum. If would mean a lot to us if you clicked here to give feedback!',
      body: 'Hey! We hoped you enjoyed your visit to the Mahindra Museum. If would mean a lot to us if you clicked here to give feedback!',
      sound: 'default'
    }
  },
  APNS: JSON.stringify({
    aps: {
      alert: {
        title: 'Hey! We hoped you enjoyed your visit to the Mahindra Museum. If would mean a lot to us if you clicked here to give feedback!',
        body: 'Hey! We hoped you enjoyed your visit to the Mahindra Museum. If would mean a lot to us if you clicked here to give feedback!',
      },
      sound: "default"

    }
  })
}
payload.GCM = JSON.stringify(payload.GCM);

payload = JSON.stringify(payload);

var params = {
  Message: payload,
  Subject: "M&M Museum Feedback!",
  //  Andriod
  TargetArn: "arn:aws:sns:ap-south-1:176378803823:endpoint/GCM/MyAndriodNotApp/7b7a0b5c-00c3-3173-82b8-9d28d66cf03d",

  MessageStructure: 'json'

};





module.exports.sendFbPush = async event => {
  var emailIds = {
    TableName: process.env.BOOKINGINFO_TABLE,
    ProjectionExpression: "Id,VisitorEmail,BookingStatus,SlotTime"

  };
  var emails = [];
  var items = [];
  // fetching users 
  var data = await documentClient.scan(emailIds).promise();

  console.log("todaysDate :: ", todaysDate)
  for (const element of data.Items) {
    console.log("element.SlotTime :: ", element.SlotTime);
    const mydate  = new Date();
   // Create a UTC date object. The moment constructor will recognize the date as UTC since it includes the 'Z' timezone specifier.
    let utcDate = moment(mydate);
   // Convert the UTC date into IST
   let istDate = moment(utcDate).tz("Asia/Kolkata");
    var slotTime = moment(element.SlotTime, ["h:mm A"]);
    var currentTime = moment(istDate).format('hh:mm A')
    console.log("currentTime",currentTime)
    // var slotEndTime = slotTime.add(45, 'mimutes').format('h:mm A');
    var slotEndTime = moment(slotTime).add(45, 'minutes').format('hh:mm A');
    console.log("slotEndTime",slotEndTime)
    console.log("slotTime : ", slotTime)
    if (element.BookingStatus == "CheckedIn" && currentTime > slotEndTime) {
      console.log("condition true!")

      emails = emails.concat(element.VisitorEmail);
      items = items.concat(element.Id);

      var changeStatusParam = {
        TableName: process.env.BOOKINGINFO_TABLE,

        Key: {
          Id: element.Id
        },
        UpdateExpression: "set BookingStatus = :BookingStatus",
        ExpressionAttributeValues: {
          ":BookingStatus": "Completed"
        },

      };
      //update in db
      documentClient.update(changeStatusParam, function (err, data) {
        if (err) { throw "ERROR in updating status for ", err }

      })
    }
  }
  var topicARN = await getTopics();
  console.log("Sending push to : ", emails);
  for (const email of emails) {

    //get tokens
    var tokens = {
      TableName: process.env.DEVICE_TOKENS,

      ProjectExpression: "Id,arn",
      KeyConditionExpression: "#Id = :email",
      // Filterexpression : "Id IN ("+Object.keys(emailObject).toString()+")",

      ExpressionAttributeNames: {

        "#Id": "Id"
      },

      ExpressionAttributeValues: {

        ":email": email,

      }
    }


    console.log(tokens);
    var res = await documentClient.query(tokens).promise();
    console.log("res :: ", res);
    subscribeParam.TopicArn = topicARN;
    subscribeParam.Endpoint = res.Items[0].ARN;
    // console.log("subscribeParam 1:: ",subscribeParam);

    //subcribe to topic
    console.log("subscribeParam :: ", subscribeParam);
    var subs = await sns.subscribe(subscribeParam).promise();
    console.log("Subscribe :: ", subs);


  }

  // if (data.LastEvaluatedKey) {

  //     emailIds.ExclusiveStartKey = data.LastEvaluatedKey;

  //      await fetchUsers();
  // } else {
  //     console.log("Dont have the LastEvaluatedKey")

  if (emails.length > 0) {
    console.log("Sending notification for : ", emails)
    await publishNotification(params);
  }


}
let respObj = { "message": "Notifiation Sent!!!" }
var err2 = "error"

//fetchUsers();
console.log("out of all function!!")
return "out of all function!!"
//return '200';




console.log("out of all loop!!")
//publich message to topic
async function publishNotification(params) {

  //params.TargetArn = await getTopics();
  console.log("params::::::: ", params)
  var res = await sns.publish(params).promise();
  console.log(res)
  var deleteParams = {
    TopicArn: params.TargetArn
  };
  await sns.deleteTopic(deleteParams).promise();
  var cretaeParams = {
    Name: topicName, /* required */

  };
  await sns.createTopic(cretaeParams).promise();

}
function response(statusCode, message) {
  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      'X-XSS-Protection': '1; mode=block',
      'x-frame-options': 'SAMEORIGIN',
      'x-content-type': 'nosniff',
    },
    body: message
  };
}

// Fetching topics
async function getTopics() {

  var lstTopicParams = {
    NextToken: ''
  };
  var topicArn = '';
  const result = await sns.listTopics(lstTopicParams).promise();
  console.log("topic::", result);           // successful response
  for (const topic of result.Topics) {
    if (topic.TopicArn.includes(topicName)) {

      subscribeParam.TopicArn = topic.TopicArn;
      topicArn = topic.TopicArn;
      params.TargetArn = topic.TopicArn;
      return topicArn;
    }
  }


  return topicArn;
}