'use strict';


var AWS = require('aws-sdk'),

  uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient();
const moment = require('moment');

module.exports.hitCount = async event => {
  var count = 0;
  var requestBody = JSON.parse(event.body);
  var date = new Date();
  //var todaysDate = moment(date).format('YYYY-MM-DD')
  var todaysDate = requestBody.recordDate;
  //var currentMonth = moment(date).format('YYYY-MM')
  var currentMonth = todaysDate.substring(0, 7)
  console.log("requestBody : ", requestBody);

  var params = {
    TableName: process.env.EXHIBITHITCOUNT_TABLE,
    Key: {
      "ExhibitId": requestBody.ExhibitId,
      "recordDate": todaysDate,

    }

  }

  var monthParams = {
    TableName: process.env.EXHIBITHITCOUNT_TABLE,
    Key: {
      "ExhibitId": requestBody.ExhibitId,
      "recordDate": currentMonth,

    }

  }
  console.log("params : ", params);
  // Check if record present
  var data = await documentClient.get(params).promise()
  var monthData = await documentClient.get(monthParams).promise()

  var element = data.Item;
  var monthElement = monthData.Item
  console.log("data ", data)
  // if yes update by +1
  if (typeof element != "undefined" && element != "{}") {

    console.log("inside update count")
    //count = element.Count + 1
    var updateParams = {
      TableName: process.env.EXHIBITHITCOUNT_TABLE,
      Key: {
        "ExhibitId": element.ExhibitId,
        "recordDate": element.recordDate,
      },
      UpdateExpression: "set hitCount=hitCount+:val",
      ExpressionAttributeValues: {
        ":val": 1,
      },
      ReturnValues: "UPDATED_NEW"
    }
    console.log(updateParams)
    var res = await documentClient.update(updateParams).promise();
    console.log(" IN UPDATING ", res)

    //return res ;

  } else {
    // if no, insert
    count = 1;
    var updateParams = {
      TableName: process.env.EXHIBITHITCOUNT_TABLE,
      Item: {
        "ExhibitId": requestBody.ExhibitId,
        "recordDate": todaysDate,
        "hitCount": count

      }
    }
    var respObj = { "message": "Exhibit for day added successfully..!" }
    var data = await documentClient.put(updateParams, respObj).promise()


  }

  if (typeof monthElement != "undefined" && monthElement != "{}") {

    console.log("inside update monthly count")
    //count = element.Count + 1
    var updateParams = {
      TableName: process.env.EXHIBITHITCOUNT_TABLE,
      Key: {
        "ExhibitId": monthElement.ExhibitId,
        "recordDate": monthElement.recordDate,
      },
      UpdateExpression: "set hitCount=hitCount+:val",
      ExpressionAttributeValues: {
        ":val": 1,
      },
      /// ReturnValues: "UPDATED_NEW"
    }
    console.log(updateParams)
    var res = await documentClient.update(updateParams).promise();
    console.log(" IN UPDATING monthly", res)

    //return res ;

  } else {
    // if no, insert
    count = 1;
    var updateParams = {
      TableName: process.env.EXHIBITHITCOUNT_TABLE,
      Item: {
        "ExhibitId": requestBody.ExhibitId,
        "recordDate": currentMonth,
        "hitCount": count

      }
    }
    var respObj = { "message": "Monthly Exhibit added successfully..!" }
    var data = await documentClient.put(updateParams, respObj).promise()


  }
  return data;
};
