'use strict';

const axios = require('axios');
const https = require('https');
module.exports = {

    sendSMSNotification: function (toMobile, text, typeOfTemplate) {

        const smsNotification_URL = "https://nzejp5.api.infobip.com/sms/2/text/advanced";
        var requestObj = {
            "messages": [
                {
                    "from": "MNMSMS",
                    "destinations": toMobile,
                    "text": text,
                    "regional": {
                        "indiaDlt": {
                            "contentTemplateId": typeOfTemplate,
                            "principalEntityId": "1201158047830698601"
                        }
                    }
                }
            ]
        }
        
        const agent = new https.Agent({
           
            rejectUnauthorized: false
            
        });
        const options = {
            url: smsNotification_URL,
            method: "POST",
            httpsAgent : agent,
            headers: {
                'Authorization': "App 8125468595bac54ad77bfa7d835caab6-e447a354-5483-4705-95e2-42489fb2141f",
                'Content-Type': 'application/json'
            },
            data: requestObj
        };
        /*axios.post(smsNotification_URL, (requestObj), {
            headers: {
                'Authorization': "App 8125468595bac54ad77bfa7d835caab6-e447a354-5483-4705-95e2-42489fb2141f"
            }
        })*/
       axios(options)    
        .then(resp => {
            console.log("Response of SMS API ---- " + JSON.stringify(resp.data));
            return ({ "statuscode": 200, "message": "SMS notification sent successfully" });

        }).catch((error) => {
            console.error(error);
            //return ({ "statuscode": 404, "message": "error occurred while sending SMS notification", "responseObj": JSON.stringify(error) });
        })


    },

    sendEmailNotification: function (toEmail, subject, text) {

    }
}
