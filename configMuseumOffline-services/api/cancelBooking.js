'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
const notification = require('../notifications/sendNotification');
const eventBridge = new AWS.EventBridge({ region: 'ap-south-1' });
const moment = require('moment')

var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}

module.exports.cancelBookingDetails = function (event, context, callback) {
    debugger;
    globalEvent = event;
    const requestBody = JSON.parse(event.body);
    //var validateToken = auth.validateToken(event, context, callback);
    // if (validateToken.statusCode != 200) {
    //     return callback(null, response(validateToken.statusCode, JSON.stringify(validateToken)));
    // }
    const Id = event.pathParameters.Id;
    if (!requestBody.VisitorEmail || !Id) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        console.log(JSON.stringify(respObj));

    }

    //-- Check whether booking data present for given Id
    var searchWithId = {
        TableName: process.env.BOOKING_TABLE,
        ExpressionAttributeValues: {
            ":id": Id
        },

        KeyConditionExpression: "Id = :id"
    }

    console.log("Search Params ..." + JSON.stringify(searchWithId));

    documentClient.query(searchWithId, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
            // console.log("GetItem succeeded:", JSON.stringify(data));
            //   console.log("count of records fro email ... " + data.Count)
            if (data.Count > 0) {
                var oldBookingData = data.Items[0];
                var email = oldBookingData.VisitorEmail.toLowerCase();

                //--- Changes to be done with old date's slots counts
                var searchWithOldDate = {
                    TableName: process.env.BOOKINGDATE_TABLE,
                    ExpressionAttributeValues: {
                        ":date": oldBookingData.BookingDate
                    },

                    KeyConditionExpression: "BookingDate = :date"
                }

                console.log("Search Params ..." + JSON.stringify(searchWithOldDate));

                documentClient.query(searchWithOldDate, function (err, data) {
                    if (err) {
                        console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                        return response(400, JSON.stringify(err));
                    } else {
                        console.log("count of records  ... " + data.Count)
                        if (data.Count > 0) {
                            var bookingData = data.Items[0];
                            var slotArray = [];
                            slotArray = data.Items[0].SlotsDetails;
                            console.log("Slot time ... " + oldBookingData.SlotTime);
                            var count = slotArray.length;


                            slotArray.forEach(slot => {
                                if (slot.slot === oldBookingData.SlotTime) {
                                    slot.bookingsAvailable = slot.bookingsAvailable + (oldBookingData.NoOfChildren + oldBookingData.NoOfAdults);
                                    slot.bookingsDone = slot.bookingsDone - (oldBookingData.NoOfChildren + oldBookingData.NoOfAdults);
                                    slot.WCAvailable = slot.WCAvailable + oldBookingData.WCRequiredCount;
                                    slot.WCBooked = slot.WCBooked - oldBookingData.WCRequiredCount;
                                    slot.HIAvailable = slot.HIAvailable + oldBookingData.HIHelp;
                                    slot.HIBooked = slot.HIBooked - oldBookingData.HIHelp;
                                    slot.VIAvailable = slot.VIAvailable + oldBookingData.VIRequiredCount;
                                    slot.VIBooked = slot.VIBooked - oldBookingData.VIRequiredCount;
                                }
                                count--;
                            })
                            if (count == 0) {
                                bookingData.SlotsDetails = slotArray;
                                //-- Update booking info from bookingDate table

                                var params = {
                                    TableName: process.env.BOOKINGDATE_TABLE,
                                    Key: {
                                        "BookingDate": bookingData.BookingDate
                                    },
                                    UpdateExpression: 'set #SlotsDetails = :SD,#UpdatedBy = :U,#UpdatedAt = :UA, #BookingStatus = :BS',
                                    ExpressionAttributeValues: {
                                        ':SD': slotArray,
                                        ':BS': "Cancelled",
                                        ':U': requestBody.VisitorEmail,
                                        ':UA': new Date().toISOString()
                                    },
                                    ExpressionAttributeNames: {
                                        '#SlotsDetails': 'SlotsDetails',
                                        '#BookingStatus': 'BookingStatus',
                                        '#UpdatedBy': 'UpdatedBy',
                                        "#UpdatedAt": "UpdatedAt"

                                    },
                                    ReturnValues: "UPDATED_NEW"
                                };
                                console.log("params.. " + JSON.stringify(params));
                                documentClient.update(params, function (err, data) {
                                    if (err) {
                                        console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                                        return callback(err, response(400, err));
                                    }
                                    console.log("Update response.. " + JSON.stringify(data));
                                    //-- Check whether bookings vailable for new date and slot
                                    updateBookingInfo(event, callback, email, oldBookingData);
                                })
                            }


                        } else {
                            console.log("Slot details are not available for given date.");
                            var respObj = { "message": "Slot details are not available for given date." };
                            return callback(null, response(404, JSON.stringify(respObj)));
                        }


                    }
                })
            } else {
                console.log("Bookings not available for given ID");
                var respObj = { "message": "Bookings not available for given ID" };
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })
}


function updateBookingInfo(event, callback, email, oldBookingData) {
    var cancelledBookingIds = []
    var requestBody = JSON.parse(event.body);
    const Id = event.pathParameters.Id;
    //-- update details into booking table
    var updateBookingData = {
        TableName: process.env.BOOKING_TABLE,
        Key: {
            "Id": Id
        },
        UpdateExpression: 'set #BookingCancelled = :BC, #CancelledBy = :CB, #UpdatedBy = :U,#UpdatedAt = :UA, #BookingStatus = :BS',
        ExpressionAttributeValues: {
            ":BC": true,
            ":CB": requestBody.VisitorEmail,
            ':U': requestBody.VisitorEmail,
            ':BS': "Cancelled",
            ':UA': new Date().toISOString()
        },
        ExpressionAttributeNames: {
            "#BookingCancelled": "BookingCancelled",
            '#BookingStatus': 'BookingStatus',
            "#CancelledBy": "CancelledBy",
            '#UpdatedBy': 'UpdatedBy',
            "#UpdatedAt": "UpdatedAt"
        },
        ReturnValues: "UPDATED_NEW"
    }
    console.log("Booking details to update... " + JSON.stringify(updateBookingData));

    documentClient.update(updateBookingData, function (err, data) {
        if (err) {
            return callback(err, response(400, err));
        }

        //-- Update booking History
        //updateBookingHistory(event, callback,email);
        console.log("Booking details updated successfully." + JSON.stringify(data));


        //-- Call SMS notification API
        var phone = (oldBookingData.CountryCode) + (oldBookingData.VisitorPhone);
        var countryCodeSign = phone.substring(0, 1);
        if (countryCodeSign === "+") {
            var modifiedPhone = phone.substring(1);
        } else {
            var modifiedPhone = phone;
        }
        //sms add
        var smsText = "Please note that your visit to Mahindra Museum on " + moment(oldBookingData.BookingDate).format("DD MMM YYYY") + " is cancelled. Please use our app to book your visit again. -M & M Ltd."
        var typeOfTemplate = '1107162712334634754';
        var smsNotification = notification.sendSMSNotification(modifiedPhone, smsText, typeOfTemplate);

        // Push Notification
        cancelledBookingIds.push(Id);
        var payload = {
            cancelledBookingIds: cancelledBookingIds,
            DetailType: 'cancle Booking',
            Detail: `cancleBooking`,

        }
        eventBridge.putEvents({
            Entries: [
                {
                    Detail: JSON.stringify({ name: payload }),
                    DetailType: 'Trigger custom push notification function',
                    EventBusName: 'mnm-museum-event-bus1',
                    Source: 'new.push',
                },
            ]
        }, function (err, data) {
            if (err) console.log("ERROR in putting event on eventbridge  ", err, err.stack); // an error occurred
            else console.log("Event put on eventBridge  ", payload);           // successful response
        });

        console.log("Booking details updated successfully." + JSON.stringify(data));
        var respObj = { "message": "Booking cancelled successfully." }
        return callback(null, response(200, JSON.stringify(respObj)));

    })




}

// function updateBookingHistory(event, callback, email) {

//     var requestBody = JSON.parse(event.body);
//     const Id = event.pathParameters.Id;

//     var searchWithEmail = {
//         TableName: process.env.BOOKINGINFO_TABLE,
//         ExpressionAttributeValues: {
//             ":vEmail": email
//         },

//         KeyConditionExpression: "VisitorEmail = :vEmail"
//     }

//     console.log("Search Params ..." + JSON.stringify(searchWithEmail));

//     documentClient.query(searchWithEmail, function (err, data) {
//         if (err) {
//             console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
//             return response(400, JSON.stringify(err));
//         } else {
//             // console.log("GetItem succeeded:", JSON.stringify(data));
//             //   console.log("count of records fro email ... " + data.Count)
//             //bookingData.Item.QRCode = "";
//             if (data.Count == 0) {


//                 var respObj = { "message": "Booking details are not available for given emailId" }
//                 return callback(null, response(404, JSON.stringify(respObj)));


//             } else {

//                 var previousData = data.Items[0];
//                 //console.log("bookingArray... " + bookingArray.length);
//                 var bookingArray = previousData.BookingData;
//                 //bookingArray.push(bookingData.Item);

//                 var bookingcount = bookingArray.length;
//                 console.log("Id.... " + Id);
//                 bookingArray.forEach(data => {
//                     console.log("BookingId.. " + data.Id);
//                     if (data.Id === Id) {
//                         data.BookingCancelled = true;
//                         data.BookingStatus = "Cancelled";

//                         var updateParams = {
//                             TableName: process.env.BOOKINGINFO_TABLE,
//                             Key: {
//                                 "VisitorEmail": email
//                             },
//                             UpdateExpression: 'set #BookingData = :BD,#UpdatedBy = :U,#UpdatedAt = :UA',
//                             ExpressionAttributeValues: {
//                                 ':BD': bookingArray,
//                                 ':U': requestBody.VisitorEmail,
//                                 ':UA': new Date().toISOString()
//                             },
//                             ExpressionAttributeNames: {
//                                 '#BookingData': 'BookingData',
//                                 '#UpdatedBy': 'UpdatedBy',
//                                 "#UpdatedAt": "UpdatedAt"

//                             },
//                             ReturnValues: "UPDATED_NEW"
//                         };
//                         //console.log("updateParams.. " + JSON.stringify(updateParams));
//                         documentClient.update(updateParams, function (err, data) {
//                             if (err) {
//                                 console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
//                                 return callback(err, response(400, err));
//                             }
//                             // console.log("Update response.. " + JSON.stringify(data));
//                             var respObj = { "message": "Booking cancelled successfully." }
//                             return callback(null, response(200, JSON.stringify(respObj)));
//                         })


//                     } else {
//                         bookingcount--;
//                     }

//                     if (bookingcount == 0) {
//                         console.log("Booking Id not found in Booking Info table");
//                         var respObj = { "message": "Booking Id not found in Booking Info table" }
//                         return callback(null, response(404, JSON.stringify(respObj)));
//                     }
//                 })

//             }

//         }
//     })
// }

//-- Delete multiple bookings

module.exports.cancelMultipleBookingDetails = async function (listOfBookingIds, email, callback) {

    //-- Validate JWT token
    // var validateToken = auth.validateToken(event, context, callback);
    var cancelledBookingIds = [];
    // console.log("token inside ... " + validateToken);
    // if (validateToken.statusCode != 200) {
    //     return callback(null, response(validateToken.statusCode, JSON.stringify(validateToken)));
    // }
    // const email = (event.pathParameters.VisitorEmail).toLowerCase();
    // const requestBody = JSON.parse(event.body);
    // console.log("Req body .. " + JSON.stringify(requestBody))
    if (!listOfBookingIds || !email) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        console.log(respObj)
    }
    // var listOfBookingIds = requestBody.listOfBookingIds;
    console.log("size of List  .. " + listOfBookingIds.length);
    var bookingcount = listOfBookingIds.length;
    if (listOfBookingIds.length > 0) {

        for (const booking of listOfBookingIds) {
            console.log("booking ", JSON.stringify(booking));
            var Id = booking;

            //-- Check whether booking data present for given Id
            var searchWithId = {
                TableName: process.env.BOOKINGINFO_TABLE,
                ExpressionAttributeValues: {
                    ":id": Id
                },

                KeyConditionExpression: "Id = :id"
            }

            console.log("Search Params ..." + JSON.stringify(searchWithId));

            var data = await documentClient.query(searchWithId).promise()


            if (data.Count > 0) {
                var oldBookingData = data.Items[0];

                if (oldBookingData.TypeOfBooking == "Normal") {
                    //-- update booking details, set BookingCancelled = true

                    var updateBookingData = {
                        TableName: process.env.BOOKINGINFO_TABLE,
                        Key: {
                            "Id": Id
                        },
                        UpdateExpression: 'set #BookingCancelled = :BC, #BookingStatus = :BS, #CancelledBy = :CB, #UpdatedBy = :U,#UpdatedAt = :UA',
                        ExpressionAttributeValues: {
                            ":BC": true,
                            ":BS": "Cancelled",
                            ":CB": oldBookingData.VisitorEmail,
                            ':U': oldBookingData.VisitorEmail,
                            ':UA': new Date().toISOString()
                        },
                        ExpressionAttributeNames: {
                            "#BookingCancelled": "BookingCancelled",
                            "#BookingStatus": "BookingStatus",
                            "#CancelledBy": "CancelledBy",
                            '#UpdatedBy': 'UpdatedBy',
                            "#UpdatedAt": "UpdatedAt"
                        },
                        ReturnValues: "UPDATED_NEW"
                    }
                    // console.log("Booking details to update... " + JSON.stringify(updateBookingData));

                    var data = await documentClient.update(updateBookingData).promise();
                    //-- Call API to genretae QR code and update the same into a booking table
                    console.log("Booking details updated successfully for Id  - > " + Id);
                    //-- Call SMS Notification.
                    //-- booking type and visitorPhone

                    var phone = oldBookingData.CountryCode + oldBookingData.VisitorPhone;
                    var modifiedPhone = '';

                    var countryCodeSign = phone.substring(0, 1);
                    if (countryCodeSign === "+") {

                        modifiedPhone = { "to": phone.substring(1) };
                    } else {
                        modifiedPhone = { "to": phone };
                    }
                    var bookingType = oldBookingData.TypeOfBooking;

                    if (bookingType == "Event") {
                        var smsText = "We regret to inform you that due to unforeseen circumstances your visit to Mahindra Museum on " + oldBookingData.BookingDate + " for the event " + oldBookingData.EventName + "is cancelled. We regret inconvenience caused -M & M Ltd."
                        var templateId = '1107162712348946217';
                    } else {
                        var smsText = "Due to unforeseen circumstances your visit to Mahindra Museum on " + oldBookingData.BookingDate + " is cancelled. We regret inconvenience caused -M & M Ltd."
                        var templateId = '1107162712328278696';
                    }
                    //--- CAll SMS notification
                    //sending SMS
                    var smsNotification = notification.sendSMSNotification(modifiedPhone, smsText, templateId);
                    bookingcount--;
                    cancelledBookingIds.push(Id);
                    console.log("bookingcount  -- ", bookingcount);
                    if (bookingcount == 0) {
                        //-- Call function to update history table for multiple delete
                        //  updateBookingHistoryForMultiple(event, callback);

                        var respObj = { "message": "Bookings with given ids deleted successfully.. !" }

                        // Sending push Notification
                        console.log("putting eventbridge data.........")
                        var payload = {
                            cancelledBookingIds: cancelledBookingIds,
                            DetailType: 'cancle Booking',
                            Detail: `cancelBooking`,

                        }
                        eventBridge.putEvents({
                            Entries: [
                                {
                                    Detail: JSON.stringify({ name: payload }),
                                    DetailType: 'Trigger custom push notification function',
                                    EventBusName: 'mnm-museum-event-bus1',
                                    Source: 'new.push',
                                },
                            ]
                        }, function (err, data) {
                            if (err) console.log("ERROR in putting event on eventbridge  ", err, err.stack); // an error occurred
                            else console.log("Event put on eventBridge  ", payload);           // successful response
                        });
                        console.log(JSON.stringify(respObj));

                    }




                } else {
                    console.log("Booking Type of given Id is NOT Normal");
                    var respObj = { "message": "Booking Type of given Id is NOT Normal" };
                    //return respObj;
                }
            } else {
                console.log("Bookings not available for given ID");
                var respObj = { "message": "Bookings not available for given ID" };
                // return respObj;
            }


        }

    } else {
        var respObj = { "message": "list of booking ids to be deleted not found." }
        console.log(JSON.stringify(respObj));
        return respObj;
    }


    function updateBookingHistoryForMultiple(event, callback) {
        const email = (event.pathParameters.VisitorEmail).toLowerCase();
        const requestBody = JSON.parse(event.body);
        console.log("Req body .. " + JSON.stringify(requestBody))
        if (!requestBody.listOfBookingIds || !email) {
            var respObj = { "message": "Manadatory input parameters are missing." }
            return callback(
                null,
                response(400, JSON.stringify(respObj))
            );
        }
        var listOfBookingIds = requestBody.listOfBookingIds;
        console.log("size of List  .. " + listOfBookingIds.length);
        var count = listOfBookingIds.length;
        if (listOfBookingIds.length > 0) {

            listOfBookingIds.forEach(booking => {
                var Id = booking.id;

                //-- Check whether booking data present for given Id
                var searchWithId = {
                    TableName: process.env.BOOKING_TABLE,
                    ExpressionAttributeValues: {
                        ":id": Id
                    },

                    KeyConditionExpression: "Id = :id"
                }

                console.log("Search Params ..." + JSON.stringify(searchWithId));

                documentClient.query(searchWithId, function (err, data) {
                    if (err) {
                        console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                        return response(400, JSON.stringify(err));
                    } else {

                        if (data.Count > 0) {
                            var oldBookingData = data.Items[0];
                            var visitorEmail = oldBookingData.VisitorEmail;


                            var searchWithEmail = {
                                TableName: process.env.BOOKINGINFO_TABLE,
                                ExpressionAttributeValues: {
                                    ":vEmail": visitorEmail.toLowerCase()
                                },

                                KeyConditionExpression: "VisitorEmail = :vEmail"
                            }

                            console.log("Search Params ..." + JSON.stringify(searchWithEmail));

                            documentClient.query(searchWithEmail, function (err, data) {
                                if (err) {
                                    console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                                    return response(400, JSON.stringify(err));
                                } else {
                                    // console.log("GetItem succeeded:", JSON.stringify(data));
                                    //   console.log("count of records fro email ... " + data.Count)
                                    //bookingData.Item.QRCode = "";
                                    if (data.Count == 0) {
                                        var respObj = { "message": "Booking details are not available for given emailId" }
                                        return callback(null, response(404, JSON.stringify(respObj)));
                                    } else {

                                        var previousData = data.Items[0];
                                        var bookingArray = previousData.BookingData;
                                        var bookingcount = bookingArray.length;
                                        console.log("Id.... " + Id);
                                        bookingArray.forEach(data => {
                                            console.log("BookingId.. " + data.Id);
                                            if (data.Id === Id) {
                                                data.BookingCancelled = true;
                                                data.BookingStatus = "Cancelled";
                                                var updateParams = {
                                                    TableName: process.env.BOOKINGINFO_TABLE,
                                                    Key: {
                                                        "VisitorEmail": visitorEmail.toLowerCase()
                                                    },
                                                    UpdateExpression: 'set #BookingData = :BD,#UpdatedBy = :U,#UpdatedAt = :UA',
                                                    ExpressionAttributeValues: {
                                                        ':BD': bookingArray,
                                                        ':U': email,
                                                        ':UA': new Date().toISOString()
                                                    },
                                                    ExpressionAttributeNames: {
                                                        '#BookingData': 'BookingData',
                                                        '#UpdatedBy': 'UpdatedBy',
                                                        "#UpdatedAt": "UpdatedAt"

                                                    },
                                                    ReturnValues: "UPDATED_NEW"
                                                };
                                                //console.log("updateParams.. " + JSON.stringify(updateParams));
                                                documentClient.update(updateParams, function (err, data) {
                                                    if (err) {
                                                        console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                                                        return callback(err, response(400, err));
                                                    }
                                                    // console.log("Update response.. " + JSON.stringify(data));
                                                    console.log("***** BokkingId => " + Id + " updated successfully....");
                                                    count--;
                                                    if (count == 0) {
                                                        var respObj = { "message": "Bookings with given ids deleted successfully.. !" }
                                                        return callback(err, response(200, JSON.stringify(respObj)));
                                                    }

                                                })


                                            } else {
                                                bookingcount--;
                                            }

                                            if (bookingcount == 0) {
                                                console.log("Booking Id not found in Booking Info table");
                                                var respObj = { "message": "Booking Id not found in Booking Info table" }
                                                return callback(null, response(404, JSON.stringify(respObj)));
                                            }
                                        })

                                    }

                                }
                            })

                        }
                    }
                })

            })
        }

    }
    return respObj;
}