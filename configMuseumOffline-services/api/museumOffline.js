'use strict';

var AWS = require('aws-sdk'),

  uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient(),
  moment = require('moment');
const date = require('date-and-time')
const auth = require('../auth/token');
var cancleBooking = require('../api/cancelBooking');
const notification = require('../notifications/sendNotification');

const eventBridge = new AWS.EventBridge({ region: 'ap-south-1' });
var dateFormat = ["YYYY-MM-DD HH:mm", "YYYY-MM-DD HH:m", "YYYY-MM-DD H:mm", "YYYY-MM-DD H:m",
  "YYYY-MM-D HH:mm", "YYYY-MM-D HH:m", "YYYY-MM-D H:mm", "YYYY-MM-D H:m",
  "YYYY-M-DD HH:mm", "YYYY-M-DD HH:m", "YYYY-M-DD H:mm", "YYYY-M-DD H:m",
  "YYYY-M-D HH:mm", "YYYY-M-D HH:m", "YYYY-M-D H:mm", "YYYY-M-D H:m",
]

var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}

module.exports.addMuseumOffline = async function (event, context, callback) {
  console.log("Inside add museum..");
  globalEvent = event;
  var Id = "";
  const requestBody = JSON.parse(event.body);
  var loggedInUser = "";
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    const requestBody = JSON.parse(event.body);
    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();

  loggedInUser = event.loggedInUser;

  if (requestBody.FullDay) {
    requestBody.TimeHrFrom = "06"
    requestBody.TimeHrTo = "11"
    requestBody.TimeMinFrom = "00"
    requestBody.TimeMinTo = "00"
    requestBody.TimeTypeFrom = "am"
    requestBody.TimeTypeTo = "pm"
  }

  // Check if offline data already present with same date and time

  // var currentOfflineData = await getFormattedToAndFrom(requestBody);
  //  var currentOfflineFrom = currentOfflineData.offlineFrom
  // var currentOfflineTo = currentOfflineData.offlineTo

  var allMuseumOfflineData = await getAllOfflineData();

  var currentOfflineFrom = moment(requestBody.DateFrom.substring(0, 10))
  currentOfflineFrom = currentOfflineFrom.toDate();
  var currentOfflineTo = moment(requestBody.DateTo.substring(0, 10))
  currentOfflineTo = currentOfflineTo.toDate();

  //console.log("currentOfflineData ", currentOfflineData);
  var respObj = "";
  for (const museumOffline of allMuseumOfflineData.responseObj.Items) {

    // var existingOfflineData = await getFormattedToAndFrom(museumOffline);
    // var existingofflineFrom = existingOfflineData.offlineFrom
    // var existingofflineTo = existingOfflineData.offlineTo
    var existingofflineFrom = moment(museumOffline.DateFrom.substring(0, 10))
    existingofflineFrom = existingofflineFrom.toDate();
    var existingofflineTo = moment(museumOffline.DateTo.substring(0, 10))
    existingofflineTo = existingofflineTo.toDate();
    // console.log(currentOfflineFrom + " > " + (existingofflineFrom))

    // console.log(currentOfflineFrom > (existingofflineFrom))
    // console.log(currentOfflineFrom + " < " + (existingofflineTo))
    // console.log(currentOfflineFrom < (existingofflineTo))

    // console.log("---------------To Time Check--------")
    // console.log(currentOfflineTo + " > " + (existingofflineFrom))
    // console.log(currentOfflineTo > (existingofflineFrom))
    // console.log(currentOfflineTo + " < " + (existingofflineTo))
    // console.log(currentOfflineTo < (existingofflineTo))

    if (currentOfflineFrom >= (existingofflineFrom) && currentOfflineTo <= (existingofflineTo)) {
      respObj = { responseObj: "Museum is already offline for this period" }
      // console.log(respObj)
      return callback(null, response(302, JSON.stringify(respObj)));

    }
    else if (currentOfflineTo >= (existingofflineFrom) && currentOfflineTo <= (existingofflineTo)) {
      respObj = { responseObj: "Museum is already offline for this period" }
      console.log(respObj)
      return callback(null, response(302, JSON.stringify(respObj)));
    } else if (currentOfflineFrom >= (existingofflineFrom) && currentOfflineFrom <= (existingofflineTo)) {
      respObj = { responseObj: "Museum is already offline for this period" }
      console.log(respObj)
      return callback(null, response(302, JSON.stringify(respObj)));
    } else if (currentOfflineFrom <= existingofflineFrom && currentOfflineTo >= existingofflineTo) {
      respObj = { responseObj: "Museum is already offline for some part of this period. Do you want to overwrite?" }
      console.log(respObj)
      if (!requestBody.updateMuseumOffline) {

        return callback(null, response(301, JSON.stringify(respObj)));
      } else {
        Id = museumOffline.Id
      }
    }
  }

  //-- Get user details from email Id 
  var searchWithEmail = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": loggedInUser
    },
    KeyConditionExpression: "Email = :email"
  }
  var data = await documentClient.query(searchWithEmail).promise()
  if (data.Count > 0) {
    if (!requestBody.updateMuseumOffline) {
      Id = uuid.v1();
    }
    var userDetails = data.Items[0];

    var params = {
      Item: {
        "Id": Id,
        "DateFrom": requestBody.DateFrom,
        "DateTo": requestBody.DateTo,
        "FullDay": requestBody.FullDay,
        "SentAlert": requestBody.SentAlert,
        "Description": requestBody.Description,
        "PushNotification": requestBody.PushNotification,
        "PushDescription": requestBody.PushDescription,
        "SMS": requestBody.SMS,
        "SMSDescription": requestBody.SMSDescription,
        "Email": requestBody.Email,
        "EmailDescription": requestBody.EmailDescription,
        "TimeHrFrom": requestBody.TimeHrFrom,
        "TimeHrTo": requestBody.TimeHrTo,
        "TimeMinFrom": requestBody.TimeMinFrom,
        "TimeMinTo": requestBody.TimeMinTo,
        "TimeTypeFrom": requestBody.TimeTypeFrom,
        "TimeTypeTo": requestBody.TimeTypeTo,
        "CreatedBy": userDetails.Name,
        "UpdatedBy": userDetails.Name,
        "CreatedAt": new Date().toISOString(),
        "UpdatedAt": new Date().toISOString()
      },
      TableName: process.env.MUSEUMOFFLINE_TABLE
    };
    respObj = { "responseObj": data.Attributes, "message": "Museum Offline created successfully..!" }

    var data = await documentClient.put(params).promise();
    //console.log("updateData ",data)

    //}
    var bookingIds = await getUsersDetailsAndCancleBooking(requestBody, loggedInUser, callback)
    return callback(null, response(200, JSON.stringify(respObj)));


  } else {
    return ({ "statusCode": 404, "userData": {}, "message": "User details not found." })
  }

}
//-- Delete MuseumOffline code 

module.exports.deleteMuseumOffline = function (event, context, callback) {
  globalEvent = event;
  const id = event.pathParameters.Id;

  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params = {
    TableName: process.env.MUSEUMOFFLINE_TABLE,
    Key: {
      "Id": id
    }
  };
  documentClient.delete(params, function (err, data) {
    if (err) {
      return callback(err, response(400, err));
    }

    var respObj = { "message": "Museum Offline Deleted successfully..!" }
    return callback(err, response(200, JSON.stringify(respObj)));
  });
}
//multiple delete
module.exports.multiDeleteMuseumOffline = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);
  var responseObj = {}
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  if (!requestBody.museumId) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //get through requestBody
  var museumId = requestBody.museumId;
  if (museumId != null) {
    let cnt = 0;
    museumId.forEach(museumOffline => {
      cnt++;
      var id = museumOffline.id;
      var params = {
        TableName: process.env.MUSEUMOFFLINE_TABLE,
        Key: {
          "Id": id
        }
      };
      documentClient.get(params, function (err, data) {
        if (err) {
          responseObj[id] = err;
        }
        else if (data && Object.keys(data).length === 0 && data.constructor === Object) {
          responseObj[id] = 'ID not found';
        }
        else {
          var deleteParams = {
            TableName: process.env.MUSEUMOFFLINE_TABLE,
            Key: {
              "Id": id
            }
          };
          documentClient.delete(deleteParams, function (err, data) {
            if (err) {
              responseObj[id] = err;
            }
            responseObj[id] = 'ID Deleted';
          });
        }
        if (museumId.length == cnt) {
          setTimeout(() => {
            return callback(err, response(200, JSON.stringify([responseObj])));
          }, 2000);
        }
      })
    });
  } else {
    var respObj = { "message": "Id was provided incorrect" }
    return callback(err, response(err, JSON.stringify(respObj)));
  }
}
// update the MuseumOffline Details
module.exports.updateMuseumOffline = function (event, context, callback) {
  globalEvent = event;

  const id = event.pathParameters.Id;
  const requestBody = JSON.parse(event.body);

  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();

  if (!id) {
    var respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params1 = {
    TableName: process.env.MUSEUMOFFLINE_TABLE,
    Key: {
      "Id": id
    },
  };
  documentClient.get(params1, function (err, data) {
    if (err) {
      return response(400, JSON.stringify(err));
    } else {
      if (data.Item) {
        const museumData = data.Item;
        //-- Get currently logged in user info
        var loggedInUser = validateTokenResp.event.loggedInUser;
        //-- Get user details from email Id 
        var searchWithEmail = {
          TableName: process.env.VISITOR_TABLE,
          ExpressionAttributeValues: {
            ":email": loggedInUser
          },
          KeyConditionExpression: "Email = :email"
        }
        documentClient.query(searchWithEmail, function (err, data) {
          if (err) {
            return ({ "statusCode": 400, "message": JSON.stringify(err) });
          } else {
            if (data.Count > 0) {
              const userDetails = data.Items[0];
              var params = {
                TableName: process.env.MUSEUMOFFLINE_TABLE,
                Key: {
                  "Id": id
                },

                UpdateExpression: 'set #Description = :D,#PushDescription = :PD,#SMSDescription = :SMSD,#EmailDescription = :ED,#TimeHrFrom = :H,#TimeHrTo = :M,#TimeMinFrom = :I,#TimeMinTo = :E,#TimeTypeFrom = :P,#TimeTypeTo = :A,#DateFrom = :F,#DateTo = :T,#FullDay = :U,#SentAlert = :N,#PushNotification = :PN,#SMS = :SMS,#Email = :EM,#UpdatedBy = :U,#UpdatedAt = :UA',
                ExpressionAttributeValues: {
                  ':D': requestBody.Description,
                  ':PD': requestBody.PushDescription,
                  ':SMSD': requestBody.SMSDescription,
                  ':ED': requestBody.EmailDescription,
                  ':F': requestBody.DateFrom,
                  ':T': requestBody.DateTo,
                  ':U': requestBody.FullDay,
                  ':N': requestBody.SentAlert,
                  ':PN': requestBody.PushNotification,
                  ':SMS': requestBody.SMS,
                  ':EM': requestBody.Email,
                  ':H': requestBody.TimeHrFrom,
                  ':M': requestBody.TimeHrTo,
                  ':I': requestBody.TimeMinFrom,
                  ':E': requestBody.TimeMinTo,
                  ':P': requestBody.TimeTypeFrom,
                  ':A': requestBody.TimeTypeTo,
                  ':U': userDetails.Name,
                  ':UA': new Date().toISOString()
                },
                ExpressionAttributeNames: {
                  '#Description': 'Description',
                  '#PushDescription': 'PushDescription',
                  '#SMSDescription': 'SMSDescription',
                  '#EmailDescription': 'EmailDescription',
                  '#DateFrom': 'DateFrom',
                  '#DateTo': 'DateTo',
                  '#FullDay': 'FullDay',
                  '#SentAlert': 'SentAlert',
                  '#PushNotification': 'PushNotification',
                  '#SMS': 'SMS',
                  '#Email': 'Email',
                  '#TimeHrFrom': 'TimeHrFrom',
                  '#TimeHrTo': 'TimeHrTo',
                  '#TimeMinFrom': 'TimeMinFrom',
                  '#TimeMinTo': 'TimeMinTo',
                  '#TimeTypeFrom': 'TimeTypeFrom',
                  '#TimeTypeTo': 'TimeTypeTo',
                  '#UpdatedBy': 'UpdatedBy',
                  "#UpdatedAt": "UpdatedAt"
                },
                ReturnValues: "UPDATED_NEW"
              };

              documentClient.update(params, function (err, data) {
                if (err) {
                  return callback(err, response(400, err));
                }
                const responseObj = {
                  "Id": museumData.Id,
                  "DateFrom": requestBody.DateFrom,
                  "DateTo": requestBody.DateTo,
                  "FullDay": requestBody.FullDay,
                  "Description": requestBody.Description,
                  "SentAlert": requestBody.SentAlert,
                  "PushNotification": requestBody.PushNotification,
                  "PushDescription": requestBody.PushDescription,
                  "SMS": requestBody.SMS,
                  "SMSDescription": requestBody.SMSDescription,
                  "Email": requestBody.Email,
                  "EmailDescription": requestBody.EmailDescription,
                  "TimeHrFrom": requestBody.TimeHrFrom,
                  "TimeHrTo": requestBody.TimeHrTo,
                  "TimeMinFrom": requestBody.TimeMinFrom,
                  "TimeMinTo": requestBody.TimeMinTo,
                  "TimeTypeFrom": requestBody.TimeTypeFrom,
                  "TimeTypeTo": requestBody.TimeTypeTo,
                  "CreatedBy": museumData.CreatedBy,
                  "UpdatedBy": userDetails.Name,
                  "CreatedAt": museumData.CreatedAt,
                  "UpdatedAt": new Date().toISOString(),
                }
                var respObj = { "responseObj": responseObj, "message": "MuseumOffline updated successfully..!" }
                return callback(err, response(200, JSON.stringify(respObj)));
              })

            } else {
              var respObj = { "message": "Logged in user details not found" }
              return callback(null, response(404, JSON.stringify(respObj)));
            }
          }
        })
      } else {
        var respObj = { "message": "Museum offline  data with given id not found" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}
// get  the AllMuseumOffline Details
module.exports.getAllMuseumOffline =  function (event, context, callback) {
  globalEvent = event;
  console.log("Inside function..");
  //-- Validate JWT token

  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } 
  }
  validateMultipleLogin();

//var respObj = await getAllOfflineData(callback);

  console.log("Inside get full data");

  var params = {
    TableName: process.env.MUSEUMOFFLINE_TABLE,
  }
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    var finalResponse = data;
    var museumData = data.Items;
    console.log("museumData .... " + museumData);
    //-- Sort data according to created date time
    if (museumData) {
      const sortedData = museumData.sort((a, b) => new Date(b.DateFrom) - new Date(a.DateFrom));
      finalResponse.Items = sortedData;
      var respObj = { "responseObj": finalResponse, "message": "Museum Offline data fetched successfully..!" };
      return callback(null, response(200, JSON.stringify(respObj)));
    } else {
      var respObj = { "responseObj": museumData, "message": "Museum Offline data fetched successfully..!" };
      return callback(null, response(200, JSON.stringify(respObj)));
    }
  })


}
// get particular Museum Offline details 
module.exports.getMuseumOffline = function (event, context, callback) {
  globalEvent = event;

  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  const id = event.pathParameters.Id;

  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params = {
    TableName: process.env.MUSEUMOFFLINE_TABLE,
    ExpressionAttributeValues: {
      ":offline": id
    },
    KeyConditionExpression: 'Id = :offline ',
  };
  documentClient.query(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    } else {
      if (data.Count > 0) {
        var respObj = { "responseObj": data.Items[0], "message": "MuseumOffline data fetched successfully." }
        return callback(null, response(200, JSON.stringify(respObj)));
      } else {
        var respObj = { "message": "MuseumOffline with type- " + id + " not found." }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}
//get Future date and details 
module.exports.getCompleteEventsTrueValue = function (event, context, callback) {
  globalEvent = event;
  var dateobj = new Date();
  // Formating the date and time
  // by using date.format() method
  const value = date.format(dateobj, 'YYYY-MM-DD');
  var dateID = value.toString();
  let params = {
    TableName: process.env.MUSEUMOFFLINE_TABLE,
    FilterExpression: '#EventDate >= :EventDate',
    ExpressionAttributeNames: {
      "#EventDate": "EventDate"
    },
    ExpressionAttributeValues: {
      ':EventDate': dateID
    },
    KeyConditionExpression: 'Complete = :Complete',

  };
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    let responseObj = data;
    let respObj = { "responseObj": responseObj, "message": "All fields fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));
  });
}

//get Future date and details 
module.exports.getFutureOfflineDetails = function (event, context, callback) {
  globalEvent = event;
  var dateobj = new Date();
  const value = date.format(dateobj, 'YYYY-MM-DD');
  var dateID = value.toString();
  let params = {
    TableName: process.env.MUSEUMOFFLINE_TABLE,
    FilterExpression: '#DateFrom >= :DateFrom',
    ExpressionAttributeNames: {
      "#DateFrom": "DateFrom"
    },
    ExpressionAttributeValues: {
      ':DateFrom': dateID
    },
    KeyConditionExpression: 'DateFrom = :DateFrom',

  };
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    let responseObj = data;
    let respObj = { "responseObj": responseObj, "message": "All fields fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));
  });
}

async function getAllOfflineData() {
  console.log("Inside getAllMuseumOffline!!!")
  var params = {
    TableName: process.env.MUSEUMOFFLINE_TABLE,
  };
  var data = await documentClient.scan(params).promise();
  var finalResponse = data;
  var museumData = data.Items;
  //-- Sort data according to created date time
  const sortedData = museumData.sort((a, b) => new Date(b.DateFrom) - new Date(a.DateFrom));
  finalResponse.Items = sortedData;
  var respObj = { "responseObj": finalResponse, "message": "Museum Offline data fetched successfully..!" };
  return respObj;

}

async function getFormattedToAndFrom(museumOffline) {

  console.log("Inside museumOffline :::::::", museumOffline.TimeHrFrom)
  var offlineFrom = moment(museumOffline.DateFrom.substring(0, 10) + " " + museumOffline.TimeHrFrom.toString() + ":" + museumOffline.TimeMinFrom.toString(), dateFormat, 'fr', true);
  // offlineFrom.add(museumOffline.TimeHrFrom,'hours')
  // offlineFrom.add(museumOffline.TimeMinFrom,'minutes')
  console.log("-------" + museumOffline.DateFrom + " " + museumOffline.TimeHrFrom.toString() + ":" + museumOffline.TimeMinFrom.toString());
  if (museumOffline.TimeTypeFrom.toLowerCase() == 'pm') {
    //  console.log("inside pm for offlineFrom ",offlineFrom);
    offlineFrom.add(12, 'hours');
    // console.log("after adding 12 hours offlineFrom ",offlineFrom);
  }
  // here it is EST
  offlineFrom = offlineFrom.toDate();
  console.log("offlineFrom ========", offlineFrom)
  console.log(museumOffline.DateTo.substring(0, 10) + " " + museumOffline.TimeHrTo.toString() + ":" + museumOffline.TimeMinTo.toString(), dateFormat, 'fr', true);
  var offlineTo = moment(museumOffline.DateTo.substring(0, 10) + " " + museumOffline.TimeHrTo.toString() + ":" + museumOffline.TimeMinTo.toString(), dateFormat, 'fr', true);
  // offlineTo.add(museumOffline.TimeHrTo,'hours')
  // offlineTo.add(museumOffline.TimeMinTo,'minutes')
  if (museumOffline.TimeTypeTo.toLowerCase() == 'pm') {
    //    console.log("inside pm to ",offlineTo);
    offlineTo.add(12, 'hours');
  }
  // here it is EST
  offlineTo = offlineTo.toDate();
  var responseObj = {
    "offlineFrom": offlineFrom,
    "offlineTo": offlineTo
  }
  console.log("getFormattedToAndFrom : responseObj : ", responseObj)
  return responseObj;
}

module.exports.fetchAndCancelBooknigs = async function (requestBody, loggedInUser, callback) {

  requestBody = requestBody.body
  console.log("requestBody   ", requestBody)
  requestBody = JSON.parse(requestBody)
  globalEvent = requestBody.event;
  console.log("globalEvent ", globalEvent)
  await getUsersDetailsAndCancleBooking(requestBody, loggedInUser, callback);

  var respObj = 'Successfully cancelled bookings'

  return callback(null, response(200, JSON.stringify(respObj)));
}

async function getUsersDetailsAndCancleBooking(requestBody, loggedInUser, callback) {

  console.log("Inside getUserDetails :::::::", requestBody)


  var offlineData = await getFormattedToAndFrom(requestBody)
  var offlineDateFrom = moment(offlineData.offlineFrom);
  var offlineDateTo = moment(offlineData.offlineTo);
  console.log("offlineDateTo ", offlineDateTo, "  offlineDateFrom ", offlineDateFrom)



  for (var d = moment(offlineDateFrom); offlineDateTo.diff(d, 'minutes') >= 0; d.add(1, 'days')) {
    var bookingIds = [];
    var phoneNos = [];
    var users = [];
    console.log("d     ", d);
    var params = {
      TableName: process.env.BOOKINGINFO_TABLE,
      IndexName: "BookingDate-index",
      KeyConditionExpression: "BookingDate = :bdate",
      ExpressionAttributeValues: {
        ":bdate": d.format('YYYY-MM-DD')

      }
    };
    //console.log("params.. " + JSON.stringify(params));
    // pick the date and fetch booking
    var data = await documentClient.query(params).promise();

    // Set the slot time in the booking date 
    for (const item of data.Items) {
      console.log("item : ", item.Id)
      var bookingDate = item.BookingDate;
      var slot = item.Slot24Hr

      if (typeof slot != "undefined") {
        if (slot.indexOf(".") === 1) {
          slot = "0" + slot.replace(".", ":")
        } else {
          slot = slot.replace(".", ":")
        }
        console.log(bookingDate + " " + slot);
        bookingDate = moment(bookingDate + " " + slot, dateFormat, 'fr', true);
        console.log("item.BookingDate11 : ", bookingDate)
        bookingDate.set(slot.substring(0, 1), 'hours')
        bookingDate.set(slot.substring(2, 3), 'minutes')
        // here it is EST
        offlineDateFrom = moment(offlineDateFrom);
        offlineDateTo = moment(offlineDateTo);
        bookingDate = bookingDate.toDate();
        offlineDateFrom = offlineDateFrom.toDate();
        offlineDateTo = offlineDateTo.toDate();
        console.log("offlineDateFrom : ", offlineDateFrom, "item.BookingDate : ", bookingDate, "offlineDateTo ", offlineDateTo, "slot ", slot)

        if (bookingDate >= (offlineDateFrom) && bookingDate <= (offlineDateTo)) {
          console.log("Got in...");

          if (item.BookingStatus != 'Cancelled') {
            var phone = "";
            if (typeof item.CountryCode != 'undefined') {
              phone = item.CountryCode + item.VisitorPhone;
            } else {
              phone = '91' + item.VisitorPhone;
            }

            var modifiedPhone = "";
            var countryCodeSign = phone.substring(0, 1);
            if (countryCodeSign === "+") {
              modifiedPhone = phone.substring(1);
            } else {
              modifiedPhone = phone;
            }

            phoneNos.push({
              "to": modifiedPhone
            })

            bookingIds.push(item.Id);
            users.push(
              {
                email: item.VisitorEmail,
                phoneNo: modifiedPhone
              }
            )
          }

        }

        offlineDateFrom = moment(offlineDateFrom);
        offlineDateTo = moment(offlineDateTo);

      } else {

        console.log("Slot details not available for Booking ID ", item.Id);
      }
    }

    // Cancle booking
    console.log("bookingIds---------------------------------- ", bookingIds)
    if (bookingIds.length > 0) {


      await cancleBooking.cancelMultipleBookingDetails(bookingIds, loggedInUser, callback);
      console.log("cancle Booking Called ====================================")
      // Send Push Notification
      console.log("Inside nptification!! :: ", requestBody.PushNotification)
      // if (requestBody.PushNotification) {
      var payload = {
        //  TimeHrFrom: requestBody.TimeHrFrom,
        //  TimeHrTo: requestBody.TimeHrTo,
        //  TimeMinFrom: requestBody.TimeMinFrom,
        //  TimeMinTo: requestBody.TimeMinTo,
        //  TimeTypeFrom: requestBody.TimeTypeFrom,
        //  TimeTypeTo: requestBody.TimeTypeTo,
        //  DateFrom: requestBody.DateFrom,
        //  DateTo: requestBody.DateTo,
        //  Description: requestBody.PushDescription,
        users: users,
        offlineDate: d.format('YYYY-MM-DD'),
        EventId: requestBody.EventId,
        DetailType: 'museum offline',
        Detail: `museumOffline`,

      }
    console.log("payload",payload)
      eventBridge.putEvents({
        Entries: [
          {
            Detail: JSON.stringify({ name: payload }),
            DetailType: 'Trigger custom push notification function',
            EventBusName: 'mnm-museum-event-bus1',
            Source: 'new.push',
          },
        ]
      }, function (err, data) {
        if (err) console.log("ERROR in putting event on eventbridge  ", err, err.stack); // an error occurred
        else console.log("Event put on eventBridge  ", payload);           // successful response
      });
      bookingIds = [];
      users = [];

    }
    if (requestBody.FullDay) {
      var searchWithOldDate = {
        TableName: process.env.BOOKINGDATE_TABLE,

        Key: {
          "BookingDate": d.format('YYYY-MM-DD')


        }

      }

      console.log("Search Params ..." + JSON.stringify(searchWithOldDate));

      var data = await documentClient.delete(searchWithOldDate).promise();
    }


    // var smstext1 = "from "+offlineDateFrom.format("DD MM YYYY HH MM") +" to "+ offlineDateTo.format("DD MM YYYY HH MM");
    //console.log("smstext1 ---------------------- ",smstext1);

    // Sens SMS
    var smsText = "We regret to inform you that due to unforeseen circumstances Mahindra Museum will be offline on " + d.format("DD MMM YYYY") + ". We regret inconvenience caused -M & M Ltd.";
    var templateId = '1107162712354948673';

    //--- CAll SMS notification
    //sending SMS
    try {
      if (phoneNos.length > 0) {
        console.log("phoneNos ", phoneNos)
        var smsNotification = notification.sendSMSNotification(phoneNos, smsText, templateId);
        phoneNos = [];
      }
    } catch (err) {
      console.log(err)
    }

  }

}