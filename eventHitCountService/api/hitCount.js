'use strict';

var AWS = require('aws-sdk');

const documentClient = new AWS.DynamoDB.DocumentClient();
const moment = require('moment');
module.exports.hitCount = async event => {

  //event = JSON.stringify(event);
  console.log("event ", event)
  //var records = []
  //records.concat(event.Records)
  var records = event.Records;
  console.log(records)
  for (const record of records) {
    console.log(record.dynamodb)
    // get data
    var bookingType = record.dynamodb.NewImage.TypeOfBooking.S
    console.log("bookingType",bookingType);
    var date = new Date();
    if (bookingType.toLowerCase() == "event") {
      var eventId = record.dynamodb.NewImage.EventId.S 
      console.log("eventId",eventId)
      var participantCount = "";
         if(record.dynamodb.NewImage.TotalNoOfParticipantVisited == undefined){ 
            participantCount = 0;  
            console.log("participantCount",participantCount)  
            }else {
            participantCount = record.dynamodb.NewImage.TotalNoOfParticipantVisited.N
            console.log("participantCount",participantCount)  
          }
            console.log("TotalNoOfParticipantVisited :: ",participantCount)
      console.log("inside event booking")
      var params = {
        TableName: process.env.REPORTSCOUNT_TABLE,
        Key: {
          "CountName": "EventCount",
          "recordDate": eventId

        }
      }
      // Check if record present
      var data = await documentClient.get(params).promise();

      var element = data.Item;
      console.log("data ", data)
      // if yes update by +1
      if (typeof element != "undefined" && element != "{}") {

        console.log("inside update count")
        //count = element.Count + 1
        var updateParams = {
          TableName: process.env.REPORTSCOUNT_TABLE,
          Key: {
            "CountName": "EventCount",
            "recordDate": eventId
          },
          UpdateExpression: "set hitCount=hitCount+:val, TotalNoOfParticipantVisited = :TotalNoOfParticipantVisited",
          ExpressionAttributeValues: {
            ":val": 1,
            ":TotalNoOfParticipantVisited": participantCount
          },
          /// ReturnValues: "UPDATED_NEW"
        }
        console.log(updateParams)
        var res = await documentClient.update(updateParams).promise();
        console.log(" IN UPDATING ", res)

        return res;

      } else {
        // if no, insert
        count = 1;
        var updateParams = {
          TableName: process.env.REPORTSCOUNT_TABLE,
          Item: {
            "CountName": "EventCount",
            "recordDate": eventId,
            "hitCount": count

          }
        }
        var respObj = { "message": "Event added successfully..!" }
        var data = await documentClient.put(updateParams, respObj).promise()
      }
    } else {

      var currentMonth = moment(date).format('YYYY-MM')
      console.log("inside non-event booking")
      var BookingVia = record.dynamodb.NewImage.BookingVia.N
      var params = {
        TableName: process.env.REPORTSCOUNT_TABLE,
        Key: {
          "CountName": "BookingMonthly",
          "recordDate": currentMonth + "_" + BookingVia

        }

      }
      // Check if record present
      var data = await documentClient.get(params).promise();

      var element = data.Item;



      // if yes update by +1
      if (typeof element != "undefined" && element != "{}") {

        console.log("inside update count")
        //count = element.Count + 1
        var updateParams = {
          TableName: process.env.REPORTSCOUNT_TABLE,
          Key: {
            "CountName": "BookingMonthly",
            "recordDate": currentMonth + "_" + BookingVia
          },
          UpdateExpression: "set hitCount=hitCount+:val",
          ExpressionAttributeValues: {
            ":val": 1,
          },
          /// ReturnValues: "UPDATED_NEW"
        }
        console.log(updateParams)
        var res = await documentClient.update(updateParams).promise();
        console.log(" IN UPDATING ", res)

        return res;

      } else {
        // if no, insert
        var count = 1;
        var updateParams = {
          TableName: process.env.REPORTSCOUNT_TABLE,
          Item: {
            "CountName": "BookingMonthly",
            "recordDate": currentMonth + "_" + BookingVia,
            "hitCount": count

          }
        }
        var respObj = { "message": "Event added successfully..!" }
        var data = await documentClient.put(updateParams, respObj).promise()
      }
    }
  };
  console.log("FUNCTION CALLED! ", event.Records[0].dynamodb)


  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Go Serverless v1.0! Your function executed successfully!',
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
