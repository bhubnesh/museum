'use strict';

var AWS = require('aws-sdk'),
    documentClient = new AWS.DynamoDB.DocumentClient();

const notification = require('../notifications/sendNotification');


//sending invite
module.exports.sendEmail = function (event, context, callback) {
  globalEvent = event;  
  const FAID = event.pathParameters.FAID;
    console.log("check ... ",FAID)
    if (!FAID) {
      let respObj = { "message": "Please provide Id" }
      return callback(
        null,
        response(400, JSON.stringify(respObj))
      );
    }
    const requestBody = JSON.parse(event.body);
    if (!requestBody.EmailId) {
      var respObj = { "message": "Manadatory input parameters are missing." }
      return callback(
        null,
        response(400, JSON.stringify(respObj))
      );
    }
    var email = (requestBody.EmailId).toLowerCase();
    var EmailDescription = requestBody.EmailDiscription;
    var params = {
      Item: {
        "Id": FAID,
        "EmailId": email,
        "EmailDiscription": EmailDescription,
        "CreatedAt": new Date().toISOString()
      },
      TableName: process.env.EVENTSINVITE_TABLE
    };
    console.log("params",params)
    var respObj = { "message": "mail sent..!" }
    documentClient.put(params, function (err, data) {
      var subject = "Thank You for Sharing Feedback"
      var emailNotification = notification.sendEmailNotification(email, subject, EmailDescription)  
      return callback(err, response(200, JSON.stringify(respObj)));
    })
  }
  
