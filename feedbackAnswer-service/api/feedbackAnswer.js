'use strict';

var AWS = require('aws-sdk'),

  uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token'),
  paths = require('../config/config');
const axios = require('axios');

var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}


module.exports.getAllAnswers = async function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();

  var feedbackResponse = [];
  let updatedDetail = [];
  var params = {
    TableName: process.env.SUBMITTEDANSWER_TABLE,
  }
  var data = await documentClient.scan(params).promise();
  // console.log("data",data)
  if (data.Count > 0) {
    var finalResponse = data.Items;
    var totalcount = finalResponse.length;
    console.log("totalcount", totalcount)
    for (const element of finalResponse) {
      let faid = element.FAID;
      let bookingId = element.BookingId;
      let vistorId = element.VisitorId;
      let name = element.VisitorName;
      let submittedOn = element.SubmittedOn;
      let starRatting = element.FeedbackAnswers;
      for (const data1 of starRatting) {
        if (data1.NoOfStars != null && data1.NoOfStars != "") {
          let noOfStars = data1.NoOfStars
          var newObject = {
            "FeedbackId": faid,
            "BookingId": bookingId,
            "VisitorId": vistorId,
            "VisitorName": name,
            "StarRating": noOfStars,
            "SubmittedOn": submittedOn
          }
          updatedDetail.push(newObject);
          console.log(updatedDetail)
        }
      }
    }
    var respObj = { "message": "Feedback details fetched successfully", "responseObj": { "feedBackList": updatedDetail } }
    return callback(null, response(200, JSON.stringify(respObj)));
  }
}
//get feedbackAnswers by BookingId through SubmittedAnswer Table
module.exports.getFeedbackAnswerById = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();

  const value = event.pathParameters.Id;
  console.log("id :: ", value);
  if (!value) {
    var respObj = { "message": "Manadatory input parameters are missing" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  var params1 = {
    TableName: process.env.SUBMITTEDANSWER_TABLE,
    KeyConditionExpression: "#BookingId = :BookingId ",
    FilterExpression: "#BookingId = :user_status_val ",
    ExpressionAttributeNames: {
      "#BookingId": "BookingId"

    },
    ExpressionAttributeValues: {
      ":user_status_val": value
    }
  };
  console.log("params1.. " + JSON.stringify(params1));
  documentClient.scan(params1, function (err, data) {
    console.log("check", params1)
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    } else {
      if (data.Count > 0) {
        var respObj = { "responseObj": data.Items[0], "message": "FeedbackAnswer's fetched successfully" }
        return callback(null, response(200, JSON.stringify(respObj)));
      } else {
        var respObj = { "message": + id + " not found." }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  });
}
