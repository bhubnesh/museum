'use strict';


var AWS = require('aws-sdk'),
    documentClient = new AWS.DynamoDB.DocumentClient();

module.exports = {

    getUserDetails: function (reqEvent) {

        console.log("Logged in user .. " + reqEvent.loggedInUser);
        var loggedInUser = reqEvent.loggedInUser;
        //-- Get user details from email Id 

        var searchWithEmail = {
            TableName: process.env.VISITOR_TABLE,
            ExpressionAttributeValues: {
                ":email": loggedInUser
            },
            KeyConditionExpression: "Email = :email"
        }

        console.log("Search Params ..." + JSON.stringify(searchWithEmail));


        documentClient.query(searchWithEmail, function (err, data) {
            if (err) {
                console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                return ({"statusCode" : 400,"message" :  JSON.stringify(err)});
            } else {
                console.log("GetItem succeeded:", JSON.stringify(data));
                console.log("count of records fro email ... " + data.Count)
                if (data.Count > 0) {
                    console.log("Full User Object ... " + JSON.stringify(data.Items[0]));
                    return ({ "statusCode": 200, "userData": data.Items[0], "message": "user details fetched successfully.." })
                }else{
                    return ({ "statusCode": 404, "userData": {}, "message": "User details not found." })
                }
            }
        })
    }
}