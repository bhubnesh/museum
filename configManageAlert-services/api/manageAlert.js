'use strict';

var AWS = require('aws-sdk'),
  uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
const async = require('async');
const eventBridge = new AWS.EventBridge({ region: 'ap-south-1' });


var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}


module.exports.addAlert = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);

  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();

  var loggedInUser = event.loggedInUser;

  //-- Get user details from email Id 
  var searchWithEmail = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": loggedInUser
    },
    KeyConditionExpression: "Email = :email"
  }

  console.log("Search Params ..." + JSON.stringify(searchWithEmail));


  documentClient.query(searchWithEmail, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return ({ "statusCode": 400, "message": JSON.stringify(err) });
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      console.log("count of records fro email ... " + data.Count)
      if (data.Count > 0) {
        console.log("Full User Object ... " + JSON.stringify(data.Items[0]));

        var Id = uuid.v1();
        var userDetails = data.Items[0];

        var params = {
          Item: {
            "Id": Id,
            "Date": requestBody.Date,
            "Hrs": requestBody.Hrs,
            "Min": requestBody.Min,
            "AMPM": requestBody.AMPM,
            "ChooseEvent": requestBody.ChooseEvent,
            "EventId": requestBody.EventId,
            "PushNotification": requestBody.PushNotification,
            "PushDescription": requestBody.PushDescription,
            "SMS": requestBody.SMS,
            "SMSDescription": requestBody.SMSDescription,
            "Email": requestBody.Email,
            "EmailDescription": requestBody.EmailDescription,
            "CreatedBy": userDetails.Name,
            "UpdatedBy": userDetails.Name,
            "CreatedAt": new Date().toISOString(),
            "UpdatedAt": new Date().toISOString()
          },
          TableName: process.env.MANAGEALERT_TABLE
        };
        console.log("Data to add ... " + params);
        var respObj = { "responseObj": data.Attributes, "message": "ManageAlert created successfully..!" }
        documentClient.put(params, function (err, data) {
          if (err) {
            console.log("ERROR in manage ALerts!!!")
          } else {
            // Send push Notification
            if (requestBody.PushNotification) {
              var payload = {

                Description: requestBody.PushDescription,
                EventId: requestBody.EventId,
                DetailType: 'Alert from admin',
                Detail: `alertFromAdmin`,
              }

              eventBridge.putEvents({
                Entries: [
                  {
                    Detail: JSON.stringify({ name: payload }),
                    DetailType: 'Trigger custom push notification function',
                    EventBusName: 'mnm-museum-event-bus1',
                    Source: 'new.push',
                  },
                ]
              }, function (err, data) {
                if (err) console.log("ERROR in putting event on eventbridge  ", err, err.stack); // an error occurred
                else console.log("Event put on eventBridge  ", data);           // successful response
              });
            }
          }
          return callback(err, response(200, JSON.stringify(respObj)));
        });

      } else {
        return ({ "statusCode": 404, "userData": {}, "message": "User details not found." })
      }
    }
  })
}
//-- Delete ManageAlert code 
module.exports.deleteAlert = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  const id = event.pathParameters.Id;

  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params = {
    TableName: process.env.MANAGEALERT_TABLE,
    Key: {
      "Id": id
    }
  };
  console.log("params.. " + JSON.stringify(params));
  documentClient.delete(params, function (err, data) {
    if (err) {
      return callback(err, response(400, err));
    }
    console.log(data);
    var respObj = { "message": "ManageAlert Deleted successfully..!" }
    return callback(err, response(200, JSON.stringify(respObj)));
  });
}
//MultipleDelete 
module.exports.multiDeleteAlert = function (event, context, callback) {
  var responseObj = {}
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();

  const requestBody = JSON.parse(event.body);
  if (!requestBody.alertId) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //get through requestBody
  var massegeId = requestBody.alertId;
  if (massegeId != null) {
    let cnt = 0;
    massegeId.forEach(manageAlert => {
      cnt++;
      var id = manageAlert.id;
      var params = {
        TableName: process.env.MANAGEALERT_TABLE,
        Key: {
          "Id": id
        }
      };
      documentClient.get(params, function (err, data) {
        if (err) {
          responseObj[id] = err;
        }
        else if (data && Object.keys(data).length === 0 && data.constructor === Object) {
          responseObj[id] = 'ID not found';
        }
        else {
          var deleteParams = {
            TableName: process.env.MANAGEALERT_TABLE,
            Key: {
              "Id": id
            }
          };
          documentClient.delete(deleteParams, function (err, data) {
            if (err) {
              responseObj[id] = err;
            }
            responseObj[id] = 'ID Deleted';
          });
        }
        console.log(data);
        if (massegeId.length == cnt) {
          setTimeout(() => {
            return callback(err, response(200, JSON.stringify([responseObj])));
          }, 2000);
        }
      })
    });
  } else {
    var respObj = { "message": "Id was provided incorrect" }
    return callback(err, response(err, JSON.stringify(respObj)));
  }
}

//-- Update alert details
module.exports.updateAlert = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  const id = event.pathParameters.Id;
  const requestBody = JSON.parse(event.body);

  if (!id) {
    var respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //-- check whether record with given id exists or not 

  var params1 = {
    TableName: process.env.MANAGEALERT_TABLE,
    Key: {
      "Id": id
    },
  };
  console.log("search params.. " + JSON.stringify(params1));
  documentClient.get(params1, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      if (data.Item) {
        console.log("Alert data with given id exists.");
        const alertData = data.Item;
        //-- Get currently logged in user info
        var loggedInUser = validateTokenResp.event.loggedInUser;
        //-- Get user details from email Id 

        var searchWithEmail = {
          TableName: process.env.VISITOR_TABLE,
          ExpressionAttributeValues: {
            ":email": loggedInUser
          },
          KeyConditionExpression: "Email = :email"
        }
        console.log("Search Params ..." + JSON.stringify(searchWithEmail));

        documentClient.query(searchWithEmail, function (err, data) {
          if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return ({ "statusCode": 400, "message": JSON.stringify(err) });
          } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            console.log("count of records fro email ... " + data.Count)
            if (data.Count > 0) {
              console.log("Full User Object ... " + JSON.stringify(data.Items[0]));
              const userDetails = data.Items[0];

              var params = {
                TableName: process.env.MANAGEALERT_TABLE,
                Key: {
                  "Id": id
                },

                UpdateExpression: 'set #PushDescription = :PD,#SMSDescription = :SMSD,#EmailDescription = :ED,#Date = :T,#Hrs = :H,#Min = :M,#AMPM = :A,#ChooseEvent = :E,#PushNotification = :PN,#SMS = :SMS,#Email = :EM,#EventId = :EI,#UpdatedBy = :U,#UpdatedAt = :UA',

                ExpressionAttributeValues: {
                  ':PD': requestBody.PushDescription,
                  ':SMSD': requestBody.SMSDescription,
                  ':ED': requestBody.EmailDescription,
                  ':T': requestBody.Date,
                  ':H': requestBody.Hrs,
                  ':M': requestBody.Min,
                  ':A': requestBody.AMPM,
                  ':E': requestBody.ChooseEvent,
                  ':PN': requestBody.PushNotification,
                  ':SMS': requestBody.SMS,
                  ':EM': requestBody.Email,
                  ':EI': requestBody.EventId,
                  ':U': userDetails.Name,
                  ':UA': new Date().toISOString()
                },
                ExpressionAttributeNames: {
                  '#PushDescription': 'PushDescription',
                  '#SMSDescription': 'SMSDescription',
                  '#EmailDescription': 'EmailDescription',
                  '#UpdatedBy': 'UpdatedBy',
                  '#Date': 'Date',
                  '#Hrs': 'Hrs',
                  '#Min': 'Min',
                  '#AMPM': 'AMPM',
                  '#ChooseEvent': 'ChooseEvent',
                  '#PushNotification': 'PushNotification',
                  '#SMS': 'SMS',
                  '#Email': 'Email',
                  '#EventId': 'EventId',
                  "#UpdatedAt": "UpdatedAt"
                },
                ReturnValues: "UPDATED_NEW"
              };
              const responseObj = {
                "Id": alertData.Id,
                "ChooseEvent": requestBody.ChooseEvent,
                "EventId": requestBody.EventId,
                "PushNotification": requestBody.PushNotification,
                "PushDescription": requestBody.PushDescription,
                "SMS": requestBody.SMS,
                "SMSDescription": requestBody.SMSDescription,
                "Email": requestBody.Email,
                "EmailDescription": requestBody.EmailDescription,
                "Date": requestBody.Date,
                "Hrs": requestBody.Hrs,
                "Min": requestBody.Min,
                "AMPM": requestBody.AMPM,
                "CreatedBy": alertData.CreatedBy,
                "UpdatedBy": userDetails.Name,
                "CreatedAt": alertData.CreatedAt,
                "UpdatedAt": new Date().toISOString(),
              }
              console.log("update params .. " + JSON.stringify(params));
              documentClient.update(params, function (err, data) {
                if (err) {
                  return callback(err, response(400, err));
                }
                console.log(data);
                var respObj = { "responseObj": responseObj, "message": "ManageAlert updated successfully..!" }
                return callback(err, response(200, JSON.stringify(respObj)));

              })
            } else {
              console.log("Logged in user details not found");
              var respObj = { "message": "Logged in user details not found" }
              return callback(null, response(404, JSON.stringify(respObj)));
            }
          }
        })

      } else {
        console.log("Alert data with given id not found");
        var respObj = { "message": "Alert data with given id not found" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })


}

//-- Get  the AllAlert Details

module.exports.getAllAlert = function (event, context, callback) {
  globalEvent = event;
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  var params = {
    TableName: process.env.MANAGEALERT_TABLE,
  }
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    // console.log(data);
    var alertData = data.Items;
    // const sortedAlertData  = alertData.sort((a,b) => Moment(a.CreatedAt).format('YYYY-MM-DD HH:m:s') -  Moment(b.CreatedAt).format('YYYY-MM-DD HH:m:s'))
    //console.log(sortedArray);

    //-- Sort data according to created date time
    const sortedAlertData = alertData.sort((a, b) => new Date(b.Date) - new Date(a.Date));
    console.log("sorted... " + JSON.stringify(sortedAlertData))
    var finalResponse = data;
    finalResponse.Items = sortedAlertData;
    var respObj = { "responseObj": finalResponse, "message": "ManageAlert data fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));


  })

}

//-- Get particular Alert details 

module.exports.getAlert = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();

  const id = event.pathParameters.Id;
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params = {
    TableName: process.env.MANAGEALERT_TABLE,
    ExpressionAttributeValues: {
      ":Id": id
    },
    KeyConditionExpression: 'Id = :Id ',
  };
  console.log("params.. " + JSON.stringify(params));
  documentClient.query(params, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return callback(err, response(400, JSON.stringify(err)));
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      console.log("count of records ... " + data.Count)
      if (data.Count > 0) {
        var respObj = { "responseObj": data.Items[0], "message": "ManageAlert data fetched successfully." }
        return callback(null, response(200, JSON.stringify(respObj)));
      } else {
        var respObj = { "message": "ManageAlert with type- " + id + " not found." }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}