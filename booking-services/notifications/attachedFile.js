var axios = require('axios');
var FormData = require('form-data');
var fs = require('fs');
var data = new FormData();
data.append('From', 'noreply-museum@museum.mahindramail.com');
data.append('To', 'ap00706922@techmahindra.com');
data.append('Subject', 'Mahindra Museum : Test Mail');
data.append('Text', 'Test only');
data.append('File', fs.createReadStream('/C:/Users/AP00706922/Desktop/Covering Letter.pdf'));

var config = {
  method: 'get',
  url: 'https://nzejp5.api.infobip.com/email/2/send',
  headers: { 
    'Authorization': 'App 8125468595bac54ad77bfa7d835caab6-e447a354-5483-4705-95e2-42489fb2141f', 
    'Content-Type': 'application/json', 
    ...data.getHeaders()
  },
  data : data
};

axios(config)
.then(function (response) {
  console.log(JSON.stringify(response.data));
})
.catch(function (error) {
  console.log(error);
});
