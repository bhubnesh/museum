'use strict';

//var AWS = require('aws-sdk'),

  // uuid = require('uuid'),
  // documentClient = new AWS.DynamoDB.DocumentClient();
//const auth = require('../auth/token');
//const axios = require('axios');
var QRCode = require('qrcode');
//var QrReader = require('qrcode-reader');

//const jimp = require('jimp');
//const path = require('path');
//let jsonData = require('./booking.json');
//let viewData = require('./viewData.json');

function response(statusCode, message) {
  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      'X-XSS-Protection' : '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
     // 'Access-Control-Allow-Headers': 'Content-Type, x-requested-with',
     'Access-Control-Allow-Headers': 'Accept',
     'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'
    },
    body: message
  };
}
module.exports.addQrCode = function (event, context, callback) {
  debugger;
  var bookingJSON = JSON.parse(event.body);
  const opts = {
    text : 'numeric'
   }
  QRCode.toDataURL(JSON.stringify(bookingJSON),opts, function (err, url) {
    if (err) {
      return callback(err, response(400, JSON.stringify(respObj)));
    } else {
      console.log("QR code generated successfully... " + url);
      var respObj = { "message": "QR Code added successfully in Booking details", "responseObj": url }
      return callback(null, response(200, JSON.stringify(respObj)));
    }
  })
} 