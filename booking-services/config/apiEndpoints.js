module.exports.API_URLS = Object.freeze({   
 
  geBookingConfigPath : "https://yh4ox1yya1.execute-api.ap-south-1.amazonaws.com/prod/booking" ,
  getMuseumOfflinePath : "https://difhz0swxb.execute-api.ap-south-1.amazonaws.com/prod/museumOffline",
  getWorkingDaysPath : " https://7k608d19m2.execute-api.ap-south-1.amazonaws.com/prod/workingDay",
  updateVisitCountPath : "https://qufngu7000.execute-api.ap-south-1.amazonaws.com/prod/visitor/visitCount/"

});
  