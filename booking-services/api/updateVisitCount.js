'use strict';

var AWS = require('aws-sdk'),
    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
var CryptoJS = require("crypto-js");
var globalEvent = "";

function response(statusCode, message) {

    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Origin;
    }
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Host;
    }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
    }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}

module.exports.updateVisitCount = function (event, context, callback) {
    globalEvent = event;
    const id = (event.pathParameters.Email).toLowerCase();

    if (!id) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }
    console.log("Token.... " + JSON.stringify(event));
    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
            console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        } else {
            VisitCount(event, context, callback);
        }
    }
    validateMultipleLogin();
}
function VisitCount(event, context, callback) {

    globalEvent = event;
    var id = (event.pathParameters.Email).toLowerCase();

    var params = {
        TableName: process.env.VISITOR_TABLE,
        ExpressionAttributeValues: {
            ":email": id
        },
        KeyConditionExpression: 'Email = :email ',

    };
    console.log("params.. " + JSON.stringify(params));
    documentClient.query(params, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
            console.log("count of records ... " + data.Count)
            if (data.Count > 0) {
                var originalItem = data.Items[0];
                var noOfVisits;
                originalItem.NoOfVisits ? noOfVisits = originalItem.NoOfVisits : noOfVisits = 0;

                var params = {
                    TableName: process.env.VISITOR_TABLE,
                    Key: {
                        "Email": id,
                        "Phone": originalItem.Phone
                    },

                    UpdateExpression: 'set #visits = :visits',
                    ExpressionAttributeValues: {
                        ':visits': noOfVisits + 1
                    },
                    ExpressionAttributeNames: {
                        '#visits': 'NoOfVisits'

                    },
                    ReturnValues: "UPDATED_NEW"
                };

                console.log("params.. " + JSON.stringify(params));
                documentClient.update(params, function (err, data) {
                    if (err) {
                        return callback(err, response(400, err));
                    } else {
                        var respObj = { "message": "Visit count updated successfully" }
                        return callback(null, response(200, JSON.stringify(respObj)));
                    }
                })

            } else {
                console.log("Visitor not found");
                var respObj = { "message": "Visitor not found." }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    });

}