'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
var QRCode = require('qrcode');
const axios = require('axios');
let apiEndpoints = require('../config/apiEndpoints');
const notification = require('../notifications/sendNotification');
const moment = require('moment');
var globalEvent = "";
var CryptoJS = require("crypto-js");

function response(statusCode, message) {

    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Origin;
    }
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Host;
    }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
    }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}


module.exports.addEventBookingDetails = function (event, context, callback) {
    debugger;
    globalEvent = event;
    const requestBody = JSON.parse(event.body);
    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.VisitorEmail, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);

    //-- Decrypt Phone
    var decryptedPhone = CryptoJS.AES.decrypt(requestBody.VisitorPhone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorPhone = decryptedPhone.toString(CryptoJS.enc.Utf8);

    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
            console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        } else {
            eventBooking(event, context, callback);
        }
    }
    validateMultipleLogin();
    function eventBooking(event, context, callback) {
        if (!requestBody.VisitorEmail || !requestBody.TypeOfBooking || !requestBody.Role || !requestBody.EventId ||
            !requestBody.EventDate || !requestBody.EventTime ||
            (requestBody.NoOfAdults <= 0) || (requestBody.NoOfChildren < 0) ||
            (requestBody.AssistanceRequired != true && requestBody.AssistanceRequired != false) ||
            (requestBody.WheelChairAssistanceRequired != true && requestBody.WheelChairAssistanceRequired != false) ||
            (requestBody.WCRequiredCount < 0) ||
            (requestBody.HIAssistanceRequired != true && requestBody.HIAssistanceRequired != false) ||
            (requestBody.HIHelp < 0) || (requestBody.VIRequiredCount < 0) ||
            (requestBody.VIAssistanceRequired != true && requestBody.VIAssistanceRequired != false) ||
            (requestBody.BookingVia != 0 && requestBody.BookingVia != 1)
        ) {

            var respObj = { "message": "Manadatory input parameters are missing." }
            return callback(
                null,
                response(400, JSON.stringify(respObj))
            );
        }



        //-- Check whether booking is available for given date and time slot

        var searchWithDate = {
            TableName: process.env.ADMINCONFIGEVENT_TABLE,
            ExpressionAttributeValues: {
                ":event": requestBody.EventId
            },

            KeyConditionExpression: "Id = :event"
        }

        // console.log("Search Params ..." + JSON.stringify(searchWithDate));

        documentClient.query(searchWithDate, function (err, data) {
            if (err) {
                console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                return response(400, JSON.stringify(err));
            } else {

                if (data.Count > 0) {
                    var eventDetails = data.Items[0];
                    if (eventDetails.AvailableBookingCount >= (requestBody.NoOfAdults + requestBody.NoOfChildren)) {

                        if (requestBody.AssistanceRequired == true) {
                            //-- Check WC required, if true then check availability
                            if (requestBody.WheelChairAssistanceRequired == true) {
                                if (requestBody.WCRequiredCount > eventDetails.WCAMaximumAvailablePerEvent || requestBody.WCRequiredCount > eventDetails.WCAvailableCount) {
                                    //console.log("VI count required is not available for this event.");
                                    var respObj = { "message": "Wleelchair assistance is not available for this event" };
                                    return callback(null, response(404, JSON.stringify(respObj)))
                                } else if (requestBody.WCRequiredCount > eventDetails.WCAMaximumAllowedPerBooking) {
                                    //console.log("You can only choose "+eventDetails.WCAMaximumAllowedPerBooking+" wheelchair per booking");
                                    var respObj = { "message": "You can only choose " + eventDetails.WCAMaximumAllowedPerBooking + " Wheelchair per booking" };
                                    return callback(null, response(404, JSON.stringify(respObj)));
                                }
                            }
                            //-- Check VI required, if true then check availability
                            if (requestBody.VIAssistanceRequired == true) {
                                if (requestBody.VIRequiredCount > eventDetails.VIAMaximumAvailablePerEvent || requestBody.VIRequiredCount > eventDetails.VIAvailableCount) {
                                    var respObj = { "message": "Visually impaired assistance is not available for this slot." };
                                    return callback(null, response(404, JSON.stringify(respObj)))
                                }
                            }
                            //-- Check HI required, if true then check availability
                            if (requestBody.HIAssistanceRequired == true) {
                                if (requestBody.HIHelp > eventDetails.HIAMaximumAvailablePerEvent || requestBody.HIHelp > eventDetails.HIAvalableCount) {
                                    var respObj = { "message": "Hearing impaired assistance is not available for this slot." };
                                    return callback(null, response(404, JSON.stringify(respObj)))
                                }
                            }
                            //-- If successfully validated, call function to add booking details
                            addBookingInfo(event, callback, eventDetails);
                        } else {
                            addBookingInfo(event, callback, eventDetails);
                        }
                    } else {
                        console.log("No Bookings available for the given event.");
                        var respObj = { "message": "No Bookings available for the given event." };
                        return callback(null, response(404, JSON.stringify(respObj)));
                    }
                } else {
                    console.log("Event details not found for the given event Id.")
                    var respObj = { "message": "Event details not found for the given event Id." }
                    return callback(null, response(404, JSON.stringify(respObj)));

                }

            }

        })
    }
}
function addBookingInfo(event, callback, eventDetails) {

    var requestBody = JSON.parse(event.body);

    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.VisitorEmail, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);

    //-- Decrypt Phone
    var decryptedPhone = CryptoJS.AES.decrypt(requestBody.VisitorPhone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorPhone = decryptedPhone.toString(CryptoJS.enc.Utf8);
    
     var decryptedParticipant = CryptoJS.AES.decrypt(requestBody.ParticipantDetails, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
     requestBody.ParticipantDetails = decryptedParticipant.toString(CryptoJS.enc.Utf8);
     var ParticipantDetails = JSON.parse(requestBody.ParticipantDetails)
    console.log("requestBody.ParticipantDetails",ParticipantDetails)

    var params = {
        TableName: process.env.ADMINCONFIGEVENT_TABLE,
        Key: {
            "Id": requestBody.EventId
        },
        UpdateExpression: 'set #AvailableBookingCount = :AB, #WCAvailableCount = :WAC ,#HIAvalableCount = :HIC , #VIAvailableCount = :VIC,#UpdatedAt = :UA',
        ExpressionAttributeValues: {
            ':AB': eventDetails.AvailableBookingCount - (requestBody.NoOfAdults + requestBody.NoOfChildren),
            ':WAC': eventDetails.WCAvailableCount - requestBody.WCRequiredCount,
            ':VIC': eventDetails.VIAvailableCount - requestBody.VIRequiredCount,
            ':HIC': requestBody.HIHelp,
            ':UA': new Date().toISOString()
        },
        ExpressionAttributeNames: {
            '#AvailableBookingCount': 'AvailableBookingCount',
            '#WCAvailableCount': 'WCAvailableCount',
            '#HIAvalableCount': 'HIAvalableCount',
            '#VIAvailableCount': 'VIAvailableCount',
            "#UpdatedAt": "UpdatedAt"

        },
        ReturnValues: "UPDATED_NEW"
    };
    //console.log("params.. " + JSON.stringify(params));
    documentClient.update(params, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return callback(err, response(400, err));
        }
        // console.log("Update response.. " + JSON.stringify(data));
        var eventDate1 = requestBody.EventDate;
        var eventDate2 = new Date(eventDate1);
        console.log("eventDate2... " + eventDate2);
        var formattedDate = moment(eventDate2).format('YYYY-MM-DD');
        //-- Create QR Code for the given details

        const random1 = Math.floor(1000 + Math.random() * 9000);
        const random2 = Math.floor(1000 + Math.random() * 9000);
        //-- Add details into booking table
        var bookingData = {
            Item: {
                "Id": "REQW" + random1 + random2,
                "EventId": requestBody.EventId,
                "CreatedAt": new Date().toISOString(),
                "UpdatedAt": new Date().toISOString(),
                "VisitorId": requestBody.VisitorId,
                "VisitorEmail": requestBody.VisitorEmail,
                "VisitorName": requestBody.VisitorName,
                "VisitorPhone": requestBody.VisitorPhone,
                "FeedbackStatus": false,
                "CountryCode": requestBody.CountryCode,
                "NoOfParticipants": (requestBody.NoOfAdults + requestBody.NoOfChildren),
                "NoOfAdults": requestBody.NoOfAdults,
                "NoOfChildren": requestBody.NoOfChildren,
                "ParticipantDetails": ParticipantDetails,
                "Language": requestBody.Language,
                "TypeOfBooking": requestBody.TypeOfBooking,
                "Role": requestBody.Role,
                "BookingDate": formattedDate,
                "Slot": "",
                "SlotTime": requestBody.EventTime,
                "AssistanceRequired": requestBody.AssistanceRequired,
                "WheelChairAssistanceRequired": requestBody.WheelChairAssistanceRequired,
                "WCRequiredCount": requestBody.WCRequiredCount,
                "HIAssistanceRequired": requestBody.HIAssistanceRequired,
                "HIHelp": requestBody.HIHelp,
                "AmericanSignLanguage": requestBody.AmericanSignLanguage,
                "IndianSignLanguage": requestBody.IndianSignLanguage,
                "VIAssistanceRequired": requestBody.VIAssistanceRequired,
                "VIRequiredCount": requestBody.VIRequiredCount,
                "Duration": requestBody.Duration,
                "EventName": requestBody.EventName,
                "BookingCancelled": false,
                "BookingStatus": "Confirmed",
                "BookingVia": requestBody.BookingVia,
                "QRCode": ""
            },
            TableName: process.env.BOOKING_TABLE
        }

        var bookingIdJSON = { "Id": bookingData.Item.Id };
        //-- Create QR Code for the given details
        QRCode.toDataURL(JSON.stringify(bookingIdJSON), function (err, url) {
            //console.log("data we get " + JSON.stringify(params));
            if (err) {
                return callback(err, response(400, JSON.stringify(respObj)));
            } else {
                //     console.log("QR code generated successfully... " + url);
                // bookingData.Item.QRCode = url;

                //     console.log("Booking details to add... " + JSON.stringify(bookingData));

                documentClient.put(bookingData, function (err, data) {
                    if (err) {
                        return callback(err, response(400, JSON.stringify(respObj)));
                    }
                    //-- Call API to genretae QR code and update the same into a booking table
                    //  console.log("Booking details added successfully." + JSON.stringify(data));
                    addDataIntoHistory(event, callback, bookingData, url);
                    //  var respObj = {"message" : "Booking details added successfully.", "responseObj" : bookingData.Item }
                    // return callback(null, response(200, JSON.stringify(respObj)));
                })
            }
        })
    })
}
function addDataIntoHistory(event, callback, bookingData, url) {

    var requestBody = JSON.parse(event.body);

    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.VisitorEmail, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);

    //-- Decrypt Phone
    var decryptedPhone = CryptoJS.AES.decrypt(requestBody.VisitorPhone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorPhone = decryptedPhone.toString(CryptoJS.enc.Utf8);

    bookingData.Item.QRCode = url;
    //-- Update user table and add no of visit count
    const updateVisitCount_URL = apiEndpoints.API_URLS.updateVisitCountPath + requestBody.VisitorEmail;

    axios.put(updateVisitCount_URL, "", {
        headers: {
            'Authorization': event.headers.Authorization
        }
    }).then(resp => {
        if (resp.status != 200) {
            return callback(null, response(resp.status, resp.message));
        } else {
            // console.log("resp.... ",resp);
            var phone = (requestBody.CountryCode) + (requestBody.VisitorPhone)
            console.log("Phone.... " + phone);
            var countryCodeSign = phone.substring(0, 1);
            if (countryCodeSign === "+") {
                var modifiedPhone = phone.substring(1);
            } else {
                var modifiedPhone = phone;
            }
            //sending SMS
            var smsText = "Your visit to Mahindra Museum is confirmed for " + moment(requestBody.BookingDate).format("DD MMM YYYY") + " " + requestBody.EventTime + " -M & M Ltd."
            var typeOfTemplate = '1107162712306677152';
            var smsNotification = notification.sendSMSNotification(modifiedPhone, smsText, typeOfTemplate);
            var email = requestBody.VisitorEmail;
            //var text = smsText + "/"+JSON.stringify(bookingData.Item)
            var subject = "Event Booking Confirmation"
            var emailNotification = notification.sendEmailNotification(email, subject, smsText)
            // console.log("Update response.. " + JSON.stringify(data));
            var respObj = { "message": "Booking details added successfully.", "responseObj": bookingData.Item }
            return callback(null, response(200, JSON.stringify(respObj)));
        }
    });

}