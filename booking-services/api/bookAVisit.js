'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
let apiEndpoints = require('../config/apiEndpoints');
const axios = require('axios');
var QRCode = require('qrcode');
const moment = require('moment');
var CryptoJS = require("crypto-js");

const notification = require('../notifications/sendNotification');

var globalEvent = "";

function response(statusCode, message) {

    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Origin;
    }
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Host;
    }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
    }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}



module.exports.addBookingDetails = function (event, context, callback) {
    debugger;
    globalEvent = event;
    
    const requestBody = JSON.parse(event.body);
    console.log("Visitor Email ... " + requestBody.VisitorEmail);
    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.VisitorEmail, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);
    console.log("Visitor Email ... " + requestBody.VisitorEmail);
    //-- Decrypt Phone
    var decryptedPhone = CryptoJS.AES.decrypt(requestBody.VisitorPhone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorPhone = decryptedPhone.toString(CryptoJS.enc.Utf8);
    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        console.log("Response.... " + JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
            console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        } else {
            addBooking(event,context,callback);
        }
    }

    validateMultipleLogin();

    function addBooking(event,context,callback) {
        const requestBody = JSON.parse(event.body);
        //-- Decrypt Email Id
        var decryptedEmail = CryptoJS.AES.decrypt(requestBody.VisitorEmail, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
        requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);
        console.log("Visitor Email ... " + requestBody.VisitorEmail);
        //-- Decrypt Phone
        var decryptedPhone = CryptoJS.AES.decrypt(requestBody.VisitorPhone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
        requestBody.VisitorPhone = decryptedPhone.toString(CryptoJS.enc.Utf8);



        if (!requestBody.VisitorEmail || !requestBody.TypeOfBooking || !requestBody.Role ||
            !requestBody.BookingDate || !requestBody.SlotTime ||
            (requestBody.NoOfAdults <= 0) || (requestBody.NoOfChildren < 0) ||
            (requestBody.AssistanceRequired != true && requestBody.AssistanceRequired != false) ||
            (requestBody.WheelChairAssistanceRequired != true && requestBody.WheelChairAssistanceRequired != false) ||
            (requestBody.WCRequiredCount < 0) ||
            (requestBody.HIAssistanceRequired != true && requestBody.HIAssistanceRequired != false) ||
            (requestBody.HIHelp < 0) || (requestBody.VIRequiredCount < 0) ||
            (requestBody.VIAssistanceRequired != true && requestBody.VIAssistanceRequired != false) ||
            (requestBody.BookingVia != 0 && requestBody.BookingVia != 1)) {

            var respObj = { "message": "Manadatory input parameters are missing." }
            return callback(
                null,
                response(400, JSON.stringify(respObj))
            );
        }
        //-- Check whether booking is available for given date and time slot
        var searchWithDate = {
            TableName: process.env.BOOKINGDATE_TABLE,
            ExpressionAttributeValues: {
                ":date": requestBody.BookingDate
            },

            KeyConditionExpression: "BookingDate = :date"
        }
        console.log("Search Params ..." + JSON.stringify(searchWithDate));
        documentClient.query(searchWithDate, function (err, data) {
            if (err) {
                console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                return response(400, JSON.stringify(err));
            } else {
                // console.log("GetItem succeeded:", JSON.stringify(data));
                //   console.log("count of records fro email ... " + data.Count)
                if (data.Count > 0) {
                    var bookingData = data.Items[0];
                    // console.log("line 70",bookingData)
                    var slotArray = [];
                    slotArray = data.Items[0].SlotsDetails;
                    console.log("Slot time ... " + requestBody.SlotTime);
                    var slotCount = slotArray.length;
                    if (slotArray.length > 0) {
                        slotArray.forEach(slot => {
                            if (slot.slot == requestBody.SlotTime) {
                                if (slot.bookingsAvailable >= (requestBody.NoOfAdults + requestBody.NoOfChildren)) {
                                    //-- Check assistance required
                                    if (requestBody.AssistanceRequired == true) {
                                        //-- Check WC required, if true then check availability
                                        if (requestBody.WheelChairAssistanceRequired == true) {
                                            // console.log("checking",slot.WCAMaximumAvailablePerSlot)
                                            if (requestBody.WCRequiredCount > slot.WCAMaximumAvailablePerSlot || requestBody.WCRequiredCount > slot.WCAvailable) {
                                                // console.log("You can only choose "+slot.WCAMaximumAvailablePerSlot+" Wheel chair per booking");
                                                var respObj = { "message": "You can only choose " + slot.WCAMaximumAvailablePerSlot + " Wheel chair per booking" };
                                                return callback(null, response(404, JSON.stringify(respObj)))
                                            }
                                        }
                                        //-- Check VI required, if true then check availability
                                        if (requestBody.VIAssistanceRequired == true) {
                                            if (requestBody.VIRequiredCount > slot.VIAMaximumAvailablePerSlot || requestBody.VIRequiredCount > slot.VIAvailable) {
                                                var respObj = { "message": "Visually impaired assistance is not available for this slot." };
                                                return callback(null, response(404, JSON.stringify(respObj)))
                                            }
                                        }
                                        //-- Check HI required, if true then check availability
                                        if (requestBody.HIAssistanceRequired == true) {
                                            if (requestBody.HIHelp > slot.HIAMaximumAvailablePerSlot || requestBody.HIHelp > slot.HIAvailable) {
                                                var respObj = { "message": "Hearing impaired assistance is not available for this slot." };
                                                return callback(null, response(404, JSON.stringify(respObj)))
                                            }
                                        }
                                        //-- If successfully validated, call function to add booking details
                                        // console.log("105")
                                        addBookingInfo(event, callback, bookingData);
                                    } else {
                                        //-- Call function to add booking details
                                        // console.log("109")
                                        addBookingInfo(event, callback, bookingData);
                                    }
                                } else {
                                    console.log("No Bookings available for the given slots.");
                                    var respObj = { "message": "No Bookings available for the given slots." };
                                    return callback(null, response(404, JSON.stringify(respObj)));
                                }
                            }
                        });
                    } else {
                        console.log("Slot details are not available for given date.");
                        var respObj = { "message": "Slot details are not available for given date." };
                        return callback(null, response(404, JSON.stringify(respObj)));
                    }
                } else {
                    console.log("Booking details not available for the given date.")
                    var respObj = { "message": "Booking details not available for the given date." }
                    return callback(null, response(404, JSON.stringify(respObj)));
                }
            }
        })
    }
}
function addBookingInfo(event, callback, bookingData) {

    var requestBody = JSON.parse(event.body);
    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.VisitorEmail, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);
    console.log("Visitor Email ... " + requestBody.VisitorEmail);
    //-- Decrypt Phone
    var decryptedPhone = CryptoJS.AES.decrypt(requestBody.VisitorPhone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorPhone = decryptedPhone.toString(CryptoJS.enc.Utf8);
    var decryptedParticipant = CryptoJS.AES.decrypt(requestBody.ParticipantDetails, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.ParticipantDetails = decryptedParticipant.toString(CryptoJS.enc.Utf8);
    var ParticipantDetails = JSON.parse(requestBody.ParticipantDetails)
    console.log("requestBody.ParticipantDetails",ParticipantDetails)
    var slotArray = [];
    slotArray = bookingData.SlotsDetails;
    var count = slotArray.length;
    if (slotArray.length > 0) {

        slotArray.forEach(slot => {
            if (slot.slot === requestBody.SlotTime) {
                slot.bookingsAvailable = slot.bookingsAvailable - (requestBody.NoOfChildren + requestBody.NoOfAdults);
                slot.bookingsDone = slot.bookingsDone + (requestBody.NoOfChildren + requestBody.NoOfAdults);
                slot.WCAvailable = slot.WCAvailable - requestBody.WCRequiredCount;
                slot.WCBooked = slot.WCBooked + requestBody.WCRequiredCount;
                slot.HIAvailable = slot.HIAvailable + requestBody.HIHelp;
                slot.HIBooked = slot.HIBooked + requestBody.HIHelp;
                slot.VIAvailable = slot.VIAvailable + requestBody.VIRequiredCount;
                slot.VIBooked = slot.VIBooked + requestBody.VIRequiredCount;
            }
            count--;
        })
        if (count == 0) {
            bookingData.SlotsDetails = slotArray;
            //-- Update booking info from bookingDate table

            var params = {
                TableName: process.env.BOOKINGDATE_TABLE,
                Key: {
                    "BookingDate": bookingData.BookingDate
                },
                UpdateExpression: 'set #SlotsDetails = :SD,#UpdatedBy = :U,#UpdatedAt = :UA',
                ExpressionAttributeValues: {
                    ':SD': slotArray,
                    ':U': requestBody.VisitorEmail,
                    ':UA': new Date().toISOString()
                },
                ExpressionAttributeNames: {
                    '#SlotsDetails': 'SlotsDetails',
                    '#UpdatedBy': 'UpdatedBy',
                    "#UpdatedAt": "UpdatedAt"

                },
                ReturnValues: "UPDATED_NEW"
            };
            // console.log("params.. " + JSON.stringify(params));
            documentClient.update(params, function (err, data) {
                if (err) {
                    console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                    return callback(err, response(400, err));
                }
                //    console.log("Update response.. " + JSON.stringify(data));
                var date1 = requestBody.BookingDate;
                console.log("date1... " + date1);
                var date2 = new Date(date1);
                console.log("date2... " + date2);
                var formattedDate = moment(date2).format('YYYY-MM-DD');
                //-- Create QR Code for the given details
                const random1 = Math.floor(1000 + Math.random() * 9000);
                const random2 = Math.floor(1000 + Math.random() * 9000);
                //-- Add details into booking table
                var bookingData = {
                    Item: {
                        "Id": "REQW" + random1 + random2,
                        "CreatedAt": new Date().toISOString(),
                        "UpdatedAt": new Date().toISOString(),
                        "VisitorId": requestBody.VisitorId,
                        "VisitorEmail": requestBody.VisitorEmail,
                        "VisitorName": requestBody.VisitorName,
                        "FeedbackStatus": false,
                        "CountryCode": requestBody.CountryCode,
                        "VisitorPhone": requestBody.VisitorPhone,
                        "NoOfParticipants": (requestBody.NoOfAdults + requestBody.NoOfChildren),
                        "NoOfAdults": requestBody.NoOfAdults,
                        "NoOfChildren": requestBody.NoOfChildren,
                        "ParticipantDetails": ParticipantDetails,
                        "Language": requestBody.Language,
                        "TypeOfBooking": requestBody.TypeOfBooking,
                        "Role": requestBody.Role,
                        "BookingDate": formattedDate,
                        "Slot": requestBody.Slot,
                        "SlotTime": requestBody.SlotTime,
                        "Slot24Hr": requestBody.Slot24Hr,
                        "AssistanceRequired": requestBody.AssistanceRequired,
                        "WheelChairAssistanceRequired": requestBody.WheelChairAssistanceRequired,
                        "WCRequiredCount": requestBody.WCRequiredCount,
                        "HIAssistanceRequired": requestBody.HIAssistanceRequired,
                        "HIHelp": requestBody.HIHelp,
                        "AmericanSignLanguage": requestBody.AmericanSignLanguage,
                        "IndianSignLanguage": requestBody.IndianSignLanguage,
                        "VIAssistanceRequired": requestBody.VIAssistanceRequired,
                        "VIRequiredCount": requestBody.VIRequiredCount,
                        "SimulatorRequired": requestBody.SimulatorRequired,
                        "ManageSimulatorSettings": requestBody.ManageSimulatorSettings,
                        "BookingCancelled": false,
                        "BookingStatus": "Confirmed",
                        "BookingVia": requestBody.BookingVia,
                        "QRCode": ""
                    },
                    TableName: process.env.BOOKING_TABLE
                }
                var bookingIdJSON = { "Id": bookingData.Item.Id };
                //-- Create QR Code for the given details
                QRCode.toDataURL(JSON.stringify(bookingIdJSON), function (err, url) {
                    //console.log("data we get " + JSON.stringify(params));
                    if (err) {
                        return callback(err, response(400, JSON.stringify(respObj)));
                    } else {
                        //console.log("QR code generated successfully... " + url);
                        //bookingData.Item.QRCode = url;
                        //console.log("Booking details to add... " + JSON.stringify(bookingData));
                        documentClient.put(bookingData, function (err, data) {
                            if (err) {
                                console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                                return response(400, JSON.stringify(err));
                            }
                            //-- Call API to genretae QR code and update the same into a booking table
                            // console.log("Booking details added successfully." + JSON.stringify(data));
                            //-- Call function to save the booking data against visitor email Id
                            updateUserTableCount(event, callback, bookingData, url);
                            //  var respObj = {"message" : "Booking details added successfully.", "responseObj" : bookingData.Item }
                            //  return callback(null, response(200, JSON.stringify(respObj)));
                        })
                    }
                })
            })
        }
    } else {
        var respObj = { "message": "Slot data not available" };
        return callback(null, response(404, JSON.stringify(respObj)));
    }
}
function updateUserTableCount(event, callback, bookingData, url) {

    var requestBody = JSON.parse(event.body);
    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.VisitorEmail, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);
    console.log("Visitor Email ... " + requestBody.VisitorEmail);
    //-- Decrypt Phone
    var decryptedPhone = CryptoJS.AES.decrypt(requestBody.VisitorPhone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorPhone = decryptedPhone.toString(CryptoJS.enc.Utf8);
    bookingData.Item.QRCode = url;
    const updateVisitCount_URL = apiEndpoints.API_URLS.updateVisitCountPath + requestBody.VisitorEmail;
    axios.put(updateVisitCount_URL, "", {
        headers: {
            'Authorization': event.headers.Authorization
        }
    }).then(resp => {
        if (resp.status != 200) {
            console.log("hello")
            return callback(null, response(resp.status, resp.message));
        } else {

            var phone = (requestBody.CountryCode) + (requestBody.VisitorPhone)
            console.log("Phone.... " + phone);

            var countryCodeSign = phone.substring(0, 1);
            if (countryCodeSign === "+") {
                var modifiedPhone = phone.substring(1);
            } else {
                var modifiedPhone = phone;
            }

            //sending SMS
            var smsText = "Your visit to Mahindra Museum is confirmed for " + moment(requestBody.BookingDate).format("DD MMM YYYY") + " " + requestBody.SlotTime + " -M & M Ltd."
            var typeOfTemplate = '1107162712306677152';
            var smsNotification = notification.sendSMSNotification(modifiedPhone, smsText, typeOfTemplate);
            var email = requestBody.VisitorEmail;
            var subject = "Booking Confirmation"
            var emailNotification = notification.sendEmailNotification(email, subject, smsText)
            var respObj = { "message": "Booking details added successfully.", "responseObj": bookingData.Item }
            return callback(null, response(200, JSON.stringify(respObj)));
        }
    });
}