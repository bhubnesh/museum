'use strict';

const auth = require('../auth/token');
let paths = require('../config/apiEndpoints');
const axios = require('axios');
const moment = require('moment');
var _ = require('underscore');
var CryptoJS = require("crypto-js");
var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}



module.exports.getCalendarDataForBooking = function (event, context, callback) {
    debugger;
    globalEvent = event;
    const requestBody = JSON.parse(event.body);
    
    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.Email, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.Email = decryptedEmail.toString(CryptoJS.enc.Utf8);

    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();


    if (!requestBody.Email || !requestBody.FromDateForBooking || !requestBody.ToDateForBooking) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }

    //--call Admin booking configs API
    const bookingConfig_URL = paths.API_URLS.geBookingConfigPath;

    axios.get(bookingConfig_URL, {
        headers: {
            'Authorization': event.headers.Authorization
        }
    })
        .then((res) => {
            if (res.status != 200) {
                return callback(null, response(res.status, res.message));
            }
            // console.log("Booking Configs ... " + JSON.stringify(res.data.responseObj));
            const bookingConfigs = res.data.responseObj;
            if (!bookingConfigs) {
                var respObj = { "message": "Unable to fetch booking configurations." }
                return callback(
                    null,
                    response(404, JSON.stringify(respObj))
                );
            }
            getCalendarData(event, callback, bookingConfigs);
        })
        .catch((error) => {
            console.error(error)
        })

}
function getCalendarData(event, callback, bookingConfigs) {
    
    var requestBody = JSON.parse(event.body);
    // const noOfDaysForBooking = bookingConfigs.NumberOfDaysBookingWillBeOpen;
    // console.log("noOfDaysForBooking .... " + noOfDaysForBooking)
    // const fromDate = moment(new Date()).format('YYYY-MM-DD');
    // console.log("From Date ... " + fromDate);
    // var toDate = new Date();
    // toDate.setDate(toDate.getDate() + noOfDaysForBooking);
    // const toDate1 = moment(toDate).format('YYYY-MM-DD');
    // console.log("To Date ... " + toDate1);

    const fromDate = requestBody.FromDateForBooking;
    const toDate = requestBody.ToDateForBooking;
    
    //-- Fetch Museum offline data
    const museumOffline_URL = paths.API_URLS.getMuseumOfflinePath;

    axios.get(museumOffline_URL, {
        headers: {
            'Authorization': event.headers.Authorization
        }
    })
        .then((res) => {
            if (res.status != 200) {
                return callback(null, response(res.status, res.statusText));
            }
            //console.log("Museum Offline ... " + JSON.stringify(res.data.responseObj));
            const museumOfflineData = res.data.responseObj;
            if (!museumOfflineData) {
                var respObj = { "message": "Unable to fetch museum offline data" }
                return callback(
                    null,
                    response(404, JSON.stringify(respObj))
                );
            }
            //-- Fetch Museum working days data
            const workingDays_URL = paths.API_URLS.getWorkingDaysPath;

            axios.get(workingDays_URL, {
                headers: {
                    'Authorization': event.headers.Authorization
                }
            })
                .then((res) => {
                    if (res.status == 200) {
                        //   console.log("Museum Working Days data " + JSON.stringify(res.data.responseObj));
                        const museumWorkingDaysData = res.data.responseObj.WorkingDaysData;
                        if (!museumWorkingDaysData) {
                            var respObj = { "message": "Unable to fetch museum offline data" }
                            return callback(
                                null,
                                response(404, JSON.stringify(respObj))
                            );
                        }
                        //--- Create Array of working days with day as a key
                        var museaumWorkingDaysModifiedArray = [];
                        if (museumWorkingDaysData.length > 0) {
                            var cnt = museumWorkingDaysData.length;
                            museumWorkingDaysData.forEach(data => {
                                var key = data.Day;
                                var obj = {};
                                obj[key] = data;
                                museaumWorkingDaysModifiedArray.push(obj);
                                cnt--;
                                if (cnt == 0) {
                                    generateCalendar(bookingConfigs, fromDate, toDate, museumOfflineData, museumWorkingDaysData, museaumWorkingDaysModifiedArray, callback);
                                }
                            }
                            )
                        } else {
                            //--- Generate Museum Calander based on the given data
                            generateCalendar(bookingConfigs, fromDate, toDate, museumOfflineData, museumWorkingDaysData, museaumWorkingDaysModifiedArray, callback);

                        }
                    } else {
                        return callback(null, response(res.status, res.message));
                    }
                }).catch((error) => {
                    console.error(error)
                    var respObj = { "message": "Unautherized User" }
                    return callback(null, response(401, JSON.stringify(respObj)));
                })


        })
        .catch((error) => {
            console.error("*******************" + error)
            var respObj = { "message": "Unautherized User" }
            return callback(null, response(401, JSON.stringify(respObj)));
        })



}

function generateCalendar(bookingConfigs, fromDate, toDate, museumOfflineData, museumWorkingDaysData, museaumWorkingDaysModifiedArray, callback) {
    
    var offlineDates = [];
    var openDates = [];
    var calendarData = [];
    const fullCalendarData = [];
    fullCalendarData.push({ "bookingConfigs": bookingConfigs, "fromDate": fromDate, "toDate": toDate, "museumOfflineData": museumOfflineData, "museumWorkingDaysData": museaumWorkingDaysModifiedArray })
    //-- Identify offline days

    const offlineData = museumOfflineData.Items;
    console.log(offlineData.length);
    var count = offlineData.length;
    if (offlineData.length > 0) {
        offlineData.forEach(data => {
            //-- Get full day off dates
            // if(fromDate.)
            var dateFrom = moment(data.DateFrom).format('YYYY-MM-DD');
            var dateTo = moment(data.DateTo).format('YYYY-MM-DD');
            console.log("DateFrom ... " + dateFrom + "  .. DateTo ... " + dateTo);
            if (dateTo >= dateFrom) {
                if (dateFrom >= fromDate && dateFrom <= toDate) {
                    if (data.DateFrom == data.DateTo) {
                        if (data.FullDay == true) {
                            //  offlineDates.push(dateFrom);
                            // console.log(offlineDates);
                            var keyDate = dateFrom;
                            //var obj = {};
                            //obj[keyDate] = 
                            var obj =
                            {
                                "Date": dateFrom,
                                "TimeHrFrom": "",
                                "TimeMinFrom": "",
                                "TimeTypeFrom": "",
                                "TimeHrTo": "",
                                "TimeMinTo": "",
                                "TimeTypeTo": "",
                                "isOpen": false

                            };

                            calendarData.push(obj);

                            count--;
                        } else {
                            //-- Add those dates along with timings into another array
/*
                            openDates.push(dateFrom);
                            var keyDate = dateFrom;
                            // var obj = {};
                            // obj[keyDate] = {
                            var obj = {
                                "Date": dateFrom,
                                "TimeHrFrom": data.TimeHrFrom,
                                "TimeMinFrom": data.TimeHrTo,
                                "TimeTypeFrom": data.TimeTypeFrom,
                                "TimeHrTo": data.TimeHrTo,
                                "TimeMinTo": data.TimeMinTo,
                                "TimeTypeTo": data.TimeTypeTo,
                                "isOpen": true
                            }

                            calendarData.push(obj);

                            console.log("Open Days .. " + openDates);
                           */ 
                            count--;
                        }
                    } else {
                        //- Check difference between dates 

                        var oneDay = 24 * 60 * 60 * 1000;
                        var d1 = new Date(dateFrom);
                        var d2 = new Date(dateTo);
                        var diffDays = (d2.getTime() - d1.getTime()) / (oneDay);
                        console.log("diffDays.. " + diffDays);
                        var diffDaysCount = diffDays;
                        for (var i = 0; i <= diffDays; i++) {
                            var daysToAdd = diffDaysCount == diffDays ? 0 : 1;
                            d1.setDate(d1.getDate() + daysToAdd);
                            if (data.FullDay == true) {

                                var dateToAdd = moment(d1).format('YYYY-MM-DD');
                                offlineDates.push(dateToAdd);
                                var keyDate = dateToAdd;
                                // var obj = {};
                                // obj[keyDate] = {
                                var obj = {
                                    "Date": dateToAdd,
                                    "TimeHrFrom": "",
                                    "TimeMinFrom": "",
                                    "TimeTypeFrom": "",
                                    "TimeHrTo": "",
                                    "TimeMinTo": "",
                                    "TimeTypeTo": "",
                                    "isOpen": false
                                }
                                calendarData.push(obj)
                               // count--;
                                diffDaysCount--;
                            } else {
                                diffDaysCount--;
                            }
                            if(diffDaysCount == 0){
                                count --;
                            }
                        }

                    }
                } else {
                    console.log("OutDated dates.")

                    //-- call function to get working days data..
                    count--;
                }
            } else {
                console.log("fromDate can not be greater than toDate");
                var respObj = { "message": "fromDate from Museum Offline can not be greater than toDate", "calandarData": calendarData }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
            console.log("Count for offline data... " + count);
            if (count == 0) {
                fullCalendarData.push({ "MuseumOfflineCalandarData": calendarData });
                var respObj = { "message": "Calandar data added successfully.. !", "fullCalendarData": fullCalendarData }
                // return callback(null, response(200, JSON.stringify(respObj)));
                //-- Call function to get working days calandar data
                getWorkingDaysDataForCalendar(fullCalendarData, callback);
            }
        });
    } else {
        fullCalendarData.push({ "MuseumOfflineCalandarData": calendarData })
       
        //-- Call function to get working days calandar data
        getWorkingDaysDataForCalendar(fullCalendarData, callback);
    }
}


function getWorkingDaysDataForCalendar(fullCalendarData, callback) {
    
    console.log("********** Inside working day ***********")
    const workingDaysData = fullCalendarData[0].museumWorkingDaysData;
    const museumOfflineData = fullCalendarData[1].MuseumOfflineCalandarData;
    console.log("******** workingDaysData" + JSON.stringify(workingDaysData));
    //-- diff between toDate and From Date


    //fullCalendarData[0].bookingConfigs.NumberOfDaysBookingWillBeOpen;
    console.log("Length .. " + workingDaysData.length);
    var fromDate = fullCalendarData[0].fromDate;
    var toDate = fullCalendarData[0].toDate;

    var oneDay = 24 * 60 * 60 * 1000;
    var d1 = new Date(fromDate);
    var d2 = new Date(toDate);
    const bookingDatesCount = (d2.getTime() - d1.getTime()) / (oneDay);
    console.log("bookingDatesCount.. " + bookingDatesCount);
    var dataForDates = [];
    if (workingDaysData.length > 0) {
        var date1 = new Date(fromDate);
        //-- loop the dates 
        var cnt = bookingDatesCount + 1;
        for (var i = 0; i <= bookingDatesCount ; i++) {
           // var addDay = (cnt == bookingDatesCount) ? 0 : 1;
            if(i == 0){
                date1.setDate(date1.getDate());    
            }else{
                date1.setDate(date1.getDate() + 1);
            }
            //date1.setDate(date1.getDate() + addDay);
            //console.log("date .. " + date1);
            var key = moment(date1).format('YYYY-MM-DD');
            console.log("Date ... " + key);
            var obj = {}

            // var day = date1.getDay();
            // console.log("Day.... " + day);
            switch (date1.getDay()) {
                case 0:
                    //obj[key] = {
                    obj = {
                        "Date": key,
                        "Day": "Sunday",
                        "TimeHrFrom": workingDaysData[6].Sunday.isActive ? workingDaysData[6].Sunday.HrsFrom : "",
                        "TimeMinFrom": workingDaysData[6].Sunday.isActive ? workingDaysData[6].Sunday.MinFrom : "",
                        "TimeTypeFrom": workingDaysData[6].Sunday.isActive ? workingDaysData[6].Sunday.AMPMFrom : "",
                        "TimeHrTo": workingDaysData[6].Sunday.isActive ? workingDaysData[6].Sunday.HrsTo : "",
                        "TimeMinTo": workingDaysData[6].Sunday.isActive ? workingDaysData[6].Sunday.MinTo : "",
                        "TimeTypeTo": workingDaysData[6].Sunday.isActive ? workingDaysData[6].Sunday.AMPMTo : "",
                        "isOpen": workingDaysData[6].Sunday.isActive
                    };
                    dataForDates.push(obj);
                    break;
                case 1:
                    //obj[key] = {
                    obj = {
                        "Date": key,
                        "Day": "Monday",
                        "TimeHrFrom": workingDaysData[0].Monday.isActive ? workingDaysData[0].Monday.HrsFrom : "",
                        "TimeMinFrom": workingDaysData[0].Monday.isActive ? workingDaysData[0].Monday.MinFrom : "",
                        "TimeTypeFrom": workingDaysData[0].Monday.isActive ? workingDaysData[0].Monday.AMPMFrom : "",
                        "TimeHrTo": workingDaysData[0].Monday.isActive ? workingDaysData[0].Monday.HrsTo : "",
                        "TimeMinTo": workingDaysData[0].Monday.isActive ? workingDaysData[0].Monday.MinTo : "",
                        "TimeTypeTo": workingDaysData[0].Monday.isActive ? workingDaysData[0].Monday.AMPMTo : "",
                        "isOpen": workingDaysData[0].Monday.isActive
                    };
                    dataForDates.push(obj);
                    break;
                case 2:
                    //obj[key] = {
                    obj = {
                        "Date": key,
                        "Day": "Tuesday",
                        "TimeHrFrom": workingDaysData[1].Tuesday.isActive ? workingDaysData[1].Tuesday.HrsFrom : "",
                        "TimeMinFrom": workingDaysData[1].Tuesday.isActive ? workingDaysData[1].Tuesday.MinFrom : "",
                        "TimeTypeFrom": workingDaysData[1].Tuesday.isActive ? workingDaysData[1].Tuesday.AMPMFrom : "",
                        "TimeHrTo": workingDaysData[1].Tuesday.isActive ? workingDaysData[1].Tuesday.HrsTo : "",
                        "TimeMinTo": workingDaysData[1].Tuesday.isActive ? workingDaysData[1].Tuesday.MinTo : "",
                        "TimeTypeTo": workingDaysData[1].Tuesday.isActive ? workingDaysData[1].Tuesday.AMPMTo : "",
                        "isOpen": workingDaysData[1].Tuesday.isActive
                    };
                    dataForDates.push(obj);
                    break;
                case 3:
                    //obj[key] = {
                    obj = {
                        "Date": key,
                        "Day": "Wednesday",
                        "TimeHrFrom": workingDaysData[2].Wednesday.isActive ? workingDaysData[2].Wednesday.HrsFrom : "",
                        "TimeMinFrom": workingDaysData[2].Wednesday.isActive ? workingDaysData[2].Wednesday.MinFrom : "",
                        "TimeTypeFrom": workingDaysData[2].Wednesday.isActive ? workingDaysData[2].Wednesday.AMPMFrom : "",
                        "TimeHrTo": workingDaysData[2].Wednesday.isActive ? workingDaysData[2].Wednesday.HrsTo : "",
                        "TimeMinTo": workingDaysData[2].Wednesday.isActive ? workingDaysData[2].Wednesday.MinTo : "",
                        "TimeTypeTo": workingDaysData[2].Wednesday.isActive ? workingDaysData[2].Wednesday.AMPMTo : "",
                        "isOpen": workingDaysData[2].Wednesday.isActive
                    };
                    dataForDates.push(obj);
                    break;

                case 4:
                    //                    obj[key] = {
                    obj = {
                        "Date": key,
                        "Day": "Thursday",
                        "TimeHrFrom": workingDaysData[3].Thursday.isActive == true ? workingDaysData[3].Thursday.HrsFrom : "",
                        "TimeMinFrom": workingDaysData[3].Thursday.isActive == true ? workingDaysData[3].Thursday.MinFrom : "",
                        "TimeTypeFrom": workingDaysData[3].Thursday.isActive == true ? workingDaysData[3].Thursday.AMPMFrom : "",
                        "TimeHrTo": workingDaysData[3].Thursday.isActive == true ? workingDaysData[3].Thursday.HrsTo : "",
                        "TimeMinTo": workingDaysData[3].Thursday.isActive == true ? workingDaysData[3].Thursday.MinTo : "",
                        "TimeTypeTo": workingDaysData[3].Thursday.isActive == true ? workingDaysData[3].Thursday.AMPMTo : "",
                        "isOpen": workingDaysData[3].Thursday.isActive
                    };
                    dataForDates.push(obj);
                    console.log("*******" + JSON.stringify(dataForDates))
                    break;
                case 5:
                    //         obj[key] = {
                    obj = {
                        "Date": key,
                        "Day": "Friday",
                        "TimeHrFrom": workingDaysData[4].Friday.isActive ? workingDaysData[4].Friday.HrsFrom : "",
                        "TimeMinFrom": workingDaysData[4].Friday.isActive ? workingDaysData[4].Friday.MinFrom : "",
                        "TimeTypeFrom": workingDaysData[4].Friday.isActive ? workingDaysData[4].Friday.AMPMFrom : "",
                        "TimeHrTo": workingDaysData[4].Friday.isActive ? workingDaysData[4].Friday.HrsTo : "",
                        "TimeMinTo": workingDaysData[4].Friday.isActive ? workingDaysData[4].Friday.MinTo : "",
                        "TimeTypeTo": workingDaysData[4].Friday.isActive ? workingDaysData[4].Friday.AMPMTo : "",
                        "isOpen": workingDaysData[4].Friday.isActive
                    };
                    dataForDates.push(obj);
                    break;

                case 6:
                    //obj[key] = {
                    obj = {
                        "Date": key,
                        "Day": "Saturday",
                        "TimeHrFrom": workingDaysData[5].Saturday.isActive ? workingDaysData[5].Saturday.HrsFrom : "",
                        "TimeMinFrom": workingDaysData[5].Saturday.isActive ? workingDaysData[5].Saturday.MinFrom : "",
                        "TimeTypeFrom": workingDaysData[5].Saturday.isActive ? workingDaysData[5].Saturday.AMPMFrom : "",
                        "TimeHrTo": workingDaysData[5].Saturday.isActive ? workingDaysData[5].Saturday.HrsTo : "",
                        "TimeMinTo": workingDaysData[5].Saturday.isActive ? workingDaysData[5].Saturday.MinTo : "",
                        "TimeTypeTo": workingDaysData[5].Saturday.isActive ? workingDaysData[5].Saturday.AMPMTo : "",
                        "isOpen": workingDaysData[5].Saturday.isActive
                    };
                    dataForDates.push(obj);
            }
            cnt--;
            console.log("Count ... " + cnt);
            if (cnt == 0) {
                //-- get Unique items from offline data and working hrs data
                const finalArrayOfDays = (museumOfflineData.length > 0) ? museumOfflineData.concat(dataForDates) : dataForDates;
                const arrayOfDates = Array.from(finalArrayOfDays);
                var uniqueArray = _.uniq(arrayOfDates, 'Date');
                const sortedData = uniqueArray.sort((a, b) => new Date(a.Date) - new Date(b.Date));
                //var respObj = { "message": "Calandar data added successfully.. !", "respObj": sortedData, "otherData": fullCalendarData }
                var respObj = { "message": "Calandar data added successfully.. !", "responseObj": sortedData }
                return callback(null, response(200, JSON.stringify(respObj)));
            }
        }
    } else {
        console.log("Working Days data not found ")
        var respObj = { "message": "Calandar data added successfully.. !", "fullCalendarData": fullCalendarData }
        return callback(null, response(200, JSON.stringify(respObj)));
    }
}

