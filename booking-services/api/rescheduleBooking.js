'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
var QRCode = require('qrcode');
const notification = require('../notifications/sendNotification');
const axios = require('axios');
let paths = require('../config/apiEndpoints');
const moment = require('moment');
var CryptoJS = require("crypto-js");
var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}

module.exports.updateBookingDetails = function (event, context, callback) {
    globalEvent = event;
    //--call Admin booking configs API
    const bookingConfig_URL = paths.API_URLS.geBookingConfigPath;
    axios.get(bookingConfig_URL, {
        headers: {
            'Authorization': event.headers.Authorization
        }
    })
        .then((res) => {
            if (res.status != 200) {
                return callback(null, response(res.status, res.message));
            }
            const bookingConfigs = res.data.responseObj;
            if (!bookingConfigs) {
                var respObj = { "message": "Unable to fetch booking configurations." }
                return callback(
                    null,
                    response(404, JSON.stringify(respObj))
                );
            }

            updateVisitorBooking(event, bookingConfigs, context, callback);
        })
}


function updateVisitorBooking(event, bookingConfigs, context, callback) {
    debugger;

    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }else{
                updateBooking(event, bookingConfigs, context, callback);
            }
    }
    validateMultipleLogin();
}
function updateBooking(event, bookingConfigs, context, callback){
    const Id = event.pathParameters.Id;
    var requestBody = JSON.parse(event.body);
    requestBody.Id = Id;
   
      //-- Decrypt Email Id
      var decryptedEmail = CryptoJS.AES.decrypt(requestBody.VisitorEmail, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
      requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);
  
      //-- Decrypt Phone
    //   var decryptedPhone = CryptoJS.AES.decrypt(requestBody.VisitorPhone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    //   requestBody.VisitorPhone = decryptedPhone.toString(CryptoJS.enc.Utf8);
    

    if (!requestBody.VisitorEmail || !requestBody.TypeOfBooking || !requestBody.Role || !requestBody.BookingDate ||
        !requestBody.SlotTime || !requestBody.Slot || !Id ||
        (requestBody.SimulatorRequired != true && requestBody.SimulatorRequired != false)
        ||
        (requestBody.BookingVia != 0 && requestBody.BookingVia != 1)) {

        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }

    //-- Check whether booking data present for given Id
    var searchWithId = {
        TableName: process.env.BOOKING_TABLE,
        ExpressionAttributeValues: {
            ":id": Id
        },

        KeyConditionExpression: "Id = :id"
    }

    console.log("Search Params ..." + JSON.stringify(searchWithId));

    documentClient.query(searchWithId, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
            // console.log("GetItem succeeded:", JSON.stringify(data));
            //   console.log("count of records fro email ... " + data.Count)
            if (data.Count > 0) {
                var oldBookingData = data.Items[0];
                if (oldBookingData.BookingCancelled != true) {

                    var slot24Hr = oldBookingData.Slot24Hr;
                    var allowVisitorRescheduleBefore = bookingConfigs.AllowVisitorRescheduleBefore;
                   
                    var currentTime = moment(new Date()).format('HH.mm');
                    currentTime = parseFloat(currentTime) 
                    //+ 5.30;
                    if(oldBookingData.BookingDate == new Date()){
                    var timeDiff = parseFloat(slot24Hr) - parseFloat(currentTime);
                    //-- If slot > current Time
                    if (timeDiff <= parseFloat(bookingConfigs.AllowVisitorRescheduleBefore)) {
                        var msg ="Booking can be rescheduled only if the slot time booked is greater than " + bookingConfigs.AllowVisitorRescheduleBefore+ " Hr(s) as that of the current time.";
                        var respObj = { "message" : msg };
                        return callback(null, response(404, JSON.stringify(respObj)));

                    }
                }
                    //--- Changes to be done with old date's slots counts
                    var searchWithOldDate = {
                        TableName: process.env.BOOKINGDATE_TABLE,
                        ExpressionAttributeValues: {
                            ":date": oldBookingData.BookingDate
                        },

                        KeyConditionExpression: "BookingDate = :date"
                    }


                    documentClient.query(searchWithOldDate, function (err, data) {
                        if (err) {
                            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                            return response(400, JSON.stringify(err));
                        } else {

                            if (data.Count > 0) {
                                var bookingData = data.Items[0];
                                var slotArray = [];
                                slotArray = data.Items[0].SlotsDetails;
                                console.log("Slot time ... " + oldBookingData.SlotTime);
                                var count = slotArray.length;


                                slotArray.forEach(slot => {
                                    if (slot.slot === oldBookingData.SlotTime) {
                                        slot.bookingsAvailable = slot.bookingsAvailable + (oldBookingData.NoOfChildren + oldBookingData.NoOfAdults);
                                        slot.bookingsDone = slot.bookingsDone - (oldBookingData.NoOfChildren + oldBookingData.NoOfAdults);
                                        slot.WCAvailable = slot.WCAvailable + oldBookingData.WCRequiredCount;
                                        slot.WCBooked = slot.WCBooked - oldBookingData.WCRequiredCount;
                                        slot.HIAvailable = slot.HIAvailable + oldBookingData.HIHelp;
                                        slot.HIBooked = slot.HIBooked - oldBookingData.HIHelp;
                                        slot.VIAvailable = slot.VIAvailable + oldBookingData.VIRequiredCount;
                                        slot.VIBooked = slot.VIBooked - oldBookingData.VIRequiredCount;
                                    }
                                    count--;
                                })
                                if (count == 0) {
                                    bookingData.SlotsDetails = slotArray;

                                    //-- Update booking info from bookingDate table

                                    var params = {
                                        TableName: process.env.BOOKINGDATE_TABLE,
                                        Key: {
                                            "BookingDate": bookingData.BookingDate
                                        },
                                        UpdateExpression: 'set #SlotsDetails = :SD,#UpdatedBy = :U,#UpdatedAt = :UA',
                                        ExpressionAttributeValues: {
                                            ':SD': slotArray,
                                            ':U': oldBookingData.VisitorEmail,
                                            ':UA': new Date().toISOString()
                                        },
                                        ExpressionAttributeNames: {
                                            '#SlotsDetails': 'SlotsDetails',
                                            '#UpdatedBy': 'UpdatedBy',
                                            "#UpdatedAt": "UpdatedAt"

                                        },
                                        ReturnValues: "UPDATED_NEW"
                                    };
                                    console.log("params.. " + JSON.stringify(params));
                                    documentClient.update(params, function (err, data) {
                                        if (err) {
                                            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                                            return callback(err, response(400, err));
                                        }
                                        console.log("Update response.. " + JSON.stringify(data));
                                        //-- Check whether bookings vailable for new date and slot
                                        verifyDateSlot(event, callback, oldBookingData);
                                    })
                                }


                            } else {
                                console.log("Slot details are not available for given date.");
                                var respObj = { "message": "Slot details are not available for given date." };
                                return callback(null, response(404, JSON.stringify(respObj)));
                            }


                        }
                    })
                } else {
                    console.log("Bookings not available for given ID");
                    var respObj = { "message": "Bookings not available for given ID" };
                    return callback(null, response(404, JSON.stringify(respObj)));
                }
            } else {
                console.log("Bookings not available for given ID");
                var respObj = { "message": "Bookings not available for given ID" };
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })
}
function verifyDateSlot(event, callback, oldBookingData) {

  var requestBody = JSON.parse(event.body);
  //-- Decrypt Email Id
  var decryptedEmail = CryptoJS.AES.decrypt(requestBody.VisitorEmail, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);

  //-- Decrypt Phone
//   var decryptedPhone = CryptoJS.AES.decrypt(requestBody.VisitorPhone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
//   requestBody.VisitorPhone = decryptedPhone.toString(CryptoJS.enc.Utf8);

    //-- Check whether booking is available for given date and time slot

    var searchWithDate = {
        TableName: process.env.BOOKINGDATE_TABLE,
        ExpressionAttributeValues: {
            ":date": requestBody.BookingDate
        },

        KeyConditionExpression: "BookingDate = :date"
    }

    console.log("Search Params ..." + JSON.stringify(searchWithDate));

    documentClient.query(searchWithDate, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
            // console.log("GetItem succeeded:", JSON.stringify(data));
            //   console.log("count of records fro email ... " + data.Count)
            if (data.Count > 0) {
                var bookingData = data.Items[0];
                var slotArray = [];
                slotArray = data.Items[0].SlotsDetails;
                console.log("Slot time ... " + requestBody.SlotTime);

                if (slotArray.length > 0) {
                    slotArray.forEach(slot => {
                        if (slot.slot == requestBody.SlotTime) {
                            if (slot.bookingsAvailable >= (oldBookingData.NoOfAdults + oldBookingData.NoOfChildren)) {
                                //-- Check assistance required
                                if (oldBookingData.AssistanceRequired == true) {
                                    //-- Check WC required, if true then check availability
                                    if (oldBookingData.WheelChairAssistanceRequired == true) {
                                        if (oldBookingData.WCRequiredCount > slot.WCAMaximumAvailablePerSlot || oldBookingData.WCRequiredCount > slot.WCAvailable) {
                                            console.log("Wheel chair count required is not available for this slot.");
                                            var respObj = { "message": "Wheel chair count required is not available for this slot." };
                                            return callback(null, response(404, JSON.stringify(respObj)))
                                        }
                                    }
                                    //-- Check VI required, if true then check availability
                                    if (oldBookingData.VIAssistanceRequired == true) {
                                        if (oldBookingData.VIRequiredCount > slot.VIAMaximumAvailablePerSlot || oldBookingData.VIRequiredCount > slot.VIAvailable) {
                                            console.log("VI count required is not available for this slot.");
                                            var respObj = { "message": "VI count required is not available for this slot." };
                                            return callback(null, response(404, JSON.stringify(respObj)))
                                        }
                                    }
                                    //-- Check HI required, if true then check availability
                                    if (oldBookingData.HIAssistanceRequired == true) {
                                        if (oldBookingData.HIHelp > slot.HIAMaximumAvailablePerSlot || oldBookingData.HIHelp > slot.HIAvailable) {
                                            console.log("HI count required is not available for this slot.");
                                            var respObj = { "message": "HI count required is not available for this slot." };
                                            return callback(null, response(404, JSON.stringify(respObj)))
                                        }
                                    }
                                    //-- If successfully validated, call function to add booking details
                                    updateBookingInfo(event, callback, bookingData, oldBookingData);
                                } else {
                                    //-- Call function to add booking details
                                    updateBookingInfo(event, callback, bookingData, oldBookingData);
                                }
                            } else {
                                console.log("No Bookings available for the given slots.");
                                var respObj = { "message": "No Bookings available for the given slots." };
                                return callback(null, response(404, JSON.stringify(respObj)));
                            }
                        }
                    });

                } else {
                    console.log("Slot details are not available for given date.");
                    var respObj = { "message": "Slot details are not available for given date." };
                    return callback(null, response(404, JSON.stringify(respObj)));
                }
            } else {
                console.log("Booking details not available for the given date.")
                var respObj = { "message": "Booking details not available for the given date." }
                return callback(null, response(404, JSON.stringify(respObj)));

            }

        }
    })
}

function updateBookingInfo(event, callback, bookingData, oldBookingData) {
    var requestBody = JSON.parse(event.body);
    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.VisitorEmail, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);

    //-- Decrypt Phone
    // var decryptedPhone = CryptoJS.AES.decrypt(requestBody.VisitorPhone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    // requestBody.VisitorPhone = decryptedPhone.toString(CryptoJS.enc.Utf8);

    const Id = event.pathParameters.Id;
    requestBody.Id = Id;
    var slotArray = [];
    slotArray = bookingData.SlotsDetails;
    var count = slotArray.length;
    if (slotArray.length > 0) {

        slotArray.forEach(slot => {
            if (slot.slot === requestBody.SlotTime) {
                slot.bookingsAvailable = slot.bookingsAvailable - (oldBookingData.NoOfChildren + oldBookingData.NoOfAdults);
                slot.bookingsDone = slot.bookingsDone + (oldBookingData.NoOfChildren + oldBookingData.NoOfAdults);
                slot.WCAvailable = slot.WCAvailable - oldBookingData.WCRequiredCount;
                slot.WCBooked = slot.WCBooked + oldBookingData.WCRequiredCount;
                slot.HIAvailable = slot.HIAvailable + oldBookingData.HIHelp;
                slot.HIBooked = slot.HIBooked + oldBookingData.HIHelp;
                slot.VIAvailable = slot.VIAvailable + oldBookingData.VIRequiredCount;
                slot.VIBooked = slot.VIBooked + oldBookingData.VIRequiredCount;
            }
            count--;
        })
        if (count == 0) {
            bookingData.SlotsDetails = slotArray;
            //-- Update booking info from bookingDate table
            var params = {
                TableName: process.env.BOOKINGDATE_TABLE,
                Key: {
                    "BookingDate": bookingData.BookingDate
                },
                UpdateExpression: 'set #SlotsDetails = :SD,#UpdatedBy = :U,#UpdatedAt = :UA',
                ExpressionAttributeValues: {
                    ':SD': slotArray,
                    ':U': requestBody.VisitorEmail,
                    ':UA': new Date().toISOString()
                },
                ExpressionAttributeNames: {
                    '#SlotsDetails': 'SlotsDetails',
                    '#UpdatedBy': 'UpdatedBy',
                    "#UpdatedAt": "UpdatedAt"
                },
                ReturnValues: "UPDATED_NEW"
            };
            console.log("params.. " + JSON.stringify(params));
            documentClient.update(params, function (err, data) {
                if (err) {
                    console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                    return callback(err, response(400, err));
                }
                console.log("Update response.. " + JSON.stringify(data));

                var bookingIdJSON = { "Id": Id };
                //-- Generate QR Code
                QRCode.toDataURL(JSON.stringify(bookingIdJSON), function (err, url) {
                    //console.log("data we get " + JSON.stringify(params));
                    if (err) {
                        return callback(err, response(400, JSON.stringify(respObj)));
                    } else {

                        requestBody.QRCode = url;


                        //-- update details into booking table
                        var updateBookingData = {
                            TableName: process.env.BOOKING_TABLE,
                            Key: {
                                "Id": Id
                            },
                            UpdateExpression: 'set #TypeOfBooking = :TB, #Role = :R, #BookingDate = :BD, #Slot = :S,#SlotTime= :ST,#Slot24Hr = :S24Hr, #SimulatorRequired = :SR ,#QRCode = :QR,#UpdatedBy = :U, #BookingVia = :BV,#UpdatedAt = :UA',
                            ExpressionAttributeValues: {
                                ":TB": requestBody.TypeOfBooking,
                                ":R": requestBody.Role,
                                ":BD": requestBody.BookingDate,
                                ":S": requestBody.Slot,
                                ":ST": requestBody.SlotTime,
                                ":S24Hr": requestBody.Slot24Hr,
                                ":SR": requestBody.SimulatorRequired,
                                ":QR": "",
                                ":BV": requestBody.BookingVia,
                                ':U': requestBody.VisitorEmail,
                                ':UA': new Date().toISOString()
                            },
                            ExpressionAttributeNames: {
                                "#TypeOfBooking": "TypeOfBooking",
                                "#Role": "Role",
                                "#BookingDate": "BookingDate",
                                "#Slot": "Slot",
                                "#SlotTime": "SlotTime",
                                "#Slot24Hr": "Slot24Hr",
                                "#SimulatorRequired": "SimulatorRequired",
                                '#QRCode': 'QRCode',
                                '#BookingVia': 'BookingVia',
                                '#UpdatedBy': 'UpdatedBy',
                                "#UpdatedAt": "UpdatedAt"
                            },
                            ReturnValues: "UPDATED_NEW"
                        }
                        console.log("Booking details to update... " + JSON.stringify(updateBookingData));

                        documentClient.update(updateBookingData, function (err, data) {
                            if (err) {
                                console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                                return callback(err, response(400, err));
                            } else {
                                // updateBookingHistory(event,callback,url);
                                // console.log("Booking details updated successfully.");
                                //-- Call SMS notification API
                                var phone = (oldBookingData.CountryCode) + (oldBookingData.VisitorPhone);
                                var countryCodeSign = phone.substring(0, 1);
                                if (countryCodeSign === "+") {
                                    var modifiedPhone = phone.substring(1);
                                } else {
                                    var modifiedPhone = phone;
                                }
                                //sms add
                                var smsText = "Booking Rescheduled to " + moment(requestBody.BookingDate).format("DD MMM YYYY")+" "+requestBody.SlotTime + " -M & M Ltd.";
                                var typeOfTemplate = '1107162712342210911';
                                var smsNotification = notification.sendSMSNotification(modifiedPhone, smsText, typeOfTemplate);
                                var email = requestBody.VisitorEmail;
                                var subject = "Booking Reschedule"
                                var emailNotification = notification.sendEmailNotification(email, subject, smsText)                    
                                var respObj = { "message": "Booking details updated successfully.", "responseObj": requestBody }
                                return callback(null, response(200, JSON.stringify(respObj)));
                            }
                        })
                    }
                })
            })
        }
    } else {
        var respObj = { "message": "Slot data not available" };
        return callback(null, response(404, JSON.stringify(respObj)));
    }
}
function updateBookingHistory(event, callback, url) {
    var requestBody = JSON.parse(event.body);
    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.VisitorEmail, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);

    //-- Decrypt Phone
    // var decryptedPhone = CryptoJS.AES.decrypt(requestBody.VisitorPhone, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    // requestBody.VisitorPhone = decryptedPhone.toString(CryptoJS.enc.Utf8);

    const Id = event.pathParameters.Id;

    var searchWithEmail = {
        TableName: process.env.BOOKINGINFO_TABLE,
        ExpressionAttributeValues: {
            ":vEmail": (requestBody.VisitorEmail).toLowerCase()
        },

        KeyConditionExpression: "VisitorEmail = :vEmail"
    }

    console.log("Search Params ..." + JSON.stringify(searchWithEmail));

    documentClient.query(searchWithEmail, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
            // console.log("GetItem succeeded:", JSON.stringify(data));
            //   console.log("count of records fro email ... " + data.Count)
            //bookingData.Item.QRCode = "";
            if (data.Count == 0) {


                var respObj = { "message": "Booking details are not available for given emailId" }
                return callback(null, response(404, JSON.stringify(respObj)));


            } else {

                var previousData = data.Items[0];
                //console.log("bookingArray... " + bookingArray.length);
                var bookingArray = previousData.BookingData;
                //bookingArray.push(bookingData.Item);

                var bookingcount = bookingArray.length;
                console.log("Id.... " + Id);
                bookingArray.forEach(data => {
                    console.log("BookingId.. " + data.Id);
                    if (data.Id === Id) {
                        data.TypeOfBooking = requestBody.TypeOfBooking;
                        data.Role = requestBody.Role;
                        data.BookingDate = requestBody.BookingDate;
                        data.Slot = requestBody.Slot;
                        data.SlotTime = requestBody.SlotTime;
                        data.SimulatorRequired = requestBody.SimulatorRequired;
                        // data.SimulatorRequiredCount = requestBody.SimulatorRequiredCount;
                        data.VisitorEmail = requestBody.VisitorEmail;
                        data.BookingVia = requestBody.BookingVia;

                        var updateParams = {
                            TableName: process.env.BOOKINGINFO_TABLE,
                            Key: {
                                "VisitorEmail": (requestBody.VisitorEmail).toLowerCase()
                            },
                            UpdateExpression: 'set #BookingData = :BD,#UpdatedBy = :U,#UpdatedAt = :UA',
                            ExpressionAttributeValues: {
                                ':BD': bookingArray,
                                ':U': requestBody.VisitorEmail,
                                ':UA': new Date().toISOString()
                            },
                            ExpressionAttributeNames: {
                                '#BookingData': 'BookingData',
                                '#UpdatedBy': 'UpdatedBy',
                                "#UpdatedAt": "UpdatedAt"

                            },
                            ReturnValues: "UPDATED_NEW"
                        };
                        //console.log("updateParams.. " + JSON.stringify(updateParams));
                        documentClient.update(updateParams, function (err, data) {
                            if (err) {
                                console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                                return callback(err, response(400, err));
                            }
                            // console.log("Update response.. " + JSON.stringify(data));
                            var respObj = { "message": "Booking details updated successfully." }
                            return callback(null, response(200, JSON.stringify(respObj)));
                        })
                    } else {
                        bookingcount--;
                    }
                    if (bookingcount == 0) {
                        console.log("Booking Id not found in Booking Info table");
                        var respObj = { "message": "Booking Id not found in Booking Info table" }
                        return callback(null, response(404, JSON.stringify(respObj)));
                    }
                })
            }
        }
    })
}
