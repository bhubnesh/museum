'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
const moment = require('moment');
const _ = require('underscore');
var QRCode = require('qrcode');
var globalEvent = "";


function response(statusCode, message) {

    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Origin;
    }
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Host;
    }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
    }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}
module.exports.getAllBookingDetails = function (event, context, callback) {
    debugger;
    globalEvent = event;

    const email = event.pathParameters.email;
    if (!email) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }

    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
            console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        } else {
            getAllBookingHistory(event, context, callback);
        }
    }
    validateMultipleLogin();
}
function getAllBookingHistory(event, context, callback) {
    globalEvent = event;
    const email = event.pathParameters.email;
    //-- Get upcoming booking details
    const currentDate = moment(new Date()).format('YYYY-MM-DD');
    var upcomingBookings = [];
    var pastBookings = [];

    var indexParams =
    {
        TableName: process.env.BOOKING_TABLE,
        IndexName: "VisitorEmail-index",
        KeyConditionExpression: "VisitorEmail = :email",
        ExpressionAttributeValues: {
            ":email": email
        },
    }
    console.log("Search Params ..." + JSON.stringify(indexParams));

    documentClient.query(indexParams, function (err, data) {
        if (err) {
            return callback(err, response(400, JSON.stringify(err)));
        } else {
            if (data.Count > 0) {

                var bookingDetails = data.Items;

                var count = bookingDetails.length;
                console.log("Length .. " + count);
                bookingDetails.forEach(details => {
                    //     console.log("Booking ID -> " + details.Id + "   " + details.BookingDate);
                    //    console.log("currentDate..." + currentDate);
                    if (details.BookingDate > currentDate || (details.BookingDate == currentDate) && (parseFloat(details.Slot24Hr) > parseFloat(moment(new Date()).format('HH.mm')))) {

                        var bookingInfo = {
                            "Id": details.Id,
                            "VisitorId": details.VisitorId,
                            "VisitorName": details.VisitorName,
                            "VisitorEmail": details.VisitorEmail,
                            "BookingDate": details.BookingDate,
                            "FeedbackStatus": details.FeedbackStatus,
                            "SlotTime": details.SlotTime,
                            "NoOfAdults": details.NoOfAdults,
                            "ActualNoOfAdults": (details.ActualNoOfAdults != undefined) ? details.ActualNoOfAdults : details.ActualNoOfAdults,
                            "TotalNoOfParticipantVisited" : (details.TotalNoOfParticipantVisited != undefined) ? details.TotalNoOfParticipantVisited : details.TotalNoOfParticipantVisited,
                            "ActualNoOfChildren" : (details.ActualNoOfChildren != undefined) ? details.ActualNoOfChildren : details.ActualNoOfChildren,
                            "NoOfChildren": details.NoOfChildren,
                            "NoOfParticipants": details.NoOfParticipants,
                            "BookingCancelled": details.BookingCancelled,
                            "TypeOfBooking": details.TypeOfBooking,
                            "EventName": details.EventName,
                            "ManageSimulatorSettings": details.ManageSimulatorSettings,
                            "UpdatedBy": details.UpdatedBy,
                            "UpdatedAt": details.UpdatedAt,
                            "CreatedAt": details.CreatedAt
                        }
                        upcomingBookings.push(bookingInfo);
                        count--;
                    }
                    else if (details.BookingDate < currentDate || (details.BookingDate == currentDate) && (parseFloat(details.Slot24Hr) < parseFloat(moment(new Date()).format('HH.mm')))) {//+ 5.30)) {
                        console.log("Current Time ... " + (parseFloat(moment(new Date()).format('HH.mm')) + 5.30));
                        var bookingInfo = {
                            "Id": details.Id,
                            "VisitorId": details.VisitorId,
                            "VisitorName": details.VisitorName,
                            "VisitorEmail": details.VisitorEmail,
                            "BookingDate": details.BookingDate,
                            "FeedbackStatus": details.FeedbackStatus,
                            "SlotTime": details.SlotTime,
                            "ActualNoOfAdults": (details.ActualNoOfAdults != undefined) ? details.ActualNoOfAdults : details.ActualNoOfAdults,
                            "TotalNoOfParticipantVisited" : (details.TotalNoOfParticipantVisited != undefined) ? details.TotalNoOfParticipantVisited : details.TotalNoOfParticipantVisited,
                            "ActualNoOfChildren" : (details.ActualNoOfChildren != undefined) ? details.ActualNoOfChildren : details.ActualNoOfChildren,
                            "NoOfAdults": details.NoOfAdults,
                            "NoOfChildren": details.NoOfChildren,
                            "NoOfParticipants": details.NoOfParticipants,
                            "BookingCancelled": details.BookingCancelled,
                            "TypeOfBooking": details.TypeOfBooking,
                            "EventName": details.EventName,
                            "ManageSimulatorSettings": details.ManageSimulatorSettings,
                            "UpdatedBy": details.UpdatedBy,
                            "UpdatedAt": details.UpdatedAt,
                            "CreatedAt": details.CreatedAt
                        }
                        pastBookings.push(bookingInfo);
                        count--;
                    } else {
                        var bookingInfo = {
                            "Id": details.Id,
                            "VisitorId": details.VisitorId,
                            "VisitorName": details.VisitorName,
                            "VisitorEmail": details.VisitorEmail,
                            "BookingDate": details.BookingDate,
                            "FeedbackStatus": details.FeedbackStatus,
                            "ActualNoOfAdults": (details.ActualNoOfAdults != undefined) ? details.ActualNoOfAdults : details.ActualNoOfAdults,
                            "TotalNoOfParticipantVisited" : (details.TotalNoOfParticipantVisited != undefined) ? details.TotalNoOfParticipantVisited : details.TotalNoOfParticipantVisited,
                            "ActualNoOfChildren" : (details.ActualNoOfChildren != undefined) ? details.ActualNoOfChildren : details.ActualNoOfChildren,
                            "SlotTime": details.SlotTime,
                            "NoOfAdults": details.NoOfAdults,
                            "NoOfChildren": details.NoOfChildren,
                            "NoOfParticipants": details.NoOfParticipants,
                            "BookingCancelled": details.BookingCancelled,
                            "EventName": details.EventName,
                            "TypeOfBooking": details.TypeOfBooking,
                            "ManageSimulatorSettings": details.ManageSimulatorSettings,
                            "UpdatedBy": details.UpdatedBy,
                            "UpdatedAt": details.UpdatedAt,
                            "CreatedAt": details.CreatedAt
                        }
                        upcomingBookings.push(bookingInfo);
                        count--;
                    }


                    if (count == 0) {
                        var bookingDetailsArray = [];
                        const sortedUpcomingBookings = upcomingBookings.sort((a, b) => new Date(a.BookingDate) - new Date(b.BookingDate));
                        const sortedPastBookings = pastBookings.sort((a, b) => new Date(a.BookingDate) - new Date(b.BookingDate));
                        bookingDetailsArray.push(sortedUpcomingBookings);
                        bookingDetailsArray.push(sortedPastBookings);
                        var respObj = { "message": "Booking details fetched successfully.. !", "responseObj": { "upcomingBookings": bookingDetailsArray[0], "pastBookings": bookingDetailsArray[1] } }
                        return callback(null, response(200, JSON.stringify(respObj)));
                    }
                
                })
            } else {
                console.log("Booking details of given email not found ")
                var respObj = { "message": "Booking details of given email not found " }
                return callback(null, response(200, JSON.stringify(respObj)));
            }
        }
    })



}

module.exports.getBookingDetailsById = function (event, context, callback) {
    globalEvent = event;
    const id = event.pathParameters.Id;

    if (!id) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }

    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
            console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        } else {
            getByIdData(event, context, callback);
        }
    }
    validateMultipleLogin();
}
function getByIdData(event, context, callback) {
    globalEvent = event;
    const id = event.pathParameters.Id;

    //-- Check whether booking data present for given Id
    var searchWithId = {
        TableName: process.env.BOOKING_TABLE,
        ExpressionAttributeValues: {
            ":id": id
        },

        KeyConditionExpression: "Id = :id"
    }

    documentClient.query(searchWithId, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {

            if (data.Count > 0) {

                //-- Get QR code
                var bookingIdJSON = { "Id": id };
                QRCode.toDataURL(JSON.stringify(bookingIdJSON), function (err, url) {
                    //console.log("data we get " + JSON.stringify(params));
                    if (err) {
                        return callback(err, response(400, JSON.stringify(respObj)));
                    } else {
                        data.Items[0].QRCode = url
                        var respObj = { "message": "Booking details for the given Id fetched successfully.", "responseObj": data.Items[0] };
                        return callback(null, response(200, JSON.stringify(respObj)));
                    }
                })
            } else {

                // console.log("Booking details not available for the given date.")
                var respObj = { "message": "Booking details not available for the given date." }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })
}

module.exports.getLatestUpcomingBooking = function (event, context, callback) {
    globalEvent = event;
    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
            console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        } else {
            getLatestFutureBooking(event, context,validateTokenResp, callback);
        }
    }
    validateMultipleLogin();

}
function getLatestFutureBooking(event, context,validateTokenResp, callback) {
    const email = (event.pathParameters.email).toLowerCase();
    if (!email) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }
    //-- Get upcoming booking details
    const currentDate = moment(new Date()).format('YYYY-MM-DD');
    var upcomingBookings = [];

    var indexParams =
    {
        TableName: process.env.BOOKING_TABLE,
        IndexName: "VisitorEmail-index",
        KeyConditionExpression: "VisitorEmail = :email",
        ExpressionAttributeValues: {
            ":email": email
        },
    }

    console.log("Search Params ..." + JSON.stringify(indexParams));

    documentClient.query(indexParams, function (err, data) {

        if (err) {
            return callback(err, response(400, JSON.stringify(err)));
        } else {

            if (data.Count > 0) {

                var bookingDetails = data.Items;

                var count = bookingDetails.length;
                console.log("Length .. " + count);

                bookingDetails.forEach(details => {

                    if (details.BookingCancelled != true) {
                        if (details.BookingDate >= currentDate) {
                            upcomingBookings.push(details);
                            count--;
                        } else {
                            count--;
                        }
                    } else {
                        count--;
                    }
                    if (count == 0) {
                        var bookingDetailsArray = [];
                        if (upcomingBookings.length > 0) {
                            const sortedUpcomingBookings = upcomingBookings.sort((a, b) => new Date(a.BookingDate) - new Date(b.BookingDate));
                            //-- If latest upcoming booking is todays date, then check timings. If time > current time then only add that booking
                            if (sortedUpcomingBookings[0].BookingDate == moment(new Date()).format('YYYY-MM-DD')) {
                                if (parseFloat(sortedUpcomingBookings[0].Slot24Hr) < parseFloat(moment(new Date()).format('HH.mm')) + 5.30) {
                                    if (upcomingBookings.length > 1) {
                                        var latestBooking = sortedUpcomingBookings[1];
                                    } else {
                                        var bookingDetailsArray = [];
                                        var respObj = { "message": "No latest upcoming details found.. !", "responseObj": { "upcomingBooking": bookingDetailsArray } }
                                        return callback(null, response(200, JSON.stringify(respObj)));
                                    }

                                } else {
                                    var latestBooking = sortedUpcomingBookings[0];
                                }
                            } else {
                                var latestBooking = sortedUpcomingBookings[0];
                            }
                            //-- Create QR Code for the given details
                            latestBooking.QRCode = "";
                            var bookingIdJSON = { "Id": latestBooking.Id };
                            QRCode.toDataURL(JSON.stringify(bookingIdJSON), function (err, url) {
                                //console.log("data we get " + JSON.stringify(params));
                                if (err) {

                                    return callback(err, response(400, JSON.stringify(respObj)));
                                } else {
                                    sortedUpcomingBookings[0].QRCode = url;
                                    bookingDetailsArray.push(sortedUpcomingBookings[0]);

                                    var respObj = { "message": "Booking details fetched successfully.. !", "responseObj": { "upcomingBooking": bookingDetailsArray } }
                                    return callback(null, response(200, JSON.stringify(respObj)));
                                }
                            })


                        } else {
                            console.log("No upcoming bookings available.")
                            var respObj = { "message": "No upcoming bookings available.", "responseObj": { "upcomingBooking": upcomingBookings } }
                            return callback(null, response(200, JSON.stringify(respObj)));
                        }

                    }
                })
            } else {
                var bookingDetailsArray = [];
                var respObj = { "message": "Booking details not found.. !", "responseObj": { "upcomingBooking": bookingDetailsArray } }
                return callback(null, response(200, JSON.stringify(respObj)));
            }

        }

    })
}