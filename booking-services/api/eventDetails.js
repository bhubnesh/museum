'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
const moment = require('moment');
const _ = require('underscore');
var CryptoJS = require("crypto-js");
const { addEventBookingDetails } = require('./bookAEvent');

var globalEvent = "";

function response(statusCode, message) {

    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Origin;
    }
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Host;
    }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
    }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}

module.exports.getAllEventBookingDetails = function (event, context, callback) {
    debugger;
    globalEvent = event;
    const email = (event.pathParameters.email).toLowerCase();
    if (!email) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }

    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
            console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        } else {
            addEventBookingDetails(event, context, callback);
        }
    }
    validateMultipleLogin();
}
function addEventBookingDetails(event, context, callback) {
    //-- Get upcoming booking details
    const currentDate = moment(new Date()).format('YYYY-MM-DD');
    var upcomingBookings = [];
    var pastBookings = [];
    var params = {
        TableName: process.env.BOOKING_TABLE,
        FilterExpression: "#VisitorEmail = :email",
        ExpressionAttributeNames: {
            "#VisitorEmail": "VisitorEmail",
        },
        ExpressionAttributeValues: { ":email": email }

    };
    console.log("params.. " + JSON.stringify(params));
    documentClient.scan(params, function (err, data) {
        if (err) {
            return callback(err, response(400, JSON.stringify(err)));
        } else {
            console.log("Length .. " + data.Count);

            var bookingDetails = data.Items;
            var count = data.Count;
            if (data.Count > 0) {
                bookingDetails.forEach(details => {
                    console.log("Booking ID -> " + details.Id + "   " + details.BookingDate);
                    console.log("currentDate..." + currentDate);
                    if (details.TypeOfBooking == "Event") {
                        if (details.BookingDate >= currentDate) {

                            var bookingInfo = {
                                "Id": details.Id,
                                "VisitorId": details.VisitorId,
                                "VisitorName": details.VisitorName,
                                "VisitorEmail": details.VisitorEmail,
                                "FeedbackStatus": details.FeedbackStatus,
                                "BookingDate": details.BookingDate,
                                "SlotTime": details.SlotTime,
                                "NoOfAdults": details.NoOfAdults,
                                "NoOfChildren": details.NoOfChildren,
                                "NoOfParticipants": details.NoOfParticipants,
                                "ActualNoOfAdults": (details.ActualNoOfAdults != undefined) ? details.ActualNoOfAdults : details.ActualNoOfAdults,
                                "TotalNoOfParticipantVisited" : (details.TotalNoOfParticipantVisited != undefined) ? details.TotalNoOfParticipantVisited : details.TotalNoOfParticipantVisited,
                                "ActualNoOfChildren" : (details.ActualNoOfChildren != undefined) ? details.ActualNoOfChildren : details.ActualNoOfChildren,
                                "BookingCancelled": details.BookingCancelled,
                                "TypeOfBooking": details.TypeOfBooking,
                                "EventName": details.EventName,
                                "ManageSimulatorSettings": details.ManageSimulatorSettings,
                                "UpdatedBy": details.UpdatedBy,
                                "UpdatedAt": details.UpdatedAt,
                                "CreatedAt": details.CreatedAt
                            }
                            upcomingBookings.push(bookingInfo);
                            count--;
                        } else {
                            var bookingInfo = {
                                "Id": details.Id,
                                "VisitorId": details.VisitorId,
                                "VisitorName": details.VisitorName,
                                "VisitorEmail": details.VisitorEmail,
                                "FeedbackStatus": details.FeedbackStatus,
                                "BookingDate": details.BookingDate,
                                "SlotTime": details.SlotTime,
                                "NoOfAdults": details.NoOfAdults,
                                "NoOfChildren": details.NoOfChildren,
                                "NoOfParticipants": details.NoOfParticipants,
                                "ActualNoOfAdults": (details.ActualNoOfAdults != undefined) ? details.ActualNoOfAdults : details.ActualNoOfAdults,
                                "TotalNoOfParticipantVisited" : (details.TotalNoOfParticipantVisited != undefined) ? details.TotalNoOfParticipantVisited : details.TotalNoOfParticipantVisited,
                                "ActualNoOfChildren" : (details.ActualNoOfChildren != undefined) ? details.ActualNoOfChildren : details.ActualNoOfChildren,
                                "BookingCancelled": details.BookingCancelled,
                                "EventName": details.EventName,
                                "TypeOfBooking": details.TypeOfBooking,
                                "ManageSimulatorSettings": details.ManageSimulatorSettings,
                                "UpdatedBy": details.UpdatedBy,
                                "UpdatedAt": details.UpdatedAt,
                                "CreatedAt": details.CreatedAt
                            }
                            pastBookings.push(bookingInfo);
                            count--;
                        }
                    } else {
                        count--;
                    }

                    if (count == 0) {
                        var bookingDetailsArray = [];
                        const sortedUpcomingBookings = upcomingBookings.sort((a, b) => new Date(a.BookingDate) - new Date(b.BookingDate));
                        const sortedPastBookings = pastBookings.sort((a, b) => new Date(a.BookingDate) - new Date(b.BookingDate));
                        bookingDetailsArray.push(sortedUpcomingBookings);
                        bookingDetailsArray.push(sortedPastBookings);
                        var respObj = { "message": "Booking details fetched successfully.. !", "responseObj": { "upcomingBookings": bookingDetailsArray[0], "pastBookings": bookingDetailsArray[1] } }
                        return callback(null, response(200, JSON.stringify(respObj)));
                    }
                })
            } else {
                console.log("Booking details of given email not found ")
                var respObj = { "message": "Booking details of given email not found " }
                return callback(null, response(200, JSON.stringify(respObj)));
            }

        }

    })

}