'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
let paths = require('../config/apiEndpoints');
const axios = require('axios');
const moment = require('moment');
const _ = require('underscore');
var CryptoJS = require("crypto-js");
var globalEvent = "";

function response(statusCode, message) {

    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Origin;
    }
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Host;
    }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
    }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}


module.exports.getSlotsDataForReschedule = function (event, context, callback) {
    globalEvent = event;
    const requestBody = JSON.parse(event.body);
    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.Email, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);

    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
            console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
    }
    validateMultipleLogin();

    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.Email, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);

    if (!requestBody.Email || !requestBody.Date || !requestBody.TimeHrFrom || !requestBody.TimeHrTo || !requestBody.TimeTypeFrom || !requestBody.TimeTypeTo
        || !requestBody.BookingId) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );

    }
    //-- Booking details based on booking Id
    var searchWithId = {
        TableName: process.env.BOOKING_TABLE,
        ExpressionAttributeValues: {
            ":id": requestBody.BookingId
        },

        KeyConditionExpression: "Id = :id"
    }

    console.log("Search Params ..." + JSON.stringify(searchWithId));

    documentClient.query(searchWithId, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {

            if (data.Count > 0) {
                var bookingInfo = {
                    "NoOfAdults": data.Items[0].NoOfAdults,
                    "NoOfChildren": data.Items[0].NoOfChildren,
                    "AssistanceRequired": data.Items[0].AssistanceRequired,
                    "WheelChairAssistanceRequired": data.Items[0].WheelChairAssistanceRequired,
                    "WCRequiredCount": data.Items[0].WCRequiredCount,
                    "HIAssistanceRequired": data.Items[0].HIAssistanceRequired,
                    "HIHelp": data.Items[0].HIHelp,
                    "VIRequiredCount": data.Items[0].VIRequiredCount
                }
                getBookingConfigs(event, bookingInfo, callback);

            } else {
                console.log("Booking details not available for the given date.")
                var respObj = { "message": "Booking details not available for the given date." }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })


}

function getBookingConfigs(event, bookingInfo, callback) {


    //--call Admin booking configs API
    const bookingConfig_URL = paths.API_URLS.geBookingConfigPath;

    axios.get(bookingConfig_URL, {
        headers: {
            'Authorization': event.headers.Authorization
        }
    })
        .then((res) => {
            if (res.status != 200) {
                return callback(null, response(res.status, res.message));
            }
            // console.log("Booking Configs ... " + JSON.stringify(res.data.responseObj));
            const bookingConfigs = res.data.responseObj;
            if (!bookingConfigs) {
                var respObj = { "message": "Unable to fetch booking configurations." }
                return callback(
                    null,
                    response(404, JSON.stringify(respObj))
                );
            }
            createDataForSlots(event, callback, bookingConfigs, bookingInfo);
        })
        .catch((error) => {
            console.error(error)
        })

}
function createDataForSlots(event, callback, bookingConfigs, bookingInfo) {
    debugger;
    //-- Check whether slots data by given date exists or not
    const requestBody = JSON.parse(event.body);
    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.Email, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);
    //const SlotInterval = bookingConfigs.SlotInterval;
    const SlotInterval = 60;
    const currentTime = moment(new Date).format("HH:mm");
    console.log("Current time ... " + currentTime);
    var toTime = parseFloat(requestBody.TimeHrTo) + (parseInt(requestBody.TimeMinTo)).toFixed(2);
    (requestBody.TimeTypeTo == "pm" || requestBody.TimeTypeTo == "PM") ? toTime = (parseInt(requestBody.TimeHrTo) + 12) : toTime = parseInt(requestBody.TimeHrTo);
    toTime = toTime + (requestBody.TimeMinTo / 100);
    var slots = [];
    var slots24Hr = [];

    var now1 = new Date();
    now1.setHours(parseInt(requestBody.TimeHrTo));
    now1.setMinutes(parseInt(requestBody.TimeMinTo));

    var j = 0;
    var setMins = (parseFloat(requestBody.TimeMinFrom) + j).toFixed(2);

    if (requestBody.TimeTypeTo == "pm" || requestBody.TimeTypeTo == "PM") {
        var hrs = (now1.getHours() + 12);
    } else {
        var hrs = now1.getHours();
    }

    var date1 = new Date();
    date1.setMinutes(setMins);
    date1.setHours(parseInt(requestBody.TimeHrFrom));

    var newDate = new Date();

    while ((date1.getHours()) <= hrs) {
        date1.setMinutes(parseFloat(date1.getMinutes()) + j); // timestamp
        newDate = new Date(date1); // Date object
        var hours = newDate.getHours();
        date1.setHours(hours);
        var minutes = newDate.getMinutes();
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var slot24_hr = hours + "." + minutes;

        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        var hours1 = hours < 10 ? '0' + hours : hours;
        var strTime = hours1 + '.' + minutes + ' ' + ampm;

        var modifiedStartTime = hours1 + ':' + minutes + ' ' + ampm;
        console.log("Time.... " + strTime);
        //-- Check slot is less than toTime
        if (parseFloat(slot24_hr).toFixed(2) < toTime) {

            var date2 = new Date(date1);
            date2.setMinutes(parseFloat(date1.getMinutes()) + j); // timestamp
            newDate = new Date(date2); // Date object
            var hours = newDate.getHours();
            date2.setHours(hours);
            var minutes = newDate.getMinutes();
            minutes = minutes < 10 ? '0' + minutes : minutes;
            // var slot24_hr = hours + "." + minutes;
            //   date2.setMinutes(parseFloat(date1.getMinutes()) + j);
            //  console.log("parseFloat(date2.getHours)" + hours);

            //-- Check slot is less than toTime
            if (slot24_hr <= toTime) {
                slots.push({ "slot": modifiedStartTime, "slot24Hr": slot24_hr });
                slots24Hr.push(slot24_hr);
            }
        }
        j = SlotInterval;
    }

    console.log("Slots... " + slots);
    console.log("Slots in 24 hr... " + slots24Hr);

    if (slots.length > 0) {
        var searchWithDate = {
            TableName: process.env.BOOKINGDATE_TABLE,
            ExpressionAttributeValues: {
                ":date": requestBody.Date
            },

            KeyConditionExpression: "BookingDate = :date"
        }

        documentClient.query(searchWithDate, function (err, data) {
            if (err) {
                console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                return response(400, JSON.stringify(err));
            } else {

                if (data.Count > 0) {
                    //-- call function to validate slots 
                    var arrayOfSlots = data.Items[0].SlotsDetails;

                    verifySlotData(event, callback, bookingConfigs, slots, arrayOfSlots, bookingInfo);

                } else {

                    //-- Call a function to craete an array to store this into DB
                    addSlotsData(event, callback, bookingConfigs, slots, slots24Hr, bookingInfo);
                }
            }
        })
    } else {
        console.log("Slots not found !")
        var respObj = { "message": "Slots not found !" }
        return callback(null, response(404, JSON.stringify(respObj)));

    }


}


function addSlotsData(event, callback, bookingConfigs, slots, slots24Hr, bookingInfo) {

    var reqBody = JSON.parse(event.body);
    var count = slots.length;
    var arrayOfSlots = [];

    slots.forEach(slot => {

        var obj = {};
        obj = {
            "slot": slot.slot,
            "slot24Hr": slot.slot24Hr,
            "maxParticipantsAllowedPerSlot": bookingConfigs.MaximumParticipantsAllowedPerSlot,
            "bookingsDone": 0,
            "bookingsAvailable": bookingConfigs.MaximumParticipantsAllowedPerSlot,
            "WCAMaximumAvailablePerSlot": bookingConfigs.WCAMaximumAvailablePerSlot,
            "WCAvailable": bookingConfigs.WCAMaximumAvailablePerSlot,
            "WCBooked": 0,
            "VIAMaximumAvailablePerSlot": bookingConfigs.VIAMaximumAvailablePerSlot,
            "VIAvailable": bookingConfigs.VIAMaximumAvailablePerSlot,
            "VIBooked": 0,
            "HIAMaximumAvailablePerSlot": bookingConfigs.HIAMaximumAvailablePerSlot,
            "HIAvailable": bookingConfigs.HIAMaximumAvailablePerSlot,
            "HIBooked": 0
        }
        arrayOfSlots.push(obj);
        count--;
        if (count == 0) {
            var slotsParams = {
                Item: {
                    "BookingDate": reqBody.Date,
                    "CreatedBy": event.loggedInUser,
                    "UpdatedBy": event.loggedInUser,
                    "CreatedAt": new Date().toISOString(),
                    "UpdatedAt": new Date().toISOString(),
                    "SlotsDetails": arrayOfSlots,

                },
                TableName: process.env.BOOKINGDATE_TABLE
            }

            documentClient.put(slotsParams, function (err, data) {
                var respObj = { "message": "Slots data added successfully.", "responseObj": slotsParams.Item };
                //return callback(err, response(200, JSON.stringify(respObj)));
                verifySlotData(event, callback, bookingConfigs, slots, arrayOfSlots, bookingInfo);
            })
        }
    });
}




function verifySlotData(event, callback, bookingConfigs, slots, arrayOfSlots, bookingInfo) {
    const requestBody = JSON.parse(event.body);

    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.Email, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);

    const WCAMaximumAllowedPerBooking = bookingConfigs.WCAMaximumAllowedPerBooking; //-- 1
    const VIAMaximumAllowedPerBooking = bookingConfigs.VIAMaximumAllowedPerBooking; //-- 10

    var wcFlag = false;
    var hiFlag = false;
    var viFlag = false;


    var slotsData = [];

    var slotCount = arrayOfSlots.length;
    console.log("Date... " + moment(new Date()).format('YYYY-MM-DD'));
    console.log("Current Time .. " + new Date().getTime);
    console.log("Current time 1 .. " + moment(new Date()).format('HH.mm'));
    //-- Check if Date is current date
    if (requestBody.Date == moment(new Date()).format('YYYY-MM-DD')) {

        arrayOfSlots.forEach(slot => {
            wcFlag = false;
            hiFlag = false;
            viFlag = false;

            var currentTime = moment(new Date()).format('HH.mm');
            currentTime = parseFloat(currentTime) + 5.30

            //-- If slot > current Time
            if (parseFloat(slot.slot24Hr) >= parseFloat(currentTime)) {
                //-- Check whether assistance required
                if (slot.bookingsAvailable >= (bookingInfo.NoOfAdults + bookingInfo.NoOfChildren)) {
                    //--If assistance required
                    if (bookingInfo.AssistanceRequired == true) {
                        if (bookingInfo.VIAssistanceRequired == true) {
                            if (bookingInfo.VIRequiredCount > VIAMaximumAllowedPerBooking) {
                                var respObj = { "message": "VI Required count is more than maximum allowed per booking" }
                                return callback(null, response(404, JSON.stringify(respObj)));
                            } else if (requestBody.VIRequiredCount > slot.VIAvailable) {
                                //-- VI Assistance not available for this slot
                                viFlag = true;
                            }//-- If difference between current time and slot is less than 1 hr, then slot is not available.
                            viFlag = (parseFloat(slot.slot24Hr) >= (parseFloat(currentTime) + 1.00)) ? false : true;
                        }
                        if (bookingInfo.HIAssistanceRequired == true) {
                            if (bookingInfo.HIHelp > slot.HIAvailable) {
                                //-- HI Assistance not available for this slot
                                hiFlag = true;
                            } //-- If difference between current time and slot is less than 1 hr, then slot is not available.
                            hiFlag = (parseFloat(slot.slot24Hr) >= (parseFloat(currentTime) + 1.00)) ? false : true;
                        }

                        if (bookingInfo.WheelChairAssistanceRequired == true) {
                            if (bookingInfo.WCRequiredCount > WCAMaximumAllowedPerBooking) {
                                var respObj = { "message": "WC Required count is more than maximum allowed per booking" }
                                return callback(null, response(404, JSON.stringify(respObj)));
                            } else if (bookingInfo.WCRequiredCount > slot.WCAvailable) {
                                //-- WC Assistance not available for this slot
                                wcFlag = true;

                            }
                        }
                        //--If all the flags are false then only add the slot
                        if (viFlag == false && hiFlag == false && wcFlag == false) {
                            slotsData.push({ "slot": slot.slot, "slot24Hr": slot.slot24Hr, "isAvailable": true });

                        } else {
                            slotsData.push({ "slot": slot.slot, "slot24Hr": slot.slot24Hr, "isAvailable": false });
                        }

                    } else {
                        //-- If assistance not required
                        slotsData.push({ "slot": slot.slot, "slot24Hr": slot.slot24Hr, "isAvailable": true });
                    }

                } else {
                    slotsData.push({ "slot": slot.slot, "slot24Hr": slot.slot24Hr, "isAvailable": false });
                }

            } else {
                slotsData.push({ "slot": slot.slot, "slot24Hr": slot.slot24Hr, "isAvailable": false });
            }
            console.log(slotCount);
            slotCount--;
            if (slotCount == 0) {
                var uniqueArray = _.uniq(slotsData);
                //                getFinalResponse(event, callback, uniqueArray);
                checkMuseumOfflineData(event, callback, uniqueArray);
            }
        })

    } else {
        //-- If required booking date is not current Date
        arrayOfSlots.forEach(slot => {
            wcFlag = false;
            hiFlag = false;
            viFlag = false;

            if (slot.bookingsAvailable >= (bookingInfo.NoOfAdults + bookingInfo.NoOfChildren)) {
                //-- Check whether assistance required

                //--If assistance required
                if (bookingInfo.AssistanceRequired == true) {
                    if (bookingInfo.VIAssistanceRequired == true) {
                        if (bookingInfo.VIRequiredCount > VIAMaximumAllowedPerBooking) {
                            var respObj = { "message": "VI Required count is more than maximum allowed per booking" }
                            return callback(null, response(404, JSON.stringify(respObj)));
                        } else if (bookingInfo.VIRequiredCount > slot.VIAvailable) {
                            //-- VI Assistance not available for this slot
                            viFlag = true;
                        }
                    }
                    if (bookingInfo.HIAssistanceRequired == true) {
                        if (bookingInfo.HIHelp > slot.HIAvailable) {
                            //-- HI Assistance not available for this slot
                            hiFlag = true;
                        }
                    }

                    if (bookingInfo.WheelChairAssistanceRequired == true) {
                        if (bookingInfo.WCRequiredCount > WCAMaximumAllowedPerBooking) {
                            var respObj = { "message": "WC Required count is more than maximum allowed per booking" }
                            return callback(null, response(404, JSON.stringify(respObj)));
                        } else if (bookingInfo.WCRequiredCount > slot.WCAvailable) {
                            //-- WC not available for this slot.
                            wcFlag = true;
                        }
                    }
                    //--If all the flags are false then only add the slot
                    if (viFlag == false && hiFlag == false && wcFlag == false) {
                        slotsData.push({ "slot": slot.slot, "slot24Hr": slot.slot24Hr, "isAvailable": true });
                    } else {
                        slotsData.push({ "slot": slot.slot, "slot24Hr": slot.slot24Hr, "isAvailable": false });
                    }

                } else {
                    //-- If assistance not required
                    slotsData.push({ "slot": slot.slot, "slot24Hr": slot.slot24Hr, "isAvailable": true });
                }

            } else {
                slotsData.push({ "slot": slot.slot, "slot24Hr": slot.slot24Hr, "isAvailable": false });
            }
            console.log(slotCount);
            slotCount--;
            if (slotCount == 0) {
                var uniqueArray = _.uniq(slotsData);
                //                getFinalResponse(event, callback, uniqueArray);
                checkMuseumOfflineData(event, callback, uniqueArray);
            }
        })

    }
}

function checkMuseumOfflineData(event, callback, uniqueArray) {
    //-- check whether given date = museum is partially off 
    const requestBody = JSON.parse(event.body);
    //-- Decrypt Email Id
    var decryptedEmail = CryptoJS.AES.decrypt(requestBody.Email, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.VisitorEmail = decryptedEmail.toString(CryptoJS.enc.Utf8);

    const bookingDate = requestBody.Date;
    var uniqueSlotArray = [];
    var offlineParams = {
        TableName: process.env.MUSEUMOFFLINE_TABLE,
    };

    documentClient.scan(offlineParams, function (err, data) {
        if (err) {
            return callback(err, response(400, JSON.stringify(err)));
        }

        if (data.Count > 0) {
            var museumOfflineArray = data.Items;
            var offlineCount = data.Count;
            museumOfflineArray.forEach(item => {
                //-- if booking date == from date
                //--  if booking date == end date
                //-- if booking date > from date && booking date < to date

                if ((bookingDate == item.DateFrom) || (bookingDate == item.DateTo) || (bookingDate > item.DateFrom && bookingDate < item.DateTo)) {
                    //-- museum fully off == false else calll function getFinalResponse
                    if (item.FullDay == false) {
                        var timeFrom = "";
                        (item.TimeTypeFrom == "pm" || item.TimeTypeFrom == "PM") ? timeFrom = parseInt(item.TimeHrFrom) + 12 : timeFrom = item.TimeHrFrom;
                        timeFrom = timeFrom + "." + item.TimeMinFrom;
                        console.log("Time From ------- " + timeFrom);

                        var timeTo = "";
                        (item.TimeTypeTo == "pm" || item.TimeTypeTo == "PM") ? timeTo = parseInt(item.TimeHrTo) + 12 : timeTo = item.TimeHrTo;
                        timeTo = timeTo + "." + item.TimeMinTo;
                        console.log("Time To ------- " + timeTo);
                        //  2 4         10 2
                        var slotcnt = uniqueSlotArray.length;
                        uniqueArray.forEach(slot => {
                            if (slot.isAvailable == true) {
                                if ((parseFloat(slot.slot24Hr) < timeFrom) && (parseFloat(slot.slot24Hr) < timeTo) ||
                                    ((parseFloat(slot.slot24Hr) > timeFrom) && (parseFloat(slot.slot24Hr) > timeTo))) {

                                    slot.isAvailable = true;
                                    slotcnt--;
                                } else {
                                    slot.isAvailable = false;
                                    slotcnt--;
                                }
                                if (slotcnt == 0) {
                                    getFinalResponse(event, callback, uniqueArray);
                                }
                            }
                        })
                        // getFinalResponse(event, callback, uniqueArray);
                        offlineCount--;
                    }
                } else {
                    offlineCount--;
                }
                if (offlineCount == 0) {
                    getFinalResponse(event, callback, uniqueArray);
                }
            })
        } else {
            getFinalResponse(event, callback, uniqueArray);
        }
    })
}
function getFinalResponse(event, callback, uniqueArray) {
    debugger;
    var morning = [];
    var afternoon = [];
    var evening = [];
    var count = uniqueArray.length;
    uniqueArray.forEach(slot => {
        if (slot.slot24Hr < 12) {
            morning.push({ "slot": slot.slot, "isAvailable": slot.isAvailable, "slot24Hr": slot.slot24Hr });
        } else if (slot.slot24Hr >= 12 && slot.slot24Hr < 17) {
            afternoon.push({ "slot": slot.slot, "isAvailable": slot.isAvailable, "slot24Hr": slot.slot24Hr });
        } else
            evening.push({ "slot": slot.slot, "isAvailable": slot.isAvailable, "slot24Hr": slot.slot24Hr });

        count--;
        if (count == 0) {
            var finalArray = [];
            finalArray.push({ "Morning": morning, "Afternoon": afternoon, "Evening": evening });
            var respObj = { "message": "Slots data fetched successfully.", "responseObj": finalArray[0] }
            return callback(null, response(200, JSON.stringify(respObj)));
        }
    })
}