'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
var CryptoJS = require("crypto-js");
var globalEvent = "";

function response(statusCode, message) {

    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Origin;
    }
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Host;
    }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
    }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}

module.exports.getVisitorBookingHistory = function (event, context, callback) {
    debugger;

    globalEvent = event;
    const email = event.pathParameters.email;
    if (!email) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }

    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
            console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        } else {
            getUserBookingHistory(event, context, callback);
        }
    }
    validateMultipleLogin();
}
function getUserBookingHistory(event, context, callback) {
    globalEvent = event;
    var email = event.pathParameters.email;
    var indexParams =
    {
        TableName: process.env.BOOKING_TABLE,
        IndexName: "VisitorEmail-index",
        KeyConditionExpression: "VisitorEmail = :email",
        ExpressionAttributeValues: {
            ":email": email
        },
    }


    documentClient.query(indexParams, function (err, data) {

        if (err) {
            return callback(err, response(400, JSON.stringify(err)));
        } else {
            if (data.Count > 0) {

                var bookingDetails = data.Items;

                var count = bookingDetails.length;
                var visitorBookings = [];
                var finalData = {};
                bookingDetails.forEach(details => {
                    finalData.VisitorEmail = details.VisitorEmail;
                    finalData.VisitorName = details.VisitorName;
                    details.VisitorPhone ? finalData.VisitorPhone = details.VisitorPhone : finalData.VisitorPhone = "";
                    var bookingInfo = {
                        "Id": details.Id,
                        "VisitorId": details.VisitorId,
                        "VisitorName": details.VisitorName,
                        "VisitorEmail": details.VisitorEmail,
                        "VisitorPhone": details.VisitorPhone,
                        "BookingDate": details.BookingDate,
                        "SlotTime": details.SlotTime,
                        "FeedbackStatus": details.FeedbackStatus,
                        "NoOfAdults": details.NoOfAdults,
                        "NoOfChildren": details.NoOfChildren,
                        "NoOfParticipants": details.NoOfParticipants,
                        "BookingStatus": details.BookingStatus,
                        "TypeOfBooking": details.TypeOfBooking,
                        "WheelChairAssistanceRequired": details.WheelChairAssistanceRequired,
                        "WCRequiredCount": details.WCRequiredCount,
                        "HIAssistanceRequired": details.HIAssistanceRequired,
                        "HIHelp": details.HIHelp,
                        "VIAssistanceRequired": details.VIAssistanceRequired,
                        "VIRequiredCount": details.VIRequiredCount,
                        "ManageSimulatorSettings": details.ManageSimulatorSettings,
                        "SimulatorRequired": details.SimulatorRequired,
                        "UpdatedBy": details.UpdatedBy,
                        "UpdatedAt": details.UpdatedAt,
                        "CreatedAt": details.CreatedAt
                    }
                    visitorBookings.push(bookingInfo);
                    count--;

                    if (count == 0) {
                        const sortedBookings = visitorBookings.sort((a, b) => new Date(a.BookingDate) - new Date(b.BookingDate));
                        finalData.BookingDetails = sortedBookings;
                        var respObj = { "message": "Booking details fetched successfully.. !", "responseObj": finalData };
                        return callback(null, response(200, JSON.stringify(respObj)));
                    }
                })
            } else {
                console.log("Booking details of given email not found ")
                var respObj = { "message": "Booking details of given email not found " }
                return callback(null, response(200, JSON.stringify(respObj)));
            }
        }
    })
}
