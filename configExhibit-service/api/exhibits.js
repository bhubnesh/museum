'use strict';

var AWS = require('aws-sdk'),

  uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient();

const auth = require('../auth/token');

var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}


//start
module.exports.addExhibit = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT Token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      addExhibitDetails(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function addExhibitDetails(event, context, validateTokenResp, callback) {

  var loggedInUser = validateTokenResp.event.loggedInUser;
  const requestBody = JSON.parse(event.body);

  if (!requestBody.ExhibitName) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //-- Get user details from email Id 
  var searchWithEmail = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": loggedInUser
    },
    KeyConditionExpression: "Email = :email"
  }
  documentClient.query(searchWithEmail, function (err, data) {
    if (err) {
      return ({ "statusCode": 400, "message": JSON.stringify(err) });
    } else {
      if (data.Count > 0) {
        var Id = uuid.v1();
        var userDetails = data.Items[0];
        var params = {
          Item: {
            "Id": Id,
            "ExhibitName": requestBody.ExhibitName,
            "ExhibitType": requestBody.ExhibitType,
            "ArtistName": requestBody.ArtistName,
            "MaterialUsed": requestBody.MaterialUsed,
            "Status": "Inactive",
            "Complete": "false",
            "CreatedBy": loggedInUser,
            "CreatedAt": new Date().toISOString()
          },
          TableName: process.env.EXHIBIT_TABLE
        };
        var responseObj = {
          "Id": Id,
          "ExhibitName": requestBody.ExhibitName,
          "ExhibitType": requestBody.ExhibitType,
          "ArtistName": requestBody.ArtistName,
          "MaterialUsed": requestBody.MaterialUsed,
          "Status": "Inactive",
          "Complete": "false",
          "CreatedBy": loggedInUser,
          "CreatedAt": new Date().toISOString()
        }
        var respObj = { "responseObj": responseObj, "message": "Exhibit added successfully..!" }
        documentClient.put(params, function (err, data) {
          return callback(err, response(200, JSON.stringify(respObj)));
        })
      } else {
        return ({ "statusCode": 404, "userData": {}, "message": "Exhibit details not found." })
      }
    }
  })
}
//get All Exhibit
module.exports.getAllExhibit = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token 
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      getAllExhibitDetails(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}

function getAllExhibitDetails(event, context, validateTokenResp, callback) {

  var params = {
    TableName: process.env.EXHIBIT_TABLE,
  }
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    var ExhibitData = data.Items;
    //-- Sort data according to created date time
    const sortedtData = ExhibitData.sort((a, b) => new Date(a.CreatedAt) - new Date(b.CreatedAt));
    var finalResponse = {}
    finalResponse.Items = sortedtData;
    var respObj = { "responseObj": finalResponse, "message": "All Exhibits fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));
  })
}
// get particular employee details By Id
module.exports.getExhibitDetailsById = function (event, context, callback) {
  globalEvent = event;
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      getExhibitInfoById(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function getExhibitInfoById(event, context, validateTokenResp, callback) {
  const id = event.pathParameters.Id;
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params = {
    TableName: process.env.EXHIBIT_TABLE,
    ExpressionAttributeValues: {
      ":Id": id
    },
    KeyConditionExpression: 'Id = :Id ',
  };
  documentClient.query(params, function (err, data) {
    // let data1 = data.filter();
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    } else {
      if (data.Count > 0) {
        var respObj = { "responseObj": data.Items[0], "message": "Answer's fetched successfully." }
        return callback(null, response(200, JSON.stringify(respObj)));
      } else {
        var respObj = { "message": + id + " not found." }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  });
}
//update exhibit
module.exports.updateExhibit = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      //console.log("hello")
      updateExhibitDetails (event, context, validateTokenResp, callback)
    }
  }
  validateMultipleLogin();
}

function updateExhibitDetails(event, context, validateTokenResp, callback) {
  //console.log("In")
  const id = event.pathParameters.Id;
  const requestBody = JSON.parse(event.body);

  if (!id) {
    var respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //-- check whether record with given id exists or not 

  var params1 = {
    TableName: process.env.EXHIBIT_TABLE,
    Key: {
      "Id": id
    },
  };
  documentClient.get(params1, function (err, data) {
    if (err) {
      return response(400, JSON.stringify(err));
    } else {
      if (data.Item) {
        const exhibitData = data.Item;
        //-- Get currently logged in user info
        var loggedInUser = validateTokenResp.event.loggedInUser;
        //-- Get user details from email Id 

        var searchWithEmail = {
          TableName: process.env.VISITOR_TABLE,
          ExpressionAttributeValues: {
            ":email": loggedInUser
          },
          KeyConditionExpression: "Email = :email"
        }
        documentClient.query(searchWithEmail, function (err, data) {
          if (err) {
            return ({ "statusCode": 400, "message": JSON.stringify(err) });
          } else {
            if (data.Count > 0) {
              const userDetails = data.Items[0];
              var params = {
                TableName: process.env.EXHIBIT_TABLE,
                Key: {
                  "Id": id
                },
                UpdateExpression: 'set #ExhibitName = :E,#ExhibitType = :T,#ArtistName = :A,#MaterialUsed=:M,#Status = :S,#Complete = :COM,#CreatedBy = :CB,#CreatedAt = :CA,#UpdatedBy = :U,#UpdatedAt = :UA',
                ExpressionAttributeValues: {
                  ':E': requestBody.ExhibitName,
                  ':T': requestBody.ExhibitType,
                  ':A': requestBody.ArtistName,
                  ':M': requestBody.MaterialUsed,
                  ':S': exhibitData.Status,
                  ':COM': exhibitData.Complete,
                  ':CB': exhibitData.CreatedBy,
                  ':CA': exhibitData.CreatedAt,
                  ':U': loggedInUser,
                  ':UA': new Date().toISOString()
                },
                ExpressionAttributeNames: {
                  '#ExhibitName': 'ExhibitName',
                  '#ExhibitType': 'ExhibitType',
                  '#ArtistName': 'ArtistName',
                  '#MaterialUsed': 'MaterialUsed',
                  '#Status': 'Status',
                  '#Complete': 'Complete',
                  '#CreatedBy': 'CreatedBy',
                  '#CreatedAt': 'CreatedAt',
                  '#UpdatedBy': 'UpdatedBy',
                  '#UpdatedAt': 'UpdatedAt'
                },
                ReturnValues: "UPDATED_NEW"
              };
              const responseObj = {
                "Id": exhibitData.Id,
                "ExhibitName": requestBody.ExhibitName,
                "ExhibitType": requestBody.ExhibitType,
                "ArtistName": requestBody.ArtistName,
                "MaterialUsed": requestBody.MaterialUsed,
                "Status": exhibitData.Status,
                "Complete": exhibitData.Complete,
                "CreatedBy": exhibitData.CreatedBy,
                "UpdatedBy": loggedInUser,
                "CreatedAt": exhibitData.CreatedAt,
                "UpdatedAt": new Date().toISOString()
              }
              documentClient.update(params, function (err, data) {
                if (err) {
                  return callback(err, response(400, err));
                }
                var respObj = { "responseObj": responseObj, "message": "ManageAlert updated successfully..!" }
                return callback(err, response(200, JSON.stringify(respObj)));

              })
            } else {
              var respObj = { "message": "Logged in user details not found" }
              return callback(null, response(404, JSON.stringify(respObj)));
            }
          }
        })
      } else {
        var respObj = { "message": "Alert data with given id not found" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}
//end update
//delete Api's
//Multiple delete
module.exports.deleteMultipleExhibit = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);
  var responseObj = {}
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      deleteMultiExhibitDetails(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function deleteMultiExhibitDetails(event, context, validateTokenResp, callback) {
  var responseObj = {}
  const requestBody = JSON.parse(event.body);
  if (!requestBody.exhibitId) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //get through requestBody
  var exhibitId = requestBody.exhibitId;
  if (exhibitId != null) {
    let cnt = 0;
    exhibitId.forEach(exhibit => {
      cnt++;
      var id = exhibit.id;
      var params = {
        TableName: process.env.EXHIBIT_TABLE,
        Key: {
          "Id": id
        }
      };
      documentClient.get(params, function (err, data) {
        if (err) {
          responseObj[id] = err;
        }
        else if (data && Object.keys(data).length === 0 && data.constructor === Object) {
          responseObj[id] = 'ID not present';
        }
        else {
          var deleteParams = {
            TableName: process.env.EXHIBIT_TABLE,
            Key: {
              "Id": id
            }
          };
          documentClient.delete(deleteParams, function (err, data) {
            if (err) {
              responseObj[id] = err;
            }
            responseObj[id] = 'ID Deleted successfully';
          });
        }
        if (exhibitId.length == cnt) {
          setTimeout(() => {
            return callback(err, response(200, JSON.stringify([responseObj])));
          }, 2000);
        }
      })
    });
  } else {
    var respObj = { "message": "provided details incorrect" }
    return callback(err, response(404, JSON.stringify(respObj)));
  }
}

//delete by Id
module.exports.deleteExhibit = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      deleteExhibitDetails(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function deleteExhibitDetails(event, context, validateTokenResp, callback) {
  const id = event.pathParameters.Id;
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params = {
    TableName: process.env.EXHIBIT_TABLE,
    Key: {
      "Id": id
    }
  };
  documentClient.delete(params, function (err, data) {
    if (err) {
      return callback(err, response(400, err));
    }
    var message = "Exhibit Details of  Id - " + id + " deleted successfully..!"
    var respObj = { "message": message }
    return callback(err, response(200, JSON.stringify(respObj)));
  });
}
//end delete
//change all data complete true.
module.exports.completeTrue = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      completeTrueFunc(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function completeTrueFunc(event, context, validateTokenResp, callback) {
  const id = event.pathParameters.Id;
  if (!id) {
    let respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //-- check whether record with given id exists or not
  let params1 = {
    TableName: process.env.EXHIBIT_TABLE,
    Key: {
      "Id": id
    },
  };
  documentClient.get(params1, function (err, data) {
    if (err) {
      return response(400, JSON.stringify(err));
    } else {
      if (data.Item) {
        const exhibitData = data.Item;
        if (exhibitData.MarkerUrl != "" && exhibitData.MarkerUrl != undefined && exhibitData.MarkerUrl != null || exhibitData.AudioVideo != "" && exhibitData.AudioVideo != undefined && exhibitData.AudioVideo != null) {
          //-- Get currently logged in user info
          let loggedInUser = validateTokenResp.event.loggedInUser;
          //-- Get user details from email Id 

          let searchWithEmail = {
            TableName: process.env.VISITOR_TABLE,
            ExpressionAttributeValues: {
              ":email": loggedInUser
            },
            KeyConditionExpression: "Email = :email"
          }
          documentClient.query(searchWithEmail, function (err, data) {
            if (err) {
              return ({ "statusCode": 400, "message": JSON.stringify(err) });
            } else {
              if (data.Count > 0) {
                const userDetails = data.Items[0];
                let params = {
                  TableName: process.env.EXHIBIT_TABLE,
                  Key: {
                    "Id": id
                  },
                  UpdateExpression: 'set #Complete = :COM,#UpdatedBy = :U,#UpdatedAt = :UA',
                  ExpressionAttributeValues: {
                    ':COM': "true",
                    ':U': loggedInUser,
                    ':UA': new Date().toISOString()
                  },
                  ExpressionAttributeNames: {
                    '#Complete': 'Complete',
                    '#UpdatedBy': 'UpdatedBy',
                    '#UpdatedAt': "UpdatedAt"
                  },
                  ReturnValues: "UPDATED_NEW"
                };
                const responseObj = {
                  "Id": exhibitData.Id,
                  "ExhibitName": exhibitData.ExhibitName,
                  "ExhibitType": exhibitData.ExhibitType,
                  "ArtistName": exhibitData.ArtistName,
                  "MaterialUsed": exhibitData.MaterialUsed,
                  "Status": exhibitData.Status,
                  "Complete": "true",
                  "MarkerUrl": exhibitData.MarkerUrl,
                  "AudioVideo": exhibitData.AudioVideo,
                  "CreatedBy": exhibitData.CreatedBy,
                  "UpdatedBy": loggedInUser,
                  "CreatedAt": exhibitData.CreatedAt,
                  "UpdatedAt": new Date().toISOString()
                }
                documentClient.update(params, function (err, data) {
                  if (err) {
                    return callback(err, response(400, err));
                  }
                  let respObj = { "responseObj": responseObj, "message": " updated successfully..!" }
                  return callback(err, response(200, JSON.stringify(respObj)));
                })
              } else {
                let respObj = { "message": "Logged in user details not found" }
                return callback(null, response(404, JSON.stringify(respObj)));
              }
            }
          })
        } else {
          let respObj = { "message": "data with given id not found ! upload Image/Audio/Video" }
          return callback(null, response(404, JSON.stringify(respObj)));
        }
      } else {
        let respObj = { "message": "Alert data not found" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}
//updating Status Inactive to Active State 
module.exports.makeActiveInactive = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      makeActiveInactiveFunc(event, context, validateTokenResp, callback)
    }
  }
  validateMultipleLogin();
}


function makeActiveInactiveFunc(event, context, validateTokenResp, callback) {
  const id = event.pathParameters.Id;
  if (!id) {
    let respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //-- check whether record with given id exists or not
  let params1 = {
    TableName: process.env.EXHIBIT_TABLE,
    Key: {
      "Id": id
    },
  };
  documentClient.get(params1, function (err, data) {
    if (err) {
      return response(400, JSON.stringify(err));
    } else {
      if (data.Item) {
        const exhibitData = data.Item;
        if (exhibitData.MarkerUrl != "" && exhibitData.MarkerUrl != undefined && exhibitData.MarkerUrl != null || exhibitData.AudioVideo != "" && exhibitData.AudioVideo != undefined && exhibitData.AudioVideo != null) {
          //-- Get currently logged in user info
          let loggedInUser = validateTokenResp.event.loggedInUser;
          //-- Get user details from email Id 

          let searchWithEmail = {
            TableName: process.env.VISITOR_TABLE,
            ExpressionAttributeValues: {
              ":email": loggedInUser
            },
            KeyConditionExpression: "Email = :email"
          }
          documentClient.query(searchWithEmail, function (err, data) {
            if (err) {
              return ({ "statusCode": 400, "message": JSON.stringify(err) });
            } else {
              if (data.Count > 0) {
                const userDetails = data.Items[0];
                if (exhibitData.Status == "Inactive") {
                  let params = {
                    TableName: process.env.EXHIBIT_TABLE,
                    Key: {
                      "Id": id
                    },
                    UpdateExpression: 'set #Status = :S,#UpdatedBy = :U,#UpdatedAt = :UA',
                    ExpressionAttributeValues: {
                      ':S': 'Active',
                      ':U': loggedInUser,
                      ':UA': new Date().toISOString()
                    },
                    ExpressionAttributeNames: {
                      '#Status': 'Status',
                      '#UpdatedBy': 'UpdatedBy',
                      '#UpdatedAt': "UpdatedAt"
                    },
                    ReturnValues: "UPDATED_NEW"
                  };
                  const responseObj = {
                    "Id": exhibitData.Id,
                    "ExhibitName": exhibitData.ExhibitName,
                    "ExhibitType": exhibitData.ExhibitType,
                    "ArtistName": exhibitData.ArtistName,
                    "MaterialUsed": exhibitData.MaterialUsed,
                    "Status": "Active",
                    "Complete": exhibitData.Complete,
                    "MarkerUrl": exhibitData.MarkerUrl,
                    "AudioVideo": exhibitData.AudioVideo,
                    "CreatedBy": exhibitData.CreatedBy,
                    "UpdatedBy": loggedInUser,
                    "CreatedAt": exhibitData.CreatedAt,
                    "UpdatedAt": new Date().toISOString()
                  }
                  documentClient.update(params, function (err, data) {
                    if (err) {
                      return callback(err, response(400, err));
                    }
                    let respObj = { "responseObj": responseObj, "message": " updated successfully..!" }
                    return callback(err, response(200, JSON.stringify(respObj)));
                  })
                } else {
                  let params = {
                    TableName: process.env.EXHIBIT_TABLE,
                    Key: {
                      "Id": id
                    },
                    UpdateExpression: 'set #ExhibitName = :E,#ExhibitType = :T,#ArtistName = :A,#MaterialUsed=:M,#Status = :S,#Complete = :COM,#MarkerUrl = :MAR,#AudioVideo = :AV,#CreatedBy = :CB,#CreatedAt = :CA,#UpdatedBy = :U,#UpdatedAt = :UA',
                    ExpressionAttributeValues: {
                      ':S': 'Inactive',
                      ':U': loggedInUser,
                      ':UA': new Date().toISOString()
                    },
                    ExpressionAttributeNames: {
                      '#Status': 'Status',
                      '#UpdatedBy': 'UpdatedBy',
                      '#UpdatedAt': "UpdatedAt"
                    },
                    ReturnValues: "UPDATED_NEW"
                  };
                  const responseObj = {
                    "Id": exhibitData.Id,
                    "ExhibitName": exhibitData.ExhibitName,
                    "ExhibitType": exhibitData.ExhibitType,
                    "ArtistName": exhibitData.ArtistName,
                    "MaterialUsed": exhibitData.MaterialUsed,
                    "Status": "Inactive",
                    "Complete": exhibitData.Complete,
                    "MarkerUrl": exhibitData.MarkerUrl,
                    "AudioVideo": exhibitData.AudioVideo,
                    "CreatedBy": exhibitData.CreatedBy,
                    "UpdatedBy": loggedInUser,
                    "CreatedAt": exhibitData.CreatedAt,
                    "UpdatedAt": new Date().toISOString()
                  }
                  documentClient.update(params, function (err, data) {
                    if (err) {
                      return callback(err, response(400, err));
                    }
                    let respObj = { "responseObj": responseObj, "message": " updated successfully..!" }
                    return callback(err, response(200, JSON.stringify(respObj)));
                  })
                }
              } else {
                let respObj = { "message": "Logged in user details not found" }
                return callback(null, response(404, JSON.stringify(respObj)));
              }
            }
          })
        } else {
          let respObj = { "message": "Update All Fields" }
          return callback(null, response(404, JSON.stringify(respObj)));
        }
      } else {
        let respObj = { "message": "Alert data not found" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}
//End
//cheking and get only Status :Active and complete : true case
module.exports.getExhibitByTrueValue = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token 
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      getExhibitByTrueValueFunc(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function getExhibitByTrueValueFunc(event, context, validateTokenResp, callback) {
  var params = {
    TableName: process.env.EXHIBIT_TABLE,
    FilterExpression: '#Status =  :status AND #Complete = :complete',
    ExpressionAttributeNames: {
      "#Status": "Status",
      "#Complete": "Complete"
    },
    ExpressionAttributeValues: {
      ':complete': "true",
      ':status': "Active"
    },
    KeyConditionExpression: 'Complete = :complete AND Status =  :status',

  };
  documentClient.scan(params, function (err, data) {
    if (err) {

      return callback(err, response(400, JSON.stringify(err)));
    }
    var respObj = { "responseObj": data, "message": "All Exhibits fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));
  });
}
//end