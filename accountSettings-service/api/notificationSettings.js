'use strict';

var AWS = require('aws-sdk'),
    documentClient = new AWS.DynamoDB.DocumentClient();
var auth = require('../auth/token');

var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}


module.exports.changeSettings = function (event, context, callback) {
    globalEvent = event;
    const email = event.pathParameters.Email;

    const requestBody = JSON.parse(event.body);

    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();




    if (!requestBody.Notifications || !email) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }


    //-- Check whether settings already exists

    var params1 = {
        TableName: process.env.SETTINGS_TABLE,
        Key: {
            "Email": email
        }
    }
    documentClient.get(params1, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            if (data.Item) {
                console.log("Settings of EmailId - " + email + " already exists.");

                var respObj = { "message": "Notification settings updated successfully" }
                //-- Update content
                var updateParams = {
                    TableName: process.env.SETTINGS_TABLE,
                    Key: {
                        "Email": email
                    },
                    UpdateExpression: 'set #Notifications = :N,#EmailNotifications = :E,#SMSNotifications = :S , #PushNotifications = :P, #UpdatedAt = :UA',
                    ExpressionAttributeValues: {
                        ':N': requestBody.Notifications,
                        ':E': requestBody.EmailNotifications,
                        ':S': requestBody.SMSNotifications,
                        ':P': requestBody.PushNotifications,
                        ':UA': new Date().toISOString()
                    },
                    ExpressionAttributeNames: {
                        '#Notifications': 'Notifications',
                        '#EmailNotifications': 'EmailNotifications',
                        '#SMSNotifications': 'SMSNotifications',
                        '#PushNotifications': 'PushNotifications',
                        "#UpdatedAt": "UpdatedAt"

                    },
                    ReturnValues: "UPDATED_NEW"
                };

                documentClient.update(updateParams, function (err, data) {
                    if (err) {
                        console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                        return callback(err, response(400, err));
                    }
                    console.log(data);
                    var respObj = { "message": "Settings updated successfully..!" }
                    return callback(err, response(200, JSON.stringify(respObj)));
                });

            } else {
                var params = {
                    Item: {
                        "Email": email,
                        "Notifications": requestBody.Notifications,
                        "EmailNotifications": requestBody.EmailNotifications,
                        "SMSNotifications": requestBody.SMSNotifications,
                        "PushNotifications": requestBody.PushNotifications,
                        "UpdatedAt": new Date().toISOString(),
                        "CreatedAt": new Date().toISOString(),
                    },
                    TableName: process.env.SETTINGS_TABLE
                };
                console.log(params);

                var respObj = { "message": "Settings created successfully..!" }
                documentClient.put(params, function (err, data) {
                    return callback(err, response(200, JSON.stringify(respObj)));
                });
            }
        }
    })
};
module.exports.getSettings = function (event, context, callback) {
    globalEvent = event;
    const email = event.pathParameters.Email;



    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();



    if (!email) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }

    //-- Check whether settings already exists

    var params1 = {
        TableName: process.env.SETTINGS_TABLE,
        ExpressionAttributeValues: {
            ":email": email
        },
        KeyConditionExpression: 'Email = :email ',
    }

    console.log("params.. " + JSON.stringify(params1));
    documentClient.query(params1, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return callback(err, response(400, JSON.stringify(err)));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            console.log("count of records ... " + data.Count)
            if (data.Count > 0) {

                var respObj = { "responseObj": data.Items[0], "message": "Settings data fetched successfully." }
                return callback(null, response(200, JSON.stringify(respObj)));
            }
            else {
                var respObj = { "message": "Settings with emailId- " + email + " not found." }
                return callback(null, response(200, JSON.stringify(respObj)));
            }
        }
    })

}
