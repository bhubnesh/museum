'use strict';

const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
var CryptoJS = require("crypto-js");
var atob = require('atob');
var globalEvent = "";

function response(statusCode, message) {

    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Origin;
    }
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Host;
    }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com', 'http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
    }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}


module.exports.reGenerateJWT = function (event, context, callback) {
    globalEvent = event;
    const email = event.pathParameters.Email;

    if (!email) {
        var respObj = { "message": "Mandatory input params are missing" }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }



    var params = {
        TableName: process.env.VISITOR_TABLE,
        ExpressionAttributeValues: {
            ":email": email
        },
        KeyConditionExpression: 'Email = :email ',
    };
    console.log("params.. " + JSON.stringify(params));
    documentClient.query(params, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            console.log("count of records ... " + data.Count)
            if (data.Count > 0) {
                var jwtToken = auth.generateJwtToken(email);
                if (jwtToken == null) {
                    var respObj = { "message": "Error creating JWT token." }
                    return callback(
                        null,
                        response(400, JSON.stringify(respObj))
                    );
                }
                // Decrypt JWT Token
                var bytes = CryptoJS.AES.decrypt(jwtToken, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
                var token = bytes.toString(CryptoJS.enc.Utf8);

                var base64Url = atob(token.split('.')[1]);

                //-- Extract session Id from token
                var sessionId = JSON.parse(base64Url).sessionId;
                console.log("sessionId ----- " + sessionId);
                //-- Store this session Id into DB
                var params = {
                    TableName: process.env.VISITOR_TABLE,
                    Key: {
                        "Email": data.Items[0].Email,
                        "Phone": data.Items[0].Phone
                    },

                    UpdateExpression: 'set #sessionId = :sessionId',
                    ExpressionAttributeValues: {
                        ':sessionId': sessionId
                    },
                    ExpressionAttributeNames: {
                        '#sessionId': 'SessionId'

                    },
                    ReturnValues: "UPDATED_NEW"
                };
                documentClient.update(params, function (err, data) {
                    if (err) {
                        return callback(err, response(400, err));
                    } else {

                        var respObj = { "message": "Token regenerated successfully", "JwtToken": jwtToken }
                        return callback(null, response(200, JSON.stringify(respObj)));


                    }
                })
            }
            else {
                console.log("Visitor not found");
                var respObj = { "message": "Visitor with email Id " + email + " not found." }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })
}

