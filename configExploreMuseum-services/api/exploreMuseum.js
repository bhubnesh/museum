'use strict';

var AWS = require('aws-sdk'),

  uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient();

const auth = require('../auth/token');

var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}



module.exports.addExploreMuseum = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      //console.log("hello")
      addExploreMuseumDetails (event, context, validateTokenResp, callback)
    }
  }
  validateMultipleLogin();
}
function addExploreMuseumDetails(event, context, validateTokenResp, callback) {
  //console.log("event+++++++++++++",event);
  let loggedInUser = event.loggedInUser;
  const requestBody = JSON.parse(event.body);

  if (!requestBody.Name) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  //-- Get user details from email Id 
  let searchWithEmail = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": loggedInUser
    },
    KeyConditionExpression: "Email = :email"
  }

  documentClient.query(searchWithEmail, function (err, data) {
    if (err) {
      //console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return ({ "statusCode": 400, "message": JSON.stringify(err) });
    } else {
      //console.log("GetItem succeeded:", JSON.stringify(data));
      //console.log("count of records fro email ... " + data.Count)
      if (data.Count > 0) {
        //console.log("Full User Object ... " + JSON.stringify(data.Items[0]));

        let Id = uuid.v1();
        let userDetails = data.Items[0];
        let params = {
          Item: {
            "Id": Id,
            "Name": requestBody.Name,
            "Type": requestBody.Type,
            "Description": requestBody.Description,
            "Complete": "false",
            "CreatedBy": userDetails.Name,
            "UpdatedBy": userDetails.Name,
            "CreatedAt": new Date().toISOString(),
            "UpdatedAt": new Date().toISOString()

          },
          TableName: process.env.CEXPLOREMUSEUM_TABLE
        };
        //console.log(params);

        documentClient.put(params, function (err, data) {
          let responseObj = {
            "Id": Id,
            "Name": requestBody.Name,
            "Type": requestBody.Type,
            "Description": requestBody.Description
          }
          //console.log("responseObj", responseObj)
          let respObj = { "responseObj": responseObj, "message": "ExploreMuseum file added successfully..!" }
          //console.log("check", respObj)
          return callback(err, response(200, JSON.stringify(respObj)));

        })
      } else {
        return ({ "statusCode": 404, "userData": {}, "message": "User details not found." })
      }
    }
  })

}

//-- Delete ExploreMuseum code 
module.exports.deleteExploreMuseum = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT Token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  const id = event.pathParameters.Id;
  if (!id) {
    let respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  let params = {
    TableName: process.env.CEXPLOREMUSEUM_TABLE,
    Key: {
      "Id": id
    }
  };
  //console.log("params.. " + JSON.stringify(params));
  documentClient.delete(params, function (err, data) {
    if (err) {
      return callback(err, response(400, err));
    }
    //console.log(data);
    let respObj = { "message": "ExploreMuseum Details Deleted successfully..!" }
    return callback(err, response(200, JSON.stringify(respObj)));
  });
}


// update the ExploreMuseum Details

module.exports.updateExploreMuseum = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      //console.log("hello")
        updateExploreMuseumDetails (event, context, validateTokenResp, callback)
    }
  }
  validateMultipleLogin();
}
function updateExploreMuseumDetails(event, context, validateTokenResp, callback) {

    const id = event.pathParameters.Id;
    const requestBody = JSON.parse(event.body);
  if (!id) {
    let respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //-- check whether record with given id exists or not 

  let params1 = {
    TableName: process.env.CEXPLOREMUSEUM_TABLE,
    Key: {
      "Id": id
    },
  };
  //console.log("search params.. " + JSON.stringify(params1));
  documentClient.get(params1, function (err, data) {
    if (err) {
      //console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
      //console.log("GetItem succeeded:", JSON.stringify(data));
      if (data.Item) {
        //console.log("Alert data with given id exists.");
        const museumData = data.Item;
         console.log("***************",data.Item)
        //-- Get currently logged in user info
        let loggedInUser = event.loggedInUser;
        //-- Get user details from email Id 

        let searchWithEmail = {
          TableName: process.env.VISITOR_TABLE,
          ExpressionAttributeValues: {
            ":email": loggedInUser
          },
          KeyConditionExpression: "Email = :email"
        }
        //console.log("Search Params ..." + JSON.stringify(searchWithEmail));

        documentClient.query(searchWithEmail, function (err, data) {
          if (err) {
            //console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return ({ "statusCode": 400, "message": JSON.stringify(err) });
          } else {
            //console.log("count of records fro email ... " + data.Count)
            if (data.Count > 0) {
              //console.log("Full User Object ... " + JSON.stringify(data.Items[0]));
              const userDetails = data.Items[0];
              let params = {
                TableName: process.env.CEXPLOREMUSEUM_TABLE,
                Key: {
                  "Id": id
                },
                UpdateExpression: 'set #Type = :D,#Name = :N,#Complete = :C,#Description = :DES,#UpdatedBy = :U,#UpdatedAt = :UA',
                ExpressionAttributeValues: {
                  ':D': requestBody.Type,
                  ':N': requestBody.Name,
                  ':DES': requestBody.Description,
                  ':C': museumData.Complete,
                  ':U': userDetails.Name,
                  ':UA': new Date().toISOString()
                },
                ExpressionAttributeNames: {
                  '#Type': 'Type',
                  '#Name': 'Name',
                  '#Description': 'Description',
                  '#Complete': 'Complete',
                  '#UpdatedBy': 'UpdatedBy',
                  "#UpdatedAt": "UpdatedAt"
                },
                ReturnValues: "UPDATED_NEW"
              };

              //console.log("params.. " + JSON.stringify(params));
              documentClient.update(params, function (err, data) {
                if (err) {
                  return callback(err, response(400, err));
                }
                //console.log(data);
                let responseObj = {
                  "Id": museumData.Id,
                  "Name": requestBody.Name,
                  "Type": requestBody.Type,
                  "Description": requestBody.Description,
                  "Complete": museumData.Complete,
                  "CreatedBy": museumData.CreatedBy,
                  "UpdatedBy": userDetails.Name,
                  "CreatedAt": museumData.CreatedAt,
                  "UpdatedAt": new Date().toISOString(),
                }
                let respObj = { "responseObj": responseObj, "message": "ExploreMuseum's Details updated successfully..!" }
                return callback(err, response(200, JSON.stringify(respObj)));
              })
            } else {
              //console.log("Logged in user details not found");
              let respObj = { "message": "Logged in user details not found" }
              return callback(null, response(JSON.stringify(respObj)));
            }
          }
        })
      } else {
        //console.log("Data with given id not found");
        let respObj = { "message": "Data with given id not found" }
        return callback(null, response(JSON.stringify(respObj)));
      }
    }
  })
}

// get  the AllExploreMuseum Details

module.exports.getAllExploreMuseum = function (event, context, callback) {

  globalEvent = event;
  let params = {
    TableName: process.env.CEXPLOREMUSEUM_TABLE,
  }
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    //console.log(data);
    let respObj = { "responseObj": data, "message": "ExploreMuseum data fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));

  })
}

// get particular ExploreMuseum details 

module.exports.getExploreMuseum = function (event, context, callback) {
  globalEvent = event;
  const id = event.pathParameters.Id;
  //console.log("Id  check ", id)
  if (!id) {
    let respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  let params = {
    TableName: process.env.CEXPLOREMUSEUM_TABLE,
    ExpressionAttributeValues: {
      ":explore": id
    },
    KeyConditionExpression: 'Id = :explore ',
  };
  //console.log("params.. " + JSON.stringify(params));
  documentClient.query(params, function (err, data) {
    if (err) {
      //console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return callback(err, response(400, JSON.stringify(err)));
    } else {
      //console.log("GetItem succeeded:", JSON.stringify(data));
      //console.log("count of records ... " + data.Count)
      if (data.Count > 0) {
        //if(imageUrl !=" ", imageUrl!= undefined)
        let respObj = { "responseObj": data.Items[0], "message": "Explore Museum details fetched successfully." }
        return callback(null, response(200, JSON.stringify(respObj)));
      } else {
        let respObj = { "message": "Explore Museum with type- " + id + " not found." }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}

//-- Delete multiple Explore Museum code 
module.exports.deleteMultipleExploreMuseum = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);
  //console.log("Req body .. " + JSON.stringify(requestBody))
  if (!requestBody.listOfExploreMuseum) {
    let respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  let listOfExploreMuseum = requestBody.listOfExploreMuseum;
  //console.log("size of List of ExploreMuseum .. " + listOfExploreMuseum.length);
  let count = listOfExploreMuseum.length;
  if (listOfExploreMuseum.length > 0) {

    listOfExploreMuseum.forEach(museum => {
      let id = museum.id;
      let params = {
        TableName: process.env.CEXPLOREMUSEUM_TABLE,
        Key: {
          "Id": id
        }
      };
      console.log("params.. " + JSON.stringify(params));
      documentClient.delete(params, function (err, data) {
        if (err) {
          return callback(err, response(400, err));
        }
        //console.log(data);
        let message = "Explore Museum of Id - " + id + " deleted successfully..!"
        //console.log(message + count);
        count--;
        if (count == 0) {
          let respObj = { "message": "Explore Museum with given ids deleted successfully.. !" }
          return callback(err, response(200, JSON.stringify(respObj)));
        }
      });
    });
  } else {
    let respObj = { "message": "list of Explore Museum ids to be deleted not found." }
    return callback(err, response(404, JSON.stringify(respObj)));
  }
}
//change all data complete true.
module.exports.submittedData = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();

  const id = event.pathParameters.Id;
  if (!id) {
    let respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //-- check whether record with given id exists or not
  let params1 = {
    TableName: process.env.CEXPLOREMUSEUM_TABLE,
    Key: {
      "Id": id
    },
  };
  //console.log("search params.. " + JSON.stringify(params1));
  documentClient.get(params1, function (err, data) {
    if (err) {
      return response(400, JSON.stringify(err));
    } else {
      if (data.Item) {
        const exploreData = data.Item;
        //console.log("checking", exploreData);
        if (exploreData.imageUrl != "" && exploreData.imageUrl != undefined && exploreData.imageUrl != null || exploreData.thumbnailUrl != "" && exploreData.thumbnailUrl == undefined && exploreData.thumbnailUrl == null) {
          //-- Get currently logged in user info
          let loggedInUser = event.loggedInUser;
          //-- Get user details from email Id 

          let searchWithEmail = {
            TableName: process.env.VISITOR_TABLE,
            ExpressionAttributeValues: {
              ":email": loggedInUser
            },
            KeyConditionExpression: "Email = :email"
          }
          //console.log("Search Params ..." + JSON.stringify(searchWithEmail));
          documentClient.query(searchWithEmail, function (err, data) {
            if (err) {
              return ({ "statusCode": 400, "message": JSON.stringify(err) });
            } else {
              if (data.Count > 0) {
                const userDetails = data.Items[0];
                let params = {
                  TableName: process.env.CEXPLOREMUSEUM_TABLE,
                  Key: {
                    "Id": id
                  },
                  UpdateExpression: 'set #Complete = :C,#UpdatedBy = :UB,#UpdatedAt = :UA',
                  ExpressionAttributeValues: {
                    ':C': "true",
                    ':UB': userDetails.Name,
                    ':UA': new Date().toISOString()
                  },
                  ExpressionAttributeNames: {
                    '#Complete': 'Complete',
                    '#UpdatedBy': 'UpdateBy',
                    '#UpdatedAt': 'UpdatedAt'
                  },
                  ReturnValues: "UPDATED_NEW"
                };
                const responseObj = {
                  "Id": exploreData.Id,
                  "Name": exploreData.Name,
                  "Type": exploreData.Type,
                  "Description": exploreData.Description,
                  "Complete": "true",
                  "imageUrl": exploreData.imageUrl,
                  "CreatedBy": exploreData.CreatedBy,
                  "CreatedAt": exploreData.CreatedAt,
                  "UpdatedBy": userDetails.Name,
                  "UpdatedAt": new Date().toISOString(),
                }
                //console.log("update params .. " + JSON.stringify(params));
                documentClient.update(params, function (err, data) {
                  if (err) {
                    return callback(err, response(400, err));
                  }
                  //console.log(data);
                  let respObj = { "responseObj": responseObj, "message": " updated successfully..!" }
                  return callback(err, response(200, JSON.stringify(respObj)));
                })
              } else {
                //console.log("Logged in user details not found");
                let respObj = { "message": "Logged in user details not found" }
                return callback(null, response(404, JSON.stringify(respObj)));
              }
            }
          })
        } else {
          //console.log("data with given id not found ! Please upload Image/Audio/Video");
          let respObj = { "message": "data with given id not found ! upload Image/Audio/Video" }
          return callback(null, response(404, JSON.stringify(respObj)));
        }
      } else {
        let respObj = { "message": "Alert data not found" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}
//Only return true value
module.exports.getCompleteTrueValue = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token 
  // let validateToken = auth.validateToken(event, context, callback);
  // //console.log("token inside ... " + validateToken);
  // if (validateToken.statusCode != 200) {
  //   return callback(null, response(validateToken.statusCode, JSON.stringify(validateToken)));
  // }
  let params = {
    TableName: process.env.CEXPLOREMUSEUM_TABLE,
    FilterExpression: '#Complete = :complete',
    ExpressionAttributeNames: {
      "#Complete": "Complete"
    },
    ExpressionAttributeValues: {
      ':complete': "true",
    },
    KeyConditionExpression: 'Complete = :complete',

  };
  //console.log("params :: ", params)
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    //console.log("value :: ", data)
    let responseObj = data;
    let respObj = { "responseObj": responseObj, "message": "All fields fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));
  });
}