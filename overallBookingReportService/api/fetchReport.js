'use strict';
const { SNS } = require('aws-sdk');
var AWS = require('aws-sdk');
const auth = require('../auth/token');
let paths = require('../config/config');
const axios = require('axios');
var documentClient = new AWS.DynamoDB.DocumentClient({ convertEmptyValues: true });

var globalEvent = "";

function response(statusCode, message) {
    
  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if(origin == ""|| origin == undefined || !origin){
     origin = globalEvent.headers.Origin;
  }
  if(origin == ""|| origin == undefined || !origin){
    origin = globalEvent.headers.Host;
 }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
      originsAllowed = origin;
 }

  return {
      statusCode: statusCode,
      'headers': {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer authToke',
          'Access-Control-Allow-Credentials': true,
          'Access-Control-Allow-Origin' : originsAllowed,
          'X-XSS-Protection': '1; mode=block',
          'X-Frame-Options': 'SAMEORIGIN',
          'X-Content-Type-Options': 'nosniff',
          'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

      },
      body: message
  };
}

module.exports.sendReport =  function (event, context, callback) {
  globalEvent = event;
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    // console.log("Response.... "+ JSON.stringify(validateTokenResp));
    if (validateTokenResp.statusCode != 200) {
      console.log("Response .in if............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      //console.log("Response . in else............. " + JSON.stringify(validateTokenResp));
      getsendReport(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
async function getsendReport(event, context, validateTokenResp, callback){
  var configBoking={
    TableName: process.env.CONFIGBOOKING_TABLE,
  }

var configData = await documentClient.scan(configBoking).promise();
console.log("hi getsendReport in configData :::::",configData)
var Simulator = configData.Items[0].ManageSimulatorSettings;
  var params = {
    TableName: process.env.REPORTSCOUNT_TABLE,
    KeyConditionExpression: "CountName =:CountName",
    ExpressionAttributeValues: {
      ":CountName": "FeedbackBookingMonthly"
    }

  }
  // Check if record present
  var data = await documentClient.query(params).promise();

  var element = data.Items;
  var star_1 = 0;

  var star_2 = 0;

  var star_3 = 0;

  var star_4 = 0;

  var star_5 = 0;
  let ratings = new Map();
  for (const record of element) {

    var starValue = record.recordDate;
    var hitCount = record.hitCount;
    starValue = starValue.charAt(8);

    // console.log("hitCount ", hitCount)
    switch (starValue) {
      case "1":
        // console.log("starValue ", starValue)
        star_1 = star_1 + hitCount;
        ratings.set("star_1", star_1)
        break;
      case "2":
        // console.log("starValue ", starValue)
        star_2 = star_2 + hitCount;
        ratings.set("star_2", star_2)
        break;
      case "3":
        // console.log("starValue ", starValue)
        star_3 = star_3 + hitCount;
        ratings.set("star_3", star_3)
        break;
      case "4":
        // console.log("starValue ", starValue)
        star_4 = star_4 + hitCount;
        ratings.set("star_4", star_4)
        break;
      case "5":
        // console.log("starValue ", starValue)
        star_5 = star_5 + hitCount;
        ratings.set("star_5", star_5)
    }

  }

  var responseArray = [];
  ratings.forEach((rating, value) => {
    // console.log(`${rating}: ${value}`);
    // console.log(rating, value);
    var obj = {
      name: `${value}`,
      value: parseInt(rating)
    };
    responseArray.push(obj);
  }
  );


   console.log("responseArray", responseArray)


  var params = {
    TableName: process.env.REPORTSCOUNT_TABLE,
    KeyConditionExpression: "CountName =:CountName",
    ExpressionAttributeValues: {
      ":CountName": "BookingMonthly"
    }

  }
  // Check if record present
  var data = await documentClient.query(params).promise();

  var element = data.Items;

  var totalNoOfBooking = 0;
  for (const record of element) {
    // console.log(totalNoOfBooking, record.hitCount)
    if (typeof record.hitCount != "undefined") {
      totalNoOfBooking = totalNoOfBooking + parseInt(record.hitCount);
    }
  }

  var params = {
    TableName: process.env.REPORTSCOUNT_TABLE,
    KeyConditionExpression: "CountName =:CountName",
    ExpressionAttributeValues: {
      ":CountName": "CheckInCountMonthly"
    }

  }
  // Check if record present
  var data = await documentClient.query(params).promise();

  var element = data.Items;
  // console.log("data ", data)

  var totalNoOfParticipantVisited = 0;
  for (const record of element) {
    // console.log(totalNoOfBooking, record.TotalNoOfParticipantVisited)
    if (typeof record.TotalNoOfParticipantVisited != "undefined") {
      totalNoOfParticipantVisited = totalNoOfParticipantVisited + parseInt(record.TotalNoOfParticipantVisited);
    }
  }
  // console.log("star_5 :", star_5, " star_4 :", star_4, " star_3", star_3, " star_2", star_2, " star_1", star_1, " totalNoOfBooking:", totalNoOfBooking, "totalNoOfParticipantVisited ", totalNoOfParticipantVisited)
      var responseObj = {
        responseArray,
        "totalNoOfBooking": totalNoOfBooking,
        "totalNoOfParticipantVisited": totalNoOfParticipantVisited,
        "Simulator is currently in use ":Simulator
      }
  var respObj = { "responseObj": responseObj, "message": "Feedback &counts Report till data fetched successfully..!" }
  //return response(200, JSON.stringify(respObj));
  console.log("message :::::",respObj)
  return callback(null, response(200, JSON.stringify(respObj)));
};
