'use strict';

var AWS = require('aws-sdk'),
  documentClient = new AWS.DynamoDB.DocumentClient();
var moment = require('moment');


module.exports.hitCount = async event => {
  // var requestBody = JSON.parse(event.body);
  console.log("Only event ", JSON.stringify(event))
  console.log("event ", JSON.stringify(event.Records[0].dynamodb))
  var rec = event.Records[0]
    let records = [];
    records.push(rec)
    // records.push(requestBody)
    for (const record of records) {
        var TypeOfBooking = record.dynamodb.NewImage.TypeOfBooking.S;
        var BookingStatus = record.dynamodb.NewImage.BookingStatus.S;
        //console.log("BookingStatus",BookingStatus)
        var count = 0;
        var date = new Date();
        var todaysDate = moment(date).format('YYYY-MM-DD')
        if (BookingStatus == "CheckedIn") {
          // var NoOfParticipants = record.dynamodb.NewImage.NoOfParticipants.N
          //console.log("NoOfParticipants",NoOfParticipants)
          var TotalNoOfParticipantVisited = record.dynamodb.NewImage.TotalNoOfParticipantVisited.N
          //console.log("TotalNoOfParticipantVisited", TotalNoOfParticipantVisited)
          var params = {
            TableName: process.env.REPORTSCOUNT_TABLE,
            Key: {
              "CountName": "CheckedInCountDaily",
              "recordDate": todaysDate + "_" + BookingStatus,
            }
          }
          // console.log("params ", params)
          // Check if record present
          var data = await documentClient.get(params).promise()
          var element = data.Item;
          // if yes update by +1
          if (typeof element != "undefined" && element != "{}") {
            console.log("inside update count")
            //count = element.Count + 1
            var updateParams = {
              TableName: process.env.REPORTSCOUNT_TABLE,
              Key: {
                "CountName": "CheckedInCountDaily",
                "recordDate": element.recordDate,
              },
              UpdateExpression: "set checkInCount=checkInCount+:val,TotalNoOfParticipantVisited=TotalNoOfParticipantVisited+:TNOP",
              ExpressionAttributeValues: {
                ":val": 1,
                ":TNOP":parseInt(TotalNoOfParticipantVisited),
              },
              /// ReturnValues: "UPDATED_NEW"
            }
            // console.log(updateParams)
            var res = await documentClient.update(updateParams).promise();
            console.log(" IN UPDATING ", res)
            var respObj ={"message":"Complete count updated successfully "}
            return response(200, JSON.stringify(respObj));
          } else {
            // if no, insert
            count = 1;
            var updateParams = {
              TableName: process.env.REPORTSCOUNT_TABLE,
              Item: {
                "CountName": "CheckedInCountDaily",
                "recordDate": todaysDate + "_" + BookingStatus,
                "checkInCount": count,
                "TotalNoOfParticipantVisited": parseInt(TotalNoOfParticipantVisited),
              }
            }
            if(TypeOfBooking.toLowerCase() == "event"){
              var eventId = record.dynamodb.NewImage.EventId.S;
              updateParams.Item.recordDate = eventId
             }
            //console.log("TotalNoOfParticipantVisited",TotalNoOfParticipantVisited)
            var data = await documentClient.put(updateParams).promise()
            console.log(" IN INSERTING ",data)
            var respObj ={"message":"Complete count added successfully "}
            return response(200, JSON.stringify(respObj));
          }
        }
      //}
    }
};
function response(statusCode, message) {
  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      'X-XSS-Protection' : '1; mode=block',
       'x-frame-options': 'SAMEORIGIN',
      'x-content-type': 'nosniff',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'
    },
    body: message
  };
}
