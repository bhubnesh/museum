'use strict';

var AWS = require('aws-sdk'),
    documentClient = new AWS.DynamoDB.DocumentClient();
    const auth = require('../auth/token');
    var CryptoJS = require("crypto-js");

var globalEvent = "";


function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}

module.exports.addContactDetails = function (event, context, callback) {
    globalEvent = event;
    const requestBody = JSON.parse(event.body);
    
    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();


    if (!requestBody.Address) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }
    //-- Decrypt Email Id
    var decryptedEmail1 = CryptoJS.AES.decrypt(requestBody.Email1, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.Email1 = decryptedEmail1.toString(CryptoJS.enc.Utf8);

    //-- Decrypt Phone
    var decryptedEmail2 = CryptoJS.AES.decrypt(requestBody.Email2, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
    requestBody.Email2 = decryptedEmail2.toString(CryptoJS.enc.Utf8);


    var params = {
        Item: {
            "Id": 1,
            "Address": requestBody.Address,
            "Email1": requestBody.Email1,
            "Email2": requestBody.Email2,
            "Landline": requestBody.Landline,
            "Mobile": requestBody.Mobile,
            "Fax": requestBody.Fax,
            "DaysFrom": requestBody.DaysFrom,
            "DaysTo": requestBody.DaysTo,
            "TimeHrFrom": requestBody.TimeHrFrom,
            "TimeMinFrom": requestBody.TimeMinFrom,
            "TimeTypeFrom": requestBody.TimeTypeFrom,
            "TimeHrTo": requestBody.TimeHrTo,
            "TimeMinTo": requestBody.TimeMinTo,
            "TimeTypeTo": requestBody.TimeTypeTo,
            "CreatedAt": new Date().toISOString(),
            "UpdatedAt": new Date().toISOString()
        },
        TableName: process.env.CONTACT_TABLE
    };
    console.log(params);

    //-- Check whether Content already exists

    var params1 = {
        TableName: process.env.CONTACT_TABLE,
        Key: {
            "Id": 1
        },


    };
    console.log("params.. " + JSON.stringify(params1));
    documentClient.get(params1, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            if (data.Item) {
                console.log("Contact details already exists.");
                var respObj = { "message": "Contact details already exists." }
                //-- Update Contact Details
                var updateParams = {
                    Item: {
                        "Id": 1,
                        "Address": requestBody.Address,
                        "Email1": requestBody.Email1,
                        "Email2": requestBody.Email2,
                        "Landline": requestBody.Landline,
                        "Mobile": requestBody.Mobile,
                        "Fax": requestBody.Fax,
                        "DaysFrom": requestBody.DaysFrom,
                        "DaysTo": requestBody.DaysTo,
                        "TimeHrFrom": requestBody.TimeHrFrom,
                        "TimeMinFrom": requestBody.TimeMinFrom,
                        "TimeTypeFrom": requestBody.TimeTypeFrom,
                        "TimeHrTo": requestBody.TimeHrTo,
                        "TimeMinTo": requestBody.TimeMinTo,
                        "TimeTypeTo": requestBody.TimeTypeTo,
                        "UpdatedAt": new Date().toISOString()
                    },
                    TableName: process.env.CONTACT_TABLE
                };
                console.log("Update params.. " + JSON.stringify(updateParams));
                documentClient.put(updateParams, function (err, data) {
                    if (err) {
                        return callback(err, response(400, JSON.stringify(err)));
                    }
                    console.log(data);
                    var respObj = { "message": "Contact details updated successfully..!" }
                    return callback(err, response(200, JSON.stringify(respObj)));
                });

            } else {
                var respObj = { "message": "Contact details created successfully..!" }
                documentClient.put(params, function (err, data) {
                    return callback(err, response(200, JSON.stringify(respObj)));
                });
            }
        }
    })

}

module.exports.getContentDetails = function (event, context, callback) {
    globalEvent = event;
    // var validateToken = auth.validateToken(event, context, callback);
    // if (validateToken.statusCode != 200) {
    //     return callback(null, response(validateToken.statusCode, JSON.stringify(validateToken)));
    // }

    console.log("Event.... " + JSON.stringify(event)) ;  
    var params = {
        TableName: process.env.CONTACT_TABLE,
        ExpressionAttributeValues: {
            ":id": 1
        },
        KeyConditionExpression: '#id = :id',
        ExpressionAttributeNames: {
            "#id": "Id"
        }

    };
    console.log("params.. " + JSON.stringify(params));
    documentClient.query(params, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return callback(err, response(400, JSON.stringify(err)));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            console.log("count of records ... " + data.Count)
            if (data.Count > 0) {
                var respObj = { "responseObj": data.Items[0], "message": "Content fetched successfully." }
                return callback(null, response(200, JSON.stringify(respObj)));
            } else {
                var respObj = { "message": "Conatct details not found." }
                return callback(null, response(200, JSON.stringify(respObj),event));
            }
        }
    })
}