'use strict';

const AWS = require('aws-sdk');

var sns = new AWS.SNS({ apiVersion: '2010-03-31' });

var documentClient = new AWS.DynamoDB.DocumentClient({ convertEmptyValues: true });
var CryptoJS = require("crypto-js");
var globalEvent = "";

function response(statusCode, message) {
    
  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if(origin == ""|| origin == undefined || !origin){
     origin = globalEvent.headers.Origin;
  }
  if(origin == ""|| origin == undefined || !origin){
    origin = globalEvent.headers.Host;
 }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
      originsAllowed = origin;
 }

  return {
      statusCode: statusCode,
      'headers': {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer authToke',
          'Access-Control-Allow-Credentials': true,
          'Access-Control-Allow-Origin' : originsAllowed,
          'X-XSS-Protection': '1; mode=block',
          'X-Frame-Options': 'SAMEORIGIN',
          'X-Content-Type-Options': 'nosniff',
          'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

      },
      body: message
  };
}

// module.exports.hello =   function(event, context, callback) {

//   var params = {
//     NextToken: 'STRING_VALUE'
//   };
//   sns.listPlatformApplications(params, function(err, data) {
//     if (err) console.log(err, err.stack); // an error occurred
//     else     console.log(data);           // successful response
//   });

module.exports.registerDevice = function (event, context, callback) {
  globalEvent = event;
  var requestBody = JSON.parse(event.body);

  console.log("requestBody : ",requestBody);

  //-- Decrypt Email Id
  var decryptedEmail = CryptoJS.AES.decrypt(requestBody.email, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.email = decryptedEmail.toString(CryptoJS.enc.Utf8);

  
  var device =requestBody.deviceType.toString().toLowerCase();
  var params = {
    PlatformApplicationArn: 'STRING_VALUE', 
    Token: 'STRING_VALUE'
  }
  var deviceARN = '';

  var lstTopicParams = {
    NextToken: ''
};

var subscribeParam = {
  Protocol: 'application',
  TopicArn: 'topicArn',
  Endpoint: 'MOBILE_END_point'
}

  let obj = sns.listPlatformApplications(lstTopicParams, function (err, data) {

    if (err) throw err; // an error occurred

    // Fetching All SNS applications
    data.PlatformApplications.forEach(
      
      app => { console.log("app : ",app,requestBody.deviceType);
      
         var platformApp = app.PlatformApplicationArn;
         console.log("deviceType tttt: ",platformApp.toLowerCase().includes('android'));
        if (device == 'android' && platformApp.toLowerCase().includes('android')) {
          console.log("deviceType : ",requestBody.deviceType,platformApp );
          params.PlatformApplicationArn = platformApp;
          params.Token = requestBody.token;
        } else if (device == "ios" && platformApp.toLowerCase().includes("ios") ) {
          console.log("deviceType : ",requestBody.deviceType,platformApp );
          params.PlatformApplicationArn = platformApp;
          params.Token = requestBody.token;
        }
      }
      )
        // Creating device End Point
        console.log("params : ",params)
        sns.createPlatformEndpoint(params, async function (err, data) {
          if (err) { throw err; } // an error occurred
          else {
            deviceARN = data.EndpointArn;
            console.log("data :: ",data);
            var params = {
              TableName: process.env.DEVICETOKENS_TABLE,
              Item: {
                "Id": requestBody.email,
                "Token": requestBody.token,
                "ARN": data.EndpointArn

              }
            };

            console.log("Adding a new device...");

            // Inserting the subscription data in DeviceToken table
            documentClient.put(params, function (err, data) {
              
              if (err) {
                console.error("Unable to add device. Error JSON:", JSON.stringify(err, null, 2));
              } else {
                console.log("Added device:", JSON.stringify(data, null, 2));
              }

            });

            // Subscribin the device to AllUSerTopic
            subscribeParam.Endpoint = deviceARN;
            subscribeParam.TopicArn = await getTopics(lstTopicParams);

            console.log(subscribeParam)
            sns.subscribe(subscribeParam, function (err, res1) {
              if (err) {
                  console.log('err in subscribe!!', err);
              } else {
                  console.log('subscribe successfull !! ', res1);
              }
          })
           
          }


        });

     

  }).promise();

  console.log("Inside Register Device!")
  var respObj = { "message": "success", "responseObj": "Device Registered" }
  return callback(null, response(200, JSON.stringify(respObj)))
}


async function getTopics(lstTopicParams){
  var topicArn ='';
  const result = await sns.listTopics(lstTopicParams).promise();
  console.log("topic::", result);           // successful response
  result.Topics.forEach(
              topic => {
                  if (topic.TopicArn.includes('AllUser')){
                      
                    topicArn = topic.TopicArn;
                      

                      return topicArn;
              }
              }
          )

          return topicArn;
}


