'use strict';

var AWS = require('aws-sdk'),

  uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient();

const auth = require('../auth/token');

var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}



module.exports.addDevice = function (event, context, callback) {
 
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if(validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }else{
      addDeviceDetails(event,context,validateTokenResp,callback);
    }
  }
  validateMultipleLogin();
}
function addDeviceDetails(event,context,validateTokenResp,callback){
  globalEvent = event;
  const requestBody = JSON.parse(event.body);

  var loggedInUser = validateTokenResp.event.loggedInUser;

  if (!requestBody.DeviceId) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  //-- Get user details from email Id 
  var searchWithEmail = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": loggedInUser
    },
    KeyConditionExpression: "Email = :email"
  }

  console.log("Search Params ..." + JSON.stringify(searchWithEmail));


  documentClient.query(searchWithEmail, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return ({ "statusCode": 400, "message": JSON.stringify(err) });
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      console.log("count of records fro email ... " + data.Count)
      if (data.Count > 0) {
        console.log("Full User Object ... " + JSON.stringify(data.Items[0]));

        var Id = uuid.v1();
        var userDetails = data.Items[0];

        var params = {
          Item: {
            "Id": Id,
            "Name": requestBody.Name,
            "DeviceId": requestBody.DeviceId,
            "Status": requestBody.Status,
            "CreatedBy": userDetails.Name,
            "UpdatedBy": userDetails.Name,
            "CreatedAt": new Date().toISOString(),
            "UpdatedAt": new Date().toISOString()
          },
          TableName: process.env.CONFIGDEVICE_TABLE
        };
        console.log(params);

        var respObj = { "responseObj": data.Attributes, "message": "Device created successfully..!" }
        documentClient.put(params, function (err, data) {
          return callback(err, response(200, JSON.stringify(respObj)));

        });

      } else {
        return ({ "statusCode": 404, "userData": {}, "message": "User details not found." })
      }
    }
  })



}
//-- Multiple Delete Device code 

module.exports.deleteAllDevice = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if(validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }else{
      deleteAllDevicesData(event,context,validateTokenResp,callback);
    }
  }
  validateMultipleLogin();
}
function deleteAllDevicesData(event,context,validateTokenResp,callback){
  const requestBody = JSON.parse(event.body);

  var params = {
    TableName: process.env.CONFIGDEVICE_TABLE,
    Key: {
      "Id": requestBody.Id

    },
    ConditionExpression: "Id= :Id"
  };

  console.log("params.. " + JSON.stringify(params));
  documentClient.delete(params, function (err, data) {
    if (err) {
      return callback(err, response(400, err));
    }
    console.log(data);
    var respObj = { "message": "Device Details Deleted successfully..!" }
    return callback(err, response(200, JSON.stringify(respObj)));
  });
}


//--  Delete Device code 
module.exports.deleteDevice = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if(validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }else{
      deleteAllDevicesData(event,context,validateTokenResp,callback);
    }
  }
  validateMultipleLogin();
}
function deleteAllDevicesData(event,context,validateTokenResp,callback){

  const id = event.pathParameters.Id;
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params = {
    TableName: process.env.CONFIGDEVICE_TABLE,
    Key: {
      "Id": id

    }

  };
  console.log("params.. " + JSON.stringify(params));
  documentClient.delete(params, function (err, data) {
    if (err) {
      return callback(err, response(400, err));
    }
    console.log(data);
    var respObj = { "message": "Device Details Deleted successfully..!" }
    return callback(err, response(200, JSON.stringify(respObj)));
  });
}


//-- Delete multiple Devices code 

module.exports.deleteMultipleDevices = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if(validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }else{
      deleteMultiDevices(event,context,validateTokenResp,callback);
    }
  }
  validateMultipleLogin();
}
function deleteMultiDevices(event,context,validateTokenResp,callback){

  const requestBody = JSON.parse(event.body);

  if (!requestBody.listOfDevices) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var listOfDevices = requestBody.listOfDevices;
  console.log("size of List of devices .. " + listOfDevices.length);
  var count = listOfDevices.length;
  if (listOfDevices.length > 0) {

    listOfDevices.forEach(device => {
      var id = device.id;
      var params = {
        TableName: process.env.CONFIGDEVICE_TABLE,
        Key: {
          "Id": id

        }

      };
      console.log("params.. " + JSON.stringify(params));
      documentClient.delete(params, function (err, data) {
        if (err) {
          return callback(err, response(400, err));
        }
        console.log(data);
        var message = "Device Details of device Id - " + id + " deleted successfully..!"
        console.log(message + count);
        count--;
        if (count == 0) {
          var respObj = { "message": "Devices with given ids deleted successfully.. !" }
          return callback(err, response(200, JSON.stringify(respObj)));
        }
      });
    });
  } else {
    var respObj = { "message": "list of device ids to be deleted not found." }
    return callback(err, response(404, JSON.stringify(respObj)));
  }
}
// update the Device Details

module.exports.updateDevice = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if(validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }else{
      updateDeviceDetails(event,context,validateTokenResp,callback);
    }
  }
  validateMultipleLogin();
}
function updateDeviceDetails(event,context,validateTokenResp,callback){
  console.log("in device update",event)
  const id = event.pathParameters.Id;
  const requestBody = JSON.parse(event.body);
  if (!id) {
    var respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //-- check whether record with given id exists or not 
  var params1 = {
    TableName: process.env.CONFIGDEVICE_TABLE,
    Key: {
      "Id": id
    },
  };
  console.log("search params.. " + JSON.stringify(params1));
  documentClient.get(params1, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      if (data.Item) {
        console.log("Device data with given id exists.");
        const deviceData = data.Item;
        console.log("*********Device Data .. " + JSON.stringify(deviceData));
        //-- Get currently logged in user info
        var loggedInUser = validateTokenResp.event.loggedInUser;
        //-- Get user details from email Id 

        var searchWithEmail = {
          TableName: process.env.VISITOR_TABLE,
          ExpressionAttributeValues: {
            ":email": loggedInUser
          },
          KeyConditionExpression: "Email = :email"
        }
        console.log("Search Params ..." + JSON.stringify(searchWithEmail));

        documentClient.query(searchWithEmail, function (err, data) {
          if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return ({ "statusCode": 400, "message": JSON.stringify(err) });
          } else {
            //console.log("GetItem succeeded:", JSON.stringify(data));
            console.log("count of records fro email ... " + data.Count)
            if (data.Count > 0) {
              //console.log("Full User Object ... " + JSON.stringify(data.Items[0]));
              const userDetails = data.Items[0];

              var params = {
                TableName: process.env.CONFIGDEVICE_TABLE,
                Key: {
                  "Id": id
                },
                UpdateExpression: 'set #Status = :D,#Name = :N,#DeviceId = :I,#UpdatedBy = :U,#UpdatedAt = :UA',
                ExpressionAttributeValues: {
                  ':D': requestBody.Status,
                  ':N': requestBody.Name,
                  ':I': requestBody.DeviceId,
                  ':U': userDetails.Name,
                  ':UA': new Date().toISOString()
                },
                ExpressionAttributeNames: {
                  '#Status': 'Status',
                  '#Name': 'Name',
                  '#DeviceId': 'DeviceId',
                  '#UpdatedBy': 'UpdatedBy',
                  "#UpdatedAt": "UpdatedAt"
                },
                ReturnValues: "UPDATED_NEW"
              };

              console.log("params.. " + JSON.stringify(params));

              const responseObj = {
                "Id": deviceData.Id,
                "Name": requestBody.Name,
                "DeviceId": requestBody.DeviceId,
                "Status": requestBody.Status,
                "CreatedBy": deviceData.CreatedBy,
                "UpdatedBy": userDetails.Name,
                "CreatedAt": deviceData.CreatedAt,
                "UpdatedAt": new Date().toISOString(),
              }
              documentClient.update(params, function (err, data) {
                if (err) {
                  return callback(err, response(400, err));
                }
                console.log(data);
                var respObj = { "responseObj": responseObj, "message": "Device's Details updated successfully..!" }
                return callback(err, response(200, JSON.stringify(respObj)));
              })
            } else {
              console.log("Logged in user details not found");
              var respObj = { "message": "Logged in user details not found" }
              return callback(null, response(404, JSON.stringify(respObj)));
            }
          }
        })
      } else {
        console.log("Device data with given id not found");
        var respObj = { "message": "Device data with given id not found" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })


}

// get  the AllDevice Details

module.exports.getAllDevice = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token 
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response....f "+ JSON.stringify(validateTokenResp));

    if(validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }else{
      getAllDeviceData(event,context,validateTokenResp,callback);
    }
  }
  validateMultipleLogin();
}
function getAllDeviceData(event,context,validateTokenResp,callback){

  var params = {
    TableName: process.env.CONFIGDEVICE_TABLE,
  }
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    var museumData = data.Items;
    //-- Sort data according to created date time
    const sortedtData = museumData.sort((a, b) => new Date(a.CreatedAt) - new Date(b.CreatedAt));
    console.log("sorted... " + JSON.stringify(sortedtData))
    var finalResponse = {}
    finalResponse.Items = sortedtData;

    var respObj = { "responseObj": finalResponse, "message": "Device's data fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));
  })

}
//get details by Id
module.exports.getDeviceById = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  const id = event.pathParameters.Id;
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params = {
    TableName: process.env.CONFIGDEVICE_TABLE,
    ExpressionAttributeValues: {
      ":Id": id
    },
    KeyConditionExpression: 'Id = :Id ',
  };
  //console.log("params.. " + JSON.stringify(params));
  documentClient.query(params, function (err, data) {
    // let data1 = data.filter();
    if (err) {
      // console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return callback(err, response(400, JSON.stringify(err)));
    } else {
      //console.log("GetItem succeeded:", JSON.stringify(data));
      //console.log("count of records ... " + data.Count)
      if (data.Count > 0) {
        var respObj = { "responseObj": data.Items[0], "message": "Answer's fetched successfully." }
        return callback(null, response(200, JSON.stringify(respObj)));
      } else {
        var respObj = { "message": + id + " not found." }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  });
}
// get particular device details by "Assigned or Available"
module.exports.getDeviceByValue = function (event, context, callback) {

  globalEvent = event;
  //-- Validate JWT token 
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if(validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }else{
      getDeviceDetailsByValue(event,context,validateTokenResp,callback);
    }
  }
  validateMultipleLogin();
}
function getDeviceDetailsByValue(event,context,validateTokenResp,callback){
  const value = event.pathParameters.Id;
  if (!value) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  if (value == "Assigned" || value == "Available") {

    var params = {
      TableName: process.env.CONFIGDEVICE_TABLE,
      FilterExpression: "#Status = :user_status_val",
      ExpressionAttributeNames: {
        "#Status": "Status",
      },
      ExpressionAttributeValues: { ":user_status_val": value }

    };
    console.log("params.. " + JSON.stringify(params));
    documentClient.scan(params, function (err, data) {
      if (err) {
        return callback(err, response(400, JSON.stringify(err)));
      }
      var museumData = data.Items;
      if (museumData.length > 0) {
        //-- Sort data according to created date time
        const sortedtData = museumData.sort((a, b) => new Date(a.CreatedAt) - new Date(b.CreatedAt));
        console.log("sorted... " + JSON.stringify(sortedtData))
        var finalResponse = {}
        finalResponse.Items = sortedtData;

        var respObj = { "responseObj": finalResponse, "message": "Device's data fetched successfully..!" }
        return callback(null, response(200, JSON.stringify(respObj)));
      } else {
        var respObj = { "responseObj": museumData, "message": "Device's data not found..!" }
        return callback(null, response(200, JSON.stringify(respObj)));
      }
    })
  } else {
    var params = {
      TableName: process.env.CONFIGDEVICE_TABLE,
      FilterExpression: "#Id = :user_device_val",
      ExpressionAttributeNames: {
        "#Id": "Id",
      },
      ExpressionAttributeValues: { ":user_device_val": value }

    };
    console.log("params.. " + JSON.stringify(params));
    documentClient.scan(params, function (err, data) {
      if (err) {
        return callback(err, response(400, JSON.stringify(err)));
      }
      var museumData = data.Items[0];
      var respObj = { "responseObj": museumData, "message": "Device's status fetched successfully..!" }
      return callback(null, response(200, JSON.stringify(respObj)));
    })
  }
}
