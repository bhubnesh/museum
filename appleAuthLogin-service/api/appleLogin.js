'use strict';

//const appleSignin = require("apple-signin");
const AWS = require('aws-sdk');
const auth = require('../auth/token');
const uuid = require('uuid');
var atob = require('atob');
var CryptoJS = require("crypto-js");
const documentClient = new AWS.DynamoDB.DocumentClient();

var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}


module.exports.signUpWithApple = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);

  if (!requestBody.identityToken) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  //-- Fetch email from "identityToken" from req
  var token = requestBody.identityToken.substring(7);
  var base64Url = atob(token.split('.')[1]);
  var userInfo = JSON.parse(base64Url);

  var visitorEmail = userInfo.email;
  if (requestBody.fullName.givenName && requestBody.fullName.familyName) {
    var visitorName = requestBody.fullName.givenName + " " + requestBody.fullName.familyName;
  } else {
    var visitorName = "";
  }
  //-- get user details and add into user table

  var reqBody = {
    Item: {
      "Id": uuid.v1(),
      "CreatedAt": new Date().toISOString(),
      "UpdatedAt": new Date().toISOString(),
      "Name": visitorName,
      "Password": "test@123",
      "Email": visitorEmail,
      "Phone": "9999999999",
      "CountryCode": "+91",
      "Gender": "",
      "Age": 0,
      "Status": "Inactive",
      "Role": "Visitor",
      "Source": "Apple",
      "ImageURL": "",
      "NoOfVisits": 0,
      "LastVisitedDate": "",
      "SessionId": 0

    },
    TableName: process.env.VISITOR_TABLE
  };

  console.log("reqBody ==============> " + JSON.stringify(reqBody));

  var searchWithEmail = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": visitorEmail
    },
    KeyConditionExpression: "Email = :email"
  }

  console.log("Search Params ..." + JSON.stringify(searchWithEmail));

  documentClient.query(searchWithEmail, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      console.log("count of records fro email ... " + data.Count)
      if (data.Count > 0) {
        console.log("Inside count");

        var activeStatus = "Active";
        if ((data.Items[0].Status).toString() === activeStatus.toString() && data.Items[0].Source != "Apple") {
          //-- User already exists.

          console.log("User with emailId already exists.");
          var respObj = { "message": "User with emailId already exists." }
          return callback(null, response(400, JSON.stringify(respObj)));

        } else {
          //-- generate JWT token
          var jwtToken = auth.generateJwtToken(visitorEmail);
          if (jwtToken == null) {
            var respObj = { "message": "Error creating JWT token." }
            return callback(
              null,
              response(400, JSON.stringify(respObj))
            );
          }
          data.Items.JwtToken = jwtToken;

          var resp = {
            "Id": data.Items[0].Id,
            "CreatedAt": data.Items[0].CreatedAt,
            "UpdatedAt": data.Items[0].UpdatedAt,
            "Name": data.Items[0].Name,
            "Password": data.Items[0].Password ? data.Items[0].Password : "",
            "Email": data.Items[0].Email,
            "Phone": data.Items[0].Phone,
            "CountryCode": data.Items[0].CountryCode,
            "Gender": data.Items[0].Gender ? data.Items[0].Gender : "",
            "Age": data.Items[0].Age ? data.Items[0].Age : "",
            "Status": data.Items[0].Status,
            "Role": data.Items[0].Role,
            "Source": data.Items[0].Source,
            "ImageURL": data.Items[0].ImageURL ? data.Items[0].ImageURL : ""

          }

          // Decrypt JWT Token
          var bytes = CryptoJS.AES.decrypt(jwtToken, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
          var token = bytes.toString(CryptoJS.enc.Utf8);

          var base64Url = atob(token.split('.')[1]);

          //-- Extract session Id from token
          var sessionId = JSON.parse(base64Url).sessionId;
          console.log("sessionId ----- " + sessionId);
          //-- Store this session Id into DB
          var params = {
            TableName: process.env.VISITOR_TABLE,
            Key: {
              "Email": data.Items[0].Email,
              "Phone": data.Items[0].Phone
            },

            UpdateExpression: 'set #sessionId = :sessionId',
            ExpressionAttributeValues: {
              ':sessionId': sessionId
            },
            ExpressionAttributeNames: {
              '#sessionId': 'SessionId'

            },
            ReturnValues: "UPDATED_NEW"
          };

          documentClient.update(params, function (err, data) {
            if (err) {
              return callback(err, response(400, err));
            } else {
              // var respObj = { "message": "Login successful", "responseObj": resBody }
              // console.log("Response ... " + JSON.stringify(response(200, JSON.stringify(respObj))));
              // return callback(null, response(200, JSON.stringify(respObj)));

              var respObj = { "message": "Login successful", "responseObj": resp, "JwtToken": jwtToken }
              console.log("Response ... " + JSON.stringify(response(200, JSON.stringify(data.Items))));
              return callback(null, response(200, JSON.stringify(respObj)));
            }
          })
        }
      } else {
        //-- generate JWT token
        var jwtToken = auth.generateJwtToken(visitorEmail);
        if (jwtToken == null) {
          var respObj = { "message": "Error creating JWT token." }
          return callback(
            null,
            response(400, JSON.stringify(respObj))
          );
        }

        // Decrypt JWT Token
        var bytes = CryptoJS.AES.decrypt(jwtToken, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
        var token = bytes.toString(CryptoJS.enc.Utf8);
        var base64Url = atob(token.split('.')[1]);


        //-- Extract session Id from token
        var sessionId = JSON.parse(base64Url).sessionId;
        console.log("sessionId ----- " + sessionId);
        reqBody.Item.SessionId = sessionId;
        console.log("Session Id DB .... " + reqBody.Item.SessionId);
        documentClient.put(reqBody, function (err, data) {

          var respObj = { "message": "User created successfully..!", "responseObj": reqBody.Item, "JwtToken": jwtToken }
          return callback(err, response(200, JSON.stringify(respObj)));
        })
      }
    }
  })


}