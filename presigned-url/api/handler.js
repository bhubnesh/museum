const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const uploadBucket = process.env.bucketName;

module.exports.getUrl = function (event, context, callback) {
    // TODO implement
     const eventBody =JSON.parse(event.body);

console.log('Event params :' +event);
//const eventBody=event;

const result =  getUploadURL(eventBody.mnmEntityId,eventBody.contentType,eventBody.fileName, eventBody.mnmEntityType, eventBody.mnmUserPhone, eventBody);


console.log('Result: ',result);

return result;

};


const getUploadURL = async function(mnmEntityId, contentType, fileName, mnmEntityType, mnmUserPhone, eventBody) {

    console.log('getUploadURL started:'+contentType);

    
    var s3Params = {
        Bucket:uploadBucket,
        Key:`${fileName}`,
        Metadata: {
            mnmEntityId:`${mnmEntityId}`,
            mnmEntityType:`${mnmEntityType}`,
            mnmUserPhone:`${mnmUserPhone}`,
            mnmArtifactType:`${eventBody.mnmArtifactType}`,
            mnmArtifactSubType:`${eventBody.mnmArtifactSubType}`,
            mnmLanguage:`${eventBody.mnmLanguage}`
        },
        ContentType:`${contentType}`

    };


    return new Promise((resolve, reject)=> {

        // Get signed URL

        let uploadURL = s3.getSignedUrl('putObject', s3Params);

        resolve({

            "statusCode": 200,
            "isBase64Encoded": false,
            "headers": {
                "Access-Control-Allow-Origin" : "*",
                "Access-Control-Allow-Headers" : "Origin, X-Requested-With, Content-Type, Accept"
            },

            "body":JSON.stringify({"responseObj":{
                "uploadURL":uploadURL,
                "photoFilename":`${fileName}`
            }
            })

        });

    });

};
