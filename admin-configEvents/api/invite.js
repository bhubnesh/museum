'use strict';

var AWS = require('aws-sdk'),
    uuid = require('uuid'),
    documentClient = new AWS.DynamoDB.DocumentClient();

const auth = require('../auth/token');
let paths = require('../config/apiEndpoints');
const axios = require('axios');
const notification = require('../notifications/sendNotification');

var globalEvent = "";

function response(statusCode, message) {
    
  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if(origin == ""|| origin == undefined || !origin){
     origin = globalEvent.headers.Origin;
  }
  if(origin == ""|| origin == undefined || !origin){
    origin = globalEvent.headers.Host;
 }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
      originsAllowed = origin;
 }

  return {
      statusCode: statusCode,
      'headers': {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer authToke',
          'Access-Control-Allow-Credentials': true,
          'Access-Control-Allow-Origin' : originsAllowed,
          'X-XSS-Protection': '1; mode=block',
          'X-Frame-Options': 'SAMEORIGIN',
          'X-Content-Type-Options': 'nosniff',
          'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

      },
      body: message
  };
}

//sending invite
module.exports.inviteEvent = function (event, context, callback) {
globalEvent = event;
  const eventId = event.pathParameters.eventId;
  console.log("check ... ",eventId)
  if (!eventId) {
    let respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  const requestBody = JSON.parse(event.body);
  if (!requestBody.EmailId) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  var email = (requestBody.EmailId);
  console.log("email",email.length)
  for(const emailId of email){
    console.log("emailId  :: ",emailId)
  }
  var EmailDescription = requestBody.EmailDiscription;
  var params = {
    Item: {
      "Id": eventId,
      "EmailId": email,
      "EmailDiscription": EmailDescription,
      "CreatedAt": new Date().toISOString()
    },
    TableName: process.env.EVENTSINVITE_TABLE
  };
  console.log("params",params)
  var respObj = { "message": "Invite sent successfully..!" }
  documentClient.put(params, function (err, data) {
    var subject = "Invite for an event"
    var emailNotification = notification.sendEmailNotification(email, subject, EmailDescription)  
    return callback(err, response(200, JSON.stringify(respObj)));
  })
}

// get particular event details By Id
module.exports.getInviteById = function (event, context, callback) {
  globalEvent = event;
  const id = event.pathParameters.Id;
  if (!id) {
    let respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  let params = {
    TableName: process.env.EVENTSINVITE_TABLE,
    ExpressionAttributeValues: {
      ":id": id
    },
    KeyConditionExpression: 'Id = :id ',
  };
  //console.log(params);
  documentClient.query(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    } else {
      if (data.Count > 0) {
        var respObj = { "responseObj": data.Items[0], "message": "Invites data fetched successfully." }
        return callback(null, response(200, JSON.stringify(respObj)));
      }
    }
  });
}
