'use strict';

var AWS = require('aws-sdk'),

  uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient();

const auth = require('../auth/token');
let paths = require('../config/apiEndpoints');
const axios = require('axios');
const date = require('date-and-time')
const moment = require('moment')
const eventBridge = new AWS.EventBridge({ region: 'ap-south-1' });
var dateFormat = ["YYYY-MM-DD HH:mm", "YYYY-MM-DD HH:m", "YYYY-MM-DD H:mm", "YYYY-MM-DD H:m",
  "YYYY-MM-D HH:mm", "YYYY-MM-D HH:m", "YYYY-MM-D H:mm", "YYYY-MM-D H:m",
  "YYYY-M-DD HH:mm", "YYYY-M-DD HH:m", "YYYY-M-DD H:mm", "YYYY-M-DD H:m",
  "YYYY-M-D HH:mm", "YYYY-M-D HH:m", "YYYY-M-D H:mm", "YYYY-M-D H:m",
]

var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  // console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}

module.exports.addEvent = function (event, context, callback) {
  globalEvent = event;
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    // console.log("Response.... "+ JSON.stringify(validateTokenResp));
    if (validateTokenResp.statusCode != 200) {
      console.log("Response .in if............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      //console.log("Response . in else............. " + JSON.stringify(validateTokenResp));
      addEventData(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function addEventData(event, context, validateTokenResp, callback) {
  const loggedInUser = validateTokenResp.event.loggedInUser;
  let requestBody = JSON.parse(event.body);
  console.log("inside ====")
  event.loggedInUser = loggedInUser;
  if (!requestBody.EventName || !requestBody.EventDate || !requestBody.MaxParticipantsAllowed
    || !requestBody.EventType) {
    let respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //-- Fetch Admin Booking configs
  //--call Admin booking configs API
  const bookingConfig_URL = paths.API_URLS.geBookingConfigPath;
  axios.get(bookingConfig_URL, {
    headers: {
      'Authorization': event.headers.Authorization
    }
  })
    .then((res) => {
      if (res.status != 200) {
        return callback(null, response(res.status, res.message));
      }
      console.log("inside Booking Configs ... ");
      const bookingConfigs = res.data.responseObj;
      if (!bookingConfigs) {
        let respObj = { "message": "Unable to fetch booking configurations." }
        return callback(
          null,
          response(404, JSON.stringify(respObj))
        );
      }
      else {
        console.log("hello bookingConfigs =========", bookingConfigs)
        addEventDetails(event, callback, bookingConfigs, context);
        console.log("hello check =========")
      }
    })
    .catch((error) => {
      console.error(error)
    })
}
async function addEventDetails(event, callback, bookingConfigs, context) {
  globalEvent = event;
  console.log("hello check =addEventDetails========")
  let requestBody = JSON.parse(event.body);
  // let validateToken = auth.validateToken(event, context, callback);
  // if (validateToken.statusCode != 200) {
  //   return callback(null, response(validateToken.statusCode, JSON.stringify(validateToken)));
  // }
  let Id = uuid.v1();
  let EventType = requestBody.EventType;

  // Check if museum offline

  var eventDatefrom = moment(requestBody.EventDate.substring(0, 10) + " " + requestBody.EventTimeInHr.toString() + ":" + requestBody.EventTimeInMin.toString(), dateFormat, 'fr', true);
  if (requestBody.EventTimeAmPm.toLowerCase() == 'pm') {
    //  console.log("inside pm for offlineFrom ",offlineFrom);
    if (requestBody.EventTimeInHr != 12) {
      eventDatefrom.add(12, 'hours');
    }
    // console.log("after adding 12 hours offlineFrom ",offlineFrom);
  }
  console.log("eventDatefrom ", eventDatefrom)

  var eventDateTo = eventDatefrom.clone()
  eventDateTo.add(requestBody.EventDurationInHr, 'hours')

  eventDateTo.add(requestBody.EventDurationInMin, 'minutes')
  console.log("eventDatefrom after ", eventDatefrom)
  console.log(requestBody.EventDurationInHr, " ", requestBody.EventDurationInMin)
  console.log("eventDateTo ", eventDateTo)
  eventDateTo = eventDateTo.toDate();
  eventDatefrom = eventDatefrom.toDate();

  var allMuseumOfflineData = await getAllOfflineData();
  if (allMuseumOfflineData.length > 0) {
    // Check if event date time lies between exisitng museum offline
    console.log("allMuseumOfflineData :: ", allMuseumOfflineData)
    for (const museumOffline of allMuseumOfflineData) {
      var respObj = '';
      var existingOfflineData = await getFormattedToAndFrom(museumOffline);
      var existingofflineFrom = existingOfflineData.offlineFrom
      var existingofflineTo = existingOfflineData.offlineTo
      console.log("eventDatefrom  = ", eventDatefrom, " ", "existingofflineFrom = ", existingofflineFrom, " eventDateTo = ", eventDateTo, "existingofflineTo = ", existingofflineTo);
      console.log(eventDatefrom >= (existingofflineFrom), eventDateTo <= (existingofflineTo))
      if (eventDatefrom >= (existingofflineFrom) && eventDateTo <= (existingofflineTo)) {
        respObj = { responseObj: "Museum is  offline for this period" }
        // console.log(respObj)
        return callback(null, response(302, JSON.stringify(respObj)));

      }
      else if (eventDateTo >= (existingofflineFrom) && eventDateTo <= (existingofflineTo)) {
        respObj = { responseObj: "Museum is already offline for this period" }
        console.log(respObj)
        return callback(null, response(302, JSON.stringify(respObj)));
      } else if (eventDatefrom >= (existingofflineFrom) && eventDatefrom <= (existingofflineTo)) {
        respObj = { responseObj: "Museum is already offline for this period" }
        console.log(respObj)
        return callback(null, response(302, JSON.stringify(respObj)));
      } else if (eventDatefrom <= existingofflineFrom && eventDateTo >= existingofflineTo) {
        respObj = { responseObj: "Museum is already offline for this period" }
        console.log(respObj)
        return callback(null, response(301, JSON.stringify(respObj)));

      }
    }
  }
  let params = {
    Item: {
      "Id": Id,
      "EventType": requestBody.EventType,
      "EventName": requestBody.EventName,
      "RequestedBy": requestBody.RequestedBy,
      "EventDate": requestBody.EventDate,
      "EventTimeInHr": requestBody.EventTimeInHr,
      "EventTimeInMin": requestBody.EventTimeInMin,
      "EventTimeAmPm": requestBody.EventTimeAmPm,
      "EventDurationInHr": requestBody.EventDurationInHr,
      "EventDurationInMin": requestBody.EventDurationInMin,
      "EventDescription": requestBody.EventDescription,
      "Notes": requestBody.Notes,
      "MaxParticipantsAllowed": requestBody.MaxParticipantsAllowed,
      "AvailableBookingCount": requestBody.MaxParticipantsAllowed,
      "NoOfParticipantPerBooking": requestBody.NoOfParticipantPerBooking,
      "Complete": "false",
      "Action": (EventType == "Private") ? "invite" : "",
      "WheelChairAssistance": bookingConfigs.WheelChairAssistance,
      "WCAMaximumAllowedPerBooking": bookingConfigs.WCAMaximumAllowedPerBooking,
      "WCAMaximumAvailablePerEvent": bookingConfigs.WCAMaximumAvailablePerSlot,
      "WCAvailableCount": bookingConfigs.WCAMaximumAvailablePerSlot,
      "VisuallyImpairedAssistance": bookingConfigs.VisuallyImpairedAssistance,
      "VIAMaximumAllowedPerBooking": bookingConfigs.VIAMaximumAllowedPerBooking,
      "VIAMaximumAvailablePerEvent": bookingConfigs.VIAMaximumAvailablePerSlot,
      "VIAvailableCount": bookingConfigs.VIAMaximumAvailablePerSlot,
      "HearingImpairedAssistance": bookingConfigs.HearingImpairedAssistance,
      "HIAMaximumAvailablePerEvent": bookingConfigs.HIAMaximumAvailablePerSlot,
      "HIAvalableCount": bookingConfigs.HIAMaximumAvailablePerSlot,
      "Status": "Active",
      "CreatedAt": new Date().toISOString(),
      "UpdatedAt": new Date().toISOString(),
      "CreatedBy": event.loggedInUser,
      "UpdatedBy": event.loggedInUser
    },
    TableName: process.env.ADMINCONFIGEVENT_TABLE
  };
  //-- Check whether Event already exists
  let params1 = {
    TableName: process.env.ADMINCONFIGEVENT_TABLE,
    Key: {
      "Id": Id
    }

  };
  documentClient.get(params1, function (err, data) {
    if (err) {
      return response(400, JSON.stringify(err));
    } else {
      if (data.Item) {
        let respObj = { "message": "EventId already exists" }
        return callback(err, response(400, JSON.stringify(respObj)));
      } else {
        documentClient.put(params, function (err, data) {
          let responseObj = {
            "Id": Id,
            "EventType": requestBody.EventType,
            "EventName": requestBody.EventName
          }
          let respObj = { "responseObj": responseObj, "message": "Event created successfully..!" }
          console.log("resp ========-========", respObj)
          return callback(err, response(200, JSON.stringify(respObj)));
        });
      }
    }
  });
}
async function getAllOfflineData() {
  console.log("Inside getAllMuseumOffline!!!")
  var params = {
    TableName: process.env.MUSEUMOFFLINE_TABLE,
  };
  var data = await documentClient.scan(params).promise();
  var finalResponse = data;
  var museumData = data.Items;
  //-- Sort data according to created date time
  console.log("museumData ::", museumData);

  // if(museumData.length !=0) {
  // const sortedData = museumData.sort((a, b) => new Date(b.DateFrom) - new Date(a.DateFrom));
  // finalResponse.Items = sortedData;
  // }
  //console.log("sorted data ------------------------ ",sortedData)
  var respObj = { "responseObj": museumData, "message": "Museum Offline data fetched successfully..!" };
  return respObj;

}
async function getFormattedToAndFrom(museumOffline) {
  if (museumOffline.TimeTypeFrom.toLowerCase() == 'pm') {
    if (museumOffline.TimeTypeFrom != 12) {
      //  console.log("inside pm for offlineFrom ",offlineFrom);
      museumOffline.TimeHrFrom = parseInt(museumOffline.TimeHrFrom) + 12;
      // console.log("after adding 12 hours offlineFrom ",offlineFrom);
    }
  }
  var offlineFrom = moment(museumOffline.DateFrom.substring(0, 10) + " " + museumOffline.TimeHrFrom.toString() + ":" + museumOffline.TimeMinFrom.toString(), dateFormat, 'fr', true);
  // offlineFrom.add(museumOffline.TimeHrFrom,'hours')
  // offlineFrom.add(museumOffline.TimeMinFrom,'minutes')
  console.log("-------" + museumOffline.DateFrom + " " + museumOffline.TimeHrFrom.toString() + ":" + museumOffline.TimeMinFrom.toString(), museumOffline.TimeTypeFrom);
  if (museumOffline.TimeTypeFrom.toLowerCase() == 'pm') {
    //  console.log("inside pm for offlineFrom ",offlineFrom);
    // offlineFrom.add(12, 'hours');
    // console.log("after adding 12 hours offlineFrom ",offlineFrom);
  }
  // here it is EST
  offlineFrom = offlineFrom.toDate();
  console.log("offlineFrom ========", offlineFrom)
  if (museumOffline.TimeTypeTo.toLowerCase() == 'pm') {
    //    console.log("inside pm to ",offlineTo);
    // offlineTo.add(12, 'hours');
    if (museumOffline.TimeHrTo != 12) {
      museumOffline.TimeHrTo = parseInt(museumOffline.TimeHrTo) + 12;
    }
  }
  console.log("-----------------", museumOffline.DateTo.substring(0, 10) + " " + museumOffline.TimeHrTo.toString() + ":" + museumOffline.TimeMinTo.toString(), museumOffline.TimeTypeTo);
  var offlineTo = moment(museumOffline.DateTo.substring(0, 10) + " " + museumOffline.TimeHrTo.toString() + ":" + museumOffline.TimeMinTo.toString(), dateFormat, 'fr', true);
  // offlineTo.add(museumOffline.TimeHrTo,'hours')
  // offlineTo.add(museumOffline.TimeMinTo,'minutes')
  if (museumOffline.TimeTypeTo.toLowerCase() == 'pm') {
    //    console.log("inside pm to ",offlineTo);
    // offlineTo.add(12, 'hours');
  }
  //offlineTo.subtract(1,'hours')
  // here it is EST
  offlineTo = offlineTo.toDate();
  console.log("offlineTo ========", offlineTo)
  var responseObj = {
    "offlineFrom": offlineFrom,
    "offlineTo": offlineTo
  }
  console.log("getFormattedToAndFrom : responseObj : ", responseObj)
  return responseObj;
}
//-- get  the AllEvent List
module.exports.getAllEventsList = function (event, context, callback) {
  globalEvent = event;
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    // console.log("Response.... "+ JSON.stringify(validateTokenResp));
    if (validateTokenResp.statusCode != 200) {
      console.log("Response .in if............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      //console.log("Response . in else............. " + JSON.stringify(validateTokenResp));
      getData(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
  function getData(event, context, validateTokenResp, callback){
  var params = {
    TableName: process.env.ADMINCONFIGEVENT_TABLE,

  };
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    var finalData = [];
    //console.log("LEngth of event array... "+ count);
    var respObj = { "responseObj": data, "message": "Events list fetched successfully..!" }
    return callback(err, response(200, JSON.stringify(respObj)));
  })
}
//-- get  the active Event List
module.exports.getAllActiveEventsList = function (event, context, callback) {
  globalEvent = event;
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    // console.log("Response.... "+ JSON.stringify(validateTokenResp));
    if (validateTokenResp.statusCode != 200) {
      console.log("Response .in if............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      //console.log("Response . in else............. " + JSON.stringify(validateTokenResp));
      getAllActiveData(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
  function getAllActiveData(event, context, validateTokenResp, callback){
  var params = {
    TableName: process.env.ADMINCONFIGEVENT_TABLE,
  };
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    var finalData = [];
    var eventData = data.Items;
    var count = eventData.length;
    console.log("LEngth of event array... " + count);
    eventData.forEach(event => {
      //-- If event status is not cancel then only fetch the details
      if (event.Status != "cancel") {
        finalData.push(event);
        count--;
      } else {
        count--;
      }
      if (count == 0) {
        var responseObj = {
          Items: finalData
        }
        var respObj = { "responseObj": responseObj, "message": "Events list fetched successfully..!" }
        return callback(err, response(200, JSON.stringify(respObj)));
      }
    })
  })
}

// get particular event details By Id
module.exports.getEventById = function (event, context, callback) {
  globalEvent = event;
  const id = event.pathParameters.Id;
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    // console.log("Response.... "+ JSON.stringify(validateTokenResp));
    if (validateTokenResp.statusCode != 200) {
      console.log("Response .in if............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      //console.log("Response . in else............. " + JSON.stringify(validateTokenResp));
      getEventDataWithId(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
  function getEventDataWithId(event, context, validateTokenResp, callback){
  const id = event.pathParameters.Id;
  if (!id) {
    let respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  let params = {
    TableName: process.env.ADMINCONFIGEVENT_TABLE,
    ExpressionAttributeValues: {
      ":event": id
    },
    KeyConditionExpression: 'Id = :event ',
  };
  documentClient.query(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    } else {
      if (data.Count > 0) {
        let responseObj = data.Items[0];
        if (responseObj.EventType != "Public") {
          let invite = {
            TableName: process.env.EVENTSINVITE_TABLE,
            ExpressionAttributeValues: {
              ":eventId": id
            },
            KeyConditionExpression: 'Id = :eventId ',
          };
          documentClient.query(invite, function (err, data) {
            if (data.Items[0] != undefined) {
              let respo = {
                "EmailId": data.Items[0]["EmailId"]
              }
              console.log("respo", respo)
              //  if(data.Items[0]["EmailId"] !="" && data.Items[0]["EmailId"] !=undefined){
              responseObj["EmailId"] = data.Items[0]["EmailId"]
              var respObj = { "responseObj": responseObj, "message": "Event's data fetched successfully." }
              return callback(null, response(200, JSON.stringify(respObj)));
            } else {
              var respObj = { "responseObj": responseObj, "message": "Event's data fetched successfully." }
              return callback(null, response(200, JSON.stringify(respObj)));
            }
          })
        } else {
          var respObj = { "responseObj": responseObj, "message": "Event's data fetched successfully." }
          return callback(null, response(200, JSON.stringify(respObj)));
        }
      }
    }
  });
}
//--Delete Event - Hard delete
module.exports.deleteEvent = function (event, context, callback) {
  globalEvent = event;
  const id = event.pathParameters.Id;
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    // console.log("Response.... "+ JSON.stringify(validateTokenResp));
    if (validateTokenResp.statusCode != 200) {
      console.log("Response .in if............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      //console.log("Response . in else............. " + JSON.stringify(validateTokenResp));
      dataDeleteEvent(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
  function dataDeleteEvent(event, context, validateTokenResp, callback){
  const id = event.pathParameters.Id;
  //const requestBody = JSON.parse(event.body);
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  let params = {
    TableName: process.env.ADMINCONFIGEVENT_TABLE,
    Key: {
      "Id": id
    }

  };
  documentClient.delete(params, function (err, data) {
    if (err) {
      return callback(err, response(400, err));
    }
    let respObj = { "message": "Event Details Deleted successfully..!" }
    return callback(err, response(200, JSON.stringify(respObj)));
  });
}
//Delete All Api hard delete
module.exports.deleteAllEvent = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    // console.log("Response.... "+ JSON.stringify(validateTokenResp));
    if (validateTokenResp.statusCode != 200) {
      console.log("Response .in if............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      //console.log("Response . in else............. " + JSON.stringify(validateTokenResp));
      dataAllDeleteEvent(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function dataAllDeleteEvent(event, context, validateTokenResp, callback){
  var responseObj = {}
  const requestBody = JSON.parse(event.body);
  if (!requestBody.listOfEvents) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //get through requestBody
  var listOfEvents = requestBody.listOfEvents;
  if (listOfEvents != null) {
    let cnt = 0;
    listOfEvents.forEach(events => {
      cnt++;
      var id = events.id;
      var params = {
        TableName: process.env.ADMINCONFIGEVENT_TABLE,
        Key: {
          "Id": id
        }
      };
      documentClient.get(params, function (err, data) {
        if (err) {
          responseObj[id] = err;
        }
        else if (data && Object.keys(data).length === 0 && data.constructor === Object) {
          responseObj[id] = 'ID not found';
        }
        else {
          var deleteParams = {
            TableName: process.env.ADMINCONFIGEVENT_TABLE,
            Key: {
              "Id": id
            }
          };
          documentClient.delete(deleteParams, function (err, data) {
            if (err) {
              responseObj[id] = err;
            }
            responseObj[id] = 'ID Deleted';
          });
        }
        if (listOfEvents.length == cnt) {
          setTimeout(() => {
            return callback(err, response(200, JSON.stringify([responseObj])));
          }, 2000);
        }
      })
    });
  } else {
    var respObj = { "message": "Id was provided incorrect" }
    return callback(err, response(err, JSON.stringify(respObj)));
  }
}
//-- update the Event Details
module.exports.updateEvent = function (event, context, callback) {
  globalEvent = event;
  const id = event.pathParameters.Id;
  const requestBody = JSON.parse(event.body)
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    // console.log("Response.... "+ JSON.stringify(validateTokenResp));
    if (validateTokenResp.statusCode != 200) {
      console.log("Response .in if............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      //console.log("Response . in else............. " + JSON.stringify(validateTokenResp));
      dataUpdateEvent(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function dataUpdateEvent(event, context, validateTokenResp, callback){
  const id = event.pathParameters.Id;
  const requestBody = JSON.parse(event.body);

  if (!id) {
    let respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  let params = {
    TableName: process.env.ADMINCONFIGEVENT_TABLE,
    Key: {
      "Id": id
    },
  }
  documentClient.get(params, function (err, data) {
    if (err) {
      return response(400, JSON.stringify(err));
    } else {
      if (data.Item) {
        const event = data.Item;
        let params1 = {
          TableName: process.env.ADMINCONFIGEVENT_TABLE,
          Key: {
            "Id": id
          },
          UpdateExpression: 'set #EventType = :T,#Action = :A,#EventName = :N,#RequestedBy = :R,#EventDate = :D,#EventTimeInHr = :ETH,#EventTimeInMin = :ETM,#EventTimeAmPm = :AMPM,#EventDurationInHr = :DUH,#EventDurationInMin = :DUM,#EventDescription = :DE,#Notes = :NO,#MaxParticipantsAllowed = :MP,#AvailableBookingCount = :ABC,#NoOfParticipantPerBooking = :NP,#Complete = :C',
          ExpressionAttributeValues: {
            ':T': requestBody.EventType,
            ':A': (requestBody.EventType == "Private") ? "invite" : "",
            ':N': requestBody.EventName,
            ':R': requestBody.RequestedBy,
            ':D': requestBody.EventDate,
            ':ETH': requestBody.EventTimeInHr,
            ':ETM': requestBody.EventTimeInMin,
            ':AMPM': requestBody.EventTimeAmPm,
            ':DUH': requestBody.EventDurationInHr,
            ':DUM': requestBody.EventDurationInMin,
            ':DE': requestBody.EventDescription,
            ':NO': requestBody.Notes,
            ':MP': requestBody.MaxParticipantsAllowed,
            ':ABC': requestBody.MaxParticipantsAllowed,
            ':NP': requestBody.NoOfParticipantPerBooking,
            ':C': event.Complete
          },
          ExpressionAttributeNames: {
            '#EventType': 'EventType',
            '#Action': 'Action',
            '#EventName': 'EventName',
            '#RequestedBy': 'RequestedBy',
            '#EventDate': 'EventDate',
            '#EventTimeInHr': 'EventTimeInHr',
            '#EventTimeInMin': 'EventTimeInMin',
            '#EventTimeAmPm': 'EventTimeAmPm',
            '#EventDurationInHr': 'EventDurationInHr',
            '#EventDurationInMin': 'EventDurationInMin',
            '#EventDescription': 'EventDescription',
            '#Notes': 'Notes',
            '#MaxParticipantsAllowed': 'MaxParticipantsAllowed',
            "#AvailableBookingCount": 'AvailableBookingCount',
            '#NoOfParticipantPerBooking': 'NoOfParticipantPerBooking',
            '#Complete': 'Complete',
          },
          ReturnValues: "UPDATED_NEW"
        };
        const responseObj = {
          "Id": event.Id,
          "EventType": requestBody.EventType,
          "Action": (requestBody.EventType == "Private") ? "invite" : "",
          "EventName": requestBody.EventName,
          "RequestedBy": requestBody.RequestedBy,
          "EventDate": requestBody.EventDate,
          "EventTimeInHr": requestBody.EventTimeInHr,
          "EventTimeInMin": requestBody.EventTimeInMin,
          "EventTimeAmPm": requestBody.EventTimeAmPm,
          "EventDurationInHr": requestBody.EventDurationInHr,
          "EventDurationInMin": requestBody.EventDurationInMin,
          "EventDescription": requestBody.EventDescription,
          "Notes": requestBody.Notes,
          "MaxParticipantsAllowed": requestBody.MaxParticipantsAllowed,
          "AvailableBookingCount": requestBody.MaxParticipantsAllowed,
          "NoOfParticipantPerBooking": requestBody.NoOfParticipantPerBooking,
          "Complete": event.Complete
        }
        documentClient.update(params1, function (err, data) {
          if (err) {
            return callback(err, response(400, err));
          }
          let respObj = { "responseObj": responseObj, "message": "Event's Details updated successfully..!" }
          return callback(err, response(200, JSON.stringify(respObj)));

        })
      }
      else {
        let respObj = { "message": "data with given id not found !" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}

//change all data complete true.
module.exports.submittedData = function (event, context, callback) {
  globalEvent = event;
  const id = event.pathParameters.Id;
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    // console.log("Response.... "+ JSON.stringify(validateTokenResp));
    if (validateTokenResp.statusCode != 200) {
      console.log("Response .in if............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      //console.log("Response . in else............. " + JSON.stringify(validateTokenResp));
      stautsChange(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function stautsChange(event, context, validateTokenResp, callback){
  const id = event.pathParameters.Id;
  if (!id) {
    let respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  let params = {
    TableName: process.env.ADMINCONFIGEVENT_TABLE,
    Key: {
      "Id": id
    },
  }
  documentClient.get(params, function (err, data) {
    if (err) {
      return response(400, JSON.stringify(err));
    } else {
      if (data.Item) {
        const event = data.Item;
        if (event.imageUrl != "" && event.imageUrl != undefined && event.imageUrl != null || event.thumbnailUrl != "" && event.thumbnailUrl == undefined && event.thumbnailUrl == null) {
          let params1 = {
            TableName: process.env.ADMINCONFIGEVENT_TABLE,
            Key: {
              "Id": id
            },
            UpdateExpression: 'set #Complete = :C',
            ExpressionAttributeValues: {
              ':C': "true",
            },
            ExpressionAttributeNames: {

              '#Complete': 'Complete',
            },
            ReturnValues: "UPDATED_NEW"
          };
          const responseObj = {
            "Id": event.Id,
            "EventType": event.EventType,
            "EventName": event.EventName,
            "RequestedBy": event.RequestedBy,
            "EventDate": event.EventDate,
            "EventTimeInHr": event.EventTimeInHr,
            "EventTimeInMin": event.EventTimeInMin,
            "EventTimeAmPm": event.EventTimeAmPm,
            "EventDurationInHr": event.EventDurationInHr,
            "EventDurationInMin": event.EventDurationInMin,
            "EventDescription": event.EventDescription,
            "Notes": event.Notes,
            "MaxParticipantsAllowed": event.MaxParticipantsAllowed,
            "AvailableBookingCount": event.MaxParticipantsAllowed,
            "NoOfParticipantPerBooking": event.NoOfParticipantPerBooking,
            "Complete": "true",
            "imageUrl": event.imageUrl
          }
          documentClient.update(params1, function (err, data) {
            if (err) {
              return callback(err, response(400, err));
            }

            // Send push Notification

            var payload = {
              EventName: event.EventName,
              DetailType: 'New Event',
              Detail: `newEvent`,
              EventDate: event.EventDate,
              EventTimeInHr: event.EventTimeInHr,
              EventTimeInMin: event.EventTimeInMin,
              EventTimeAmPm: event.EventTimeAmPm
            }

            eventBridge.putEvents({
              Entries: [
                {
                  Detail: JSON.stringify({ name: payload }),
                  DetailType: 'Trigger custom push notification function',
                  EventBusName: 'mnm-museum-event-bus1',
                  Source: 'new.push',
                },
              ]
            }, function (err, data) {
              if (err) console.log("ERROR in putting event on eventbridge  ", err, err.stack); // an error occurred
              else console.log("Event put on eventBridge  ", data);           // successful response
            });
            let respObj = { "responseObj": responseObj, "message": "Event's Details updated successfully..!" }
            return callback(err, response(200, JSON.stringify(respObj)));

          })
        }
        else {
          let respObj = { "message": "data with given id not found ! upload Image/Audio/Video" }
          return callback(null, response(404, JSON.stringify(respObj)));
        }
      }
      else {
        let respObj = { "message": "No response found may be data not there" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}
//Only return true value
module.exports.getCompleteEventsTrueValue = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT Token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
    else{
      gettingTruevalue(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function gettingTruevalue(event, context, validateTokenResp, callback){
  let dateobj = new Date();
  // Formating the date and time
  // by using date.format() method
  const value = date.format(dateobj, 'YYYY-MM-DD');
  let dateID = value.toString();
  let params = {
    TableName: process.env.ADMINCONFIGEVENT_TABLE,
    FilterExpression: '#Complete = :Complete AND #Status = :Status AND #EventDate >= :EventDate',
    //FilterExpression: '#Complete = :Complete AND #Status = :Status',
    ExpressionAttributeNames: {
      "#Complete": "Complete",
      "#Status": "Status",
      "#EventDate": "EventDate"
    },
    ExpressionAttributeValues: {
      ':Complete': "true",
      ':Status': "Active",
      ':EventDate': dateID
    },
    KeyConditionExpression: 'Complete = :Complete AND Status = :Status AND EventDate = :EventDate',

  };
  console.log("Params ...",params);
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    let responseObj = data;
    let respObj = { "responseObj": responseObj, "message": "All fields fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));
  });
}

module.exports.filterEvent = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT Token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  const requestParams = (event.queryStringParameters);
  var value1 = moment(requestParams.DateTo).add(1, 'days');
  console.log("value1 :::", value1)
  var value = moment(value1).format('YYYY-MM-DD');
  console.log("date :::", value)
  let params = {
    TableName: process.env.ADMINCONFIGEVENT_TABLE,
    FilterExpression: '#EventType = :EventType AND #EventDate BETWEEN :dateFrom and :dateTo',
    ExpressionAttributeNames: {
      "#EventType": "EventType",
      "#EventDate": "EventDate"
    },
    ExpressionAttributeValues: {
      ':EventType': requestParams.EventType,
      ':dateFrom': requestParams.DateFrom,
      ':dateTo': value
    },
    KeyConditionExpression: 'EventType = :EventType AND EventDate BETWEEN :datefrom and :dateTo',
  };
  //console.log("params :: ",params)
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    let responseObj = data;
    let respObj = { "responseObj": responseObj, "message": "All fields fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));
  });
}
//Only return true value and getting only upcomming data
module.exports.upcommingEvents = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT Token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();


  let dateobj = new Date();
  const value = date.format(dateobj, 'YYYY-MM-DD');
  let dateID = value.toString();
  let params = {
    TableName: process.env.ADMINCONFIGEVENT_TABLE,
    FilterExpression: '#Complete = :Complete AND #Status = :Status AND #EventDate >= :EventDate',
    //FilterExpression: '#Complete = :Complete AND #Status = :Status',
    ExpressionAttributeNames: {
      "#Complete": "Complete",
      "#Status": "Status",
      "#EventDate": "EventDate"
    },
    ExpressionAttributeValues: {
      ':Complete': "true",
      ':Status': "Active",
      ':EventDate': dateID
    },
    KeyConditionExpression: 'Complete = :Complete AND Status = :Status AND EventDate = :EventDate',
  };
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    if (data.Count <= 6) {
      let responseObj = data;
      var eventData = data.Items;
      const sortedData = eventData.sort((a, b) => new Date(a.EventDate) - new Date(b.EventDate));
      responseObj.Items = sortedData;

      let respObj = { "responseObj": responseObj, "message": "All fields fetched successfully..!" }
      return callback(null, response(200, JSON.stringify(respObj)));
    }
  });
}
module.exports.pastEvents = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT Token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();
  let dateobj = new Date();
  const value = date.format(dateobj, 'YYYY-MM-DD');
  let dateID = value.toString();
  let params = {
    TableName: process.env.ADMINCONFIGEVENT_TABLE,
    FilterExpression: '#Complete = :Complete AND #Status = :Status AND #EventDate < :EventDate',
    //FilterExpression: '#Complete = :Complete AND #Status = :Status',
    ExpressionAttributeNames: {
      "#Complete": "Complete",
      "#Status": "Status",
      "#EventDate": "EventDate"
    },
    ExpressionAttributeValues: {
      ':Complete': "true",
      ':Status': "Active",
      ':EventDate': dateID
    },
    KeyConditionExpression: 'Complete = :Complete AND Status = :Status AND EventDate = :EventDate',
  };
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    let responseObj = data;
    let respObj = { "responseObj": responseObj, "message": "All fields fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));

  });

}