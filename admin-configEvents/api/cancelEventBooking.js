'use strict';

var AWS = require('aws-sdk'),

documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
const notification = require('../notifications/sendNotification');
const eventBridge = new AWS.EventBridge({ region: 'ap-south-1' });

var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}


module.exports.cancelEventBooking = async function (Id,email,eventData ,event) {
    globalEvent = event;
    debugger;

    // const requestBody = JSON.parse(event.body);
    // var validateToken = auth.validateToken(event, context, callback);
    // if (validateToken.statusCode != 200) {
    //     return callback(null, response(validateToken.statusCode, JSON.stringify(validateToken)));
    // }
    if (!Id) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }

    //-- Check whether booking data present for given Id
    var searchWithId = {
        TableName: process.env.BOOKING_TABLE,
        ExpressionAttributeValues: {
            ":id": Id
        },

        KeyConditionExpression: "Id = :id"
    }

    console.log("Search Params ..." + JSON.stringify(searchWithId));

    var data = await documentClient.query(searchWithId).promise();

    // console.log("GetItem succeeded:", JSON.stringify(data));
    //   console.log("count of records fro email ... " + data.Count)
    if (data.Count > 0) {
        var modifiedPhone = ""
        var oldBookingData = data.Items[0];
        var visitorEmail = oldBookingData.VisitorEmail;
        updateBookingInfo(Id, email, oldBookingData);
        var phone = (oldBookingData.CountryCode) + (oldBookingData.VisitorPhone);
        var countryCodeSign = phone.substring(0, 1);
        if (countryCodeSign === "+") {
            modifiedPhone = phone.substring(1);
        } else {
            modifiedPhone = phone;
        }

        //sms add
        var smsText = "We regret to inform you that due to unforeseen circumstances your visit to Mahindra Museum on " + oldBookingData.BookingDate + " for the event " + eventData.EventName + " is cancelled. We regret inconvenience caused -M & M Ltd."
        var typeOfTemplate = '1107162712348946217';
        var smsNotification = notification.sendSMSNotification(modifiedPhone, smsText, typeOfTemplate);

        // Email Notifications
        var subject = "Event Cancle"
        var emailNotification = notification.sendEmailNotification(visitorEmail, subject, smsText)

        // Sending push Notification
        var cancelledBookingIds = [];
        cancelledBookingIds.push(oldBookingData.Id);
        console.log("putting eventbridge data.........")
        var payload = {
            EventDate: eventData.EventDate,
            EventTimeAmPm: eventData.EventTimeAmPm,
            EventTimeInHr: eventData.EventTimeInHr,
            EventTimeInMin: eventData.EventTimeInMin,
            EventName: eventData.EventName,
            cancelledBookingIds: cancelledBookingIds,
            DetailType: 'cancle Booking',
            Detail: 'cancelBooking',

        }

        console.log("payload",payload)
        eventBridge.putEvents({
            Entries: [
                {
                    Detail: JSON.stringify({ name: payload }),
                    DetailType: 'Trigger custom push notification function',
                    EventBusName: 'mnm-museum-event-bus1',
                    Source: 'new.push',
                },
            ]
        }, function (err, data) {
            if (err) console.log("ERROR in putting event on eventbridge  ", err, err.stack); // an error occurred
            else console.log("Event put on eventBridge  ", data);           // successful response
        });

    } else {
        console.log("Bookings not available for given ID");
        var respObj = { "message": "Bookings not available for given ID" };
        return callback(null, response(404, JSON.stringify(respObj)));
    }

}



function updateBookingInfo(Id, email, oldBookingData) {
    // globalEvent = event;
    // var requestBody = JSON.parse(event.body);
    // const Id = event.pathParameters.Id;
    //-- update details into booking table
    var updateBookingData = {
        TableName: process.env.BOOKING_TABLE,
        Key: {
            "Id": Id
        },
        UpdateExpression: 'set #BookingCancelled = :BC, #BookingStatus = :BS, #CancelledBy = :CB, #UpdatedBy = :U,#UpdatedAt = :UA',
        ExpressionAttributeValues: {
            ":BC": true,
            ":BS": "Cancelled",
            ":CB": email,
            ':U': email,
            ':UA': new Date().toISOString()
        },
        ExpressionAttributeNames: {
            "#BookingCancelled": "BookingCancelled",
            "#BookingStatus": "BookingStatus",
            "#CancelledBy": "CancelledBy",
            '#UpdatedBy': 'UpdatedBy',
            "#UpdatedAt": "UpdatedAt"
        },
        ReturnValues: "UPDATED_NEW"
    }

    console.log("Data..... " + updateBookingData);
    documentClient.update(updateBookingData, function (err, data) {
        //-- Call API to genretae QR code and update the same into a booking table
        if (err) {
            console.log("Data............" + err);
        }
        console.log("Data............" + JSON.stringify(data));

        var phone = (oldBookingData.CountryCode) + (oldBookingData.VisitorPhone);
        var countryCodeSign = phone.substring(0, 1);
        if (countryCodeSign === "+") {
            var modifiedPhone = phone.substring(1);
        } else {
            var modifiedPhone = phone;
        }
        //sms add
        var smsText = "Please note that your visit to Mahindra Museum on " + oldBookingData.BookingDate + " is cancelled. Please use our app to book your visit again. -M & M Ltd."
        var typeOfTemplate = '1107162712334634754';
        var smsNotification = notification.sendSMSNotification(modifiedPhone, smsText, typeOfTemplate);

        console.log("Booking details updated successfully." + JSON.stringify(data));
        var respObj = { "message": "Booking cancelled successfully." }
        console.log(respObj)


        //  updateBookingHistory(event, callback, visitorEmail);

    })
}

function updateBookingHistory(event, callback, visitorEmail) {
    globalEvent = event;

    // let validateToken = auth.validateToken(event, context, callback);
    // if (validateToken.statusCode != 200) {
    //     return callback(null, response(validateToken.statusCode, JSON.stringify(validateToken)));
    // }
    var requestBody = JSON.parse(event.body);
    const Id = event.pathParameters.Id;

    var searchWithEmail = {
        TableName: process.env.BOOKINGINFO_TABLE,
        ExpressionAttributeValues: {
            ":vEmail": visitorEmail
        },

        KeyConditionExpression: "VisitorEmail = :vEmail"
    }

    console.log("Search Params ..." + JSON.stringify(searchWithEmail));

    documentClient.query(searchWithEmail, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
            // console.log("GetItem succeeded:", JSON.stringify(data));
            //   console.log("count of records fro email ... " + data.Count)
            //bookingData.Item.QRCode = "";
            if (data.Count == 0) {

                var respObj = { "message": "Booking details are not available for given emailId" }
                return callback(null, response(404, JSON.stringify(respObj)));

            } else {

                var previousData = data.Items[0];
                //console.log("bookingArray... " + bookingArray.length);
                var bookingArray = previousData.BookingData;
                //bookingArray.push(bookingData.Item);

                var bookingcount = bookingArray.length;
                console.log("Id.... " + Id);
                bookingArray.forEach(data => {
                    console.log("BookingId.. " + data.Id);
                    if (data.Id === Id) {
                        data.BookingCancelled = true;
                        data.BookingStatus = "Cancelled";

                        var updateParams = {
                            TableName: process.env.BOOKINGINFO_TABLE,
                            Key: {
                                "VisitorEmail": visitorEmail
                            },
                            UpdateExpression: 'set #BookingData = :BD,#UpdatedBy = :U,#UpdatedAt = :UA',
                            ExpressionAttributeValues: {
                                ':BD': bookingArray,
                                ':U': requestBody.VisitorEmail,
                                ':UA': new Date().toISOString()
                            },
                            ExpressionAttributeNames: {
                                '#BookingData': 'BookingData',
                                '#UpdatedBy': 'UpdatedBy',
                                "#UpdatedAt": "UpdatedAt"

                            },
                            ReturnValues: "UPDATED_NEW"
                        };
                        //console.log("updateParams.. " + JSON.stringify(updateParams));
                        documentClient.update(updateParams, function (err, data) {
                            if (err) {
                                console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                                return callback(err, response(400, err));
                            }
                            // console.log("Update response.. " + JSON.stringify(data));
                            var respObj = { "message": "Booking cancelled successfully." }
                            return callback(null, response(200, JSON.stringify(respObj)));
                        })


                    } else {
                        bookingcount--;
                    }

                    if (bookingcount == 0) {
                        console.log("Booking Id not found in Booking Info table");
                        var respObj = { "message": "Booking Id not found in Booking Info table" }
                        return callback(null, response(404, JSON.stringify(respObj)));
                    }
                })

            }

        }
    })
}



