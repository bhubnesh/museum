'use strict';

var AWS = require('aws-sdk'),

    uuid = require('uuid'),
    documentClient = new AWS.DynamoDB.DocumentClient();

const auth = require('../auth/token');
let paths = require('../config/apiEndpoints');
let cancleBooking = require('../api/cancelEventBooking');
const axios = require('axios');
//const { cancelEventBooking } = require('../../booking-services/api/cancelEventBooking');
const eventBridge = new AWS.EventBridge({ region: 'ap-south-1' });
const notification = require('../notifications/sendNotification');
var pushNotificationEmails = [];

var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}


//change all data complete true.
module.exports.cancleEvents = async function (event, context, callback) {
    globalEvent = event;
    var err = null;
    const id = event.pathParameters.Id;
    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();

    const loggedInUser = event.loggedInUser;
    if (!id) {
        let respObj = { "message": "Please provide Id" }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }
    let params = {
        TableName: process.env.ADMINCONFIGEVENT_TABLE,
        Key: {
            "Id": id
        },
    }
    var data = await documentClient.get(params).promise()
    console.log("data",data)
    if (data.Item) {
        const eventData = data.Item;
        if (eventData.Status == "Active") {
            let params1 = {
                TableName: process.env.ADMINCONFIGEVENT_TABLE,
                Key: {
                    "Id": id
                },
                UpdateExpression: 'set #Status = :S',
                ExpressionAttributeValues: {
                    ':S': "cancel"
                },
                ExpressionAttributeNames: {
                    '#Status': 'Status'
                },
                ReturnValues: "UPDATED_NEW"
            };
            const responseObj = {
                "Id": id,
                "EventType": eventData.EventType,
                "EventName": eventData.EventName,
                "RequestedBy": eventData.RequestedBy,
                "EventDate": eventData.EventDate,
                "EventTimeInHr": eventData.EventTimeInHr,
                "EventTimeInMin": eventData.EventTimeInMin,
                "EventTimeAmPm": eventData.EventTimeAmPm,
                "EventDurationInHr": eventData.EventDurationInHr,
                "EventDurationInMin": eventData.EventDurationInMin,
                "EventDescription": eventData.EventDescription,
                "Notes": eventData.Notes,
                "MaxParticipantsAllowed": eventData.MaxParticipantsAllowed,
                "AvailableBookingCount": eventData.MaxParticipantsAllowed,
                "NoOfParticipantPerBooking": eventData.NoOfParticipantPerBooking,
                "Complete": eventData.Complete,
                "imageUrl": eventData.imageUrl,
                "Status": "cancel"
            }
            await documentClient.update(params1).promise();


            var bookingIds = await getEventUser(eventData);
            console.log("bookingIds    ", bookingIds);
            for (const id of bookingIds) {
               await cancleBooking.cancelEventBooking(id, loggedInUser, eventData,event)


            }
            // Send Push Notifications
            var payload = {
                EventName: event.EventName,
                RequestedBy: event.RequestedBy,
                EventDate: event.EventDate,
                EventTimeInHr: event.EventTimeInHr,
                EventTimeInMin: event.EventTimeInMin,
                EventTimeAmPm: event.EventTimeAmPm,
                EventId: event.Id,
                pushNotificationEmails: pushNotificationEmails,
                DetailType: 'Cancel Event',
                Detail: `EventCancelled`,
            }
            console.log("payload in js ",payload)
            eventBridge.putEvents({
                Entries: [
                    {
                        Detail: JSON.stringify({ name: payload }),
                        DetailType: 'Trigger custom push notification function',
                        EventBusName: 'mnm-museum-event-bus1',
                        Source: 'new.push',
                    },
                ]
            }, function (err, data) {
                if (err) { err = err; console.log("ERROR in putting event on eventbridge  ", err, err.stack); } // an error occurred
                else console.log("Event put on eventBridge  ", data);           // successful response
            });


            let respObj = { "responseObj": responseObj, "message": "Event's is cancelled now!" }
            return callback(err, response(200, JSON.stringify(respObj)));

        }
        else {
            let respObj = { "message": "Given id Status is already Cancle ! Please try another event" }
            return callback(null, response(404, JSON.stringify(respObj)));
        }
    }
    else {
        let respObj = { "message": "No response found may be data not there" }
        return callback(null, response(404, JSON.stringify(respObj)));
    }

}

//change all data complete true.
module.exports.cancleAllEvents = async function (event, context, callback) {
    globalEvent = event;
    var responseObjArray = []
    const requestBody = JSON.parse(event.body);
    if (!requestBody.listOfEvents) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }
    //get through requestBody
    var listOfEvents = requestBody.listOfEvents;
    console.log("listOfEvents", listOfEvents)
    if (listOfEvents != null) {
        let cnt = 0;
        for (const events of listOfEvents) {
            pushNotificationEmails = [];
            cnt++;
            var id = events.id;
            var params = {
                TableName: process.env.ADMINCONFIGEVENT_TABLE,
                Key: {
                    "Id": id
                }
            };
            var data = await documentClient.get(params).promise();
            console.log("data", data)
            if (data.Item) {
                const event = data.Item;
                if (event.Status == "Active") {
                    let params1 = {
                        TableName: process.env.ADMINCONFIGEVENT_TABLE,
                        Key: {
                            "Id": id
                        },
                        UpdateExpression: 'set #Status = :S',
                        ExpressionAttributeValues: {
                            ':S': "cancel"
                        },
                        ExpressionAttributeNames: {
                            '#Status': 'Status'
                        },
                        ReturnValues: "UPDATED_NEW"
                    };
                    const responseObj = {
                        "Id": event.Id,
                        "EventType": event.EventType,
                        "EventName": event.EventName,
                        "RequestedBy": event.RequestedBy,
                        "EventDate": event.EventDate,
                        "EventTimeInHr": event.EventTimeInHr,
                        "EventTimeInMin": event.EventTimeInMin,
                        "EventTimeAmPm": event.EventTimeAmPm,
                        "EventDurationInHr": event.EventDurationInHr,
                        "EventDurationInMin": event.EventDurationInMin,
                        "EventDescription": event.EventDescription,
                        "Notes": event.Notes,
                        "MaxParticipantsAllowed": event.MaxParticipantsAllowed,
                        "AvailableBookingCount": event.MaxParticipantsAllowed,
                        "NoOfParticipantPerBooking": event.NoOfParticipantPerBooking,
                        "Complete": event.Complete,
                        "imageUrl": event.imageUrl,
                        "Status": "cancel"
                    }
                    var data = documentClient.update(params1).promise();
                    responseObjArray.push(responseObj)
                   
                    var bookingIds = await getEventUser(event);
                    console.log("bookingIds ", bookingIds)
                    for (const id of bookingIds) {
                        // Cancle Event
                        await cancleBooking.cancelEventBooking(id, loggedInUser, event)
                    }
                    // Send Push Notifications
                    var payload = {
                        EventName: event.EventName,
                        RequestedBy: event.RequestedBy,
                        EventDate: event.EventDate,
                        EventTimeInHr: event.EventTimeInHr,
                        EventTimeInMin: event.EventTimeInMin,
                        EventTimeAmPm: event.EventTimeAmPm,
                        EventId: event.Id,
                        pushNotificationEmails: pushNotificationEmails,
                        DetailType: 'Cancel Event',
                        Detail: `EventCancelled`,
                    }
                    
                    eventBridge.putEvents({
                        Entries: [
                            {
                                Detail: JSON.stringify({ name: payload }),
                                DetailType: 'Trigger custom push notification function',
                                EventBusName: 'mnm-museum-event-bus1',
                                Source: 'new.push',
                            },
                        ]
                    }, function (err, data) {
                        if (err) { err = err; console.log("ERROR in putting event on eventbridge  ", err, err.stack); } // an error occurred
                        else console.log("Event put on eventBridge  ", data);           // successful response
                    });

                }
                else {
                    let respObj = { "message": "Given id Status is already Cancle ! Please try another event" }
                    return callback(null, response(404, JSON.stringify(respObj)));
                }
            }
            else {
                let respObj = { "message": "No response found may be data not there" }
                return callback(null, response(404, JSON.stringify(respObj)));
            }

        }
        let respObj = { "responseObj": responseObjArray, "message": "Event's is cancelled now!" }
        return callback(null, response(200, JSON.stringify(respObj)));



    } else {
        var respObj = { "message": "Id was provided incorrect" }
        return callback(err, response(err, JSON.stringify(respObj)));
    }
}


async function getEventUser(event) {
   // globalEvent = event;

    var eventId = event.Id
    console.log("eventId :: ", eventId);
    var bookingIds = [];




    var emailIds = {
        TableName: process.env.BOOKING_TABLE

    };
    // get users from the event
    var data = await documentClient.scan(emailIds).promise();
    //console.log("data :: ", data);
    for (const item of data.Items) {
        if (item.EventId && item.EventId == eventId) {


            bookingIds = bookingIds.concat(item.Id);
            pushNotificationEmails.push(item.VisitorEmail)

        }

    }
    console.log("bookingIds :: ", bookingIds);

    return bookingIds;
}