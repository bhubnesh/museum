'use strict';
//-- For about us, Terms and conditions and Privacy policy

var AWS = require('aws-sdk'),
    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');

var globalEvent = "";

function response(statusCode, message) {

    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Origin;
    }
    if (origin == "" || origin == undefined || !origin) {
        origin = globalEvent.headers.Host;
    }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
    }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}


module.exports.addContent = function (event, context, callback) {
    globalEvent = event;
    const requestBody = JSON.parse(event.body);
    
    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();

    if (!requestBody.Type || !requestBody.Content) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }
    var params = {
        Item: {
            "Type": requestBody.Type,
            "Content": requestBody.Content,
            "UpdatedAt": new Date().toISOString(),
            "CreatedAt": new Date().toISOString(),
        },
        TableName: process.env.CONTENT_TABLE
    };
    console.log("params",params);
    //-- Check whether Content already exists
    var params1 = {
        TableName: process.env.CONTENT_TABLE,
        Key: {
            "Type": requestBody.Type
        },
    };
    console.log("params1.. " + JSON.stringify(params1));
    documentClient.get(params1, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            if (data.Item) {
                console.log("Content with type - **************" + requestBody.Type + " already exists.");
                var respObj = { "message": "Content already exists" }
                //-- Update content
                var updateParams = {
                    Item:{
                    "Type": requestBody.Type,
                    "Content": requestBody.Content,
                    "UpdatedAt": new Date().toISOString(),
                    "CreatedAt": new Date().toISOString(),
                    },
                    TableName: process.env.CONTENT_TABLE
                };
                console.log("Update params.. " + JSON.stringify(updateParams));
                documentClient.put(updateParams, function (err, data) {
                    if (err) {
                        return callback(err, response(400, JSON.stringify(err)));
                    }
                    console.log(data);
                    var respObj = { "message": "Content updated successfully..!" }
                    return callback(err, response(200, JSON.stringify(respObj)));
                });

            } else {
                var respObj = { "message": "Content created successfully..!" }
                documentClient.put(params, function (err, data) {
                    return callback(err, response(200, JSON.stringify(respObj)));
                });
            }
        }
    })
}
module.exports.updateContent = function (event, context, callback) {
    globalEvent = event;
    const requestBody = JSON.parse(event.body);

    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if (validateTokenResp.statusCode != 200) {
            console.log("Response .............. " + JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
    }
    validateMultipleLogin();

    if (!requestBody.Type) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }

    var params = {
        TableName: process.env.CONTENT_TABLE,
        ExpressionAttributeValues: {
            ":type": requestBody.Type
        },
        KeyConditionExpression: '#type = :type',
        ExpressionAttributeNames: {
            "#type": "Type"
        }

    };
    console.log("params.. " + JSON.stringify(params));
    documentClient.query(params, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            console.log("count of records ... " + data.Count)
            if (data.Count > 0) {

                var updateParams = {
                    Key: {
                        "Type": requestBody.Type
                    },
                    TableName: process.env.CONTENT_TABLE,
                    UpdateExpression: "set #content = :content ",
                    ExpressionAttributeValues: {
                        ":content": requestBody.Content
                    },
                    ExpressionAttributeNames: {
                        "#content": "Content"
                    },
                    ReturnValues: "UPDATED_NEW"
                };
                console.log("Update params.. " + JSON.stringify(updateParams));
                documentClient.update(updateParams, function (err, data) {
                    if (err) {
                        return callback(err, response(400, JSON.stringify(err)));
                    }
                    console.log(data);
                    var respObj = { "message": "Content updated successfully..!" }
                    return callback(err, response(200, JSON.stringify(respObj)));
                });
            } else {
                console.log("Content not found");
                var respObj = { "message": "Content not found." }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })
}

module.exports.getAllContents = function (event, context, callback) {
    globalEvent = event;
    /*
        var validateToken = auth.validateToken(event, context, callback);
    
        if (validateToken.statusCode != 200) {
            return callback(null, response(validateToken.statusCode, JSON.stringify(validateToken)));
        }
    */
    var params = {
        TableName: process.env.CONTENT_TABLE,
    }
    documentClient.scan(params, function (err, data) {
        if (err) {
            return callback(err, response(400, JSON.stringify(err)));
        }
        console.log(data);
        var respObj = { "responseObj": data.Items, "message": "Content data fetched successfully..!" }
        return callback(null, response(200, JSON.stringify(respObj)));
    })

}

module.exports.getContent = function (event, context, callback) {
    const id = event.pathParameters.Type;
    globalEvent = event;
    // var validateToken = auth.validateToken(event, context, callback);
    // if (validateToken.statusCode != 200) {
    //     return callback(null, response(validateToken.statusCode, JSON.stringify(validateToken)));
    // }

    if (!id) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }

    var params = {
        TableName: process.env.CONTENT_TABLE,
        ExpressionAttributeValues: {
            ":type": id
        },
        KeyConditionExpression: '#type = :type',
        ExpressionAttributeNames: {
            "#type": "Type"
        }

    };
    console.log("params.. " + JSON.stringify(params));
    documentClient.query(params, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return callback(err, response(400, JSON.stringify(err)));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            console.log("count of records ... " + data.Count)
            if (data.Count > 0) {
                var respObj = { "responseObj": data.Items[0], "message": "Content fetched successfully." }
                return callback(null, response(200, JSON.stringify(respObj)));
            } else {
                var respObj = { "message": "Content with type- " + id + " not found." }
                return callback(null, response(200, JSON.stringify(respObj)));
            }
        }
    })

}
