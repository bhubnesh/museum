'use strict';

const { SNS } = require('aws-sdk');
var AWS = require('aws-sdk');
var sns = new AWS.SNS();
var documentClient = new AWS.DynamoDB.DocumentClient({ convertEmptyValues: true });
var moment = require('moment');
var alluser = false;
var eventUser = false;
const AllUserTopic = "AllUserTopic";
const EventTopic = "EventTopic";
var eventType = '';
var topicNameToDelete = [];
const notification = require('../notifications/sendNotification');

var params = {
  Message: "",
  Subject: "",
  TargetArn: "",
  MessageStructure: 'json'

};

var subscribeParam = {
  Protocol: 'application',
  TopicArn: 'topicArn',
  Endpoint: 'MOBILE_END_point'
}

var dateFormat = ["YYYY-MM-DD HH:mm", "YYYY-MM-DD HH:m", "YYYY-MM-DD H:mm", "YYYY-MM-DD H:m",
  "YYYY-MM-D HH:mm", "YYYY-MM-D HH:m", "YYYY-MM-D H:mm", "YYYY-MM-D H:m",
  "YYYY-M-DD HH:mm", "YYYY-M-DD HH:m", "YYYY-M-DD H:mm", "YYYY-M-DD H:m",
  "YYYY-M-D HH:mm", "YYYY-M-D HH:m", "YYYY-M-D H:mm", "YYYY-M-D H:m",
]

var title = ";"

module.exports.sendPush = async (event, callback) => {

  console.log(event.detail);
  //const requestBody = JSON.parse(event.detail);
  // real request
  const requestBody = event.detail.name;
  //test request
  //const requestBody = JSON.parse(event.body);
  console.log(requestBody);
  eventType = requestBody.Detail;
  console.log(eventType);






  // Setting message for new event
  if (eventType == 'newEvent') {
    var eventDate = moment(requestBody.eventDate).format("DD MMM YYYY");
    var eventTime = requestBody.EventTimeInHr + ":" + requestBody.EventTimeInMin + " " + requestBody.EventTimeAmPm;
    title = "New Event at the Mahindra Museum on " + eventDate + " at " + eventTime + ". Seats are limited, so hurry and book."
    alluser = true;
    eventUser = false;
    let payload = {
      default: 'default',
      GCM: {
        notification: {
          title: title,
          body: title,
          sound: 'default'
        }
      },
      APNS: JSON.stringify({
        default: 'default',
        aps: {
          alert: {
            title: title,
            body: requestBody.EventName,
            action: "action url"
          },
          sound: "default"

        }
      })
    }
    payload.GCM = JSON.stringify(payload.GCM);

    payload = JSON.stringify(payload);
    console.log("requestBody :: ", requestBody.EventName);
    params.Subject = requestBody.EventName;
    params.Message = payload;
    params.TargetArn = await getTopics();

  } else if (eventType == 'alertFromAdmin') {
    alluser = true;
    eventUser = false;
    // Setting message for alert from admin

    let payload = {
      default: 'default',
      GCM: {
        notification: {
          title: 'Alert!! From @M&M Museum',
          body: requestBody.Description,
          sound: 'default'
        }
      },
      APNS: JSON.stringify({
        aps: {
          alert: {
            title: 'Alert!! From @M&M Museum',
            body: requestBody.Description,
            action: "action url"
          },
          sound: "default"

        }
      })
    }
    payload.GCM = JSON.stringify(payload.GCM);

    payload = JSON.stringify(payload);
    console.log("requestBody.EventId.length :: ", requestBody.EventId);
    params.Subject = requestBody.Description;
    params.Message = payload;



    if (typeof requestBody.EventId != null && requestBody.EventId != null && requestBody.EventId.length > 0) {

      alluser = false;
      eventUser = true
    }
    params.TargetArn = await getTopics(requestBody);

    if (typeof requestBody.EventId != null && requestBody.EventId != null && requestBody.EventId.length > 0) {
      params.TargetArn = await getEventUser(requestBody);

    } else {
      console.log("EventId not received!!");
    }
    //subscribeParam.TopicArn = params.TargetArn;

  } else if (eventType == 'cancelBooking' || eventType == 'cancleBooking') {
    // Setting message for alert from admin
    // eventUser = true
    // alluser = false;
    var cancelledBookingIds = requestBody.cancelledBookingIds;
    for (const id of cancelledBookingIds) {
      var searchWithId = {
        TableName: process.env.BOOKINGINFO_TABLE,
        ExpressionAttributeValues: {
          ":id": id
        },

        KeyConditionExpression: "Id = :id"
      }

      console.log("Search Params ..." + JSON.stringify(searchWithId));

      var data = await documentClient.query(searchWithId).promise();
      console.log("data : ",data)
      var user = data.Items[0];
      var tokens = {
        TableName: process.env.DEVICE_TOKENS,

        ProjectExpression: "Id,ARN",
        KeyConditionExpression: "#Id = :email",
        // Filterexpression : "Id IN ("+Object.keys(emailObject).toString()+")",
        ExpressionAttributeNames: {
          "#Id": "Id"
        },

        ExpressionAttributeValues: {
          ":email": user.VisitorEmail
        }
      };


      console.log(tokens);
      var bookingDate = moment(user.BookingDate)
      var res = await documentClient.query(tokens).promise();
      console.log("res : ", res);
      if(user.TypeOfBooking == "Event"){
        console.log("Event Booking......................")
        var eventData = requestBody
        var eventDate = moment(eventData.eventDate).format("DD MMM YYYY");
        if(typeof eventData.EventTimeAmPm != 'offline'){
        var eventTime = eventData.EventTimeInHr + ":" + eventData.EventTimeInMin + " " + eventData.EventTimeAmPm;
        }else{
          var eventTime = eventData.EventTime
        }
        var body ="This is to inform you that your booking has been cancelled for the event "+eventData.EventName+" on "+eventDate+" at "+eventTime+" due to unforseen circumstances. You can contact us at museum@mahindra.com to know more"
       
  
      }else{
        console.log("Normal Booking......................")
        var body = "This is to inform you that your booking has been cancelled for " + bookingDate.format("DD MMM YYYY") + " at the time of your booking. You can contact us at museum@mahindra.com to know more";
      }
      let payload = {
        default: 'default',
        GCM: {
          notification: {
            title: 'Visit Cancelled',
            body: body,
            sound: 'default'
          }
        },
        APNS: JSON.stringify({
          aps: {
            alert: {
              title: 'Visit Cancelled',
              body: body,
              action: "action url"
            },
            sound: "default"
          }
        })
      }
      payload.GCM = JSON.stringify(payload.GCM);

      payload = JSON.stringify(payload);
      if (res.Items.length > 0) {
        params.TargetArn = res.Items[0].ARN;

        params.Subject = "Booking Cancelled!!";
        params.Message = payload;
        // console.log("params:: ", params);
        try{
        await publishNotification(params);
        }catch{
          throw Error;
        }

      }
    }
      // let payload = {
      //   default: 'default',
      //   GCM: {
      //     notification: {
      //       title: 'Alert!! Simulator offline',
      //       body: 'Alert!! Simulator offline at your visit',
      //       sound: 'default'
      //     }
      //   },
      //   APNS: JSON.stringify({
      //     aps: {
      //       alert: {
      //         title: 'Alert!! Simulator offline',
      //         body: 'Alert!! Simulator offline at your visit',
      //         action: "action url"
      //       },
      //       sound: "default"

      //     }
      //   })
      // }
      // payload.GCM = JSON.stringify(payload.GCM);

      // payload = JSON.stringify(payload);
      // console.log("requestBody :: ", requestBody.EventName);
      // params.Subject = Date() + 'Alert!! Simulator offline at your visit',
      //   params.Message = payload;
      // var eventIds = await getEventDetails(requestBody);
      // console.log("Inside event simulator Offline");
      // var eventIds = await getEventDetails(requestBody);
      // //console.log("eventIds : ",eventIds)

      // for (const eventId of eventIds) {

      //   params.TargetArn = await getTopics(requestBody);
      //   console.log("params in for eventID:: ", params)
      //   requestBody.EventId = eventId;
      //   params.TargetArn = await getEventUser(requestBody);

      // }

    } else if (eventType == 'museumOffline') {

      eventUser = true;
      alluser = false;



      console.log("Inside event museum Offline");

      // var eventIds = await getEventDetails(requestBody);
      //console.log("eventIds : ",eventIds)
      params.TargetArn = await getTopics(requestBody);
      await subscribeAndSendPush(requestBody);
      // for (const eventId of eventIds) {


      //   console.log("params in for eventID:: ", params)
      //   requestBody.EventId = eventId;
      // params.TargetArn = await getEventUser(requestBody);

      // }

    } else if (eventType == 'EventCancelled') {
      eventUser = true
      alluser = false;
      var eventData = requestBody
      var eventDate = moment(eventData.eventDate).format("DD MMM YYYY");
      var eventTime = eventData.EventTimeInHr + ":" + eventData.EventTimeInMin + " " + eventData.EventTimeAmPm;
      var body = "This is to inform you that the event" + eventData.EventName + " on " + eventDate + " at " + eventTime + " is cancelled due to unforseen circumstances."
     

      let payload = {
        default: 'default',
        GCM: {
          notification: {
            title: 'Alert!! Event Cancelled',
            body: body,
            sound: 'default'
          }
        },
        APNS: JSON.stringify({
          aps: {
            alert: {
              title: 'Alert!! Event Cancelled',
              body: body,
              action: "action url"
            },
            sound: "default"

          }
        })
      }
      payload.GCM = JSON.stringify(payload.GCM);

      payload = JSON.stringify(payload);
      console.log("eventData :: ", eventData.EventName);
      params.Subject = "Event Cancelled!";
      params.Message = payload;

      console.log("Inside event cancelled");
      params.Subject = eventData.EventName;
      params.Message = payload;
      params.TargetArn = await getTopics(requestBody);
    var users = eventData.pushNotificationEmails
    console.log("eventData ",eventData)
    if (eventData.EventId.length > 0) {
      await subscribeAndPush(users, tokens, res);

      } else {
        console.log("EventId not received!!");
      }

    }

    // Publish Notifications
    if (alluser) {
      var result = await publishNotification(params);
    }



    //return callback(null, response(200, JSON.stringify(respObj)));
  }

async function subscribeAndPush(users, res) {
  for (const user of users) {
    var tokens = {
      TableName: process.env.DEVICE_TOKENS,

      ProjectExpression: "Id,ARN",
      KeyConditionExpression: "#Id = :email",
      // Filterexpression : "Id IN ("+Object.keys(emailObject).toString()+")",
      ExpressionAttributeNames: {
        "#Id": "Id"
      },

      ExpressionAttributeValues: {
        ":email": user,
      }
    };


    console.log(tokens);
    var res = await documentClient.query(tokens).promise();
    console.log("res : ", res);

    subscribeParam.Endpoint = res.Items[0].ARN;


    console.log("subscribeParam 1:: ", subscribeParam);
    //subcribe to topic              
   var subRes = await sns.subscribe(subscribeParam).promise();
    console.log("subscribe :: ", subRes);
  }
  if (users.length > 0) {
    console.log("params:: ", params);
    await publishNotification(params);
  }

}

  async function subscribeAndSendPush(requestBody) {
    // let bookingMap = new Map();
    // bookingMap = await getUsersDetails(requestBody);

    // console.log(Object.entries(bookingMap))
    // for (const [key, value] of bookingMap) {
      console.log("++++++++++++++++++dateIdMap")
      var date1 = moment(requestBody.offlineDate);
      var users = requestBody.users;

      for (const user of users) {
        var tokens = {
          TableName: process.env.DEVICE_TOKENS,

          ProjectExpression: "Id,ARN",
          KeyConditionExpression: "#Id = :email",
          // Filterexpression : "Id IN ("+Object.keys(emailObject).toString()+")",
          ExpressionAttributeNames: {
            "#Id": "Id"
          },

          ExpressionAttributeValues: {
            ":email": user.email,
          }
        };


        console.log(tokens);
        var res = await documentClient.query(tokens).promise();
        console.log("res : ", res);

        // Set topic ARN to subscribe

        let payload = {
          default: 'default',
          GCM: {
            notification: {
              title: 'Museum Offline! Visit Cancelled',
              body: "We regret to inform you that the Mahindra Museum will be closed on " + date1.format("DD MMM YYYY") + " at the time of your booking. We request you to book another slot at your convenience",
              sound: 'default'
            }
          },
          APNS: JSON.stringify({
            aps: {
              alert: {
                title: 'Museum Offline! Visit Cancelled',
                body: "We regret to inform you that the Mahindra Museum will be closed on " + date1.format("DD MMM YYYY") + " at the time of your booking. We request you to book another slot at your convenience",
                action: "action url"
              },
              sound: "default"
            }
          })
        }
        payload.GCM = JSON.stringify(payload.GCM);

        payload = JSON.stringify(payload);
        if (res.Items.length > 0) {
          if(typeof res.Items != 'undeinfed' || res.Items != '[]'){
          params.TargetArn = res.Items[0].ARN;

          params.Subject = "Museum Offline!!";
          params.Message = payload;
           console.log("params:: ", params);
          await publishNotification(params);
          }
        }
        var smsText = "We regret to inform you that due to unforeseen circumstances Mahindra Museum will be offline on " + date1.format("DD MMM YYYY") + ". We regret inconvenience caused -M & M Ltd."
        var templateId = '1107162712354948673';

        //--- CAll SMS notification
        //sending SMS
       
        var smsNotification = notification.sendSMSNotification(user.phoneNo, smsText, templateId);
      }
    //}
    // var topicName = museumOffline_key;
    // topicNameToDelete.push(topicName)

    //create new topic for a date
    //   var cretaeParams = {
    //         Name: topicName, 

    //       };
    //     }
    //      var topicARN =  await sns.createTopic(cretaeParams).promise();
    //      console.log("topicARN : ",topicARN);

    //      // Get users mobileARN
    // for (const user of users) {
    //   var tokens = {
    //     TableName: process.env.DEVICE_TOKENS,

    //     ProjectExpression: "Id,ARN",
    //     KeyConditionExpression: "#Id = :email",
    //     // Filterexpression : "Id IN ("+Object.keys(emailObject).toString()+")",
    //     ExpressionAttributeNames: {
    //       "#Id": "Id"
    //     },

    //     ExpressionAttributeValues: {
    //       ":email": user,
    //     }
    //   };


    //   //console.log(tokens);
    //   var res = await documentClient.query(tokens).promise();
    //   console.log("res : ", res);

    //   // Set topic ARN to subscribe
    //   subscribeParam.TopicArn = topicARN;
    //   // Set USer ARN to subscribe
    //   subscribeParam.Endpoint = res.Items[0].ARN;


    //   console.log("subscribeParam 1:: ", subscribeParam);
    //   //subcribe to topic              
    //   var subRes = await sns.subscribe(subscribeParam).promise();
    //   console.log("subscribe :: ", subRes);


  }

  // Get Event Ids
  async function getEventDetails(requestBody) {

    var eventParams = {
      TableName: process.env.ADMINCONFIGEVENT_TABLE,
      ProjectionExpression: "Id, EventDate,EventDurationInHr,EventDurationInMin,EventTimeAmPm,EventTimeInHr,EventTimeInMin",
    };
    console.log("Inside event  getEventDetails museum Offline");
    console.log("eventParams : ", eventParams)
    var data = await documentClient.scan(eventParams).promise();
    var events = data.Items;

    var eventIds = [];

    var offlineDateFrom = moment(requestBody.DateFrom.substring(0, 10) + " " + requestBody.TimeHrFrom.toString() + ":" + requestBody.TimeMinFrom.toString(), dateFormat, 'fr', true);
    console.log(requestBody.DateFrom + " " + requestBody.TimeHrFrom.toString() + ":" + requestBody.TimeMinFrom.toString())
    if (requestBody.TimeTypeFrom.toLowerCase() == 'pm') {
      //offlineDateTo.setHours(offlineDateTo.getHours+12)
      offlineDateFrom.add(12, 'hours');
    }
    offlineDateFrom = offlineDateFrom.toDate();
    var offlineDateTo = moment(requestBody.DateTo.substring(0, 10) + " " + requestBody.TimeHrTo.toString() + ":" + requestBody.TimeMinTo.toString(), dateFormat, 'fr', true);

    if (requestBody.TimeTypeTo.toLowerCase() == 'pm') {
      console.log("inside pm to")
      offlineDateTo.add(12, 'hours');
    }
    offlineDateTo = offlineDateTo.toDate();


    console.log("offlineDateFrom : ", offlineDateFrom)
    console.log("offlineDateTo : ", offlineDateTo)
    for (const event of events) {

      var startDate = moment(event.EventDate.substring(0, 10) + " " + event.EventTimeInHr + ":" + event.EventTimeInMin, dateFormat, 'fr', true);
      if (event.EventTimeAmPm.toLowerCase() == 'pm') {

        startDate.add(12, 'hours');
      }
      startDate = startDate.toDate();
      console.log("eventId being considered : ", event.Id);
      console.log("startDate before : ", startDate);
      if (startDate >= offlineDateFrom && startDate < offlineDateTo) {
        console.log("startDate after : ", startDate);
        console.log(event.EventDate + " " + event.EventTimeInHr + ":" + event.EventTimeInMin)
        eventIds.push(event.Id);
        //console.log(eventIds)
      } else {
        console.log("Coudln't find the eventids in the slot")
      }
      // var endDate = new Date(startDate);
      // endDate.setMinutes(startDate.getHours + EventDurationInHr);
      // endDate.setMinutes(startDate.getMinutes  +EventDurationInMin);     
    }
    console.log(eventIds)
    return eventIds;
  }

  async function getEventUser(requestBody) {
    console.log("params:: ", params)
    console.log("getEventUser : requestBody : ", requestBody);
    var eventId = requestBody.EventId;
    console.log("eventId :: ", eventId);
    var users = [];





    var emailIds = {
      TableName: process.env.BOOKINGINFO_TABLE,
      // ProjectionExpression: "VisitorEmail, BookingData",

    };

    // get users from the event
    var data = await documentClient.scan(emailIds).promise();
    //console.log("data :: ", data);
    for (const item of data.Items) {
      //console.log("value :: ",value);

      // for (const value of items) {
      //console.log("value :: ",value.EventId, value.EventId == eventId, value.EventId && value.EventId == eventId);
      if (item.EventId && item.EventId == eventId) {
        console.log("value.SimulatorRequired : ", item.SimulatorRequired)
        //console.log("value :: ",value);
        if (eventType == 'simulatorOffline' && item.SimulatorRequired) {
          console.log("Event id matched & simulator required: ", item.VisitorEmail)
          users = users.concat(item.VisitorEmail);
          console.log(users)
        } else if (eventType == 'museumOffline' || eventType == 'EventCancelled' || eventType == 'alertFromAdmin') {
          console.log("Event id matched : ", item.VisitorEmail)
          // console.log("value :: ",value)
          users = users.concat(item.VisitorEmail);
          console.log(users)


        }
      }
      //}
    }
    console.log("users :: ", users);
    var subRes = " var subRes";
    for (const user of users) {
      var tokens = {
        TableName: process.env.DEVICE_TOKENS,

        ProjectExpression: "Id,ARN",
        KeyConditionExpression: "#Id = :email",
        // Filterexpression : "Id IN ("+Object.keys(emailObject).toString()+")",
        ExpressionAttributeNames: {
          "#Id": "Id"
        },

        ExpressionAttributeValues: {
          ":email": user,
        }
      };


      console.log(tokens);
      var res = await documentClient.query(tokens).promise();
      console.log("res : ", res);

      subscribeParam.Endpoint = res.Items[0].ARN;


      console.log("subscribeParam 1:: ", subscribeParam);
      //subcribe to topic              
      subRes = await sns.subscribe(subscribeParam).promise();
      console.log("subscribe :: ", subRes)
    }
    if (users.length > 0) {
      console.log("params:: ", params)
      await publishNotification(params);
    }

  }

  async function getUsersDetails(requestBody) {

    console.log("Inside getUserDetails :::::::")



    var offlineDateFrom = moment(requestBody.DateFrom.substring(0, 10) + " " + requestBody.TimeHrFrom.toString() + ":" + requestBody.TimeMinFrom.toString(), dateFormat, 'fr', true);
    console.log("-------" + requestBody.DateFrom + " " + requestBody.TimeHrFrom.toString() + ":" + requestBody.TimeMinFrom.toString())
    if (requestBody.TimeTypeFrom.toLowerCase() == 'pm') {
      //offlineDateTo.setHours(offlineDateTo.getHours+12)
      offlineDateFrom.add(12, 'hours');
    }
    offlineDateFrom = offlineDateFrom.toDate();
    var offlineDateTo = moment(requestBody.DateTo.substring(0, 10) + " " + requestBody.TimeHrTo.toString() + ":" + requestBody.TimeMinTo.toString(), dateFormat, 'fr', true);
    if (requestBody.TimeTypeTo.toLowerCase() == 'pm') {
      console.log("inside pm to")
      offlineDateTo.add(12, 'hours');
    }
    offlineDateTo = offlineDateTo.toDate();
    debugger;

    // var param = {
    //   TableName: process.env.BOOKINGINFO_TABLE,
    //   ProjectionExpression: "VisitorEmail, BookingDate",

    // };

    // get users from the event
    // var data = await documentClient.scan(param).promise();
    //console.log("data :: ", data);

    let dateIdMap = new Map();
    for (var d = moment(offlineDateFrom); d.isBefore(offlineDateTo); d.add(1, 'days')) {
      var emailsIds = [];
      //console.log(d.format('YYYY-MM-DD'));
      var params = {
        TableName: process.env.BOOKINGINFO_TABLE,
        IndexName: "BookingDate-index",
        KeyConditionExpression: "BookingDate = :bdate",
        ExpressionAttributeValues: {
          ":bdate": d.format('YYYY-MM-DD')
        }
      };
      //console.log("params.. " + JSON.stringify(params));
      // pick the date and fetch booking
      var data = await documentClient.query(params).promise();

      // Set the slot time in the booking date 
      for (const item of data.Items) {
        //console.log("item : ",item)
        var bookingDate = item.BookingDate;
        var slot = item.Slot24Hr

        if (typeof slot != "undefined") {
          if (slot.indexOf(".") === 1) {
            slot = "0" + slot.replace(".", ":")
          } else {
            slot = slot.replace(".", ":")
          }
          bookingDate = moment(bookingDate + " " + slot, dateFormat, 'fr', true);
          // console.log("offlineDateFrom : ", offlineDateFrom, "item.BookingDate : ", bookingDate, "offlineDateTo ", offlineDateTo, "slot ", slot)
          //console.log(offlineDateFrom<=item.BookingDate,  item.BookingDate<offlineDateTo  )
          // compare if it falls between offline period
          if (bookingDate.isAfter(offlineDateFrom) && bookingDate.isBefore(offlineDateTo)) {
            console.log("Got in...");
            var phone = item.CountryCode + item.VisitorPhone;


            var countryCodeSign = phone.substring(0, 1);
            if (countryCodeSign === "+") {
              var modifiedPhone = phone.substring(1);
            } else {
              var modifiedPhone = phone;
            }


            var dataArray = {
              email: item.VisitorEmail,
              phoneNo: modifiedPhone
            }

            emailsIds.push(dataArray)

          }
        }
        //console.log(emailsIds);
      }
      if (emailsIds.length > 0) {

        dateIdMap.set(bookingDate, emailsIds)
      }

    }
    console.log(dateIdMap)


    return dateIdMap;

  }

  function response(statusCode, message) {
    return {
      statusCode: statusCode,
      'headers': {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer authToke',
        'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      'X-XSS-Protection' : '1; mode=block',
       'x-frame-options': 'SAMEORIGIN',
      'x-content-type': 'nosniff',
        'Access-Control-Allow-Credentials': true,

      },
      body: message
    };
  }

  // Publish Notifications
  async function publishNotification(params1) {


    console.log("params1::::::: ", params1)
    const res = await sns.publish(params).promise();
    // sns.publish(params1, function (err, res) {
    //   if (err) {
    //     throw err;
    //   }
    // })
    console.log('Publish successfull !! ', res);

    // var deleteParams = {
    //     TopicArn: params.TargetArn
    //   };
    // await  sns.deleteTopic(deleteParams).promise();
    // if(alluser){
    // var cretaeParams = {
    //     Name: AllUserTopic, /* required */

    //   };
    // }else if(eventUser){
    //   var cretaeParams = {
    //     Name: EventTopic, /* required */

    //   };
    // }
    //   await sns.createTopic(cretaeParams).promise();

  }


  // Fetching topics
  async function getTopics(requestBody) {
    console.log("alluser :: ", alluser)
    console.log("eventUser :: ", eventUser)
    var lstTopicParams = {
      NextToken: ''
    };
    var topicArn = "";
    const result = await sns.listTopics(lstTopicParams).promise();
    //console.log("topic::", result);           // successful response
    result.Topics.forEach(
      topic => {

        // if msg is for all user
        if (alluser && topic.TopicArn.includes(AllUserTopic)) {

          topicArn = topic.TopicArn;
          params.TargetArn = topic.TopicArn;

          //return topicArn;
        } else if (eventUser && topic.TopicArn.includes(EventTopic)) {
          console.log("alluser :: ", "inside event topic")

          topicArn = topic.TopicArn;
          subscribeParam.TopicArn = topic.TopicArn;
          params.TargetArn = topic.TopicArn;
          //subscribeParam.TopicArn = topic.TopicArn;
        }
      }
    )


    return topicArn;
  }
