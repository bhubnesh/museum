'use strict';


var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const moment = require('moment');

module.exports.eventCountMonthly = async (event) => {
    //  var requestBody = JSON.parse(event.body);
    console.log("event",event)
    console.log("event ", JSON.stringify(event.Records[0].dynamodb))
    var rec = event.Records[0]
    let records = [];
    //  records.push(requestBody)
    records.push(rec)
    //console.log("records",records)
    for (const record of records) {
        let BookingId = record.dynamodb.NewImage.BookingId.S
        console.log("BookingId", BookingId)
        var eventParams = {
            TableName: process.env.BOOKING_TABLE,
            Key: {
                "Id": BookingId
            }
        };
        // console.log("eventType",eventParams)
        var eventfind = await documentClient.get(eventParams).promise()
        var eventId = eventfind.Item.EventId
        // console.log("Event By Id",eventId)
        var eventParams = {
            TableName: process.env.ADMINCONFIGEVENT_TABLE,
            Key: {
                "Id": eventId
            }
        };
        //2a147d70-fe65-11eb-a099-b91a9792b9bd
        //    console.log("eventParams",eventParams)
        var eventData = await documentClient.get(eventParams).promise()
        //    console.log("EventType : Public or Private :: ",eventData)
        let EventType = eventData.Item.EventType
        // console.log("EventType",EventType)
        if (EventType == "Public" && EventType != undefined && EventType != null && EventType != "") {
            let Feedbacksubmit = record.dynamodb.NewImage.FeedbackAnswers.L;
            for (const data1 of Feedbacksubmit) {
                var arrayDatainfo = data1['M']
                //console.log("Hello dddd20", arrayDatainfo)
                if (arrayDatainfo.NoOfStars.S != undefined && arrayDatainfo.NoOfStars.S != null && arrayDatainfo.NoOfStars.S != '') {
                    // console.log("arrayDatainfo 22 ", arrayDatainfo.NoOfStars.S)
                    var noOfStars = arrayDatainfo['NoOfStars']['S']
                    // console.log("noofstars", noOfStars)
                    //var stars = noOfStars.split(' ').join('_');
                    var stars = noOfStars
                    var count = 0;
                    var date = new Date();
                    var todaysDate = moment(date).format('YYYY-MM')

                    var params = {
                        TableName: process.env.REPORTSCOUNT_TABLE,
                        Key: {
                            "CountName": "FeedbackEventMonthly",
                            "recordDate": todaysDate + "_" + stars,
                        }
                    }
                    // Check if record present
                    var data = await documentClient.get(params).promise()
                    var element = data.Item;
                    console.log("data ", data)
                    if (typeof element != "undefined" && element != "{}") {
                        console.log("inside update count")
                        var updateParams = {
                            TableName: process.env.REPORTSCOUNT_TABLE,
                            Key: {
                                "CountName": "FeedbackEventMonthly",
                                "recordDate": element.recordDate,
                            },
                            UpdateExpression: "set hitCount=hitCount+:val",
                            ExpressionAttributeValues: {
                                ":val": 1,
                            },
                        }
                        console.log(updateParams)
                        var res = await documentClient.update(updateParams).promise();
                        console.log(" IN UPDATING ", res)
                    } else {
                        // if no, insert
                        count = 1;
                        var updateParams = {
                            TableName: process.env.REPORTSCOUNT_TABLE,
                            Item: {
                                "CountName": "FeedbackEventMonthly",
                                "recordDate": todaysDate + "_" + stars,
                                "hitCount": count
                            }
                        }
                        var data = await documentClient.put(updateParams).promise()
                        console.log(" IN INSERTING ", data)
                    }
                }
            }
        }
    }
}