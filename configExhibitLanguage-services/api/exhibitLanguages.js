'use strict';

const { request } = require('http');
const auth = require('../auth/token');
var AWS = require('aws-sdk'),
  fs = require('fs'),

  uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient();
const s3 = new AWS.S3;
const moment = require('moment');
const buffer = require('buffer');
const fileType = require('file-type');
uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient();

var globalEvent = "";

function response(statusCode, message) {
    
  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if(origin == ""|| origin == undefined || !origin){
     origin = globalEvent.headers.Origin;
  }
  if(origin == ""|| origin == undefined || !origin){
    origin = globalEvent.headers.Host;
 }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
      originsAllowed = origin;
 }

  return {
      statusCode: statusCode,
      'headers': {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer authToke',
          'Access-Control-Allow-Credentials': true,
          'Access-Control-Allow-Origin' : originsAllowed,
          'X-XSS-Protection': '1; mode=block',
          'X-Frame-Options': 'SAMEORIGIN',
          'X-Content-Type-Options': 'nosniff',
          'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

      },
      body: message
  };
}

module.exports.addExhibitLanguage = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);
  //-- Validate JWT token 
  const validateMultipleLogin = async() =>{
    const validateTokenResp =  await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if  (validateTokenResp.statusCode != 200) {
            console.log("Response .............. "+ JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
}
validateMultipleLogin();

  if (!requestBody.Language) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var Id = uuid.v1();
  var params = {
    Item: {
      "Id": Id,
      "Language": requestBody.Language,
      "AddBy": requestBody.AddBy,
      "URL": requestBody.URL
    },
    TableName: process.env.EXHIBITLANGUAGE_TABLE
  };
  //-- Check whether Language already exists
  var params1 = {
    TableName: process.env.EXHIBITLANGUAGE_TABLE,
    Key: {
      "Id": Id
    }
  };
  console.log("params.. " + JSON.stringify(params1));
  documentClient.get(params1, function (err, data) {
    if (err) {
      return response(400, JSON.stringify(err));
    } else {
      if (data.Item) {
        var respObj = { "message": "Exhibit Language already exists" }
        return callback(err, response(400, JSON.stringify(respObj)));
      } else {
        console.log("Hii")
        var respObj = { "responseObj": data.Attributes, "message": "Exhibit Language Added successfully..!" }
        documentClient.put(params, function (err, data) {
          return callback(err, response(200, JSON.stringify(respObj)));

        });
      }
    }
  });
}
//-- Delete ExhibitLanguage code 

module.exports.deleteExhibitLanguage = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token 
  const validateMultipleLogin = async() =>{
    const validateTokenResp =  await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if  (validateTokenResp.statusCode != 200) {
            console.log("Response .............. "+ JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
}
validateMultipleLogin();

  const id = event.pathParameters.Id;
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params = {
    TableName: process.env.EXHIBITLANGUAGE_TABLE,
    Key: {
      "Id": id

    }

  };
  //console.log("params.. " + JSON.stringify(params));
  documentClient.delete(params, function (err, data) {
    if (err) {
      return callback(err, response(400, err));
    }
    var respObj = { "message": "Exhibit Language Deleted successfully..!" }
    return callback(err, response(200, JSON.stringify(respObj)));
  });
}
// update the Exhibit Language Details

module.exports.updateExhibitLanguage = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token 
  const validateMultipleLogin = async() =>{
    const validateTokenResp =  await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if  (validateTokenResp.statusCode != 200) {
            console.log("Response .............. "+ JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
}
validateMultipleLogin();

  const id = event.pathParameters.Id;
  const requestBody = JSON.parse(event.body);
  if (!id) {
    var respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params = {
    TableName: process.env.EXHIBITLANGUAGE_TABLE,
    Key: {
      "Id": id

    },

    UpdateExpression: 'set #AddBy = :D,#Language = :L',
    ExpressionAttributeValues: {
      ':D': requestBody.AddBy,
      ':L': requestBody.Language

    },
    ExpressionAttributeNames: {
      '#AddBy': 'AddBy',
      '#Language': 'Language'

    },
    ReturnValues: "UPDATED_NEW"
  };

  //console.log("params.. " + JSON.stringify(params));
  documentClient.update(params, function (err, data) {
    if (err) {
      return callback(err, response(400, err));
    }
    var respObj = { "responseObj": data.Attributes, "message": "Exhibit Language updated successfully..!" }
    return callback(err, response(200, JSON.stringify(respObj)));


  });
}

// get  the All Exhibit Language Details

module.exports.getAllExhibitLanguage = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token 
  const validateMultipleLogin = async() =>{
    const validateTokenResp =  await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if  (validateTokenResp.statusCode != 200) {
            console.log("Response .............. "+ JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
}
validateMultipleLogin();

  var params = {
    TableName: process.env.EXHIBITLANGUAGE_TABLE,
  }
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    //console.log(data);
    var respObj = { "responseObj": data, "message": "Exhibit Language data fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));
  })

}

// get particular Exhibit Language details 

module.exports.getExhibitLanguage = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token 
  const validateMultipleLogin = async() =>{
    const validateTokenResp =  await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if  (validateTokenResp.statusCode != 200) {
            console.log("Response .............. "+ JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
}
validateMultipleLogin();

  const id = event.pathParameters.Id;
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  var params = {
    TableName: process.env.EXHIBITLANGUAGE_TABLE,
    ExpressionAttributeValues: {
      ":language": id
    },
    KeyConditionExpression: 'Id = :language',
  };
  //console.log("params.. " + JSON.stringify(params));
  documentClient.query(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    } else {
      if (data.Count > 0) {
        var respObj = { "responseObj": data.Items[0], "message": "Exhibit Language data fetched successfully." }
        return callback(null, response(200, JSON.stringify(respObj)));
      } else {
        var respObj = { "message": "DeviceId with type- " + id + " not found." }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })

}

//Fileupload API 

module.exports.Fileupload = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token 
  const validateMultipleLogin = async() =>{
    const validateTokenResp =  await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if  (validateTokenResp.statusCode != 200) {
            console.log("Response .............. "+ JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
}
validateMultipleLogin();

  const requestBody = JSON.parse(event.body);

  let base64String = requestBody.base64String;
  //console.log("Input String .. "+ base64String);
  let buffer = new Buffer.alloc(base64String, 'base64');
  let fileMime = fileType(buffer);
  if (fileMime === null) {
    return context.fail('not a file type');
  }

  let file = getfile(fileMime, buffer);
  let params = file.params;

  s3.putObject(params, function (err, data) {
    if (err) {
      return console.log(err);
    }
    return console.log('file URL', file.full_path)
  });


  let getfile = function (fileMime, buffer) {
    let fileExt = fileMime.ext;
    let hash = sha1(new Buffer.alloc(new Date().toString()));
    let now = moment().format('YYYY-MM-DD HH:MM:SS');

    let filepath = hash + '/';
    let fileName = unixTime + '.' + fileExt;
    let fileFullName = filepath + fileName;
    let filefullPath = 'arn:aws:s3:::fileuploadmm' + fileFullName;

    let params = {
      Bucket: 'fileuploadmm',
      key: 'windows_Image',
      Body: buffer
    };
    let uploadFile = {
      size: buffer.toString('ascii').length,
      type: fileMime.mine,
      name: fileName,
      full_path: filefullPath,

    }
    return {
      'params': params,
      'uploadFile': uploadFile
    }
  }


}