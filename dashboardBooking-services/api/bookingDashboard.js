'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');


var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}

module.exports.getBookingSlotDataDashboard = function (event, context, callback) {
    debugger;
    globalEvent = event;
    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        // console.log("Response.... "+ JSON.stringify(validateTokenResp));
        if (validateTokenResp.statusCode != 200) {
          console.log("Response .in if............. " + JSON.stringify(validateTokenResp));
          return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        } else {
          //console.log("Response . in else............. " + JSON.stringify(validateTokenResp));
          bookingSlotDataDashboard(event, context, validateTokenResp, callback);
        }
      }
      validateMultipleLogin();
    }
    async function bookingSlotDataDashboard(event, context, validateTokenResp, callback){
    const date = event.pathParameters.Date;
    if (!date) {
        console.log("Mandatory inut params are missing.");
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }
    //-- Check whether slot data is present for given date
    var searchWithDate = {
        TableName: process.env.BOOKINGDATE_TABLE,
        ExpressionAttributeValues: {
            ":date": date
        },

        KeyConditionExpression: "BookingDate = :date"
    }
    console.log("Search Params ..." + JSON.stringify(searchWithDate));
    documentClient.query(searchWithDate, function (err, data) {
        if (err) {
            return response(400, JSON.stringify(err));
        } else {
            // console.log("GetItem succeeded:", JSON.stringify(data));
            //   console.log("count of records fro email ... " + data.Count)
            if (data.Count > 0) {

                var slotArray = [];
                slotArray = data.Items[0].SlotsDetails;
                var slotCount = slotArray.length;
                var resultArray = [];
                if (slotArray.length > 0) {
                    slotArray.forEach(slot => {
                        var slotObj = {
                            "MaxParticipantsAllowedPerSlot": slot.maxParticipantsAllowedPerSlot,
                            "BookingsDone": slot.bookingsDone,
                            "SlotTime": slot.slot,
                            "WCBooked": slot.WCBooked,
                            "WCAvailable": slot.WCAvailable,
                            "VIBooked": slot.VIBooked,
                            "VIAvailable": slot.VIAvailable,
                            "HIAvailable": slot.HIAvailable,
                            "HIBooked": slot.HIBooked,
                        }
                        resultArray.push(slotObj);
                        slotCount--;
                        if (slotCount == 0) {
                            var respObj = { "message": "Slots data for given date fetched successfully.", "responseObj": resultArray };
                            return callback(null, response(200, JSON.stringify(respObj)));
                        }
                    })
                } else {
                    console.log("Slot details are not available for given date.");
                    var respObj = { "message": "Slot details are not available for given date." };
                    return callback(null, response(404, JSON.stringify(respObj)));
                }
            } else {
                console.log("Booking details not available for the given date.")
                var respObj = { "message": "Booking details not available for the given date." }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })



}