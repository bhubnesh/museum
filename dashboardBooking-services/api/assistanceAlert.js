'use strict';

var AWS = require('aws-sdk'),

    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
const moment = require('moment');
var _ = require('underscore');

var globalEvent = "";

function response(statusCode, message) {

    var originsAllowed = "";
    const origin = globalEvent.headers.origin;
    console.log("Host Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
    }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'x-frame-options': 'SAMEORIGIN',
            'x-content-type': 'nosniff'

        },
        body: message
    };
}



module.exports.getFutureAssistanceAlert = function (event, context, callback) {
    globalEvent = event;
    const validateMultipleLogin = async () => {
        const validateTokenResp = await auth.validateToken(event, context, callback);
        // console.log("Response.... "+ JSON.stringify(validateTokenResp));
        if (validateTokenResp.statusCode != 200) {
          console.log("Response .in if............. " + JSON.stringify(validateTokenResp));
          return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        } else {
          //console.log("Response . in else............. " + JSON.stringify(validateTokenResp));
          futureAssistanceAlert(event, context, validateTokenResp, callback);
        }
      }
      validateMultipleLogin();
    }
    async function futureAssistanceAlert(event, context, validateTokenResp, callback){
    var bookingDate = new Date();
    //-- Get data for current date + 2 days
    bookingDate.setDate(bookingDate.getDate() + 2);
    const requiredBookingDate = moment(bookingDate).format('YYYY-MM-DD');
    console.log("To Date ... " + requiredBookingDate);

    var indexParams =
    {
        TableName: process.env.BOOKING_TABLE,
        IndexName: "BookingDate-index",
        KeyConditionExpression: "BookingDate = :bdate",
        ExpressionAttributeValues: {
            ":bdate": requiredBookingDate
        },
    }

    console.log("Search Params ..." + JSON.stringify(indexParams));

    documentClient.query(indexParams, function (err, data) {

        if (err) {
            return callback(err, response(400, JSON.stringify(err)));
        } else {
            if (data.Count > 0) {
                var bookingData = data.Items;
                var count = bookingData.length;
                var assistanceArray = [];
                if (bookingData.length > 0) {
                    bookingData.forEach(item => {
                        if (item.AssistanceRequired == true) {
                            if (item.WheelChairAssistanceRequired == true || item.VIAssistanceRequired == true) {
                                assistanceArray.push(item);
                                count--;
                            } else {
                                count--;
                            }
                        } else {
                            count--;
                        }
                        if (count == 0) {
                            var wcAlerts = [];
                            var viAlerts = [];
                            var finalResultArray = [];
                            //-- Filter array of booking based on slots
                            const sortedBookings = assistanceArray.sort((a, b) => new Date(a.Slot24Hr) - new Date(b.Slot24Hr));
                            var bookingCount = sortedBookings.length;
                            if (sortedBookings.length > 0) {
                                sortedBookings.forEach(booking => {
                                    if (booking.WheelChairAssistanceRequired == true && booking.VIAssistanceRequired == true) {
                                        var bookingWCObj = {
                                            "BookingDate": booking.BookingDate,
                                            "SlotTime": booking.SlotTime,
                                            "Slot24Hr": booking.Slot24Hr,
                                            "WCRequiredCount": booking.WCRequiredCount
                                        }
                                        wcAlerts.push(bookingWCObj);

                                        var bookingVIObj = {
                                            "BookingDate": booking.BookingDate,
                                            "SlotTime": booking.SlotTime,
                                            "Slot24Hr": booking.Slot24Hr,
                                            "VIRequiredCount": booking.VIRequiredCount
                                        }
                                        viAlerts.push(bookingVIObj);
                                        bookingCount--;
                                    } else if (booking.WheelChairAssistanceRequired == true && booking.VIAssistanceRequired == false) {
                                        var bookingWCObj = {
                                            "BookingDate": booking.BookingDate,
                                            "SlotTime": booking.SlotTime,
                                            "Slot24Hr": booking.Slot24Hr,
                                            "WCRequiredCount": booking.WCRequiredCount
                                        }
                                        wcAlerts.push(bookingWCObj);
                                        bookingCount--;

                                    }
                                    else if (booking.WheelChairAssistanceRequired == false && booking.VIAssistanceRequired == true) {
                                        var bookingVIObj = {
                                            "BookingDate": booking.BookingDate,
                                            "SlotTime": booking.SlotTime,
                                            "Slot24Hr": booking.Slot24Hr,
                                            "VIRequiredCount": booking.VIRequiredCount
                                        }
                                        viAlerts.push(bookingVIObj);
                                        bookingCount--;
                                    }
                                    if (bookingCount == 0) {
                                        finalResultArray.push({ "WCAlerts": wcAlerts, "VIAlerts": viAlerts });
                                        return callback(null, response(200, JSON.stringify(finalResultArray)));
                                    }
                                })
                            } else {
                                console.log("No data found");
                                var respObj = { "message": "No data found for given date." }
                                return callback(null, response(404, JSON.stringify(respObj)));
                            }

                        }
                    })
                } else {
                    console.log("Booking details for future date not found")
                    var respObj = { "message": "Booking details of given email not found " }
                    return callback(null, response(404, JSON.stringify(respObj)));
                }

            } else {
                console.log("Booking details for future date not found.")
                var respObj = { "message": "Booking details for future date not found" }
                return callback(null, response(404, JSON.stringify(respObj)));
            }
        }
    })
}

