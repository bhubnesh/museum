'use strict';

var AWS = require('aws-sdk'),

  documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');

var globalEvent = "";

function response(statusCode, message) {
    
  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if(origin == ""|| origin == undefined || !origin){
     origin = globalEvent.headers.Origin;
  }
  if(origin == ""|| origin == undefined || !origin){
    origin = globalEvent.headers.Host;
 }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
      originsAllowed = origin;
 }

  return {
      statusCode: statusCode,
      'headers': {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer authToke',
          'Access-Control-Allow-Credentials': true,
          'Access-Control-Allow-Origin' : originsAllowed,
          'X-XSS-Protection': '1; mode=block',
          'X-Frame-Options': 'SAMEORIGIN',
          'X-Content-Type-Options': 'nosniff',
          'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

      },
      body: message
  };
}

module.exports.getFeedbackCount = async function (event, context, callback) {
  globalEvent = event;
  const validateMultipleLogin = async() =>{
    const validateTokenResp =  await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if  (validateTokenResp.statusCode != 200) {
            console.log("Response .............. "+ JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
}
validateMultipleLogin();


  const todaysDate = event.pathParameters.date;
  //var date = new Date();
  var finalResponseArray = []
  // var todaysDate = moment(date).format('YYYY-MM-DD')
  for (var i = 1; i < 6; i++) {
    var params = {
      TableName: process.env.REPORTSCOUNT_TABLE,
      Key: {
        "CountName": "FeedbackBookingDaily",
        "recordDate": todaysDate + "_" + i + " Star"
      }
    }
    console.log("params", params)
    var data = await documentClient.get(params).promise()
    console.log("data",data)
    var finalResponse = data.Item;
    finalResponseArray.push(finalResponse)
  }
  //console.log("finalResponseArray",finalResponseArray)
  var responseArray = []
  let value = 0;
  let name = "";
  var totalFeedBackCount = 0;
  for (const element of finalResponseArray) {
    //console.log("element",element)
    if (typeof element != undefined && element != null && element != "") {
      name = element.recordDate.substring(17, 11)
      // console.log("name", name)
      value = element.hitCount
      var resp = {
        "name": name,
        "value": value
      }
      totalFeedBackCount = totalFeedBackCount + value;
      responseArray.push(resp)
    }
  }
  var responseObj = {
    responseArray,
    "totalFeedBackCount": totalFeedBackCount
  }
   console.log("arrayObj",responseObj)
  var respObj = { "responseObj": responseObj, "message": "FeedbackReport daily data fetched successfully..!" }
  return response(200, JSON.stringify(respObj));
}