'use strict';

var AWS = require('aws-sdk'),

  documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');



var globalEvent = "";

function response(statusCode, message) {
    
  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if(origin == ""|| origin == undefined || !origin){
     origin = globalEvent.headers.Origin;
  }
  if(origin == ""|| origin == undefined || !origin){
    origin = globalEvent.headers.Host;
 }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
      originsAllowed = origin;
 }

  return {
      statusCode: statusCode,
      'headers': {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer authToke',
          'Access-Control-Allow-Credentials': true,
          'Access-Control-Allow-Origin' : originsAllowed,
          'X-XSS-Protection': '1; mode=block',
          'X-Frame-Options': 'SAMEORIGIN',
          'X-Content-Type-Options': 'nosniff',
          'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

      },
      body: message
  };
}


module.exports.getFeedbackCountMonthly = async function (event, context, callback) {
  globalEvent = event;
  const validateMultipleLogin = async() =>{
    const validateTokenResp =  await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

        if  (validateTokenResp.statusCode != 200) {
            console.log("Response .............. "+ JSON.stringify(validateTokenResp));
            return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
        }
}

validateMultipleLogin();

  const requestParams = (event.queryStringParameters);
  if (!requestParams.FromMonth || !requestParams.ToMonth) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return response(400, JSON.stringify(respObj));
  }
  var params = {
    TableName: process.env.REPORTSCOUNT_TABLE,
    ExpressionAttributeValues: {
      ":FromMonth": requestParams.FromMonth,
      ":ToMonth": requestParams.ToMonth,
      ":countName": "FeedbackBookingMonthly"
    },
    KeyConditionExpression: "CountName = :countName AND recordDate BETWEEN :FromMonth and :ToMonth"
  };
  // console.log("params", params)
  var data = await documentClient.query(params).promise()
  console.log("data",data)
  var finalResponse = data.Items;
  var responseArray = []
  var value = 0;
  let name = "";
  var totalFeedBackCount = 0;
  var element = data.Items;
  console.log("data :: ", element)
  var star_1 = 0;

  var star_2 = 0;

  var star_3 = 0;

  var star_4 = 0;

  var star_5 = 0;
  let ratings = new Map();
  for (const record of element) {

    var starValue = record.recordDate;
    var hitCount = record.hitCount;
    starValue = starValue.charAt(8);

    // console.log("hitCount ", hitCount)
    switch (starValue) {
      case "1":
        console.log("starValue ", starValue)
        star_1 = star_1 + hitCount;
        ratings.set("star_1", star_1)
        totalFeedBackCount = totalFeedBackCount + parseInt(record.hitCount);
        break;
      case "2":
        // console.log("starValue ", starValue)
        star_2 = star_2 + hitCount;
        ratings.set("star_2", star_2)
        totalFeedBackCount = totalFeedBackCount + parseInt(record.hitCount);
        break;
      case "3":
        // console.log("starValue ", starValue)
        star_3 = star_3 + hitCount;
        ratings.set("star_3", star_3)
        totalFeedBackCount = totalFeedBackCount + parseInt(record.hitCount);
        break;
      case "4":
        // console.log("starValue ", starValue)
        star_4 = star_4 + hitCount;
        ratings.set("star_4", star_4)
        totalFeedBackCount = totalFeedBackCount + parseInt(record.hitCount);
        break;
      case "5":
        // console.log("starValue ", starValue)
        star_5 = star_5 + hitCount;
        ratings.set("star_5", star_5)
        totalFeedBackCount = totalFeedBackCount + parseInt(record.hitCount);
    }

  }
  ratings.forEach((rating, value) => {
    console.log("checking ", `${rating}: ${value}`);
    console.log("checking test", rating, value);
    var obj = {
      name: `${value}`,
      value: parseInt(rating)
    };
    responseArray.push(obj);
    console.log("checking test :::", obj);
  }
  );
  var responseObj = {
    responseArray,
    "totalFeedBackCount": totalFeedBackCount
  }
//console.log("respon ::",responseObj)
  var respObj = { "responseObj": responseObj, "message": "FeedbackReport Monthly data fetched successfully..!" }
  return response(200, JSON.stringify(respObj));
}