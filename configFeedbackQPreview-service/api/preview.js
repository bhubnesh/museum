'use strict';

var AWS = require('aws-sdk'),
    documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
const axios = require('axios');

var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}



module.exports.updatePrviewData = function (event, context, callback) {
    globalEvent = event;
    //-- Validate JWT token
    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();

    const requestBody = JSON.parse(event.body);
    const loggedInUser = event.loggedInUser;

    if (!requestBody.FeedbackQuestionsPreviewData) {
        var respObj = { "message": "Manadatory input parameters are missing." }
        return callback(
            null,
            response(400, JSON.stringify(respObj))
        );
    }

    var feedbackQuestionsPreviewData = requestBody.FeedbackQuestionsPreviewData;
    console.log("size of List of feedback Questions .. " + feedbackQuestionsPreviewData.length);
    var count = feedbackQuestionsPreviewData.length;
    if (feedbackQuestionsPreviewData.length > 0) {
        feedbackQuestionsPreviewData.forEach(questions => {
            //-- check whether record with given id exists or not 
            var params1 = {
                TableName: process.env.FEEDBACKQUESTIONS_TABLE,
                Key: {
                    "Id": questions.Id
                },
            };
            console.log("search params.. " + JSON.stringify(params1));
            documentClient.get(params1, function (err, data) {
                if (err) {
                    console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                    return response(400, JSON.stringify(err));
                } else {
                    console.log("GetItem succeeded:", JSON.stringify(data));
                    if (data.Item) {
                        console.log("Data with given id exists.");

                        var updateParams = {
                            TableName: process.env.FEEDBACKQUESTIONS_TABLE,
                            Key: {
                                "Id": questions.Id

                            },
                            UpdateExpression: 'set #Question = :Q, #Type = :A,#NumberOfoptions = :N,#Option1 = :P,#Option2 = :T,#Option3 = :I,#Option4 = :O,#OrderNum = :ON,#UpdatedBy = :U,#UpdatedAt = :UA',
                            ExpressionAttributeValues: {
                                ':Q': questions.Question,
                                ':A': questions.Type,
                                ':N': questions.NumberOfoptions,
                                ':P': questions.Option1,
                                ':T': questions.Option2,
                                ':I': questions.Option3,
                                ':O': questions.Option4,
                                ':ON': questions.OrderNum,
                                ':U': loggedInUser,
                                ':UA': new Date().toISOString()
                            },
                            ExpressionAttributeNames: {
                                '#Question': 'Question',
                                '#Type': 'Type',
                                '#NumberOfoptions': 'NumberOfoptions',
                                '#Option1': 'Option1',
                                '#Option2': 'Option2',
                                '#Option3': 'Option3',
                                '#Option4': 'Option4',
                                '#OrderNum': 'OrderNum',
                                '#UpdatedBy': 'UpdatedBy',
                                "#UpdatedAt": "UpdatedAt"

                            },
                            ReturnValues: "UPDATED_NEW"
                        };

                        console.log("Update params.. " + JSON.stringify(updateParams));
                        documentClient.update(updateParams, function (err, data) {
                            if (err) {
                                return callback(err, response(400, err));
                            }

                            console.log("updated .. " + JSON.stringify(data));

                            var respObj = { "message": "Feedback Questions updated successfully..!" }
                            console.log("Count ... " + count);
                            if (count > 0) {
                                count--;
                            }
                            if (count == 0) {
                                var respObj = { "message": "Feedback Questions with given ids updated successfully.. !" }
                                //-- Call API to update Preview table
                                var updatePreviewAPI = "hhttps://pxg59ooick.execute-api.ap-south-1.amazonaws.com/prod/feedbackQuestionsPreview"
                                var reqBody = JSON.stringify(event);
                                axios.post(updatePreviewAPI, (reqBody), {
                                    headers: {
                                        'Authorization': event.headers.Authorization
                                    }
                                }).then(resp => {
                                    //console.log("=============in put data=============")
                                    console.log(console.log(resp));
                                    return callback(err, response(200, JSON.stringify(respObj)));
                                }).catch((error) => {
                                    console.error(error)
                                })
                                //return callback(err, response(200, JSON.stringify(respObj)));
                            }

                        });

                    } else {
                        console.log("Logged in user details not found");
                        var respObj = { "message": "Logged in user details not found" }
                        return callback(null, response(404, JSON.stringify(respObj)));
                    }
                }
            })

        });

    } else {
        var respObj = { "message": "list of feed back questions to be updated not found." }
        return callback(err, response(404, JSON.stringify(respObj)));
    }




}
module.exports.addPrviewData = function (event, context, callback) {
    globalEvent = event;
    //-- Validate JWT token
    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();

    const loggedInUser = event.loggedInUser;
    //-- Get all feedbackQuestions from the table
    var params = {
        TableName: process.env.FEEDBACKQUESTIONS_TABLE,
    }
    documentClient.scan(params, function (err, data) {
        if (err) {
            return callback(err, response(400, JSON.stringify(err)));
        }
        var previewData = data.Items;
        console.log("Preview data .. " + JSON.stringify(previewData));
        if (previewData.length > 0) {
            //---Add/Update preview table with this data

            var params = {
                TableName: process.env.FEEDBACKPREVIEW_TABLE,
                ExpressionAttributeValues: {
                    ":id": 1
                },
                KeyConditionExpression: '#id = :id',
                ExpressionAttributeNames: {
                    "#id": "Id"
                }

            };
            console.log("params.. " + JSON.stringify(params));
            documentClient.query(params, function (err, data) {
                if (err) {
                    console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                    return callback(err, response(400, JSON.stringify(err)));
                } else {


                    //               console.log("GetItem succeeded:", JSON.stringify(data));
                    console.log("count of records ... " + data.Count)
                    var previewData1 = {};
                    //previewData1.FeedbackQuestionsPreviewData = previewData;
                    if (data.Count > 0) {
                        //-- Update the data

                        var updateParams = {
                            TableName: process.env.FEEDBACKPREVIEW_TABLE,
                            Key: {
                                "Id": 1

                            },
                            UpdateExpression: 'set #FeedbackQ = :Q, #UpdatedBy = :U,#UpdatedAt = :UA',
                            ExpressionAttributeValues: {
                                ':Q': previewData,
                                ':U': loggedInUser,
                                ':UA': new Date().toISOString()
                            },
                            ExpressionAttributeNames: {
                                '#FeedbackQ': 'FeedbackQuestionsPreviewData',
                                '#UpdatedBy': 'UpdatedBy',
                                "#UpdatedAt": "UpdatedAt"

                            },
                            ReturnValues: "UPDATED_NEW"
                        };

                        console.log("Update Params ... " + JSON.stringify(updateParams));
                        documentClient.update(updateParams, function (err, data) {
                            if (err) {
                                return callback(err, response(400, err));
                            }

                            console.log("update params .. " + JSON.stringify(updateParams));

                            var respObj = { "message": "Feedback Questions updated successfully..!" }
                            return callback(null, response(200, JSON.stringify(respObj)));
                        })
                    } else {
                        //-- Add the data
                        var params = {
                            Item: {
                                "Id": 1,
                                "FeedbackQuestionsPreviewData": previewData,
                                "CraetedBy": loggedInUser,
                                "UpdatedBy": loggedInUser,
                                "CreatedAt": new Date().toISOString(),
                                "UpdatedAt": new Date().toISOString()
                            },
                            TableName: process.env.FEEDBACKPREVIEW_TABLE
                        }
                        console.log("Data to be added .. " + JSON.stringify(params));
                        var respObj = { "message": "Preview details created successfully..!" }
                        documentClient.put(params, function (err, data) {
                            return callback(err, response(200, JSON.stringify(respObj)));
                        });
                    }
                }
            })
        } else {
            console.log("No data found");
            var respObj = { "message": "No data found" }
            return callback(err, response(404, respObj));
        }
    })
}
