'use strict';

var AWS = require('aws-sdk'),

  uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');

var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}



module.exports.addFAQ = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      addFeedbackQ(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}

function addFeedbackQ(event, context, validateTokenResp, callback) {

  const loggedInUser = validateTokenResp.event.loggedInUser;

  const requestBody = JSON.parse(event.body);

  if (!requestBody.Question) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  //-- Get user details from email Id 
  var searchWithEmail = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": loggedInUser
    },
    KeyConditionExpression: "Email = :email"
  }

  console.log("Search Params ..." + JSON.stringify(searchWithEmail));


  documentClient.query(searchWithEmail, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return ({ "statusCode": 400, "message": JSON.stringify(err) });
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      console.log("count of records fro email ... " + data.Count)
      if (data.Count > 0) {
        console.log("Full User Object ... " + JSON.stringify(data.Items[0]));
        var Id = uuid.v1();
        var userDetails = data.Items[0];
        var params = {
          Item: {
            "Id": Id,
            "Question": requestBody.Question,
            "Answer": requestBody.Answer,
            "CreatedBy": userDetails.Name,
            "UpdatedBy": userDetails.Name,
            "CreatedAt": new Date().toISOString(),
            "UpdatedAt": new Date().toISOString()

          },
          TableName: process.env.MANAGECONTENTFAQ_TABLE
        };
        console.log(params);
        var respObj = { "responseObj": data.Attributes, "message": "FAQ created successfully..!" }
        documentClient.put(params, function (err, data) {
          return callback(err, response(200, JSON.stringify(respObj)));
        });
      } else {
        return ({ "statusCode": 404, "userData": {}, "message": "User details not found." })
      }
    }
  })

}
//-- Delete FAQ code 

module.exports.deleteFAQ = function (event, context, callback) {
  globalEvent = event;

  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      deleteFeedbackQ(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function deleteFeedbackQ(event, context, validateTokenResp, callback) {
  const id = event.pathParameters.Id;
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var params = {
    TableName: process.env.MANAGECONTENTFAQ_TABLE,
    Key: {
      "Id": id
    }
  };
  console.log("params.. " + JSON.stringify(params));
  documentClient.delete(params, function (err, data) {
    if (err) {
      return callback(err, response(400, err));
    }
    console.log(data);
    var respObj = { "message": "FAQ Deleted successfully..!" }
    return callback(err, response(200, JSON.stringify(respObj)));
  });
}
// update the FAQ Language Details

module.exports.updateFAQ = function (event, context, callback) {
  globalEvent = event;

  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      updateFeedbackQ(event, context, validateTokenResp, callback)
    }
  }
  validateMultipleLogin();
}

function updateFeedbackQ(event, context, validateTokenResp, callback) {
  const id = event.pathParameters.Id;
  const requestBody = JSON.parse(event.body);

  if (!id) {
    var respObj = { "message": "Please provide Id" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  //-- check whether record with given id exists or not 

  var params1 = {
    TableName: process.env.MANAGECONTENTFAQ_TABLE,
    Key: {
      "Id": id
    },
  };
  console.log("search params.. " + JSON.stringify(params1));
  documentClient.get(params1, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return response(400, JSON.stringify(err));
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      if (data.Item) {
        console.log("Data with given id exists.");
        const faqData = data.Item;
        //-- Get currently logged in user info
        var loggedInUser = validateTokenResp.event.loggedInUser;
        //-- Get user details from email Id 

        var searchWithEmail = {
          TableName: process.env.VISITOR_TABLE,
          ExpressionAttributeValues: {
            ":email": loggedInUser
          },
          KeyConditionExpression: "Email = :email"
        }
        console.log("Search Params ..." + JSON.stringify(searchWithEmail));

        documentClient.query(searchWithEmail, function (err, data) {
          if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return ({ "statusCode": 400, "message": JSON.stringify(err) });
          } else {
            console.log("GetItem succeeded:", JSON.stringify(data));
            console.log("count of records fro email ... " + data.Count)
            if (data.Count > 0) {
              console.log("Full User Object ... " + JSON.stringify(data.Items[0]));
              const userDetails = data.Items[0];
              var params = {
                TableName: process.env.MANAGECONTENTFAQ_TABLE,
                Key: {
                  "Id": id
                },
                UpdateExpression: 'set #Question = :Q, #Answer = :A,#UpdatedBy = :U,#UpdatedAt = :UA',
                ExpressionAttributeValues: {
                  ':T': requestBody.Title,
                  ':B': requestBody.Body,
                  ':Q': requestBody.Question,
                  ':A': requestBody.Answer,
                  ':U': userDetails.Name,
                  ':UA': new Date().toISOString()
                },
                ExpressionAttributeNames: {
                  '#Question': 'Question',
                  '#Answer': 'Answer',
                  '#UpdatedBy': 'UpdatedBy',
                  "#UpdatedAt": "UpdatedAt"
                },
                ReturnValues: "UPDATED_NEW"
              };

              console.log("params.. " + JSON.stringify(params));
              documentClient.update(params, function (err, data) {
                if (err) {
                  return callback(err, response(400, err));
                }
                console.log(data);
                const responseObj = {
                  "Id": faqData.Id,
                  "Answer": requestBody.Answer,
                  "Question": requestBody.Question,
                  "CreatedBy": faqData.CreatedBy,
                  "UpdatedBy": userDetails.Name,
                  "CreatedAt": faqData.CreatedAt,
                  "UpdatedAt": new Date().toISOString(),
                }
                var respObj = { "responseObj": responseObj, "message": "FAQ updated successfully..!" }
                return callback(err, response(200, JSON.stringify(respObj)));

              });
            } else {
              console.log("Logged in user details not found");
              var respObj = { "message": "Logged in user details not found" }
              return callback(null, response(404, JSON.stringify(respObj)));
            }
          }
        })
      } else {
        console.log("FAQ data with given id not found");
        var respObj = { "message": "FAQ data with given id not found" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
  ///////////////////


}

// get  the All FAQ Language Details

module.exports.getAllFAQ = function (event, context, callback) {
  globalEvent = event;
  // //-- Validate JWT token
  // var validateToken = auth.validateToken(event, context, callback);
  // console.log("token inside ... " + validateToken);
  // if (validateToken.statusCode != 200) {
  //   return callback(null, response(validateToken.statusCode, JSON.stringify(validateToken)));
  // }

  var params = {
    TableName: process.env.MANAGECONTENTFAQ_TABLE,
  }
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }
    var faqMuseumData = data.Items;
    //-- Sort data according to created date time
    const sortedData = faqMuseumData.sort((a, b) => new Date(b.CreatedAt) - new Date(a.CreatedAt));
    console.log("sorted... " + JSON.stringify(sortedData))
    var finalResponse = data;
    finalResponse.Items = sortedData;

    var respObj = { "responseObj": finalResponse, "message": "FAQ data fetched successfully..!" }
    return callback(null, response(200, JSON.stringify(respObj)));
  })

}

// get particular FAQ Language details 

module.exports.getFAQ = function (event, context, callback) {
  globalEvent = event;
  const id = event.pathParameters.Id;
  // //-- Validate JWT token
  // var validateToken = auth.validateToken(event, context, callback);
  // console.log("token inside ... " + validateToken);
  // if (validateToken.statusCode != 200) {
  //   return callback(null, response(validateToken.statusCode, JSON.stringify(validateToken)));
  // }

  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  var params = {
    TableName: process.env.MANAGECONTENTFAQ_TABLE,
    ExpressionAttributeValues: {
      ":faq": id
    },
    KeyConditionExpression: 'Id = :faq ',
  };
  console.log("params.. " + JSON.stringify(params));
  documentClient.query(params, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return callback(err, response(400, JSON.stringify(err)));
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      console.log("count of records ... " + data.Count)
      if (data.Count > 0) {
        var respObj = { "responseObj": data.Items[0], "message": "FAQ  data fetched successfully." }
        return callback(null, response(200, JSON.stringify(respObj)));
      } else {
        var respObj = { "message": "FAQ with type- " + id + " not found." }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })

}

//-- Delete multiple FAQs 

module.exports.deleteMultipleFAQ = function (event, context, callback) {
  globalEvent = event;

  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      deleteMultipleQ(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function deleteMultipleQ(event, context, validateTokenResp, callback) {

  const requestBody = JSON.parse(event.body);
  console.log("Req body .. " + JSON.stringify(requestBody))
  if (!requestBody.listOfFAQ) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var listOfFAQ = requestBody.listOfFAQ;
  console.log("size of List of FAQ .. " + listOfFAQ.length);
  var count = listOfFAQ.length;
  if (listOfFAQ.length > 0) {

    listOfFAQ.forEach(faq => {
      var id = faq.id;
      var params = {
        TableName: process.env.MANAGECONTENTFAQ_TABLE,
        Key: {
          "Id": id

        }

      };
      console.log("params.. " + JSON.stringify(params));
      documentClient.delete(params, function (err, data) {
        if (err) {
          return callback(err, response(400, err));
        }
        console.log(data);
        var message = "FAQ of Id - " + id + " deleted successfully..!"
        console.log(message + count);
        count--;
        if (count == 0) {
          var respObj = { "message": "FAQ with given ids deleted successfully.. !" }
          return callback(err, response(200, JSON.stringify(respObj)));
        }
      });
    });


  } else {
    var respObj = { "message": "list of FAQ ids to be deleted not found." }
    return callback(err, response(404, JSON.stringify(respObj)));
  }
}
