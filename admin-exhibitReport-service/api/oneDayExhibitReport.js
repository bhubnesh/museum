'use strict';

var AWS = require('aws-sdk'),

  documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
const moment = require('moment');

var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}



module.exports.oneDayExhibitReport = async function (event, context, callback) {
  debugger;
  globalEvent = event;
  const date = event.pathParameters.Date;
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();

  if (!date) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  var exhibitResponse = [];
  var todaysDate = moment(date).format('YYYY-MM-DD')

  var params = {
    TableName: process.env.EXHIBITHITCOUNT_TABLE,
    KeyConditionExpression: "#date = :date",
    ExpressionAttributeNames: {
      "#date": "recordDate"
    },
    ExpressionAttributeValues: {
      ":date": date
    }

  }

  console.log("params", params)
  // Check if record present
  var data = await documentClient.query(params).promise();
  console.log("data ", data)
  var elements = data.Items;
  var exhibitCountMap = new Map();
  for (const element of elements) {
    exhibitCountMap.set()
  }
  var exhibitParams = {
    TableName: process.env.EXHIBIT_TABLE,

  }
  console.log("exhibitParams", exhibitParams)
  var exhibits = await documentClient.scan(exhibitParams).promise();
  console.log("data exhibitParams ", exhibits.Count)

  for (const Exhibit of exhibits.Items) {
    var exhibitName = Exhibit.ExhibitName;
    var responseObj = {
      "ExhibitName": exhibitName,
      "NoOfTimesPlayed": 0
    }
    exhibitResponse.push(responseObj)
    console.log(exhibitResponse)
    for (const element of elements) {



      // var exhibitParams = {
      //   TableName: process.env.EXHIBIT_TABLE,
      //   Key: {
      //     "Id": element.ExhibitId
      //   }
      // }
      // console.log("exhibitParams", exhibitParams)
      // var Exhibits = await documentClient.get(exhibitParams).promise();

      console.log((element.ExhibitId === Exhibit.Id))
      if (element.ExhibitId === Exhibit.Id) {


        var count = element.hitCount
        var exhibitName = Exhibit.ExhibitName;
        responseObj = {
          "ExhibitName": exhibitName,
          "NoOfTimesPlayed": count
        }
        exhibitResponse.push(responseObj)
        break;

      }



    }
    console.log(responseObj)
    if (typeof responseObj != 'undefined') {
      exhibitResponse.push(responseObj)
    }

  }
  console.log(exhibitResponse.length)
  console.log(exhibitResponse)


  // console.log(moment())
  // // Fetch Daily record from date
  // // Iterate
  // // get the exhibit name
  // // Fetching Exhibits

  // console.log("Scan ",moment())
  // //console.log("response ", data)
  // var exhibits = data.Items;

  // for (const exhibit of exhibits) {

  //   var params = {
  //     TableName: process.env.EXHIBITHITCOUNT_TABLE,
  //     Key: {
  //       "ExhibitId": exhibit.Id,
  //       "recordDate": todaysDate
  //     }

  //   }

  //   // console.log("params",params)
  //   // Check if record present
  //   var data = await documentClient.get(params).promise();
  //   //console.log("data ", data)
  //   var element = data.Item;
  //   if (typeof element != "undefined") {
  //     var count = element.hitCount
  //   } else {
  //     var count = 0;
  //   }
  //   var responseObj = {
  //     "ExhibitName": exhibit.ExhibitName,
  //     "NoOfTimesPlayed": count
  //   }
  //   exhibitResponse.concat(responseObj)

  // }
  // console.log("Scan ",moment())
  // console.log("exhibitResponse ", exhibitResponse)






  // var exhibitResponse = [{
  //     "ExhibitName": "test1",
  //     "CheckedInParticipants": 20,
  //     "NoOfTimesPlayed": 10
  // }, {
  //     "ExhibitName": "test2",
  //     "CheckedInParticipants": 20,
  //     "NoOfTimesPlayed": 10
  // },
  // {
  //     "ExhibitName": "test3",
  //     "CheckedInParticipants": 20,
  //     "NoOfTimesPlayed": 10
  // },
  // {
  //     "ExhibitName": "test4",
  //     "CheckedInParticipants": 20,
  //     "NoOfTimesPlayed": 10
  // },
  // {
  //     "ExhibitName": "ABC5",
  //     "CheckedInParticipants": 20,
  //     "NoOfTimesPlayed": 10
  // }
  // ]

  var respObj = { "messgae": "Exhibit data fetched successfully.", "responseObj": exhibitResponse };
  return callback(null, response(200, JSON.stringify(respObj)));

}