'use strict';

var AWS = require('aws-sdk'),

  documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
const moment = require('moment');


var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}



module.exports.getMonthlyExhiibitReport = async function (event, context, callback) {
  debugger;
  globalEvent = event;
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();

  const requestParams = (event.queryStringParameters);
  if (!requestParams.FromMonth || !requestParams.ToMonth) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var fromMonth = parseInt(moment(requestParams.FromMonth).format('MM').replace('0', ''));
  var toMonth = parseInt(moment(requestParams.ToMonth).format('MM').replace('0', ''));
  var exhibitResponse = [];
  //var currentMonth = moment(date).format('YYYY-MM')
  var year = moment(requestParams.ToMonth).format('YYYY');




  var exhibitParams = {
    TableName: process.env.EXHIBIT_TABLE
  }


  // Fetching Exhibits
  var data = await documentClient.scan(exhibitParams).promise();

  //console.log("response ", data)
  var exhibits = data.Items;
  var dataMap = await getCount(fromMonth, toMonth, year, event);
  console.log("datamap ::", dataMap)
  for (const exhibit of exhibits) {
    if (!dataMap.has(exhibit.Id)) {

      dataMap.set(exhibit.ExhibitName, 0)
    } else if (dataMap.has(exhibit.Id)) {
      var hitCount = dataMap.get(exhibit.Id);
      dataMap.delete(exhibit.Id)
      dataMap.set(exhibit.ExhibitName, hitCount)
    }

  }

  exhibitResponse = Array.from(dataMap, ([ExhibitName, NoOfTimesPlayed]) => ({ ExhibitName, NoOfTimesPlayed }));



  console.log("exhibitResponse ", exhibitResponse)



  // var exhibitResponse = [{
  //     "ExhibitName": "test1",
  //     "CheckedInParticipants": 20,
  //     "NoOfTimesPlayed": 10
  // }, {
  //     "ExhibitName": "test2",
  //     "CheckedInParticipants": 20,
  //     "NoOfTimesPlayed": 10
  // },
  // {
  //     "ExhibitName": "test3",
  //     "CheckedInParticipants": 20,
  //     "NoOfTimesPlayed": 10
  // },
  // {
  //     "ExhibitName": "test4",
  //     "CheckedInParticipants": 20,
  //     "NoOfTimesPlayed": 10
  // },
  // {
  //     "ExhibitName": "ABC5",
  //     "CheckedInParticipants": 20,
  //     "NoOfTimesPlayed": 10
  // }
  // ]

  var respObj = { "messgae": "Exhibit data fetched successfully.", "responseObj": exhibitResponse };
  return callback(null, response(200, JSON.stringify(respObj)));

}

async function getCount(fromMonth, toMonth, year, event) {
  globalEvent = event;
  console.log("event headers ::", event)
  var exhibitCountMap = new Map();
  var hitCount = 0;
  for (var month = fromMonth; month <= toMonth; month++) {
    if (month < 10) {
      var currentMonth = year + "-0" + month;
    } else {
      var currentMonth = year + "-" + month;
    }


    // Get data for particular month
    var params = {
      TableName: process.env.EXHIBITHITCOUNT_TABLE,
      KeyConditionExpression: "#date = :date",
      ExpressionAttributeNames: {
        "#date": "recordDate"
      },
      ExpressionAttributeValues: {
        ":date": currentMonth
      }
    };



    // Check if record present
    var data = await documentClient.query(params).promise();

    var elements = data.Items;
    for (const element of elements) {

      if (exhibitCountMap.has(element.ExhibitId)) {
        //update count if the exhibit is same
        hitCount = exhibitCountMap.get(element.ExhibitId) + element.hitCount;
      } else {
        hitCount = + element.hitCount;
      }
      exhibitCountMap.set(element.ExhibitId, hitCount)
    }

  }

  return exhibitCountMap;
}
