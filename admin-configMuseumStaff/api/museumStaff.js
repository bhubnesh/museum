'use strict';

var AWS = require('aws-sdk'),

  uuid = require('uuid'),
  documentClient = new AWS.DynamoDB.DocumentClient();
const auth = require('../auth/token');
let paths = require('../config/config');
const axios = require('axios');
const notification = require('../notifications/sendNotification');
var CryptoJS = require("crypto-js");

var globalEvent = "";

function response(statusCode, message) {

  var originsAllowed = "";
  var origin = globalEvent.headers.origin;
  console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
  console.log("Origin From Event ... " + origin);
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Origin;
  }
  if (origin == "" || origin == undefined || !origin) {
    origin = globalEvent.headers.Host;
  }
  console.log("Origin .... " + origin);
  const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
  if (allowedOrigins.includes(origin)) {
    originsAllowed = origin;
  }

  return {
    statusCode: statusCode,
    'headers': {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer authToke',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Origin': originsAllowed,
      'X-XSS-Protection': '1; mode=block',
      'X-Frame-Options': 'SAMEORIGIN',
      'X-Content-Type-Options': 'nosniff',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

    },
    body: message
  };
}



//-- Add Staff
module.exports.addStaff = function (event, context, callback) {
  globalEvent = event;
  const requestBody = JSON.parse(event.body);

  //-- Validate JWT Token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      addStaffEmp(event, context, validateTokenResp, callback);
    }
  }

  validateMultipleLogin();
}

function addStaffEmp(event, context, validateTokenResp, callback) {
  const requestBody = JSON.parse(event.body);
  const loggedInUser =event.loggedInUser;

  //-- Decrypt Email Id
  var decryptedEmail = CryptoJS.AES.decrypt(requestBody.EmailId, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.EmailId = decryptedEmail.toString(CryptoJS.enc.Utf8);

  //-- Decrypt Contact No
  var decryptedContactNo = CryptoJS.AES.decrypt(requestBody.ContactNo, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.ContactNo = decryptedContactNo.toString(CryptoJS.enc.Utf8);


  if (!requestBody.EmpId || !requestBody.EmailId || !requestBody.ContactNo || !requestBody.Role) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  //-- Get user details from email Id 
  var searchWithEmail = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": loggedInUser
    },
    KeyConditionExpression: "Email = :email"
  }

  console.log("Search Params ..." + JSON.stringify(searchWithEmail));


  documentClient.query(searchWithEmail, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return ({ "statusCode": 400, "message": JSON.stringify(err) });
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      console.log("count of records fro email ... " + data.Count)
      if (data.Count > 0) {
        console.log("Full User Object ... " + JSON.stringify(data.Items[0]));

        var userDetails = data.Items[0];

        //-- Check whether given staff member is already there in user table or not
        var searchUser = {
          TableName: process.env.VISITOR_TABLE,
          ExpressionAttributeValues: {
            ":email": requestBody.EmailId.toLowerCase()
          },
          KeyConditionExpression: "Email = :email"
        }

        documentClient.query(searchUser, function (err, data) {
          if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
            return response(400, JSON.stringify(err));
          } else {
            console.log("count of records fro email ... " + data.Count)
            if (data.Count > 0) {

              console.log("User with emailId already exists.");
              var respObj = { "message": "User with emailId already exists." }
              return callback(null, response(400, JSON.stringify(respObj)));
            } else {

              var Id = uuid.v1();
              //-- Add new record into staff table
              var params = {
                Item: {
                  "Id": Id,
                  //empId shouldbe primary key
                  "EmpId": requestBody.EmpId,
                  "Name": requestBody.Name,
                  "EmailId": requestBody.EmailId,
                  "CountryCode": requestBody.CountryCode,
                  "ContactNo": requestBody.ContactNo,
                  "Role": requestBody.Role,
                  "DeviceId": requestBody.DeviceId,
                  "AllottedDeviceId": requestBody.AllottedDeviceId,
                  "CreatedBy": userDetails.Name,
                  "UpdatedBy": userDetails.Name,
                  "CreatedAt": new Date().toISOString(),
                  "UpdatedAt": new Date().toISOString()
                },
                TableName: process.env.ADMINCONFIGSTAFF_TABLE
              };

              documentClient.put(params, function (err, data) {
                if (err) {
                  return response(400, JSON.stringify(err));
                }
                //-- Create random password for added staff member
                var password = uuid.v1().substr(0, 7);
                console.log("Password ... " + password);
                //-- Add record into user table also
                var userParams = {
                  Item: {
                    "Id": Id,
                    "Name": requestBody.Name,
                    "EmpId": requestBody.EmpId,
                    "Email": requestBody.EmailId.toLowerCase(),
                    "CountryCode": requestBody.CountryCode,
                    "Phone": requestBody.ContactNo,
                    "Password": password,
                    "Gender": requestBody.Gender,
                    "Age": requestBody.Age,
                    "Status": "Active",
                    "Role": requestBody.Role,
                    "Source": requestBody.Source ? requestBody.Source : "",
                    "CreatedAt": new Date().toISOString(),
                    "UpdatedAt": new Date().toISOString(),
                  },
                  TableName: process.env.VISITOR_TABLE
                };

                documentClient.put(userParams, function (err, data) {
                  //--- Send password via Email to added staff member's email Id
                  var subject = "Mahindra Museum Login Credentials";
                  var emailText = "Welcome to Mahindra Museum !  Password  = " + password;
                  var emailNotification = notification.sendEmailNotification(requestBody.EmailId, subject, emailText)

                  if (requestBody.DeviceId) {
                    updateDevice(event, callback);
                  } else {
                    var respObj = { "message": "Staff member added successfully..!" }
                    return callback(null, response(200, JSON.stringify(respObj)));
                  }
                })
              });

            }
          }

        });


      } else {
        var respObj = { "message": "USer details not found. " };
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}

function updateDevice(event, callback) {
  globalEvent = event;
  var requestBody = JSON.parse(event.body);
  ////// ========= update device status functionality if any device assigned to staff =======
  if (requestBody.DeviceId) {
    //console.log("================== update functionality path ================")
    const getPath = paths.constants.getDevicePath + requestBody.DeviceId;
    // console.log(getPath);
    const putPath = paths.constants.putDevicePath + requestBody.DeviceId;
    // console.log(putPath);
    axios.get(getPath, {
      headers: {
        'Authorization': event.headers.Authorization
      }
    })
      .then((res) => {
        if (res.status != 200) {
          return callback(null, response(res.status, res.message));
        } else {
          //console.log("=========Device data from get========")
         console.log("hello ******************************",res.data.responseObj.Id)
          if (res.data.responseObj.Id) {
            console.log("Inside if device id not is null to call put =======> ")
            res.data.responseObj.Status = "Assigned"
            var jsonDeviceData = JSON.stringify(res.data.responseObj);
            //console.log("====Device Data=====")
            //console.log(jsonDeviceData)
            axios.put(putPath, (jsonDeviceData), {
              headers: {
                'Authorization': event.headers.Authorization
              }
            }).then(resp => {
              if (resp.status != 200) {
                return callback(null, response(resp.status, resp.message));
              } else {
                //console.log("=============in put data=============")
                //      console.log(response);
                var respObj = { "message": "Staff member added successfully..!" }
                return callback(null, response(200, JSON.stringify(respObj)));
              }
            });
          }
        }
      })
      .catch((error) => {
        console.error(error)
      })
  }

}
//-- get  the AllEmployee List
module.exports.getAllEmployeeList = function (event, context, callback) {
  globalEvent = event;
  // //-- Validate JWT Token
  // var validateToken = auth.validateToken(event, context, callback);
  // console.log("token inside ... " + validateToken);
  // if (validateToken.statusCode != 200) {
  //   return callback(null, response(validateToken.statusCode, JSON.stringify(validateToken)));
  // }

  var params = {
    TableName: process.env.ADMINCONFIGSTAFF_TABLE,
  }
  documentClient.scan(params, function (err, data) {
    if (err) {
      return callback(err, response(400, JSON.stringify(err)));
    }

    var museumData = data.Items;
    //-- Sort data according to created date time
    const sortedtData = museumData.sort((a, b) => new Date(a.CreatedAt) - new Date(b.CreatedAt));

    var finalResponse = {}
    finalResponse.Items = sortedtData;

    var respObj = { "responseObj": finalResponse, "message": "Employee list fetched successfully..!" }
    return callback(err, response(200, JSON.stringify(respObj)));
  })
}

// get particular employee details By Id
module.exports.getEmployeeDetailsById = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT Token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();

  const id = event.pathParameters.Id;
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  var params = {
    TableName: process.env.ADMINCONFIGSTAFF_TABLE,
    ExpressionAttributeValues: {
      ":emp": id
    },
    KeyConditionExpression: 'Id = :emp',

  };
  console.log("params.. " + JSON.stringify(params));
  documentClient.query(params, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return callback(err, response(400, JSON.stringify(err)));
    } else {
      // console.log("GetItem succeeded:", JSON.stringify(data));
      console.log("count of records ... " + data.Count)
      if (data.Count > 0) {
        var respObj = { "responseObj": data.Items[0], "message": "Employee details fetched successfully." }
        return callback(null, response(200, JSON.stringify(respObj)));
      }
    }
  })
}

//--Delete Employee Details - Hard delete
module.exports.deleteEmployeeDetails = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT Token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    }
  }
  validateMultipleLogin();
  const id = event.pathParameters.Id;
  if (!id) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var deleteParams = {
    TableName: process.env.ADMINCONFIGSTAFF_TABLE,
    Key: {
      "Id": id
    }
  };

  var searchWithId = {
    TableName: process.env.ADMINCONFIGSTAFF_TABLE,
    ExpressionAttributeValues: {
      ":Id": id
    },
    KeyConditionExpression: "Id = :Id"
  }

  console.log("Search Params ..." + JSON.stringify(searchWithId));


  documentClient.query(searchWithId, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return ({ "statusCode": 400, "message": JSON.stringify(err) });
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      console.log("count of records fro email ... " + data.Count)
      if (data.Count > 0) {

        var employeeData = data.Items[0];
        if (employeeData.DeviceId || employeeData.DeviceId != null) {
          //-- call update device api to change status assinged to available
          const getPathNew = paths.constants.getDevicePath + employeeData.DeviceId;
          // console.log(getPath);
          const putPathNew = paths.constants.putDevicePath + employeeData.DeviceId;
          // console.log(putPath);
          axios.get(getPathNew, {
            headers: {
              'Authorization': event.headers.Authorization
            }
          })
            .then((resn) => {
              if (resn.status != 200) {
                return callback(null, response(res.status, res.message));
              }

              resn.data.responseObj.Status = "Available"
              var jsonDeviceData = JSON.stringify(resn.data.responseObj);
              axios.put(putPathNew, (jsonDeviceData), {
                headers: {
                  'Authorization': event.headers.Authorization
                }
              }).then(respn => {
                if (respn.status != 200) {
                  return callback(null, response(res.status, res.message));
                } else {
                  //-- Delete employee
                  documentClient.delete(deleteParams, function (err, data) {
                    if (err) {
                      return callback(err, response(400, err));
                    }
                    //-- Delete user from user table
                    var deleteUserParams = {
                      TableName: process.env.VISITOR_TABLE,
                      Key: {
                        "Email": employeeData.EmailId,
                        "Phone": employeeData.ContactNo
                      },
                      ConditionExpression: "Email = :email",
                      ExpressionAttributeValues: {
                        ":email": employeeData.EmailId
                      }
                    };
                    documentClient.delete(deleteUserParams, function (err, data) {
                      if (err) {
                        return callback(err, response(400, JSON.stringify(err)));
                      }

                      console.log("Visitor deleted successfully..!");
                      var respObj = { "message": "Employee Details Deleted successfully..!" }
                      return callback(err, response(200, JSON.stringify(respObj)));
                    })

                  });
                }
              })
                .catch((error) => {
                  console.error(error)
                })
            }).catch((error) => {
              console.error(error)
            })
        } else {
          documentClient.delete(deleteParams, function (err, data) {
            if (err) {
              return callback(err, response(400, err));
            }

            //-- Delete user from user table
            var deleteUserParams = {
              TableName: process.env.VISITOR_TABLE,
              Key: {
                "Email": employeeData.Email,
                "Phone": employeeData.ContactNo
              },
              ConditionExpression: "Email = :email",
              ExpressionAttributeValues: {
                ":email": employeeData.Email
              }
            };
            documentClient.delete(deleteUserParams, function (err, data) {
              if (err) {
                return callback(err, response(400, JSON.stringify(err)));
              }
              console.log("Visitor deleted successfully..!");
              var respObj = { "message": "Employee Details Deleted successfully..!" }
              return callback(err, response(200, JSON.stringify(respObj)));
            })
            var respObj = { "message": "Employee Details Deleted successfully..!" }
            return callback(err, response(200, JSON.stringify(respObj)));
          });
        }
      } else {
        var respObj = { "message": "Employee Details not found..!" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })



}

//-- update the Employee Details

module.exports.updateEmployeeDetails = function (event, context, callback) {
  globalEvent = event;
  //-- Validate JWT Token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      updateEmployee(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function updateEmployee(event, context, validateTokenResp, callback) {
  const id = event.pathParameters.Id;
  const requestBody = JSON.parse(event.body);

  //-- Decrypt Email Id
  var decryptedEmail = CryptoJS.AES.decrypt(requestBody.EmailId, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.EmailId = decryptedEmail.toString(CryptoJS.enc.Utf8);
  
  //-- Decrypt Email Id
  var decryptedContactNo = CryptoJS.AES.decrypt(requestBody.ContactNo, 'OurBusinessIsOurBusinessNoneOfYourBusiness');
  requestBody.ContactNo = decryptedContactNo.toString(CryptoJS.enc.Utf8);
  //-- Get currently logged in user info
  var loggedInUser = validateTokenResp.event.loggedInUser;

  const newDeviceId = requestBody.DeviceId;
  //console.log("=====new device id ======= "+newDeviceId)
  if (!id) {
    var respObj = { "message": "Please provide EmpId" }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }

  //-- Get user details from email Id 

  var searchWithEmail = {
    TableName: process.env.VISITOR_TABLE,
    ExpressionAttributeValues: {
      ":email": loggedInUser
    },
    KeyConditionExpression: "Email = :email"
  }
  console.log("Search Params ..." + JSON.stringify(searchWithEmail));

  documentClient.query(searchWithEmail, function (err, data) {
    if (err) {
      console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
      return ({ "statusCode": 400, "message": JSON.stringify(err) });
    } else {
      console.log("GetItem succeeded:", JSON.stringify(data));
      console.log("count of records fro email ... " + data.Count)
      if (data.Count > 0) {
        console.log("Full User Object ... " + JSON.stringify(data.Items[0]));
        const userDetails = data.Items[0];


        const getStaff = paths.constants.getStaffPath + id;
        //To check device assigned is same or changed
        axios.get(getStaff, {
          headers: {
            'Authorization': event.headers.Authorization
          }
        })
          .then((res) => {
            if (res.status != 200) {
              return callback(null, response(res.status, res.message));
            }


            console.log(res.data.responseObj)

            const oldDeviceId = res.data.responseObj.DeviceId
            //-- If no device was assignd previously
            if (!oldDeviceId) {
              console.log("If no device was assigned previously...")

              if (!newDeviceId) {
                var params = {
                  TableName: process.env.ADMINCONFIGSTAFF_TABLE,
                  Key: {
                    "Id": id
                  },
                  UpdateExpression: 'set #EmpId = :I,#Name = :N,#EmailId = :E,#ContactNo = :C,#Role = :R,#DeviceId = :D,#AllottedDeviceId = :AD,#UpdatedBy = :U,#UpdatedAt = :UA',
                  ExpressionAttributeValues: {
                    ':I': requestBody.EmpId,
                    ':N': requestBody.Name,
                    ':E': requestBody.EmailId,
                    ':C': requestBody.ContactNo,
                    ':R': requestBody.Role,
                    ':D': requestBody.DeviceId,
                    ':AD': requestBody.AllottedDeviceId,
                    ':U': userDetails.Name,
                    ':UA': new Date().toISOString()
                  },
                  ExpressionAttributeNames: {
                    '#EmpId': 'EmpId',
                    '#Name': 'Name',
                    '#EmailId': 'EmailId',
                    '#ContactNo': 'ContactNo',
                    '#Role': 'Role',
                    '#DeviceId': 'DeviceId',
                    '#AllottedDeviceId': 'AllottedDeviceId',
                    '#UpdatedBy': 'UpdatedBy',
                    "#UpdatedAt": "UpdatedAt"

                  },
                  ReturnValues: "UPDATED_NEW"
                };
                console.log("params.. " + JSON.stringify(params));
                documentClient.update(params, function (err, data) {
                  if (err) {
                    console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                    return callback(err, response(400, err));
                  }
                  console.log(data);
                  var respObj = { "responseObj": data.Attributes, "message": "Employee Details updated successfully..!" }
                  return callback(err, response(200, JSON.stringify(respObj)));
                })

              } else {
                const getPathNew = paths.constants.getDevicePath + newDeviceId;
                const putPathNew = paths.constants.putDevicePath + newDeviceId;
                //-- Get device details of selected device
                axios.get(getPathNew, {
                  headers: {
                    'Authorization': event.headers.Authorization
                  }
                })
                  .then((resn) => {
                    if (resn.status != 200) {
                      return callback(null, response(res.status, res.message));
                    }
                    //-- Change status to "Assined" and update device details

                    resn.data.responseObj.Status = "Assigned";
                    var jsonDeviceData = JSON.stringify(resn.data.responseObj);

                    axios.put(putPathNew, (jsonDeviceData), {
                      headers: {
                        'Authorization': event.headers.Authorization
                      }
                    }).then(respn => {
                      //-- If successfully updated, update device Id in Staff table
                      var params = {
                        TableName: process.env.ADMINCONFIGSTAFF_TABLE,
                        Key: {
                          "Id": id
                        },
                        UpdateExpression: 'set #EmpId = :I,#Name = :N,#EmailId = :E,#ContactNo = :C,#Role = :R,#DeviceId = :D,#AllottedDeviceId = :AD,#UpdatedBy = :U,#UpdatedAt = :UA',
                        ExpressionAttributeValues: {
                          ':I': requestBody.EmpId,
                          ':N': requestBody.Name,
                          ':E': requestBody.EmailId,
                          ':C': requestBody.ContactNo,
                          ':R': requestBody.Role,
                          ':D': requestBody.DeviceId,
                          ':AD': requestBody.AllottedDeviceId,
                          ':U': userDetails.Name,
                          ':UA': new Date().toISOString()
                        },
                        ExpressionAttributeNames: {
                          '#EmpId': 'EmpId',
                          '#Name': 'Name',
                          '#EmailId': 'EmailId',
                          '#ContactNo': 'ContactNo',
                          '#Role': 'Role',
                          '#DeviceId': 'DeviceId',
                          '#AllottedDeviceId': 'AllottedDeviceId',
                          '#UpdatedBy': 'UpdatedBy',
                          "#UpdatedAt": "UpdatedAt"

                        },
                        ReturnValues: "UPDATED_NEW"
                      };
                      console.log("params.. " + JSON.stringify(params));
                      documentClient.update(params, function (err, data) {
                        if (err) {
                          console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                          return callback(err, response(400, err));
                        }
                        console.log(data);
                        var respObj = { "responseObj": data.Attributes, "message": "Employee Details updated successfully..!" }
                        return callback(err, response(200, JSON.stringify(respObj)));
                      })
                    })
                      .catch((error) => {
                        console.error(error)
                      })
                  }).catch((error) => {
                    console.error(error)
                  })

              }
            }
            ////console.log("===oldDeviceId======"+oldDeviceId)

            else if (newDeviceId == oldDeviceId) {
              //-- Update other emp details 
              console.log("=== If device id has not updated ===")
              var params = {
                TableName: process.env.ADMINCONFIGSTAFF_TABLE,
                Key: {
                  "Id": id
                },
                UpdateExpression: 'set #EmpId = :I,#Name = :N,#EmailId = :E,#ContactNo = :C,#Role = :R,#DeviceId = :D, #AllottedDeviceId = :AD,#UpdatedBy = :U,#UpdatedAt = :UA',
                ExpressionAttributeValues: {
                  ':I': requestBody.EmpId,
                  ':N': requestBody.Name,
                  ':E': requestBody.EmailId,
                  ':C': requestBody.ContactNo,
                  ':R': requestBody.Role,
                  ':D': requestBody.DeviceId,
                  ':AD': requestBody.AllottedDeviceId,
                  ':U': userDetails.Name,
                  ':UA': new Date().toISOString()
                },
                ExpressionAttributeNames: {
                  '#EmpId': 'EmpId',
                  '#Name': 'Name',
                  '#EmailId': 'EmailId',
                  '#ContactNo': 'ContactNo',
                  '#Role': 'Role',
                  '#DeviceId': 'DeviceId',
                  '#AllottedDeviceId': 'AllottedDeviceId',
                  '#UpdatedBy': 'UpdatedBy',
                  "#UpdatedAt": "UpdatedAt"

                },
                ReturnValues: "UPDATED_NEW"
              };
              console.log("params.. " + JSON.stringify(params));
              documentClient.update(params, function (err, data) {
                if (err) {
                  console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                  return callback(err, response(400, err));
                }
                console.log(data);
                var respObj = { "responseObj": data.Attributes, "message": "Employee Details updated successfully..!" }
                return callback(err, response(200, JSON.stringify(respObj)));
              })

            }
            else if (newDeviceId != oldDeviceId) {

              if (!newDeviceId) {
                var params = {
                  TableName: process.env.ADMINCONFIGSTAFF_TABLE,
                  Key: {
                    "Id": id
                  },
                  UpdateExpression: 'set #EmpId = :I,#Name = :N,#EmailId = :E,#ContactNo = :C,#Role = :R,#DeviceId = :D, #AllottedDeviceId = :AD,#UpdatedBy = :U,#UpdatedAt = :UA',
                  ExpressionAttributeValues: {
                    ':I': requestBody.EmpId,
                    ':N': requestBody.Name,
                    ':E': requestBody.EmailId,
                    ':C': requestBody.ContactNo,
                    ':R': requestBody.Role,
                    ':D': requestBody.DeviceId,
                    ':AD': requestBody.AllottedDeviceId,
                    ':U': userDetails.Name,
                    ':UA': new Date().toISOString()
                  },
                  ExpressionAttributeNames: {
                    '#EmpId': 'EmpId',
                    '#Name': 'Name',
                    '#EmailId': 'EmailId',
                    '#ContactNo': 'ContactNo',
                    '#Role': 'Role',
                    '#DeviceId': 'DeviceId',
                    '#AllottedDeviceId': 'AllottedDeviceId',
                    '#UpdatedBy': 'UpdatedBy',
                    "#UpdatedAt": "UpdatedAt"

                  },
                  ReturnValues: "UPDATED_NEW"
                };
                console.log("params.. " + JSON.stringify(params));
                documentClient.update(params, function (err, data) {
                  if (err) {
                    console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                    return callback(err, response(400, err));
                  }
                  console.log(data);
                  var respObj = { "responseObj": data.Attributes, "message": "Employee Details updated successfully..!" }
                  return callback(err, response(200, JSON.stringify(respObj)));
                })
              } else {
                console.log("== If device was assigned and now re-assigning different device Id")
                const getPathNew = paths.constants.getDevicePath + newDeviceId;
                const putPathNew = paths.constants.putDevicePath + newDeviceId;
                console.log("getPathNew",getPathNew)
                //-- Get the details of new device Id
                axios.get(getPathNew, {
                  headers: {
                    'Authorization': event.headers.Authorization
                  }
                })
                  .then((resn) => {
                    if (resn.status != 200) {
                      console.log("data")
                      return callback(null, response(res.status, res.message));
                    }
                    //-- Change the status of new device to "Assigned"
                    console.log("data in")
                    resn.data.responseObj.Status = "Assigned"
                    var jsonDeviceData = JSON.stringify(resn.data.responseObj);
                    axios.put(putPathNew, (jsonDeviceData), {
                      headers: {
                        'Authorization': event.headers.Authorization
                      }
                    }).then(respn => {


                    })
                      .catch((error) => {
                        console.error(error)
                      })
                  }).catch((error) => {
                    console.error(error)
                  })
                const getPath = paths.constants.getDevicePath + oldDeviceId;
                const putPath = paths.constants.putDevicePath + oldDeviceId;
                //-- Get the details of old device Id

                axios.get(getPath, {
                  headers: {
                    'Authorization': event.headers.Authorization
                  }
                })
                  .then((res) => {
                    //-- change the status of old device to "Available" and update
                    res.data.responseObj.Status = "Available"
                    var jsonDeviceData = JSON.stringify(res.data.responseObj);

                    axios.put(putPath, (jsonDeviceData), {
                      headers: {
                        'Authorization': event.headers.Authorization
                      }
                    }).then(resp => {
                      //-- Update new device Id in staff table

                      var params = {
                        TableName: process.env.ADMINCONFIGSTAFF_TABLE,
                        Key: {
                          "Id": id
                        },
                        UpdateExpression: 'set #EmpId = :I,#Name = :N,#EmailId = :E,#ContactNo = :C,#Role = :R,#DeviceId = :D, #AllottedDeviceId = :AD,#UpdatedBy = :U,#UpdatedAt = :UA',
                        ExpressionAttributeValues: {
                          ':I': requestBody.EmpId,
                          ':N': requestBody.Name,
                          ':E': requestBody.EmailId,
                          ':C': requestBody.ContactNo,
                          ':R': requestBody.Role,
                          ':D': requestBody.DeviceId,
                          ':AD': requestBody.AllottedDeviceId,
                          ':U': userDetails.Name,
                          ':UA': new Date().toISOString()
                        },
                        ExpressionAttributeNames: {
                          '#EmpId': 'EmpId',
                          '#Name': 'Name',
                          '#EmailId': 'EmailId',
                          '#ContactNo': 'ContactNo',
                          '#Role': 'Role',
                          '#DeviceId': 'DeviceId',
                          '#AllottedDeviceId': 'AllottedDeviceId',
                          '#UpdatedBy': 'UpdatedBy',
                          "#UpdatedAt": "UpdatedAt"

                        },
                        ReturnValues: "UPDATED_NEW"
                      };
                      console.log("params.. " + JSON.stringify(params));
                      documentClient.update(params, function (err, data) {
                        if (err) {
                          console.error("Unable to read item. Error JSON:", JSON.stringify(err, null));
                          return callback(err, response(400, err));
                        }
                        console.log(data);
                        var respObj = { "responseObj": data.Attributes, "message": "Employee Details updated successfully..!" }
                        return callback(err, response(200, JSON.stringify(respObj)));
                      })
                    });
                  })
                  .catch((error) => {
                    console.error(error)
                  })
              }
            }
          }).catch((error) => {
            console.error(error)
          })
      } else {
        console.log("Logged in user details not found");
        var respObj = { "message": "Logged in user details not found" }
        return callback(null, response(404, JSON.stringify(respObj)));
      }
    }
  })
}


//-- Delete multiple Employees code 

module.exports.deleteMultipleEmployees = function (event, context, callback) {
  globalEvent = event;

  //-- Validate JWT token
  const validateMultipleLogin = async () => {
    const validateTokenResp = await auth.validateToken(event, context, callback);
    //console.log("Response.... "+ JSON.stringify(validateTokenResp));

    if (validateTokenResp.statusCode != 200) {
      console.log("Response .............. " + JSON.stringify(validateTokenResp));
      return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
    } else {
      deleteMultiEmp(event, context, validateTokenResp, callback);
    }
  }
  validateMultipleLogin();
}
function deleteMultiEmp(event, context, validateTokenResp, callback) {
  const requestBody = JSON.parse(event.body);
  if (!requestBody.listOfEmployees) {
    var respObj = { "message": "Manadatory input parameters are missing." }
    return callback(
      null,
      response(400, JSON.stringify(respObj))
    );
  }
  var listOfEmployees = requestBody.listOfEmployees;
  console.log("size of List of Employees .. " + listOfEmployees.length);
  var count = listOfEmployees.length;
  if (listOfEmployees.length > 0) {

    listOfEmployees.forEach(employees => {
      var id = employees.id;
      var params = {
        TableName: process.env.ADMINCONFIGSTAFF_TABLE,
        Key: {
          "Id": id
        }

      };
      console.log("params.. " + JSON.stringify(params));
      documentClient.delete(params, function (err, data) {
        if (err) {
          return callback(err, response(400, err));
        }
        console.log(data);
        var message = "Employee of Id - " + id + " deleted successfully..!"
        console.log(message + count);
        count--;
        if (count == 0) {
          var respObj = { "message": "Employee with given ids deleted successfully.. !" }
          return callback(null, response(200, JSON.stringify(respObj)));
        }
      });
    });


  } else {
    var respObj = { "message": "list of employee ids to be deleted not found." }
    return callback(null, response(404, JSON.stringify(respObj)));
  }
}
