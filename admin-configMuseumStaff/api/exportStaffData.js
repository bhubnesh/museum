'use strict';

var AWS = require('aws-sdk');
const Stream = require('stream')

const auth = require('../auth/token'),
    paths = require('../config/config'),
    axios = require('axios'),
    Excel = require('exceljs')

var globalEvent = "";

function response(statusCode, message) {
    
    var originsAllowed = "";
    var origin = globalEvent.headers.origin;
    console.log("Headers from Event ... " + JSON.stringify(globalEvent.headers));
    console.log("Origin From Event ... " + origin);
    if(origin == ""|| origin == undefined || !origin){
       origin = globalEvent.headers.Origin;
    }
    if(origin == ""|| origin == undefined || !origin){
      origin = globalEvent.headers.Host;
   }
    console.log("Origin .... " + origin);
    const allowedOrigins = ['http://museum.mahindra.com','http://museum.mahindra.com.s3-website.ap-south-1.amazonaws.com', 'http://museumstaff.mahindra.com.s3-website.ap-south-1.amazonaws.com'];
    if (allowedOrigins.includes(origin)) {
        originsAllowed = origin;
   }

    return {
        statusCode: statusCode,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer authToke',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin' : originsAllowed,
            'X-XSS-Protection': '1; mode=block',
            'X-Frame-Options': 'SAMEORIGIN',
            'X-Content-Type-Options': 'nosniff',
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE'

        },
        body: message
    };
}



const exportStaffData = (event, context, callback) => {
    globalEvent = event;
    //-- Validate JWT Token
    const validateMultipleLogin = async() =>{
        const validateTokenResp =  await auth.validateToken(event, context, callback);
        //console.log("Response.... "+ JSON.stringify(validateTokenResp));
    
            if  (validateTokenResp.statusCode != 200) {
                console.log("Response .............. "+ JSON.stringify(validateTokenResp));
                return callback(null, response(validateTokenResp.statusCode, JSON.stringify(validateTokenResp)));
            }
    }
    validateMultipleLogin();

    const loggedInUser = validateTokenResp.event.loggedInUser;

    const getStaff = paths.constants.getMuseumStaffPath;
    var employees = [];
    axios.get(getStaff, {
        headers: {
            'Authorization': event.headers.Authorization
        }
    })
        .then((res) => {
            console.log("================= Staff data ==================")
            console.log(res.data.responseObj);
            const empCount = res.data.responseObj.Count;
            employees = res.data.responseObj.Items;
            console.log("employees.... " + employees);

            if (employees.length > 0) {
                try {
                    var workbook = new Excel.Workbook();
                    var worksheet = workbook.addWorksheet('Museum Staff');
                    worksheet.columns = [
                        { header: 'sNo', key: 'sNo', width: 10 },
                        { header: 'EmpId', key: 'EmpId', width: 10 },
                        { header: 'EmailId', key: 'EmailId', width: 32 },
                        { header: 'Name', key: 'Name', width: 20 },
                        { header: 'Role', key: 'Role', width: 20 },
                        { header: 'DeviceId', key: 'DeviceId', width: 20 }
                    ];
                    var count = 1;
                    employees.forEach(emp => {
                        emp.sNo = count;
                        worksheet.addRow(emp);
                        count += 1;
                    });

                    worksheet.getRow(1).eachCell((cell) => {
                        cell.font = { bold: true }
                    })

                    var fileName = "museum.xlsx";
                    workbook.xlsx.writeFile(fileName).then(async function () {
                        var resObj = { "message": "Success" }
                        return callback(null, response(200, JSON.stringify(resObj)));
                    });
                } catch (err) {
                    console.log("Some error occurred ... " + err)
                    return response(400, JSON.stringify(err));
                }

            }

        })
}
module.exports = {
    exportStaffData,
};

